import React, {Component} from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import ProSelect from "../../components/ProSelect";
import registerStaffFingerStyle from "../../assets/jss/registerStaffFingerStyle";
import {fetchStaffs} from "../../actions/staff/staff.action";
import {connect} from "react-redux";
import { InlineDatePicker, MuiPickersUtilsProvider} from "material-ui-pickers";
import DateFnsUtils from '@date-io/date-fns';
import withStyles from "@material-ui/core/styles/withStyles";


const style = theme => ({
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '50%',
        marginTop: '25%',
    },
    button: {
        margin: theme.spacing.unit,
    },
    sidebarDateButtons: {
        width: '100%',
        height: '33px',
        backgroundColor: '#BBBBBB',
        marginLeft: 0,
        borderRadius: '4px',
        top: '-10px',
        color: 'white',
        '&:hover': {
            backgroundColor: '#959191'
        },
        fontFamily: 'Fira Sans',
        lineHeight: '12px',
        fontSize: '11px',
    },
    datepickerInputs: {
        backgroundColor: 'white !important',
        padding: '4px 0px 4px 0px !important'
    },
    datepickerInputsDiv: {
        paddingRight: '12px !important',
    },
});

function makeDataForSelect(array) {
    const newArray = [];
    if (array !== undefined) {
        array.map(arr => newArray.push({value: arr.userId, label: arr.userName}))
    }
    return newArray
}

class FpProcesses extends Component {
    state = {
        selected_operation: null,
        selected_staff: null,
        selected_date: new Date()
    };

    handleDateChange= (date,type) => {
        this.setState({[type === 1 ? 'selected_date1' : 'selected_date2'] : date});
    };

    handleOperationSelectChange = (val) => {
        if (val) {
            this.setState({
                selected_operation: val
            })
        } else {
            this.setState({
                selected_operation: null
            })
        }
    };

    handleStaffSelectChange = (val) => {
        if (val) {
            this.setState({
                selected_staff: val
            });
        } else {
            this.setState({
                selected_staff: null
            })
        }
    };

    componentWillMount() {
        this.props.fetchStaffs(1, 16330);
    }


    render() {
        const { classes } = this.props;
        return (
            <Grid container spacing={24}>
                <Grid item md={12}>
                    <h3 style={{
                        marginLeft: 75,
                        marginTop: 50,
                        fontWeight: 'bold'
                    }}>Qurğu əməliyyatları</h3>
                </Grid>
                <Grid item md={12}>
                    <Grid container spacing={24}>
                        <Grid item md={2}>
                            <ProSelect
                                name="operation_name"
                                isLoading={false}
                                options={[
                                    {value: 1, label: 'Işə qəbul'},
                                    {value: 2, label: 'Xitam əmri'},
                                    {value: 3, label: 'Məzuniyyət əmri'}
                                ]}
                                className="basic-multi-select"
                                classNamePrefix="select"
                                value={this.state.selected_operation}
                                onChange={this.handleOperationSelectChange}
                                styles={{
                                    ...registerStaffFingerStyle,
                                    control: (base, state) => ({
                                        ...base,
                                        '&:hover': {borderColor: '#55AB80'}, // border style on hover
                                        border: '1px solid #55AB80', // default border color
                                        boxShadow: 'none', // no box-shadow
                                    }),
                                }}
                            />
                        </Grid>
                        <Grid item md={2}>
                            <ProSelect
                                name="staff_name"
                                isLoading={false}
                                options={makeDataForSelect(this.props.staffs.total_data.data)}
                                className="basic-multi-select"
                                classNamePrefix="select"
                                value={this.state.selected_staff}
                                onChange={this.handleStaffSelectChange}
                                styles={{
                                    ...registerStaffFingerStyle,
                                    control: (base, state) => ({
                                        ...base,
                                        '&:hover': {borderColor: '#55AB80'}, // border style on hover
                                        border: '1px solid #55AB80', // default border color
                                        boxShadow: 'none', // no box-shadow
                                    }),
                                }}
                            />
                        </Grid>
                        <Grid item md={4}>
                            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                                <Grid container spacing={24} style={{
                                    marginTop: '-24px'
                                }}>
                                    <Grid item md={6} className={classes.datepickerInputsDiv}>
                                        <InlineDatePicker
                                            name={"selectedSideBarDate1"}
                                            margin="normal"
                                            value={this.state.selected_date1}
                                            onChange={(e) => this.handleDateChange(e, 1)}
                                            format={"dd-MM-yyyy"}
                                            className={"custom-datepicker-input"}
                                            InputProps={{
                                                disableUnderline: true,
                                            }}

                                        />
                                    </Grid>
                                    <Grid item md={6} className={classes.datepickerInputsDiv} style={{
                                        marginLeft: '-10px'
                                    }}>
                                        <InlineDatePicker
                                            name={"selectedSideBarDate2"}
                                            margin="normal"
                                            value={this.state.selected_date2}
                                            onChange={(e) => this.handleDateChange(e, 2)}
                                            format={"dd-MM-yyyy"}
                                            className={"custom-datepicker-input"}
                                            InputProps={{
                                                disableUnderline: true,
                                            }}
                                        />
                                    </Grid>
                                </Grid>
                            </MuiPickersUtilsProvider>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

FpProcesses.propTypes = {};

const mapStateToProps = ({staffs}) => {
    return {
        staffs,
    }
};

const mapDispatchToProps = {
    fetchStaffs
};
export default withStyles(style)(connect(mapStateToProps, mapDispatchToProps)(FpProcesses));