import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import { Table , TableBody , TableCell , TableHead , TableRow ,Paper , Grid , Button , InputBase ,CircularProgress } from '@material-ui/core';
import { connect } from "react-redux";
import {fetchAllOperations} from "../../actions/fp/fp_operations";
import {fetchDeviceFingerOperations} from '../../actions/msk/device_finger_operation.action';
import {fetchDevices} from '../../actions/device/devices.action';
import { fetchFingerTemplatesWithFingerIndex,fetchRegIdWithUserId,fetchStaffs } from '../../actions/staff/staff.action';
import moment from 'moment';
import {ProPassAlert} from "../../components/ProPassAlert";
import ProSelect from "../../components/ProSelect";
import makeAnimated from "react-select/lib/animated";
import { makeDataForFingerSelectDevice,makeDataForFPOperationSelect,makeDataForUserSelect } from "../../helper/standart";
import ProDatePicker from "../../components/ProDatePicker";
import registerStaffFingerStyle from "../../assets/jss/registerStaffFingerStyle";
import ProStepper from "../../components/ProStepper";
import axios from "axios";
import SearchIcon from '@material-ui/icons/Search';
import devMonitorinqStyles from "../../assets/jss/devMonitorinqStyle";
import classNames from 'classnames';

class DevMonitorinq extends Component {

    state = {
        selected_status: null,
        selected_staff : null,
        selected_device: null,
        selected_operation: null,
        selected_date: null,
        test_fp_modal: false,
        selected_operation_name: null,
        selected_finger_name: null,
        selected_device_name: null,
        fingerFetching: false,
        loadStep: 1,
        errorStep: 0,
        controlled_devices: [],
        device_monitoring_modal: false,
        selected_hand_name : null,
        device_clear_modal : false,
        clear_device : null,
        stepperMessage : null
    };

    componentWillMount() {
        this.props.fetchAllOperations();
        this.props.fetchDeviceFingerOperations();
        this.props.fetchDevices();
        this.props.fetchStaffs(1, 16330);
    }

    handleChangeStatus = (val) => {
        if (val) {
            this.setState({
                selected_status: val
            });
            this.props.fetchAllOperations({
                status: val.value,
                operation: this.state.selected_operation ? this.state.selected_operation.value : null,
                device: this.state.selected_device ? this.state.selected_device.value : null,
                date : this.state.selected_date
            });
        } else {
            this.setState({
                selected_status: null
            });
            this.props.fetchAllOperations({
                operation: this.state.selected_operation ? this.state.selected_operation.value : null,
                device: this.state.selected_device ? this.state.selected_device.value : null,
                date : this.state.selected_date
            });
        }
    };

    handleStaffSelectChange = (val) => {
        if (val) {
            this.setState({
                selected_staff: val
            });
            this.props.fetchRegIdWithUserId(val.value);
        } else {
            this.setState({
                selected_staff: null
            });
            this.props.fetchRegIdWithUserId(0);
        }
    };

    handleChangeDevice = (val) => {
        if (val) {
            this.setState({
                selected_device: val
            });
            this.props.fetchAllOperations({
                device: val.value,
                operation: this.state.selected_operation ? this.state.selected_operation.value : null,
                status: this.state.selected_status ? this.state.selected_status.value : null,
                date : this.state.selected_date
            });
        } else {
            this.setState({
                selected_device: null
            });
            this.props.fetchAllOperations({
                operation: this.state.selected_operation ? this.state.selected_operation.value : null,
                status: this.state.selected_status ? this.state.selected_status.value : null,
                date : this.state.selected_date
            });
        }
    };

    handleChangeOperation = (val) => {
        if (val) {
            this.setState({
                selected_operation: val
            });
            this.props.fetchAllOperations({
                operation: val.value,
                device: this.state.selected_device ? this.state.selected_device.value : null,
                status: this.state.selected_status ? this.state.selected_status.value : null,
                date : this.state.selected_date
            });
        } else {
            this.setState({
                selected_operation: null
            });
            this.props.fetchAllOperations({
                device: this.state.selected_device ? this.state.selected_device.value : null,
                status: this.state.selected_status ? this.state.selected_status.value : null,
                date : this.state.selected_date
            });
        }
    };

    handleOperationNameSelectChange = (val) => {
        if (val) {
            this.setState({
                selected_operation_name: val
            });
        } else {
            this.setState({
                selected_operation_name: null
            });
        }
    };

    handleChangeDeviceModal = (val) => {
        if (val) {
            this.setState({
                selected_device_name: val
            });
        } else {
            this.setState({
                selected_device_name: null
            });
        }
    };

    handleHandNameSelectChange = (val) => {
        if (val) {
            this.setState({
                selected_hand_name: val
            });
        } else {
            this.setState({
                selected_hand_name: null
            });
        }
    };

    handleChangeDate = (e) => {
        this.setState({
            selected_date : e
        });
        this.props.fetchAllOperations({
            status: this.state.selected_status ? this.state.selected_status.value : null,
            operation: this.state.selected_operation ? this.state.selected_operation.value : null,
            device: this.state.selected_device ? this.state.selected_device.value : null,
            date : e ? moment(e).format('YYYY-MM-DD') : null
        });
    };

    handleClearDate = (e) => {
        e.preventDefault();
        this.setState({
            selected_date : null
        });
        this.props.fetchAllOperations({
            status: this.state.selected_status ? this.state.selected_status.value : null,
            operation: this.state.selected_operation ? this.state.selected_operation.value : null,
            device: this.state.selected_device ? this.state.selected_device.value : null,
            date :null
        });
    };

    handleChangeDeviceForClearDeviceModal = (val) => {
        this.setState({
            clear_device : val
        })
    };

    handleDeviceMonitoringModalClick = () => {

        if (this.state.device_monitoring_modal) {
            this.setState({
                controlled_devices: []
            });
        } else {
            this.props.devices.total_data.data.map(device => (
                axios.get('http://172.16.0.132:8081/iclock/pull_sdk?prospectsbs:connect:' + device.ip_address + ':' + device.id + '')
                    .then(result => {
                        if (result.data.status === "success") {
                            this.setState({
                                controlled_devices: [...this.state.controlled_devices, {
                                    id: device.id,
                                    ip_address: device.ip_address,
                                    status: 1
                                }]
                            })
                        } else {
                            this.setState({
                                controlled_devices: [...this.state.controlled_devices, {
                                    id: device.id,
                                    ip_address: device.ip_address,
                                    status: 2
                                }]
                            })
                        }
                    }).catch(error => {
                    this.setState({
                        controlled_devices: [...this.state.controlled_devices, {
                            id: device.id,
                            ip_address: device.ip_address,
                            status: 3
                        }]
                    })
                })
            ))
        }

        this.setState({
            device_monitoring_modal: !this.state.device_monitoring_modal,
        });

    };

    handleFingerNameSelectChange = (val) => {
        if (val) {
            this.setState({
                selected_finger_name: val
            });
        } else {
            this.setState({
                selected_finger_name: null
            });
        }
    };

    handleFpModalOpenClick = () => {
        this.setState({
            test_fp_modal : !this.state.test_fp_modal
        })
    };

    handleFPModalClick = () => {
        if (this.state.selected_operation_name) {
            this.setState({
                fingerFetching: true,
                test_fp_modal: false
            });
            axios.get('http://172.16.0.132:8081/iclock/pull_sdk?prospectsbs:connect:' + this.state.selected_device_name.ip_address + ':' + this.state.selected_device_name.value)
                .then(result => {
                    if (result.data.status === "success") {
                        this.setState({
                            loadStep: 2
                        });
                        switch (this.state.selected_operation_name.value) {
                            case 1 :
                                axios.get('http://172.16.0.132:8081/iclock/pull_sdk?prospectsbs:enroll:'+this.props.staff.regId.data.reg_id+':'+this.state.selected_finger_name.value+':1:'+this.state.selected_device_name.ip_address+':'+this.state.selected_device_name.value+'')
                                    .then(result => {
                                        if (result.data.status === "success"){
                                            // alert("enrolled");
                                            this.setState({
                                                loadStep : 3
                                            });
                                            if (this.state.selected_device_name.value > 0 && this.state.selected_staff.value > 0) {
                                                this.props.fetchFingerTemplatesWithFingerIndex(this.state.selected_staff.value,this.state.selected_device_name.value,this.state.selected_finger_name.value);
                                                setTimeout(() => {
                                                    this.setState({
                                                        loadStep : 4,
                                                        errorStep : 4
                                                    })
                                                },2000);
                                            }
                                        }
                                        else if(result.data.status === "error"){
                                            // alert("error bas verdi");
                                            this.setState({
                                                errorStep: 2
                                            })
                                        }
                                    })
                                    .catch(error => console.log(error));
                                break;
                            case 2 :
                                axios.get('http://172.16.0.132:8081/iclock/pull_sdk?prospectsbs:del_template:'+this.props.staff.regId.data.reg_id+':'+this.state.selected_finger_name.value+':'+this.state.selected_device_name.ip_address+':'+this.state.selected_device_name.value)
                                    .then(result => {
                                        if (result.data.status === "success"){
                                            // alert("enrolled");
                                            this.setState({
                                                loadStep : 3
                                            });
                                            setTimeout(() => {
                                                this.setState({
                                                    loadStep : 4,
                                                    errorStep : 4
                                                })
                                            },2000);
                                        }
                                        else if(result.data.status === "error"){
                                            // alert("error bas verdi");
                                            this.setState({
                                                errorStep: 2
                                            })
                                        }
                                    })
                                    .catch(error => console.log(error));
                                break;
                            case 3 :
                                axios.get('http://172.16.0.132:8081/iclock/pull_sdk?prospectsbs:enable:'+this.props.staff.regId.data.reg_id+':1:'+this.state.selected_device_name.ip_address+':'+this.state.selected_device_name.value+'')
                                    .then(result => {
                                        if (result.data.status === "success"){
                                            // alert("enrolled");
                                            this.setState({
                                                loadStep : 3
                                            });
                                            setTimeout(() => {
                                                this.setState({
                                                    loadStep : 4,
                                                    errorStep : 4
                                                })
                                            },2000);
                                        }
                                        else if(result.data.status === "error"){
                                            // alert("error bas verdi");
                                            this.setState({
                                                errorStep: 2
                                            })
                                        }
                                    })
                                    .catch(error => console.log(error));
                                break;
                            case 4 :
                                axios.get('http://172.16.0.132:8081/iclock/pull_sdk?prospectsbs:enable:'+this.props.staff.regId.data.reg_id+':0:'+this.state.selected_device_name.ip_address+':'+this.state.selected_device_name.value+'')
                                    .then(result => {
                                        if (result.data.status === "success"){
                                            // alert("enrolled");
                                            this.setState({
                                                loadStep : 3
                                            });
                                            setTimeout(() => {
                                                this.setState({
                                                    loadStep : 4,
                                                    errorStep : 4
                                                })
                                            },2000);
                                        }
                                        else if(result.data.status === "error"){
                                            // alert("error bas verdi");
                                            this.setState({
                                                errorStep: 2
                                            })
                                        }
                                    })
                                    .catch(error => console.log(error));
                                break;
                            default :
                                break;
                        }
                    } else {
                        this.setState({
                            errorStep: 1
                        })
                    }
                })
                .catch(error => console.log(error));
        }
    };

    handleDeviceRestart = () => {
        axios.get('http://172.16.0.132:8081/iclock/pull_sdk?prospectsbs:restart:' + this.state.selected_device_name.ip_address)
            .then(result => {
                if (result.data.status === "success") {
                    alert("restart edildi")
                } else {
                    alert(result.data.message)
                }
            }).catch(error => {
                alert("Problem bas verdi.Consola baxin");
                console.log(error);
            });
    };

    handleDeviceProcessCancel = () => {
      if (this.state.selected_device_name || this.state.clear_device){
          let value = this.state.selected_device_name
              ? this.state.selected_device
              : this.state.clear_device
                  ? this.state.clear_device
                  : null;

          axios.get('http://172.16.0.132:8081/iclock/pull_sdk?prospectsbs:cancel:' + value.ip_address + ':' + value.value)
              .then(result => {
                  if (result.data.status === "success") {
                      this.setState({
                          errorStep : this.state.loadStep
                      })
                  } else {
                      alert(result.data.message)
                  }
              }).catch(error => {
              alert("Problem bas verdi.Consola baxin");
              console.log(error);
          });
      }
    };

    handleDeviceClearModalClick = () => {
        if (this.state.device_clear_modal){
            this.setState({
                stepperMessage : null
            })
        }
        else{
            this.setState({
                stepperMessage : 'Clear Process'
            })
        }
        this.setState({
            device_clear_modal : !this.state.device_clear_modal
        })
    };

    handleDeviceClearProcess = () => {
        if (this.state.clear_device){
            this.setState({
                fingerFetching: true,
                device_clear_modal: false
            });
            axios.get('http://172.16.0.132:8081/iclock/pull_sdk?prospectsbs:connect:' + this.state.clear_device.ip_address + ':' + this.state.clear_device.value)
                .then(result => {
                    if (result.data.status === "success") {
                        this.setState({
                            loadStep: 2
                        });
                        axios.get('http://172.16.0.132:8081/iclock/pull_sdk?prospectsbs:clearall:' + this.state.clear_device.ip_address + ':' + this.state.clear_device.value)
                            .then(result => {
                                if (result.data.status === "success") {
                                    this.setState({
                                        loadStep: 3,
                                        errorStep : 4
                                    });
                                } else {
                                    this.setState({
                                        errorStep: 2
                                    })
                                }
                            })
                            .catch(error => console.log(error))
                    } else {
                        this.setState({
                            errorStep: 1
                        })
                    }
                })
                .catch(error => console.log(error));
        }
    };

    handleFpOperation = (type) => {
        let users = this.state.selected_staff,
            device = this.state.selected_device_name,
            handId = this.state.selected_hand_name,
            fingerIndexId = this.state.selected_finger_name;

        switch (type) {
            case 1 : {
               let fingerIndex = handId && fingerIndexId ? handId.value === 2 ? fingerIndexId.value : fingerIndexId.value + 5 : 0;
               console.log(fingerIndex);
                users && users.map(user => {

                });
                break;
            }
            default : {
                //alert();
            }
        }
    };

    render() {
        const {classes} = this.props;

        return (
            <Fragment>
                {
                    this.props.device_finger_operations.fetching ?
                        <CircularProgress className={classes.progress} size={70}/> :
                        <Fragment>
                            <Grid container spacing={32} style={{height: '100%',margin : 0}}>
                                <Grid item md={2} style={{ flexBasis: '21%',maxWidth: '21%',backgroundColor: '#E9E9E9' }}>
                                    <h4 className={"staff-sidebar-title"} style={{padding: '34px'}}>Qurğu logları</h4>
                                    <div className={classes.colDivider}/>
                                    <div style={{
                                        paddingLeft: '22px',
                                        paddingTop: '28px',
                                        paddingBottom: '28px'
                                    }}>
                                        <div className={classes.search}>
                                            <InputBase
                                                placeholder="Axtar…"
                                                classes={{
                                                    root: classes.inputRoot,
                                                    input: classes.inputInput,
                                                }}
                                            />
                                            <div className={classes.searchIcon}>
                                                <SearchIcon/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={classes.colDivider}/>
                                    <div style={{
                                        padding: 20,
                                        paddingRight: 10
                                    }}>
                                        <ProSelect
                                            id={"select_components"}
                                            className="menu-outer-top"
                                            classNamePrefix="select"
                                            isDisabled={false}
                                            placeholder={"Əməliyyat"}
                                            isLoading={this.props.device_finger_operations.fetching}
                                            isClearable={true}
                                            isSearchable={true}
                                            components={makeAnimated()}
                                            name="operation_name"
                                            value={this.state.selected_operation}
                                            options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                                            onChange={this.handleChangeOperation}
                                        />
                                    </div>
                                    <div style={{
                                        paddingLeft: 20,
                                        paddingRight: 10
                                    }}>
                                        <ProSelect
                                            id={"select_components"}
                                            className="menu-outer-top"
                                            classNamePrefix="select"
                                            isDisabled={false}
                                            placeholder={"Qurğu"}
                                            isLoading={this.props.devices.fetching}
                                            isClearable={true}
                                            isSearchable={true}
                                            components={makeAnimated()}
                                            name="operation_device"
                                            value={this.state.selected_device}
                                            options={makeDataForFingerSelectDevice(this.props.devices.total_data.data)}
                                            onChange={this.handleChangeDevice}
                                        />
                                    </div>
                                    <div style={{
                                        paddingLeft: 20,
                                        paddingRight: 10,
                                        paddingTop: 5
                                    }}>
                                        <ProDatePicker
                                            datepickerName={"selected_date"}
                                            formatDate={"dd-MM-yyyy"}
                                            selected_date={this.state.selected_date}
                                            handleChangeDate={(e) => this.handleChangeDate(e)}
                                            handleClearDate={(e) => this.handleClearDate(e)}
                                        />
                                    </div>
                                    <div style={{
                                        paddingLeft: 20,
                                        paddingRight: 12,
                                        paddingTop: 10
                                    }}>
                                        <ProSelect
                                            id={"select_components"}
                                            className="menu-outer-top"
                                            classNamePrefix="select"
                                            isDisabled={false}
                                            placeholder={"Status"}
                                            isLoading={false}
                                            isClearable={true}
                                            isSearchable={true}
                                            components={makeAnimated()}
                                            name="operation_status"
                                            value={this.state.selected_status}
                                            options={[
                                                {value: 1, label: 'OK'},
                                                {value: 2, label: 'NOT OK'}
                                            ]}
                                            onChange={this.handleChangeStatus}
                                        />
                                    </div>
                                </Grid>
                                <Grid item md={9}>
                                    <Grid container spacing={32} style={{
                                        marginTop: 50
                                    }}>
                                        <Grid item md={12} style={{ marginRight : 10}}>
                                            <Grid container spacing={8}>
                                                <Grid item md={3}>
                                                    <ProSelect
                                                        name="staff_name"
                                                        isLoading={false}
                                                        isMulti
                                                        options={makeDataForUserSelect(this.props.staffs.total_data.data)}
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                        value={this.state.selected_staff}
                                                        onChange={this.handleStaffSelectChange}
                                                        styles={{
                                                            ...registerStaffFingerStyle,
                                                            control: (base, state) => ({
                                                                ...base,
                                                                '&:hover': {borderColor: '#55AB80'}, // border style on hover
                                                                border: '1px solid #55AB80', // default border color
                                                                boxShadow: 'none', // no box-shadow
                                                            }),
                                                        }}
                                                        placeholder={"Əməkdaş seç"}
                                                    />
                                                </Grid>
                                                <Grid item md={3}>
                                                    <ProSelect
                                                        name="device_name"
                                                        isLoading={false}
                                                        isClearable={true}
                                                        options={makeDataForFingerSelectDevice(this.props.devices.total_data.data)}
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                        value={this.state.selected_device_name}
                                                        onChange={this.handleChangeDeviceModal}
                                                        styles={{
                                                            ...registerStaffFingerStyle,
                                                            control: (base, state) => ({
                                                                ...base,
                                                                '&:hover': {borderColor: '#55AB80'}, // border style on hover
                                                                border: '1px solid #55AB80', // default border color
                                                                boxShadow: 'none', // no box-shadow
                                                            }),
                                                        }}
                                                        placeholder={"Qurğu seç"}
                                                    />
                                                </Grid>
                                                <Grid item md={3}>
                                                    <ProSelect
                                                        name="hand_name"
                                                        isLoading={false}
                                                        isClearable={true}
                                                        options={[
                                                            {value: 1, label: 'Sağ əl'},
                                                            {value: 2, label: 'Sol əl'},
                                                        ]}
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                        value={this.state.selected_hand_name}
                                                        onChange={this.handleHandNameSelectChange}
                                                        styles={{
                                                            ...registerStaffFingerStyle,
                                                            control: (base, state) => ({
                                                                ...base,
                                                                '&:hover': {borderColor: '#55AB80'}, // border style on hover
                                                                border: '1px solid #55AB80', // default border color
                                                                boxShadow: 'none', // no box-shadow
                                                            }),
                                                        }}
                                                        placeholder={"Əl seç"}
                                                    />
                                                </Grid>
                                                <Grid item md={3}>
                                                    <ProSelect
                                                        name="finger_name"
                                                        isLoading={false}
                                                        isClearable={true}
                                                        options={[
                                                            {value: 0, label: 'Baş barmaq'},
                                                            {value: 1 , label: 'Şəhadət barmağı'},
                                                            {value: 2, label: 'Orta barmaq'},
                                                            {value: 3, label: 'Adsız barmaq'},
                                                            {value: 4, label: 'Çeçelə barmaq'}
                                                        ]}
                                                        className="basic-multi-select"
                                                        classNamePrefix="select"
                                                        value={this.state.selected_finger_name}
                                                        onChange={this.handleFingerNameSelectChange}
                                                        styles={{
                                                            ...registerStaffFingerStyle,
                                                            control: (base, state) => ({
                                                                ...base,
                                                                '&:hover': {borderColor: '#55AB80'}, // border style on hover
                                                                border: '1px solid #55AB80', // default border color
                                                                boxShadow: 'none', // no box-shadow
                                                            }),
                                                        }}
                                                        placeholder={"Barmaq seç"}
                                                    />
                                                </Grid>
                                            </Grid>
                                        </Grid>
                                        <Grid item md={12} style={{
                                            marginTop: '-15px',
                                            marginBottom: '-25px'
                                        }}>
                                            <Grid container spacing={8}>
                                                <Button variant="outlined" className={classNames(classes.button,classes.writeBtnStyle)}
                                                        onClick={() => this.handleFpOperation(1)}
                                                >
                                                    Yaz
                                                </Button>
                                                <Button variant="outlined" className={classNames(classes.button,classes.deleteBtnStyle)}>
                                                    SİL
                                                </Button>
                                                <Button variant="outlined" className={classNames(classes.button,classes.activeBtnStyle)}>
                                                    AKTİV ET
                                                </Button>
                                                <Button variant="outlined" className={classNames(classes.button,classes.deActiveBtnStyle)}>
                                                    DEAKTİV ET
                                                </Button>
                                                <Button variant="outlined" className={classNames(classes.button,classes.checkFPWithDBBtnStyle)}>
                                                    Bİ DB İLƏ YOXLA
                                                </Button>
                                                <Button variant="outlined" className={classNames(classes.button,classes.refreshBtnStyle)}>
                                                    REFRESH ET
                                                </Button>
                                                <Button variant="outlined" className={classNames(classes.button,classes.deviceControlBtnStyle)}>
                                                    QURĞULAR İLƏ MONİTORİNQ ET
                                                </Button>
                                            </Grid>
                                        </Grid>
                                        <Grid item md={12}>
                                            <Paper className={classes.root}>
                                                <Table className={classes.table}>
                                                    <TableHead>
                                                        <TableRow>
                                                            <TableCell className={classes.headTitle}>#</TableCell>
                                                            <TableCell align="center" className={classes.headTitle}>Əməliyyat
                                                                adı</TableCell>
                                                            <TableCell align="center"
                                                                       className={classes.headTitle}>Qurğu</TableCell>
                                                            <TableCell align="center"
                                                                       className={classes.headTitle}>Tarix</TableCell>
                                                            <TableCell align="center"
                                                                       className={classes.headTitle}>Status</TableCell>
                                                            <TableCell align="center"
                                                                       className={classes.headTitle}>Message</TableCell>
                                                        </TableRow>
                                                    </TableHead>
                                                    {
                                                        <TableBody>
                                                            {
                                                                this.props.fp_logs.fetching ?
                                                                    <TableBody>
                                                                        <TableRow>
                                                                            <TableCell colSpan={6}>
                                                                                <CircularProgress
                                                                                    className={classes.progressTable}
                                                                                    size={70}/>
                                                                            </TableCell>
                                                                        </TableRow>
                                                                    </TableBody> :
                                                                    this.props.fp_logs.total_data.data.data.map((row, index) => (
                                                                        <TableRow key={row.id}>
                                                                            <TableCell component="th" scope="row">
                                                                                {index + 1}
                                                                            </TableCell>
                                                                            <TableCell
                                                                                align="right">{row.operation_name}</TableCell>
                                                                            <TableCell
                                                                                align="right">{row.ip_address}</TableCell>
                                                                            <TableCell
                                                                                align="right">{moment(row.created_at).format('DD/MM/YYYY')}</TableCell>
                                                                            <TableCell style={{padding: "15px"}}>{
                                                                                parseInt(row.status) === 1 ?
                                                                                    <span
                                                                                        className={classes.messageBtnStyle}
                                                                                        style={{
                                                                                            backgroundColor: '#6fc99c',
                                                                                            color: 'white'
                                                                                        }}>OK</span> :
                                                                                    <span
                                                                                        className={classes.messageBtnStyle}
                                                                                        style={{
                                                                                            backgroundColor: 'rgb(241, 82, 89)',
                                                                                            color: 'white'
                                                                                        }}>NOT OK</span>
                                                                            }</TableCell>
                                                                            <TableCell align="right">
                                                                                <ProPassAlert
                                                                                    variant={parseInt(row.status) === 1 ? "success" : "error"}
                                                                                    className={classes.margin}
                                                                                    message={row.message ? row.message : parseInt(row.status) === 1 ? "success" : "error"}
                                                                                    isClose={false}
                                                                                    style={{
                                                                                        minWidth: 200,
                                                                                        maxWidth: 200
                                                                                    }}
                                                                                />
                                                                            </TableCell>
                                                                        </TableRow>
                                                                    ))}
                                                        </TableBody>
                                                    }
                                                </Table>
                                            </Paper>
                                        </Grid>
                                        <div className={classes.stepperStyle}
                                             style={this.state.fingerFetching ? {display: 'block'} : {display: 'none'}}>
                                            <Paper className={classes.stepperDivStyle}>
                                                <Grid container spacing={24}>
                                                    <Grid item md={ this.state.stepperMessage ? 6 : 4 }>
                                                        <ProStepper classes={classes} currentStep={1}
                                                                    loadStep={this.state.loadStep}
                                                                    stepLoaderStyle={"stepLoaderOneStyle"}
                                                                    stepperText={"Connection"}
                                                                    errorStep={this.state.errorStep}/>
                                                    </Grid>
                                                    <Grid item md={this.state.stepperMessage ? 6 : 4}>
                                                        <ProStepper classes={classes} currentStep={2}
                                                                    loadStep={this.state.loadStep}
                                                                    stepLoaderStyle={"stepLoaderTwoStyle"}
                                                                    stepperText={this.state.stepperMessage}
                                                                    errorStep={this.state.errorStep}/>
                                                    </Grid>
                                                    <Grid item md={4} style={this.state.stepperMessage ? {display : 'none'} : {display : 'block'}}>
                                                        <ProStepper classes={classes} currentStep={3}
                                                                    loadStep={this.state.loadStep}
                                                                    stepLoaderStyle={"stepLoaderThreeStyle"}
                                                                    stepperText={"Finish"}
                                                                    errorStep={this.state.errorStep}/>
                                                    </Grid>
                                                   <Grid container spacing={24}>
                                                       <Grid item md={3} style={{
                                                           marginLeft: 45,
                                                           marginTop: 15
                                                       }}>
                                                           <Button contained style={{
                                                               backgroundColor: 'rgb(220, 109, 113)',
                                                               color: 'white',
                                                               textTransform: 'inherit',
                                                               marginTop: '20px'
                                                           }} onClick={this.handleDeviceRestart}>Restart Device</Button>
                                                       </Grid>
                                                       <Grid item md={3} style={{
                                                           marginLeft: 50,
                                                           marginTop: 15
                                                       }}>
                                                           <Button contained style={{
                                                               backgroundColor: 'rgb(220, 109, 113)',
                                                               color: 'white',
                                                               textTransform: 'inherit',
                                                               marginTop: '20px'
                                                               }} onClick={this.handleDeviceProcessCancel}>Cancel Process</Button>
                                                       </Grid>
                                                       <Grid item md={3} style={this.state.errorStep > 0 ? {
                                                           display: 'block',
                                                           marginLeft: 50
                                                       } : {display: 'none'}}>
                                                           <Button contained style={{
                                                               backgroundColor: '#99a49f',
                                                               color: 'white',
                                                               textTransform: 'inherit',
                                                               marginTop: '35px',
                                                               width : 110
                                                           }} onClick={() => this.setState({
                                                               fingerFetching: false,
                                                               errorStep: 0,
                                                               loadStep: 1
                                                           })}>Bağla</Button>
                                                       </Grid>
                                                   </Grid>
                                                </Grid>
                                            </Paper>
                                        </div>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Fragment>
                }
            </Fragment>
        );
    }
}

DevMonitorinq.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({fp_logs, device_finger_operations, devices, staffs , staff }) => {
    return {
        fp_logs,
        device_finger_operations,
        devices,
        staffs,
        staff
    }
};

const mapDispatchToProps = {
    fetchAllOperations,
    fetchDeviceFingerOperations,
    fetchStaffs,
    fetchDevices,
    fetchRegIdWithUserId,
    fetchFingerTemplatesWithFingerIndex
};

export default withStyles(devMonitorinqStyles)(connect(mapStateToProps, mapDispatchToProps)(DevMonitorinq));
