import React from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {
    Table, TableBody, TableCell, TableHead, TablePagination, TableRow, TableSortLabel,
    Paper, IconButton, Tooltip, TextField, CircularProgress,
    Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle,
    Button
} from '@material-ui/core';

import {
    Delete as DeleteIcon,
    Edit as EditIcon,
    Save as SaveIcon,
    Cancel as CancelIcon
} from '@material-ui/icons';
import {Helmet} from 'react-helmet';
import {connect} from 'react-redux';

import {
    fetchDeviceDestinations ,
    addNewDeviceDestination ,
    cancelDeviceDestination ,
    deleteDeviceDestination ,
    saveOrUpdateDeviceDestination
} from '../../actions/msk/device_destination.action';
import MskToolBarComponent from "../../components/MskToolBarComponent";

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}
function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}
function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const rows = [
    {id: 'name', numeric: false, disablePadding: true, sortDirection: true, label: 'Qurğu istiqaməti'},
    {id: 'operation', numeric: false, disablePadding: true, sortDirection: false, label: 'Əməliyyat'},
];

class EnhancedTableHead extends React.Component {
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const {order, orderBy} = this.props;

        return (
            <TableHead>
                <TableRow>
                    <TableCell padding="checkbox">
                        <span>#</span>
                    </TableCell>
                    {rows.map((row, index) => {
                        return (
                            <TableCell
                                key={row.id}
                                style={rows.length === index + 1 ? {
                                    textAlign: 'end',
                                    marginRight: '24px !important'
                                } : {}}
                                align={row.numeric ? 'right' : 'left'}
                                padding={row.disablePadding ? 'none' : 'default'}
                                sortDirection={orderBy === row.id && row.sortDirection ? order : false}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={false}
                                        direction={order}
                                        onClick={this.createSortHandler(row.id)}
                                    >
                                        {row.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}

EnhancedTableHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};



const styles = theme => ({
    root: {
        width: '80%',
        marginTop: theme.spacing.unit * 3,
    },
    table: {
        minWidth: '100%',
    },
    tableWrapper: {
        overflowX: 'auto',
        padding: '15px'
    },
    fab: {
        position: 'absolute',
        bottom: '44px',
        right: '4%',
        color: 'white',
        backgroundColor: '63c79d'
    },
    addBtnCss: {
        color: 'rgb(230, 237, 234)',
        backgroundColor: 'rgb(99, 199, 157)',
        '&:hover': {
            backgroundColor: 'rgb(88, 183, 142)',
            color: 'rgb(230, 237, 234)'
        },
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        height: '45px'
    },
    // cssLabel: {
    //     color : 'green'
    // },

    cssOutlinedInput: {
        '&$cssFocused $notchedOutline': {
            borderColor: '#63c79d !important',
        }
    },

    cssFocused: {},

    notchedOutline: {
        // borderWidth: '1px',
        // borderColor: 'green !important'
    },
    dense: {
        marginTop: 16,
    },
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '50%',
        marginTop: '25%',
    },
});

class DeviceDestination extends React.Component {
    state = {
        order: 'asc',
        orderBy: 'device_destinations',
        selected: [],
        page: 0,
        rowsPerPage: 6,
        lastRowId: 0,
        editRowId: 0,
        device_name: '',
        hasAddRow: false,
        deleteDialogIsOpen: false,
        deletedRowId : 0
    };

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({order, orderBy});
    };


    handleChangePage = (event, page) => {
        this.setState({page});
    };

    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };


    // start my functions
    handleEditDeviceDestination = (e, id) => {
        e.preventDefault();
        if (!this.state.hasAddRow) {
            this.setState({
                editRowId: id,
            });
        }
    };

    handleDeleteDeviceDestination = (e, id) => {
        e.preventDefault();
        this.props.deleteDeviceDestination(id);
        this.setState({
            deleteDialogIsOpen: !this.state.deleteDialogIsOpen
        });
    };

    handleSaveDeviceDestination = (e, id, isAdd) => {
        if (isAdd) {
            // console.log({ id : 0 , name : this.state.device_name});
            this.props.saveOrUpdateDeviceDestination({id: 0, name: this.state.device_name});
            this.props.cancelDeviceDestination(id);
            this.setState({
                hasAddRow: false
            })
        } else {
            this.props.saveOrUpdateDeviceDestination({id: id, name: this.state.device_name});
            this.setState({
                editRowId: 0
            });
        }
    };

    handleCancelDeviceDestination = (e, id, isAdd) => {
        e.preventDefault();
        if (isAdd) {
            this.props.cancelDeviceDestination(id);
            this.setState({
                hasAddRow: false
            })
        } else {
            this.setState({
                editRowId: 0
            });
        }
    };

    handleAddDeviceDestination = (e) => {
        e.preventDefault();
        if (!this.state.hasAddRow && this.state.editRowId === 0) {
            this.props.addNewDeviceDestination(this.props.device_destinations.total_data.data.length + 1);
            this.setState({
                hasAddRow: true
            });
        }
    };

    componentWillMount() {
        this.props.fetchDeviceDestinations();
    };


    handleInputChange = (e) => {
        this.setState({
            device_name: e.target.value
        })
    };

    handelDeleteDialogToggle = (e, id) => {
        e.preventDefault();
        this.setState({
            deleteDialogIsOpen: !this.state.deleteDialogIsOpen,
            deletedRowId : !this.state.deleteDialogIsOpen ? id : 0
        });
    };

    // end my functions

    render() {

        const {classes, fullScreen} = this.props;
        const {order, orderBy, selected, rowsPerPage, page} = this.state;

        return (
            <div style={{
                padding: 40
            }}>
                <Helmet>
                    <title>Qurğu təyinatları - ProPass</title>
                </Helmet>
                {
                    this.props.device_destinations.fetching ?
                        <CircularProgress className={classes.progress} size={70}/> :
                        <div>
                            <Paper className={classes.root}>
                                <MskToolBarComponent classes={classes} toolBarTitle={"Qurertewltlke"} handleAddNewRow={(e) => this.handleAddDeviceDestination(e)}/>
                                {/*<EnhancedTableToolbar numSelected={selected.length} handleAddDeviceDestination={(e) => this.handleAddDeviceDestination(e)}/>*/}
                                <div className={classes.tableWrapper}>
                                    <Table className={classes.table} aria-labelledby="tableTitle">
                                        <EnhancedTableHead
                                            numSelected={selected.length}
                                            order={order}
                                            orderBy={orderBy}
                                            onRequestSort={this.handleRequestSort}
                                            rowCount={this.props.device_destinations.total_data.data.length}
                                        />
                                        <TableBody>
                                            {stableSort(this.props.device_destinations.total_data.data, getSorting(order, orderBy))
                                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                                .map((n, index) => {
                                                    const {editRowId} = this.state;
                                                    return (
                                                        <TableRow
                                                            hover
                                                            tabIndex={-1}
                                                            key={n.id}
                                                        >
                                                            <TableCell component="th" scope="row" padding="none">
                                                                <span style={{marginLeft: '10px'}}>{index + 1}</span>
                                                            </TableCell>
                                                            <TableCell component="th" scope="row" padding="none">
                                                                {
                                                                    editRowId === n.id || n.isAdd ?
                                                                        <TextField
                                                                            id="outlined-bare"
                                                                            className={classes.textField}
                                                                            defaultValue={n.name}
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            name={"device_name"}
                                                                            onChange={e => this.handleInputChange(e, n.isAdd)}
                                                                            InputLabelProps={{
                                                                                classes: {
                                                                                    // root: classes.cssLabel,
                                                                                    focused: classes.cssFocused,
                                                                                },
                                                                            }}
                                                                            InputProps={{
                                                                                classes: {
                                                                                    root: classes.cssOutlinedInput,
                                                                                    focused: classes.cssFocused,
                                                                                    notchedOutline: classes.notchedOutline,
                                                                                },
                                                                            }}
                                                                        /> :
                                                                        <span
                                                                            style={{paddingLeft: '10px'}}>{n.name}</span>
                                                                }
                                                            </TableCell>
                                                            <TableCell component="th" scope="row" padding={"dense"}
                                                                       style={{textAlign: 'end'}}>
                                                                {
                                                                    editRowId === n.id || n.isAdd ?
                                                                        <div>
                                                                            <Tooltip title="Save">
                                                                                <IconButton aria-label="Save"
                                                                                            onClick={(e) => this.handleSaveDeviceDestination(e, n.id, n.isAdd)}>
                                                                                    <SaveIcon/>
                                                                                </IconButton>
                                                                            </Tooltip>
                                                                            <Tooltip title="Cancel">
                                                                                <IconButton aria-label="Cancel"
                                                                                            onClick={(e) => this.handleCancelDeviceDestination(e, n.id, n.isAdd)}>
                                                                                    <CancelIcon/>
                                                                                </IconButton>
                                                                            </Tooltip>
                                                                        </div>
                                                                        :
                                                                        <div>
                                                                            <Tooltip title="Edit">
                                                                                <IconButton aria-label="Edit"
                                                                                            onClick={(e) => this.handleEditDeviceDestination(e, n.id)}>
                                                                                    <EditIcon/>
                                                                                </IconButton>
                                                                            </Tooltip>
                                                                            <Tooltip title="Delete">
                                                                                <IconButton aria-label="Delete"
                                                                                            onClick={(e) => this.handelDeleteDialogToggle(e, n.id)}>
                                                                                    <DeleteIcon/>
                                                                                </IconButton>
                                                                            </Tooltip>
                                                                        </div>
                                                                }
                                                            </TableCell>
                                                        </TableRow>
                                                    );
                                                })}
                                        </TableBody>
                                        <Dialog
                                            fullScreen={fullScreen}
                                            open={this.state.deleteDialogIsOpen}
                                            onClose={this.handelDeleteDialogToggle}
                                            aria-labelledby="responsive-dialog-title"
                                        >
                                            <DialogTitle
                                                id="responsive-dialog-title">{"Xəbərdarlıq!"}</DialogTitle>
                                            <DialogContent>
                                                <DialogContentText>
                                                    Bu qurğu tipini silmək istədiyinizə əminsiniz ?
                                                </DialogContentText>
                                            </DialogContent>
                                            <DialogActions>
                                                <Button onClick={e => this.handleDeleteDeviceDestination(e,this.state.deletedRowId)} color="primary" autoFocus>
                                                    Bəlİ
                                                </Button>
                                                <Button onClick={this.handelDeleteDialogToggle} color="primary">
                                                    Xeyİr
                                                </Button>
                                            </DialogActions>
                                        </Dialog>
                                    </Table>
                                </div>
                                <TablePagination
                                    rowsPerPageOptions={[]}
                                    component="div"
                                    count={this.props.device_destinations.total_data.data.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    backIconButtonProps={{
                                        'aria-label': 'Previous Page',
                                    }}
                                    nextIconButtonProps={{
                                        'aria-label': 'Next Page',
                                    }}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                />
                            </Paper>
                        </div>

                }
            </div>
        );
    }
}

DeviceDestination.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ device_destinations }) => {
    return {
        device_destinations
    }
};

const mapDispatchToProps = {
    fetchDeviceDestinations ,
    addNewDeviceDestination ,
    cancelDeviceDestination ,
    deleteDeviceDestination ,
    saveOrUpdateDeviceDestination
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(DeviceDestination));