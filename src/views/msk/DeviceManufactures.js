import React, {Fragment} from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {
    Table, TableBody, TableCell, TableHead, TablePagination, TableRow, TableSortLabel,
    Toolbar, Typography, Paper, IconButton, Tooltip, TextField, CircularProgress,Grid
} from '@material-ui/core';

import {
    Add as AddIcon,
    Delete as DeleteIcon,
    Edit as EditIcon,
    Save as SaveIcon,
    Cancel as CancelIcon
} from '@material-ui/icons';
import {Helmet} from 'react-helmet';
import {lighten} from '@material-ui/core/styles/colorManipulator';

import {connect} from 'react-redux';
import {
    fetchDeviceManufactures,
    addNewDeviceManufacture,
    cancelDeviceManufacture,
    deleteDeviceManufacture,
    saveOrUpdateDeviceManufacture
} from '../../actions/msk/device_manufactures.action';

import { fetchDeviceModels } from '../../actions/msk/device_models.action';

import DeviceModels from "../subs/DeviceModels";
import ProPassDialog from "../../components/ProPassDialog";

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const rows = [
    {id: 'name', numeric: false, disablePadding: true, sortDirection: true, label: 'Qurğu istehsalçısı'},
    {id: 'operation', numeric: false, disablePadding: true, sortDirection: false, label: 'Əməliyyat'},
];

class EnhancedTableHead extends React.Component {
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const {order, orderBy} = this.props;

        return (
            <TableHead>
                <TableRow>
                    <TableCell padding="checkbox">
                        <span>#</span>
                    </TableCell>
                    {rows.map((row, index) => {
                        return (
                            <TableCell
                                key={row.id}
                                style={rows.length === index + 1 ? {
                                    textAlign: 'end',
                                    marginRight: '24px !important'
                                } : {}}
                                align={row.numeric ? 'right' : 'left'}
                                padding={row.disablePadding ? 'none' : 'default'}
                                sortDirection={orderBy === row.id && row.sortDirection ? order : false}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={false}
                                        direction={order}
                                        onClick={this.createSortHandler(row.id)}
                                    >
                                        {row.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}

EnhancedTableHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired
};

const toolbarStyles = theme => ({
    root: {
        paddingRight: theme.spacing.unit,
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    spacer: {
        flex: '1 1 100%',
    },
    actions: {
        color: theme.palette.text.secondary,
    },
    title: {
        flex: '0 0 auto',
    },
});

let EnhancedTableToolbar = props => {
    const {numSelected, classes , handleAddDeviceManufacture } = props;

    return (
        <Toolbar
            className={classNames(classes.root, {
                [classes.highlight]: numSelected > 0,
            })}
        >
            <div className={classes.title}>
                {numSelected > 0 ? (
                    <Typography color="inherit" variant="subtitle1">
                        {numSelected} selected
                    </Typography>
                ) : (
                    <Typography variant="h6" id="tableTitle">
                        Qurğu istehsalçıları
                    </Typography>
                )}
            </div>
            <div className={classes.spacer}/>
            <div className={classes.actions}>
                <Tooltip title="Yeni qurğu istehsalçısı">
                    <IconButton aria-label="Add" onClick={handleAddDeviceManufacture}>
                        <AddIcon className={classes.customIconSizes}/>
                    </IconButton>
                </Tooltip>
            </div>
        </Toolbar>
    );
};

EnhancedTableToolbar.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        height : '100%'
    },
    table: {
        minWidth: '100%',
    },
    tableWrapper: {
        overflowX: 'auto',
        padding: '15px'
    },
    fab: {
        position: 'absolute',
        bottom: '44px',
        right: '4%',
        color: 'white',
        backgroundColor: '63c79d'
    },
    addBtnCss: {
        color: 'rgb(230, 237, 234)',
        backgroundColor: 'rgb(99, 199, 157)',
        '&:hover': {
            backgroundColor: 'rgb(88, 183, 142)',
            color: 'rgb(230, 237, 234)'
        },
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        height: '38px',
        bottom: '3px'
    },

    cssOutlinedInput: {
        '&$cssFocused $notchedOutline': {
            borderColor: '#63c79d !important',
        }
    },

    cssFocused: {},

    notchedOutline: {
        // borderWidth: '1px',
        // borderColor: 'green !important'
    },

    dense: {
        marginTop: 16,
    },
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '50%',
        marginTop: '25%',
    },
    customIconSizes : {
        fontSize : '21px'
    },
    resize:{
        fontSize:13
    },
    isActiveRow : {
        backgroundColor : 'rgba(224, 224, 224, 1)'
    }
});

class DeviceManufactures extends React.Component {
    state = {
        order: 'asc',
        orderBy: 'device_manufactures',
        selected: [],
        page: 0,
        rowsPerPage: 4,
        lastRowId: 0,
        editRowId: 0,
        device_name: '',
        hasAddRow: false,
        deleteDialogIsOpen: false,
        deletedRowId: 0,
        activeTableRow : 26,
        notFoundModel : true
    };

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({order, orderBy});
    };


    handleChangePage = (event, page) => {
        this.setState({page});
    };

    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };


    // start my functions
    handleEditDeviceManufacture = (e, id) => {
        e.stopPropagation();
        e.preventDefault();
        if (!this.state.hasAddRow) {
            this.setState({
                editRowId: id,
            });
        }
    };

    handleDeleteDeviceManufacture = (e, id) => {
        e.stopPropagation();
        e.preventDefault();
        this.props.deleteDeviceManufacture(id);
        this.setState({
            deleteDialogIsOpen: !this.state.deleteDialogIsOpen,
            activeTableRow : this.props.device_manufactures.total_data.data[0]['id']
        });
        this.props.fetchDeviceModels(this.props.device_manufactures.total_data.data[0]['id'])
    };

    handleSaveDeviceManufacture = (e, id, isAdd) => {
        e.stopPropagation();
        e.preventDefault();
        if (isAdd) {
            this.props.saveOrUpdateDeviceManufacture({id: 0, name: this.state.device_name});
            this.props.cancelDeviceManufacture(id);
            this.setState({
                hasAddRow: false
            })
        } else {
            this.props.saveOrUpdateDeviceManufacture({id: id, name: this.state.device_name});
            this.setState({
                editRowId: 0
            });
        }
    };

    handleCancelDeviceManufacture = (e, id, isAdd) => {
        e.stopPropagation();
        e.preventDefault();
        if (isAdd) {
            this.props.cancelDeviceManufacture(id);
            this.setState({
                hasAddRow: false
            })
        } else {
            this.setState({
                editRowId: 0
            });
        }
    };


    handleAddDeviceManufacture = (e) => {
        e.stopPropagation();
        e.preventDefault();
        if (!this.state.hasAddRow && this.state.editRowId === 0) {
            this.props.addNewDeviceManufacture(this.props.device_manufactures.total_data.data.length + 1);
            this.setState({
                hasAddRow: true
            });
        }
    };

    componentWillMount() {
        this.props.fetchDeviceManufactures();
        this.props.fetchDeviceModels(this.state.activeTableRow);
    };

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.device_manufactures.total_data.data !== undefined){
            if (nextProps.device_manufactures.total_data.data.length > 0) {
                this.setState({
                    activeTableRow : nextProps.device_manufactures.total_data.data[0]['id'],
                    notFoundModel : true
                });
            }
            else{
                this.props.fetchDeviceModels(0);
                this.setState({
                    notFoundModel : false
                })
            }
        }
    }

    handleInputChange = (e) => {
        this.setState({
            device_name: e.target.value
        })
    };

    handelDeleteDialogToggle = (e, id) => {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            deleteDialogIsOpen: !this.state.deleteDialogIsOpen,
            deletedRowId: !this.state.deleteDialogIsOpen ? id : 0
        });
    };

    handleTableRowClick  = (e,id) => {
        e.preventDefault();
        e.stopPropagation();
        if (id > 0 && id !== this.state.activeTableRow){
            this.setState({
                activeTableRow : id
            });
            this.props.fetchDeviceModels(id)
        }
    };


    // end my functions

    render() {

        const {classes} = this.props;
        const {order, orderBy, selected, rowsPerPage, page} = this.state;

        return (
            <div style={{
                padding: 40
            }}>
                <Helmet>
                    <title>Qurğu istehsalçıları və modelləri - ProPass</title>
                </Helmet>
                {
                    this.props.device_manufactures.fetching ?
                        <CircularProgress className={classes.progress} size={70}/> :
                        <Grid container spacing={24}>
                            <Grid item xs={this.state.notFoundModel ? 4 : 12}>
                                <Paper className={classes.root}>
                                    <EnhancedTableToolbar numSelected={selected.length} handleAddDeviceManufacture={(e) => this.handleAddDeviceManufacture(e)}/>
                                    <div className={classes.tableWrapper}>
                                        <Table className={classes.table} aria-labelledby="tableTitle">
                                            <EnhancedTableHead
                                                numSelected={selected.length}
                                                order={order}
                                                orderBy={orderBy}
                                                onRequestSort={this.handleRequestSort}
                                                rowCount={this.props.device_manufactures.total_data.data.length}
                                            />
                                            <TableBody>
                                                {stableSort(this.props.device_manufactures.total_data.data, getSorting(order, orderBy))
                                                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                                    .map((n, index) => {
                                                        const {editRowId} = this.state;
                                                        return (
                                                            <TableRow
                                                                hover
                                                                tabIndex={-1}
                                                                data-row-id={index}
                                                                key={n.id}
                                                                style={{ cursor : 'pointer' }}
                                                                className={this.state.activeTableRow  === n.id ? classes.isActiveRow : ''}
                                                                onClick={(e) => this.handleTableRowClick(e,n.id)}
                                                            >
                                                                <TableCell component="th" scope="row" padding="none">
                                                                    <span
                                                                        style={{marginLeft: '10px'}}>{index + 1}</span>
                                                                </TableCell>
                                                                <TableCell component="th" scope="row" padding="none">
                                                                    {
                                                                        editRowId === n.id || n.isAdd ?
                                                                            <TextField
                                                                                id="outlined-bare"
                                                                                className={classes.textField}
                                                                                defaultValue={n.name}
                                                                                margin="normal"
                                                                                variant="outlined"
                                                                                name={"device_name"}
                                                                                onChange={e => this.handleInputChange(e, n.isAdd)}
                                                                                InputLabelProps={{
                                                                                    classes: {
                                                                                        // root: classes.cssLabel,
                                                                                        focused: classes.cssFocused,
                                                                                    },
                                                                                }}
                                                                                InputProps={{
                                                                                    classes: {
                                                                                        root: classes.cssOutlinedInput,
                                                                                        focused: classes.cssFocused,
                                                                                        notchedOutline: classes.notchedOutline,
                                                                                        input: classes.resize,
                                                                                    },
                                                                                }}
                                                                            /> :
                                                                            <span
                                                                                style={{paddingLeft: '10px'}}>{n.name}</span>
                                                                    }
                                                                </TableCell>
                                                                <TableCell component="th" scope="row" padding={"dense"}
                                                                           style={{textAlign: 'end' , width: '150px' , paddingRight: '6px'}}>
                                                                    {
                                                                        editRowId === n.id || n.isAdd ?
                                                                            <Fragment>
                                                                                <Tooltip title="Save">
                                                                                    <IconButton aria-label="Save"
                                                                                                onClick={(e) => this.handleSaveDeviceManufacture(e, n.id, n.isAdd)}>
                                                                                        <SaveIcon className={classes.customIconSizes}/>
                                                                                    </IconButton>
                                                                                </Tooltip>
                                                                                <Tooltip title="Cancel">
                                                                                    <IconButton aria-label="Cancel"
                                                                                                onClick={(e) => this.handleCancelDeviceManufacture(e, n.id, n.isAdd)}>
                                                                                        <CancelIcon className={classes.customIconSizes}/>
                                                                                    </IconButton>
                                                                                </Tooltip>
                                                                            </Fragment>
                                                                            :
                                                                            <Fragment>
                                                                                <Tooltip title="Edit">
                                                                                    <IconButton aria-label="Edit"
                                                                                                onClick={(e) => this.handleEditDeviceManufacture(e, n.id)}>
                                                                                        <EditIcon className={classes.customIconSizes}/>
                                                                                    </IconButton>
                                                                                </Tooltip>
                                                                                <Tooltip title="Delete">
                                                                                    <IconButton aria-label="Delete"
                                                                                                onClick={(e) => this.handelDeleteDialogToggle(e, n.id)}>
                                                                                        <DeleteIcon className={classes.customIconSizes}/>
                                                                                    </IconButton>
                                                                                </Tooltip>
                                                                            </Fragment>
                                                                    }
                                                                </TableCell>
                                                            </TableRow>
                                                        );
                                                    })}
                                            </TableBody>
                                            <ProPassDialog
                                                is_open={this.state.deleteDialogIsOpen}
                                                message_title = "Xəbərdarlıq"
                                                message_content = "Bu qurğu istiqamətini silmək istədiyinizə əminsiniz ?"
                                                handleToggleProcess = {this.handelDeleteDialogToggle}
                                                handleDeleteProcess = {e => this.handleDeleteDeviceManufacture(e, this.state.deletedRowId)}
                                            />
                                        </Table>
                                    </div>
                                    <TablePagination
                                        rowsPerPageOptions={[]}
                                        component="div"
                                        count={this.props.device_manufactures.total_data.data.length}
                                        rowsPerPage={rowsPerPage}
                                        page={page}
                                        backIconButtonProps={{
                                            'aria-label': 'Previous Page',
                                        }}
                                        nextIconButtonProps={{
                                            'aria-label': 'Next Page',
                                        }}
                                        onChangePage={this.handleChangePage}
                                        onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                        style={{ marginTop : '156px'}}
                                    />
                                </Paper>
                            </Grid>
                            <Grid item xs={this.state.notFoundModel ? 8 : 12} style={!this.state.notFoundModel ? {display : 'none'} : {} }>
                                <DeviceModels device_manufacture_id = {this.state.activeTableRow} />
                            </Grid>
                        </Grid>

                }
            </div>
        );
    }
}

DeviceManufactures.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({device_manufactures}) => {
    return {
        device_manufactures
    }
};

const mapDispatchToProps = {
    fetchDeviceManufactures,
    addNewDeviceManufacture,
    cancelDeviceManufacture,
    deleteDeviceManufacture,
    saveOrUpdateDeviceManufacture,
    fetchDeviceModels
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(DeviceManufactures));