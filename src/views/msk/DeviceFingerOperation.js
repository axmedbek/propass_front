import React, {Fragment} from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {
    Table, TableBody, TableCell, TableHead, TablePagination, TableRow, TableSortLabel,
    Toolbar, Typography, Paper, IconButton, Tooltip, TextField, CircularProgress
} from '@material-ui/core';

import {
    Add as AddIcon,
    Delete as DeleteIcon,
    Edit as EditIcon,
    Save as SaveIcon,
    Cancel as CancelIcon
} from '@material-ui/icons';
import {lighten} from '@material-ui/core/styles/colorManipulator';

import {connect} from 'react-redux';
import {
    fetchDeviceFingerOperations,
    addNewDeviceFingerOperation,
    cancelDeviceFingerOperation,
    saveOrUpdateDeviceFingerOperation,
    deleteDeviceFingerOperation
} from '../../actions/msk/device_finger_operation.action';
import ProPassDialog from "../../components/ProPassDialog";

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const rows = [
    {id: 'name', numeric: false, disablePadding: true, sortDirection: true, label: 'Əməliyyat adı'},
    {id: 'slug', numeric: false, disablePadding: true, sortDirection: true, label: 'Slug'},
    {id: 'operation', numeric: false, disablePadding: true, sortDirection: false, label: 'Əməliyyat'},
];

class EnhancedTableHead extends React.Component {
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const {order, orderBy} = this.props;

        return (
            <TableHead>
                <TableRow>
                    <TableCell padding="checkbox">
                        <span>#</span>
                    </TableCell>
                    {rows.map((row, index) => {
                        return (
                            <TableCell
                                key={row.id}
                                style={rows.length === index + 1 ? {
                                    textAlign: 'end',
                                    marginRight: '24px !important'
                                } : {}}
                                align={row.numeric ? 'right' : 'left'}
                                padding={row.disablePadding ? 'none' : 'default'}
                                sortDirection={orderBy === row.id && row.sortDirection ? order : false}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={false}
                                        direction={order}
                                        onClick={this.createSortHandler(row.id)}
                                    >
                                        {row.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}

EnhancedTableHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};

const toolbarStyles = theme => ({
    root: {
        paddingRight: theme.spacing.unit,
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    spacer: {
        flex: '1 1 100%',
    },
    actions: {
        color: theme.palette.text.secondary,
    },
    title: {
        flex: '0 0 auto',
    },
});

let EnhancedTableToolbar = props => {
    const {numSelected, classes , handleAddDeviceDestination } = props;

    return (
        <Toolbar
            className={classNames(classes.root, {
                [classes.highlight]: numSelected > 0,
            })}
        >
            <div className={classes.title}>
                {numSelected > 0 ? (
                    <Typography color="inherit" variant="subtitle1">
                        {numSelected} selected
                    </Typography>
                ) : (
                    <Typography variant="h6" id="tableTitle">
                        FP Əməliyyat
                    </Typography>
                )}
            </div>
            <div className={classes.spacer}/>
            <div className={classes.actions}>
                <Tooltip title="Yeni qurğu təyinatı">
                    <IconButton aria-label="Add" onClick={handleAddDeviceDestination}>
                        <AddIcon/>
                    </IconButton>
                </Tooltip>
            </div>
        </Toolbar>
    );
};

EnhancedTableToolbar.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({
    root: {
        width: '80%',
        marginTop: theme.spacing.unit * 3,
    },
    table: {
        minWidth: '100%',
    },
    tableWrapper: {
        overflowX: 'auto',
        padding: '15px'
    },
    fab: {
        position: 'absolute',
        bottom: '44px',
        right: '4%',
        color: 'white',
        backgroundColor: '63c79d'
    },
    addBtnCss: {
        color: 'rgb(230, 237, 234)',
        backgroundColor: 'rgb(99, 199, 157)',
        '&:hover': {
            backgroundColor: 'rgb(88, 183, 142)',
            color: 'rgb(230, 237, 234)'
        },
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        height: '45px'
    },
    // cssLabel: {
    //     color : 'green'
    // },

    cssOutlinedInput: {
        '&$cssFocused $notchedOutline': {
            borderColor: '#63c79d !important',
        }
    },

    cssFocused: {},

    notchedOutline: {
        // borderWidth: '1px',
        // borderColor: 'green !important'
    },
    dense: {
        marginTop: 16,
    },
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '50%',
        marginTop: '25%',
    },
});

class DeviceTypes extends React.Component {
    state = {
        order: 'asc',
        orderBy: 'device_finger_operations',
        selected: [],
        page: 0,
        rowsPerPage: 6,
        lastRowId: 0,
        editRowId: 0,
        operation_name : null,
        slug_name : null,
        hasAddRow: false,
        deleteDialogIsOpen: false,
        deletedRowId : 0
    };

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({order, orderBy});
    };


    handleChangePage = (event, page) => {
        this.setState({page});
    };

    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };


    // start my functions
    handleEditDeviceDestination = (e, n) => {
        e.preventDefault();
        if (!this.state.hasAddRow) {
            this.setState({
                editRowId: n.id,
                slug_name : n.slug,
                operation_name : n.name
            });
        }
    };

    handleDeleteDeviceDestination = (e, id) => {
        e.preventDefault();
        this.props.deleteDeviceFingerOperation(id);
        this.setState({
            deleteDialogIsOpen: !this.state.deleteDialogIsOpen
        });
    };

    handleSaveDeviceDestination = (e, id, isAdd) => {
        if (isAdd) {
            // console.log({ id : 0 , name : this.state.device_name});
            this.props.saveOrUpdateDeviceFingerOperation({id: 0, name: this.state.operation_name , slug : this.state.slug_name });
            this.props.cancelDeviceFingerOperation(id);
            this.setState({
                hasAddRow: false
            })
        } else {
            this.props.saveOrUpdateDeviceFingerOperation({id: id, name: this.state.operation_name , slug : this.state.slug_name });
            this.setState({
                editRowId: 0
            });
        }
    };

    handleCancelDeviceDestination = (e, id, isAdd) => {
        e.preventDefault();
        if (isAdd) {
            this.props.cancelDeviceFingerOperation(id);
            this.setState({
                hasAddRow: false
            })
        } else {
            this.setState({
                editRowId: 0
            });
        }
    };

    handleAddDeviceDestination = (e) => {
        e.preventDefault();
        if (!this.state.hasAddRow && this.state.editRowId === 0) {
            this.props.addNewDeviceFingerOperation(this.props.device_finger_operations.total_data.data.length + 1);
            this.setState({
                hasAddRow: true
            });
        }
    };

    componentWillMount() {
        this.props.fetchDeviceFingerOperations();
    };


    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    };

    handelDeleteDialogToggle = (e, id) => {
        e.preventDefault();
        this.setState({
            deleteDialogIsOpen: !this.state.deleteDialogIsOpen,
            deletedRowId : !this.state.deleteDialogIsOpen ? id : 0
        });
    };

    // end my functions

    render() {

        const { classes } = this.props;
        const { order, orderBy, selected, rowsPerPage, page } = this.state;

        return (
            <Fragment>
                {
                    this.props.device_finger_operations.fetching ?
                        <CircularProgress className={classes.progress} size={70}/> :
                        <div style={{
                            padding: 40
                        }}>
                            <Paper className={classes.root}>
                                <EnhancedTableToolbar numSelected={selected.length} handleAddDeviceDestination={(e) => this.handleAddDeviceDestination(e)}/>
                                <div className={classes.tableWrapper}>
                                    <Table className={classes.table} aria-labelledby="tableTitle">
                                        <EnhancedTableHead
                                            numSelected={selected.length}
                                            order={order}
                                            orderBy={orderBy}
                                            onRequestSort={this.handleRequestSort}
                                            rowCount={this.props.device_finger_operations.total_data.data.length}
                                        />
                                        <TableBody>
                                            {stableSort(this.props.device_finger_operations.total_data.data, getSorting(order, orderBy))
                                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                                .map((n, index) => {
                                                    const {editRowId} = this.state;
                                                    return (
                                                        <TableRow
                                                            hover
                                                            tabIndex={-1}
                                                            key={n.id}
                                                        >
                                                            <TableCell component="th" scope="row" padding="none">
                                                                <span style={{marginLeft: '10px'}}>{index + 1}</span>
                                                            </TableCell>
                                                            <TableCell component="th" scope="row" padding="none">
                                                                {
                                                                    editRowId === n.id || n.isAdd ?
                                                                        <TextField
                                                                            id="outlined-bare"
                                                                            className={classes.textField}
                                                                            defaultValue={n.name}
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            name={"operation_name"}
                                                                            onChange={this.handleInputChange}
                                                                            InputLabelProps={{
                                                                                classes: {
                                                                                    // root: classes.cssLabel,
                                                                                    focused: classes.cssFocused,
                                                                                },
                                                                            }}
                                                                            InputProps={{
                                                                                classes: {
                                                                                    root: classes.cssOutlinedInput,
                                                                                    focused: classes.cssFocused,
                                                                                    notchedOutline: classes.notchedOutline,
                                                                                },
                                                                            }}
                                                                        /> :
                                                                            <span
                                                                                style={{paddingLeft: '10px'}}>{n.name}</span>

                                                                }
                                                            </TableCell>

                                                            <TableCell component="th" scope="row" padding="none">
                                                                {
                                                                    editRowId === n.id || n.isAdd ?
                                                                        <TextField
                                                                            id="outlined-bare"
                                                                            className={classes.textField}
                                                                            defaultValue={n.slug}
                                                                            margin="normal"
                                                                            variant="outlined"
                                                                            name={"slug_name"}
                                                                            onChange={this.handleInputChange}
                                                                            InputLabelProps={{
                                                                                classes: {
                                                                                    // root: classes.cssLabel,
                                                                                    focused: classes.cssFocused,
                                                                                },
                                                                            }}
                                                                            InputProps={{
                                                                                classes: {
                                                                                    root: classes.cssOutlinedInput,
                                                                                    focused: classes.cssFocused,
                                                                                    notchedOutline: classes.notchedOutline,
                                                                                },
                                                                            }}
                                                                        /> :
                                                                        <span
                                                                            style={{paddingLeft: '10px'}}>{n.slug}</span>

                                                                }
                                                            </TableCell>

                                                            <TableCell component="th" scope="row" padding={"dense"}
                                                                       style={{textAlign: 'end'}}>
                                                                {
                                                                    editRowId === n.id || n.isAdd ?
                                                                        <div>
                                                                            <Tooltip title="Save">
                                                                                <IconButton aria-label="Save"
                                                                                            onClick={(e) => this.handleSaveDeviceDestination(e, n.id, n.isAdd)}>
                                                                                    <SaveIcon/>
                                                                                </IconButton>
                                                                            </Tooltip>
                                                                            <Tooltip title="Cancel">
                                                                                <IconButton aria-label="Cancel"
                                                                                            onClick={(e) => this.handleCancelDeviceDestination(e, n.id, n.isAdd)}>
                                                                                    <CancelIcon/>
                                                                                </IconButton>
                                                                            </Tooltip>
                                                                        </div>
                                                                        :
                                                                        <div>
                                                                            <Tooltip title="Edit">
                                                                                <IconButton aria-label="Edit"
                                                                                            onClick={(e) => this.handleEditDeviceDestination(e, n)}>
                                                                                    <EditIcon/>
                                                                                </IconButton>
                                                                            </Tooltip>
                                                                            <Tooltip title="Delete">
                                                                                <IconButton aria-label="Delete"
                                                                                            onClick={(e) => this.handelDeleteDialogToggle(e, n.id)}>
                                                                                    <DeleteIcon/>
                                                                                </IconButton>
                                                                            </Tooltip>
                                                                        </div>
                                                                }
                                                            </TableCell>
                                                        </TableRow>
                                                    );
                                                })}
                                        </TableBody>
                                        <ProPassDialog
                                            is_open={this.state.deleteDialogIsOpen}
                                            message_title = "Xəbərdarlıq"
                                            message_content = "Bu qurğu tipini silmək istədiyinizə əminsiniz ?"
                                            handleToggleProcess = {this.handelDeleteDialogToggle}
                                            handleDeleteProcess = {e => this.handleDeleteDeviceDestination(e, this.state.deletedRowId)}
                                        />
                                    </Table>
                                </div>
                                <TablePagination
                                    rowsPerPageOptions={[]}
                                    component="div"
                                    count={this.props.device_finger_operations.total_data.data.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    backIconButtonProps={{
                                        'aria-label': 'Previous Page',
                                    }}
                                    nextIconButtonProps={{
                                        'aria-label': 'Next Page',
                                    }}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                />
                            </Paper>
                        </div>

                }
            </Fragment>
        );
    }
}

DeviceTypes.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ device_finger_operations }) => {
    return {
        device_finger_operations
    }
};

const mapDispatchToProps = {
    fetchDeviceFingerOperations,
    addNewDeviceFingerOperation,
    cancelDeviceFingerOperation,
    saveOrUpdateDeviceFingerOperation,
    deleteDeviceFingerOperation
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(DeviceTypes));