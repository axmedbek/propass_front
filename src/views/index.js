export { default as DeviceTypes } from './msk/DeviceTypes';
export { default as DeviceDestination } from './msk/DeviceDestination';
export { default as DeviceManufactures } from './msk/DeviceManufactures';
export { default as DeviceFingerCodeTypes } from './msk/DeviceFingerCodeTypes';
export { default as DeviceSdkTypes } from './msk/DeviceSdkTypes';
export { default as DashboardPage } from './dashboard/DashboardPage';
export { default as DevicePage } from './device/DevicePage';
export { default as NewDevicePage } from './device/NewDevicePage';
export { default as StaffPage } from './staff/StaffPage';
export { default as SurveillancePage } from './surveillance/SurveillancePage';
export { default as AllDevicesPage } from './device/AllDevicesPage';
export { default as registerStaffPage } from './staff/registerStaffPage';
export { default as PageNotFound } from './PageNotFound';
export { default as AccessType } from './msk/AccessType';
export { default as AccessPage } from './access/AccessPage';
export { default as NewTransitionPage } from './access/NewTransitionPage';
export { default as DevMonitorinq } from './dev/DevMonitorinq';
export { default as FpProcesses } from './dev/FPProcesses';
export { default as DeviceFingerOperation } from './msk/DeviceFingerOperation';
export { default as BioHistoryPage } from './biometric_history/BioHistoryPage';
export { default as BioHistoryDetailedPage } from './biometric_history/BioHistoryDetailedPage';
export { default as LoginPage } from './auth/login/LoginPage';
export { default as SoftUsersPage } from './users/SoftUsersPage';
export { default as UserRolesPage } from './roles/UserRolesPage';
export { default as UserRoleUsersPage } from './roles/UsersPage';






