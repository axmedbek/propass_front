import React from 'react';
import {Button, Modal, ModalHeader, ModalBody, ModalFooter} from 'reactstrap';
import TextField from "@material-ui/core/TextField/TextField";
import { withStyles } from '@material-ui/core/styles';


const styles = theme => ({
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
});

class AddDeviceTypeModal extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            addDeviceTypeModal: false
        };
    }

    handleAddDeviceTypeModal = () => {
        this.setState({
            addDeviceTypeModal : !this.state.addDeviceTypeModal
        })
    };


    render() {
        const { classes } = this.props;
        return (
            <Modal isOpen={this.state.addDeviceTypeModal} toggle={this.toggle} className={this.props.className}>
                <ModalHeader toggle={this.handleAddDeviceTypeModal}>Yeni qurğu təyinatı</ModalHeader>
                <ModalBody>
                    <form className={classes.container} noValidate autoComplete="off">
                        <TextField
                            id="outlined-email-input"
                            label="Email"
                            className={classes.textField}
                            type="email"
                            name="email"
                            autoComplete="email"
                            margin="normal"
                            variant="outlined"
                        />
                    </form>
                </ModalBody>
                <ModalFooter>
                    <Button color="primary" onClick={this.handleAddDeviceTypeModal}>Yadda saxla</Button>{' '}
                    <Button color="secondary" onClick={this.handleAddDeviceTypeModal}>Bağla</Button>
                </ModalFooter>
            </Modal>
        );
    }
}

export default withStyles(styles)(AddDeviceTypeModal);