import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import ProPassInput from "../../../components/ProPassInput";
import {FormGroup, Label} from "reactstrap";
import propassRoutes from "../../../routes/propass";
import Grid from "@material-ui/core/Grid";
import RadioPass from "../../../components/RadioPass";

const RoleSelection = ({value, index, handleChangeRadio, state}) => {
    const icon = value.isMenu ? require(`../../../assets/images/icons/sidebar/${value.icon}.svg`) : '';
    // const icon = require('../../../assets/images/icons/sidebar/access.svg');
    return (
        <Grid container>
            <Grid item md={2}>
                {
                    index === 0
                        ? <Label>Modullar</Label>
                        : ''
                }
            </Grid>
            <Grid item md={10}>
                <Grid container>
                    {
                        value.isMenu
                            ? value.hasSubMenu
                            ? <React.Fragment>
                                <Grid item md={5}
                                      style={{backgroundColor: 'rgb(74, 73, 86)', color: 'white', padding: '15px'}}>
                                    <img style={{width: 20}} src={icon}
                                         alt={`${value.sidebarName.toLowerCase()} icon`}/>
                                    <span style={{marginLeft: '4px', fontSize: 14}}>{value.sidebarName}</span>
                                </Grid>
                                <Grid item md={7}>
                                    <RadioPass
                                        handleRadioChange={handleChangeRadio}
                                        radioName={`radio_${value.route_name}`}
                                        radioState={Object.keys(state).length ? parseInt(state[`radio_${value.route_name}`]) === 1 : true}
                                        radioValue={1}
                                        radioLabel={"Read & Write"}
                                    />
                                    <RadioPass
                                        handleRadioChange={handleChangeRadio}
                                        radioName={`radio_${value.route_name}`}
                                        radioState={parseInt(state[`radio_${value.route_name}`]) === 2}
                                        radioValue={2}
                                        radioLabel={"Only Read"}
                                    />
                                    <RadioPass
                                        handleRadioChange={handleChangeRadio}
                                        radioName={`radio_${value.route_name}`}
                                        radioState={parseInt(state[`radio_${value.route_name}`]) === 3}
                                        radioValue={3}
                                        radioLabel={"None"}
                                    />
                                </Grid>
                                {
                                    value.sub_menus.map((sub_value, sub_index) => {
                                        const subIcon = value.isMenu ? require(`../../../assets/images/icons/sidebar/${sub_value.icon}.svg`) : '';
                                        return (
                                            <Grid container key={sub_index}>
                                                <Grid item md={5} style={{
                                                    backgroundColor: 'rgb(74, 73, 86)',
                                                    color: 'white',
                                                    padding: '15px'
                                                }}>
                                                    <img style={{width: 20, marginLeft: 50}} src={subIcon}
                                                         alt={`${sub_value.sidebarName.toLowerCase()} icon`}/>
                                                    <span style={{
                                                        marginLeft: '4px',
                                                        fontSize: 14
                                                    }}>{sub_value.sidebarName}</span>
                                                </Grid>
                                                <Grid item md={7}>
                                                    <RadioPass
                                                        handleRadioChange={handleChangeRadio}
                                                        radioName={`radio_${sub_value.route_name}`}
                                                        radioState={Object.keys(state).length ? parseInt(state[`radio_${sub_value.route_name}`]) === 1 : true}
                                                        radioValue={1}
                                                        radioLabel={"Read & Write"}
                                                    />
                                                    <RadioPass
                                                        handleRadioChange={handleChangeRadio}
                                                        radioName={`radio_${sub_value.route_name}`}
                                                        radioState={parseInt(state[`radio_${sub_value.route_name}`]) === 2}
                                                        radioValue={2}
                                                        radioLabel={"Only Read"}
                                                    />
                                                    <RadioPass
                                                        handleRadioChange={handleChangeRadio}
                                                        radioName={`radio_${sub_value.route_name}`}
                                                        radioState={parseInt(state[`radio_${sub_value.route_name}`]) === 3}
                                                        radioValue={3}
                                                        radioLabel={"None"}
                                                    />
                                                </Grid>
                                            </Grid>
                                        )
                                    })
                                }
                            </React.Fragment>
                            :
                            <React.Fragment key={index}>
                                <Grid item md={5}
                                      style={{backgroundColor: 'rgb(74, 73, 86)', color: 'white', padding: '15px'}}>
                                    <img style={{width: 20}} src={icon} alt={""}/>
                                    <span style={{marginLeft: '4px', fontSize: 14}}>{value.sidebarName}</span>
                                </Grid>
                                <Grid item md={7}>
                                    <RadioPass
                                        handleRadioChange={handleChangeRadio}
                                        radioName={`radio_${value.route_name}`}
                                        radioState={Object.keys(state).length ? parseInt(state[`radio_${value.route_name}`]) === 1 : true}
                                        radioValue={1}
                                        radioLabel={"Read & Write"}
                                    />
                                    <RadioPass
                                        handleRadioChange={handleChangeRadio}
                                        radioName={`radio_${value.route_name}`}
                                        radioState={parseInt(state[`radio_${value.route_name}`]) === 2}
                                        radioValue={2}
                                        radioLabel={"Only Read"}
                                    />
                                    <RadioPass
                                        handleRadioChange={handleChangeRadio}
                                        radioName={`radio_${value.route_name}`}
                                        radioState={parseInt(state[`radio_${value.route_name}`]) === 3}
                                        radioValue={3}
                                        radioLabel={"None"}
                                    />
                                </Grid>
                            </React.Fragment>
                            : ''
                    }
                </Grid>
            </Grid>
        </Grid>
    );
};

class UserRoleModal extends Component {

    state = {};

    handleChangeRadio = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        });
        console.log(e.target.name, e.target.value)
    };

    render() {
        return (
            <form action="#" method={"post"}>
                <ProPassInput labelText={"Rol adı"}/>
                <FormGroup>
                    {
                        propassRoutes.map((value, index) => (
                            <RoleSelection
                                value={value}
                                index={index}
                                handleChangeRadio={this.handleChangeRadio}
                                state={this.state}
                            />
                        ))
                    }
                </FormGroup>
            </form>
        );
    }
}

UserRoleModal.propTypes = {};

export default UserRoleModal;