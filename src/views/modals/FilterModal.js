import React, {Component} from 'react';
import {Row, Col, FormGroup, Button} from "reactstrap";
import ProSelect from "../../components/ProSelect";
import makeAnimated from "react-select/lib/animated";
import {
    fetchDepartmentFilterBySelect,
    fetchDepartmentFilters,
    removeDepartmentFilter,
    selectedDepartments,
    removeSelectedDepartments
} from "../../actions/device/devices.action";
import {connect} from "react-redux";
import {Table, TableHead, TableRow, TableCell, TableBody, withStyles} from "@material-ui/core";
import TrashIcon from "../../assets/images/icons/trash.svg";
import filterModalStyle from "../../assets/jss/filterModalStyle";
import {findElementById, makeDataForSelect, removeElementById} from "../../helper/standart";


class FilterModal extends Component {
    state = {
        selectedDepartments: []
    };

    componentWillMount() {
        this.props.fetchDepartmentFilters();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.department_filters.total_data.length !== this.props.department_filters.total_data.length) {
            if (nextProps.department_filters.total_data.data !== undefined) {
                if (nextProps.department_filters.total_data.data.length > 0) {
                    let fisrtFilterElement = nextProps.department_filters.total_data.data[0];
                    this.props.fetchDepartmentFilterBySelect(fisrtFilterElement.id, 0, fisrtFilterElement.tip.toLowerCase());
                    nextProps.department_filters.total_data.data.map(value => {
                        this.setState({
                            [value.tip.toLowerCase() + "Value"]: null
                        });
                        return null;
                    });
                }
            }
        }
    }

    handleFilterFirstSelectChange = (val) => {
        if (val && val.value > 0) {
            this.setState({
                ["parentSelectId" + 0]: val.value,
                portfelValue: val
            });
            this.props.department_filters.total_data.data.map((value, index) => {
                if (index > 0) {
                    this.props.fetchDepartmentFilterBySelect(value.id, val.value, value.tip.toLowerCase())
                }
                return null;
            });
        } else {
            this.setState({
                ["parentSelectId" + 0]: 0,
                portfelValue: null
            });
            this.props.department_filters.total_data.data.map((value, index) => {
                if (index > 0) {
                    this.props.removeDepartmentFilter(value.tip.toLowerCase());
                    this.setState({
                        [value.tip.toLowerCase() + "Value"]: null
                    })
                }
                return null;
            });
        }
    };

    handleFilterSelectChange = (val, selectedIndex, tip) => {
        if (val && val.value > 0) {
            this.setState({
                ["parentSelectId" + selectedIndex]: val.value,
                [tip + "Value"]: val
            });
            this.props.department_filters.total_data.data.map((value, index) => {
                if (index > selectedIndex) {
                    this.props.fetchDepartmentFilterBySelect(value.id, val.value, value.tip.toLowerCase())
                }
                return null;
            });
        } else {
            let parentSelectedIndex = 0;
            for (let i = selectedIndex - 1; selectedIndex - 1 >= 0; i--) {
                if (this.state["parentSelectId" + i] && this.state["parentSelectId" + i] !== 0) {
                    parentSelectedIndex = i;
                    break;
                }
            }

            const selectedIndexCount = this.props.department_filters.total_data.data.length;
            for (let i = selectedIndexCount; i > parentSelectedIndex; i--) {
                this.setState({
                    ["parentSelectId" + i]: 0,
                    [tip + "Value"]: null
                })
            }

        }
        this.props.department_filters.total_data.data.map((value, index) => {
            if (index > selectedIndex) {
                this.props.removeDepartmentFilter(value.tip.toLowerCase());
                this.setState({
                    [value.tip.toLowerCase() + "Value"]: null
                })
            }
            return null;
        });
    };


    handleClickBtn = (e) => {
        e.preventDefault();
        let strukturName = "",
            parentId = 0;
        this.props.department_filters.total_data.data.map(value => {
            if (this.state[[value.tip.toLowerCase() + "Value"]]) {
                strukturName = strukturName + (strukturName ? "->" : "") + this.state[[value.tip.toLowerCase() + "Value"]].label;
                parentId = this.state[[value.tip.toLowerCase() + "Value"]].value;
            }
            return null;
        });
        if (findElementById(this.props.department_filters.selected_departments, parentId) === undefined && parentId > 0 && strukturName) {
            this.props.selectedDepartments({value: parentId, label: strukturName});
            this.setState({
                ["parentSelectId" + 0]: 0,
                portfelValue: null
            });
            this.props.department_filters.total_data.data.map((value, index) => {
                if (index > 0) {
                    this.props.removeDepartmentFilter(value.tip.toLowerCase());
                    this.setState({
                        [value.tip.toLowerCase() + "Value"]: null
                    })
                }
                return null;
            });
        }
    };

    handleRemoveSelectedFilter = (e, id) => {
        e.preventDefault();
        this.props.removeSelectedDepartments(id);
    };

    render() {
        const {department_filters, filter_portfels, classes, modalBtnHandleText, modalBtnCloseBtnText, handleModalToggle} = this.props;
        return (
            <React.Fragment>
                <Row>
                    {!department_filters.fetching &&
                    department_filters.total_data.data.map((value, index) => {
                        return (
                            <Col md={"4"} key={index + 1}>
                                <FormGroup>
                                    {
                                        index === 0 ?
                                            <ProSelect
                                                id={"select_components"}
                                                className="menu-outer-top"
                                                classNamePrefix="select"
                                                isDisabled={false}
                                                placeholder={value.struktur_tipi}
                                                isLoading={filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase() + "Fetching"]}
                                                isClearable={true}
                                                isSearchable={true}
                                                components={makeAnimated()}
                                                name={value.tip.toLowerCase()}
                                                value={this.state.portfelValue}
                                                options={makeDataForSelect(filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase()].data)}
                                                onChange={this.handleFilterFirstSelectChange}
                                            /> :
                                            <ProSelect
                                                id={"select_components"}
                                                className="menu-outer-top"
                                                classNamePrefix="select"
                                                isDisabled={false}
                                                placeholder={value.struktur_tipi}
                                                isLoading={filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase() + "Fetching"]}
                                                isClearable={true}
                                                isSearchable={true}
                                                components={makeAnimated()}
                                                name={value.tip.toLowerCase()}
                                                options={
                                                    makeDataForSelect(filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase()].data)
                                                }
                                                onChange={(e) => this.handleFilterSelectChange(e, index, value.tip.toLowerCase())}
                                                value={this.state[value.tip.toLowerCase() + "Value"]}
                                            />
                                    }
                                </FormGroup>
                            </Col>)
                    })}
                </Row>
                <div style={{
                    height: 1,
                    backgroundColor: '#d0c9c9',
                    marginTop: 15
                }}/>
                <Row style={{
                    padding: 14
                }}>


                    <Col md={"12"}>
                        {
                            this.props.department_filters.selected_departments.length > 0 ?

                                <Table className={classes.table}>
                                    <TableHead>
                                        <TableRow>
                                            <TableCell>#</TableCell>
                                            <TableCell align="left">Struktur ardıcıllığı</TableCell>
                                            <TableCell align="right">Sil</TableCell>
                                        </TableRow>
                                    </TableHead>
                                    <TableBody>
                                        {this.props.department_filters.selected_departments.map((row, index) => (
                                            <TableRow key={index}>
                                                <TableCell component="th" scope="row">
                                                    {index + 1}
                                                </TableCell>
                                                <TableCell align="left">{row.label}</TableCell>
                                                <TableCell align="right">
                                                    <img src={TrashIcon} alt="delete icon"
                                                         style={{cursor: 'pointer'}}
                                                         onClick={(e) => this.handleRemoveSelectedFilter(e, row.value)}/>
                                                </TableCell>
                                            </TableRow>
                                        ))}
                                    </TableBody>
                                </Table> : ''
                        }
                    </Col>


                </Row>
                <Row style={{float: 'right', marginRight: 0}}>
                    <Col md={"12"} style={{marginTop: '30px'}}>
                        <Button className={classes.departmentFilterSelectBtn} color="primary"
                                onClick={(e) => this.handleClickBtn(e)}>{modalBtnHandleText}</Button>{' '}
                        <Button color="secondary" onClick={handleModalToggle}>{modalBtnCloseBtnText}</Button>
                    </Col>
                </Row>
            </React.Fragment>
        );
    }
}

FilterModal.propTypes = {};

const mapStateToProps = ({department_filters, filter_portfels}) => {
    return {
        filter_portfels,
        department_filters
    }
};

const mapDispatchToProps = {
    fetchDepartmentFilters,
    fetchDepartmentFilterBySelect,
    removeDepartmentFilter,
    selectedDepartments,
    removeSelectedDepartments
};

export default withStyles(filterModalStyle)(connect(mapStateToProps, mapDispatchToProps)(FilterModal));