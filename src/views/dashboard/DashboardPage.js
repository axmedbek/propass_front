import React, {Component} from 'react';
import {withStyles} from "@material-ui/core";
import {Grid, Card, CardContent} from "@material-ui/core";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import classNames from 'classnames';

const styles = theme => ({
    card: {
        borderRadius: '5px',
        boxShadow: '0 1px 3px rgba(0,0,0,0.12), 0 1px 2px rgba(0,0,0,0.24)',
        transition: 'all 0.3s cubic-bezier(.25,.8,.25,1)',
        '&:hover' : {
            boxShadow: '0 14px 28px rgba(0,0,0,0.25), 0 10px 10px rgba(0,0,0,0.22)',
            cursor : 'pointer'
        }
    },
    bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
    },
    title: {
        fontSize: 14,
    },
    pos: {
        marginBottom: 12,
    },
    iconGridStyle: {
        backgroundColor: '#93e2c1',
        // boxShadow: '3px 3px #a59a98',
        textAlign: 'center',
        padding: '4px'
    },
    iconStyle: {
        fontSize: '35px',
        color: 'rgb(124, 123, 130)'
    },
    dashTextGridStyle: {
        padding: '0px !important',
        marginTop: '12px'
        // borderRight: '3px solid #a59998'
    },
    dashTextStyle: {
        textAlign: 'center',
        color: 'rgb(107, 106, 113)',
        fontSize: '18px',
        [theme.breakpoints.down("sm")]: {
            fontSize: 12
        }
    },
    dashTextCountStyle: {
        padding: '0px !important',
        marginTop: '12px'
    },
    dashCommonGridStyle: {
        boxShadow: '8px 11px 7px 0px #e0e0e0',
    }
});

class DashboardPage extends Component {

    render() {
        const {classes} = this.props;
        return (
            <Grid container spacing={32} style={{
                padding: '45px'
            }}>
                <Grid item md={2} sm={4} xl={2} xs={6}>
                    <Card className={classNames(classes.card, classes.dashCommonGridStyle)}>
                        <CardContent>
                            <Grid container spacing={32}>
                                <Grid item md={12} className={classes.iconGridStyle}>
                                    <FontAwesomeIcon icon={"arrows-alt-h"} className={classes.iconStyle}/>
                                </Grid>
                                <Grid item md={8} className={classes.dashTextGridStyle}>
                                    <h2 className={classes.dashTextStyle}>Keçid</h2>
                                </Grid>
                                <Grid item md={4} className={classes.dashTextCountStyle}>
                                    <h2 className={classes.dashTextStyle}>44</h2>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item md={2} sm={4} xl={2} xs={6}>
                    <Card className={classNames(classes.card, classes.dashCommonGridStyle)}>
                        <CardContent>
                            <Grid container spacing={32}>
                                <Grid item md={12} className={classes.iconGridStyle}>
                                    <FontAwesomeIcon icon={"exclamation-circle"} className={classes.iconStyle}/>
                                </Grid>
                                <Grid item md={8} className={classes.dashTextGridStyle}>
                                    <h2 className={classes.dashTextStyle}>İcazəsiz</h2>
                                </Grid>
                                <Grid item md={4} className={classes.dashTextCountStyle}>
                                    <h2 className={classes.dashTextStyle}>11</h2>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item md={2} sm={4} xl={2} xs={6}>
                    <Card className={classNames(classes.card, classes.dashCommonGridStyle)}>
                        <CardContent>
                            <Grid container spacing={32}>
                                <Grid item md={12} className={classes.iconGridStyle}>
                                    <FontAwesomeIcon icon={"eye"} className={classes.iconStyle}/>
                                </Grid>
                                <Grid item md={8} className={classes.dashTextGridStyle}>
                                    <h2 className={classes.dashTextStyle}>Nəzarət</h2>
                                </Grid>
                                <Grid item md={4} className={classes.dashTextCountStyle}>
                                    <h2 className={classes.dashTextStyle}>3</h2>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item md={2} sm={4} xl={2} xs={6}>
                    <Card className={classNames(classes.card, classes.dashCommonGridStyle)}>
                        <CardContent>
                            <Grid container spacing={32}>
                                <Grid item md={12} className={classes.iconGridStyle}>
                                    <FontAwesomeIcon icon={"business-time"} className={classes.iconStyle}/>
                                </Grid>
                                <Grid item md={8} className={classes.dashTextGridStyle}>
                                    <h2 className={classes.dashTextStyle}>Yubanma</h2>
                                </Grid>
                                <Grid item md={4} className={classes.dashTextCountStyle}>
                                    <h2 className={classes.dashTextStyle}>15</h2>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item md={2} sm={4} xl={2} xs={6}>
                    <Card className={classNames(classes.card, classes.dashCommonGridStyle)}>
                        <CardContent>
                            <Grid container spacing={32}>
                                <Grid item md={12} className={classes.iconGridStyle}>
                                    <FontAwesomeIcon icon={"id-card-alt"} className={classes.iconStyle}/>
                                </Grid>
                                <Grid item md={8} className={classes.dashTextGridStyle}>
                                    <h2 className={classes.dashTextStyle}>Ziyarətçi</h2>
                                </Grid>
                                <Grid item md={4} className={classes.dashTextCountStyle}>
                                    <h2 className={classes.dashTextStyle}>22</h2>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
                <Grid item md={2} sm={4} xl={2} xs={6}>
                    <Card className={classNames(classes.card, classes.dashCommonGridStyle)}>
                        <CardContent>
                            <Grid container spacing={32}>
                                <Grid item md={12} className={classes.iconGridStyle}>
                                    <FontAwesomeIcon icon={"truck-moving"} className={classes.iconStyle}/>
                                </Grid>
                                <Grid item md={8} className={classes.dashTextGridStyle}>
                                    <h2 className={classes.dashTextStyle}>Nəqliyyat</h2>
                                </Grid>
                                <Grid item md={4} className={classes.dashTextCountStyle}>
                                    <h2 className={classes.dashTextStyle}>7</h2>
                                </Grid>
                            </Grid>
                        </CardContent>
                    </Card>
                </Grid>
            </Grid>
        );
    }
}

DashboardPage.propTypes = {};

export default withStyles(styles)(DashboardPage);