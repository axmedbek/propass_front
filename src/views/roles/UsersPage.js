import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Grid from "@material-ui/core/Grid";
import UserSelectBarImg from '../../assets/images/icons/role_user_select_bar.svg';
import UserAddBarImg from '../../assets/images/icons/role_user_add_bar.svg';
import RoleSelectMenuImg from '../../assets/images/icons/tab_select_icon.svg';
import {FormGroup, Label} from "reactstrap";
import makeAnimated from "react-select/lib/animated";
import {makeDataForSelect} from "../../helper/standart";
import {
    fetchDepartmentFilterBySelect,
    fetchDepartmentFilters,
    removeDepartmentFilter, removeSelectedDepartments, selectedDepartments
} from "../../actions/device/devices.action";
import {connect} from "react-redux";
import Select from "react-select";
import {fetchStaffs} from "../../actions/staff/staff.action";
import ProCheckBox from "../../components/ProCheckBox";

class UsersPage extends Component {
    state = {
        selectedDepartments: []
    };

    componentWillMount() {
        this.props.fetchDepartmentFilters();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.department_filters.total_data.length !== this.props.department_filters.total_data.length) {
            if (nextProps.department_filters.total_data.data !== undefined) {
                if (nextProps.department_filters.total_data.data.length > 0) {
                    let fisrtFilterElement = nextProps.department_filters.total_data.data[0];
                    this.props.fetchDepartmentFilterBySelect(fisrtFilterElement.id, 0, fisrtFilterElement.tip.toLowerCase());
                    nextProps.department_filters.total_data.data.map(value => {
                        this.setState({
                            [value.tip.toLowerCase() + "Value"]: null
                        });
                        return null;
                    });
                }
            }
        }
    }

    handleFilterFirstSelectChange = (val) => {
        if (val && val.value > 0) {
            this.setState({
                ["parentSelectId" + 0]: val.value,
                portfelValue: val
            });
            this.props.department_filters.total_data.data.map((value, index) => {
                if (index > 0) {
                    this.props.fetchDepartmentFilterBySelect(value.id, val.value, value.tip.toLowerCase())
                }
                return null;
            });
            this.props.fetchStaffs(1,val.value);
        } else {
            this.setState({
                ["parentSelectId" + 0]: 0,
                portfelValue: null
            });
            this.props.department_filters.total_data.data.map((value, index) => {
                if (index > 0) {
                    this.props.removeDepartmentFilter(value.tip.toLowerCase());
                    this.setState({
                        [value.tip.toLowerCase() + "Value"]: null
                    })
                }
                return null;
            });
        }
    };

    handleFilterSelectChange = (val, selectedIndex, tip) => {
        if (val && val.value > 0) {
            this.props.fetchStaffs(1,val.value);
            this.setState({
                ["parentSelectId" + selectedIndex]: val.value,
                [tip + "Value"]: val
            });
            this.props.department_filters.total_data.data.map((value, index) => {
                if (index > selectedIndex) {
                    this.props.fetchDepartmentFilterBySelect(value.id, val.value, value.tip.toLowerCase())
                }
                return null;
            });
        } else {
            let parentSelectedIndex = 0;
            for (let i = selectedIndex - 1; selectedIndex - 1 >= 0; i--) {
                if (this.state["parentSelectId" + i] && this.state["parentSelectId" + i] !== 0) {
                    parentSelectedIndex = i;
                    this.props.fetchStaffs(1,this.state["parentSelectId"+i]);
                    break;
                }
            }

            const selectedIndexCount = this.props.department_filters.total_data.data.length;
            for (let i = selectedIndexCount; i > parentSelectedIndex; i--) {
                this.setState({
                    ["parentSelectId" + i]: 0,
                    [tip + "Value"]: null
                })
            }

        }
        this.props.department_filters.total_data.data.map((value, index) => {
            if (index > selectedIndex) {
                this.props.removeDepartmentFilter(value.tip.toLowerCase());
                this.setState({
                    [value.tip.toLowerCase() + "Value"]: null
                })
            }
            return null;
        });
    };

    render() {
        const { department_filters,filter_portfels } = this.props;
        return (
            <Grid container spacing={24}>
                <Grid item md={12}>
                    <Grid container spacing={8}>
                        {!department_filters.fetching &&
                          department_filters.total_data.data.map((value, index) => {
                            return (
                                <Grid item md={4} key={index + 1}>
                                    <FormGroup>
                                        <Label style={{ color: '#B4B4B4' }}>{value.struktur_tipi}</Label>
                                        {
                                            index === 0 ?
                                                <Select
                                                    id={"select_components"}
                                                    className="menu-outer-top"
                                                    classNamePrefix="select"
                                                    isDisabled={false}
                                                    placeholder={value.struktur_tipi}
                                                    isLoading={filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase() + "Fetching"]}
                                                    isClearable={true}
                                                    isSearchable={true}
                                                    components={makeAnimated()}
                                                    name={value.tip.toLowerCase()}
                                                    value={this.state.portfelValue}
                                                    options={makeDataForSelect(filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase()].data)}
                                                    onChange={this.handleFilterFirstSelectChange}
                                                    styles={{
                                                        control: (base, state) => ({
                                                            ...base,
                                                            '&:hover': { borderColor: '#63C79D' }, // on hover
                                                            border: '1px solid #dcdbdb', // default border color
                                                            boxShadow: 'none', // no box-shadow
                                                        }),
                                                        option : (base,state) => ({
                                                            ...base,
                                                            '&:hover': {
                                                                backgroundColor: '#96dab8',
                                                                color : 'white'
                                                            },
                                                            backgroundColor : state.isSelected ? '#63C79D' : 'white',
                                                        })
                                                    }}
                                                /> :
                                                <Select
                                                    id={"select_components"}
                                                    className="menu-outer-top"
                                                    classNamePrefix="select"
                                                    isDisabled={false}
                                                    placeholder={value.struktur_tipi}
                                                    isLoading={filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase() + "Fetching"]}
                                                    isClearable={true}
                                                    isSearchable={true}
                                                    components={makeAnimated()}
                                                    name={value.tip.toLowerCase()}
                                                    options={
                                                        makeDataForSelect(filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase()].data)
                                                    }
                                                    onChange={(e) => this.handleFilterSelectChange(e, index, value.tip.toLowerCase())}
                                                    value={this.state[value.tip.toLowerCase() + "Value"]}
                                                    styles={{
                                                        control: (base, state) => ({
                                                            ...base,
                                                            '&:hover': { borderColor: '#63C79D' }, // on hover
                                                            border: '1px solid #dcdbdb', // default border color
                                                            boxShadow: 'none', // no box-shadow
                                                        }),
                                                        option : (base,state) => ({
                                                            ...base,
                                                            '&:hover': {
                                                                backgroundColor: '#96dab8',
                                                                color : 'white'
                                                            },
                                                            backgroundColor : state.isSelected ? '#63C79D' : 'white',
                                                        })
                                                    }}
                                                />
                                        }
                                    </FormGroup>
                                </Grid>)
                        })}
                    </Grid>
                </Grid>
               <Grid item md={12}>
                   <Grid container spacing={24}>
                       <Grid item md={6}>
                           <Grid container>
                               <Grid item md={12} style={{
                                   width : 479,
                                   height : 40,
                                   backgroundColor : '#E8E8E8',
                                   border : '1px solid #DDDDDD',
                                   borderRadius : 4,
                                   paddingLeft : 20,
                                   paddingTop : 9,
                                   color : '#737373',
                                   fontWeight : 'bold'
                               }}>Seçim et</Grid>
                               <Grid item md={12} style={{
                                   widt : 479,
                                   height : 327,
                                   backgroundColor : '#F2F2F2',
                                   borderRadius : 4,
                                   marginTop : 8
                               }}>
                                   {
                                       !Object.keys(this.props.staffs.total_data).length
                                           ? <img src={UserSelectBarImg} alt="user select"/>
                                           :
                                           <Grid container>
                                              {
                                                  this.props.staffs.total_data.data.map(value => {
                                                      return(
                                                         <Grid container>
                                                             <Grid item md={1}>
                                                                 <ProCheckBox/>
                                                             </Grid>
                                                             <Grid item md={10}>
                                                                 { value.userName }
                                                             </Grid>
                                                             <Grid item md={1}>
                                                                 <img src={RoleSelectMenuImg} alt=""/>
                                                             </Grid>
                                                         </Grid>
                                                      );
                                                  })
                                              }
                                           </Grid>
                                   }
                               </Grid>
                           </Grid>
                       </Grid>
                       <Grid item md={6}>
                           <Grid container>
                               <Grid item md={12} style={{
                                   width : 479,
                                   height : 40,
                                   backgroundColor : '#E8E8E8',
                                   border : '1px solid #DDDDDD',
                                   borderRadius : 4,
                                   paddingLeft : 20,
                                   paddingTop : 9,
                                   color : '#737373',
                                   fontWeight : 'bold'
                               }}>Seçilmişlər</Grid>
                               <Grid item md={12} style={{
                                   widt : 479,
                                   height : 327,
                                   backgroundColor : '#F2F2F2',
                                   borderRadius : 4,
                                   marginTop : 8
                               }}><img src={UserAddBarImg} alt="user add"/></Grid>
                           </Grid>
                       </Grid>
                   </Grid>
               </Grid>
            </Grid>
        );
    }
}

UsersPage.propTypes = {};

const mapStateToProps = ({department_filters, filter_portfels, staffs }) => {
    return {
        filter_portfels,
        department_filters,
        staffs
    }
};

const mapDispatchToProps = {
    fetchDepartmentFilters,
    fetchDepartmentFilterBySelect,
    removeDepartmentFilter,
    selectedDepartments,
    removeSelectedDepartments,
    fetchStaffs
};

export default connect(mapStateToProps,mapDispatchToProps)(UsersPage);