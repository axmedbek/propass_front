import React, {Component, Fragment} from 'react';
import {CircularProgress, Tab, Tabs, withStyles} from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import ProPassInput from "../../components/ProPassInput";
import AddBtnImg from '../../assets/images/icons/add_btn.svg';
import EditBtnImg from '../../assets/images/icons/pen.svg';
import CloseBtnImg from '../../assets/images/icons/close.svg';
import CheckBtnImg from '../../assets/images/icons/check_btn.svg';
import {fetchAllRoles, removeRole, saveOrEditRole} from "../../actions/role/role.action";
import {connect} from "react-redux";
import ProPassDialog from "../../components/ProPassDialog";
import classNames from 'classnames';
import rolePageStyle from "../../assets/jss/rolePageStyle";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import SwipeableViews from "react-swipeable-views";
import TabContainer from "../../components/TabContainer";
import Paper from "@material-ui/core/Paper";
import propassRoutes from "../../routes/propass";
import TableBody from "@material-ui/core/TableBody";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/es/TableHead";
import Table from "@material-ui/core/Table";
import ProCheckBox from "../../components/ProCheckBox";
import { UserRoleUsersPage } from '../../views'

class UserRolesPage extends Component {

    state = {
        role_name: '',
        activeRole: null,
        deleteRoleDialog: false,
        editRoleId: 0,
        editRoleName: '',
        tabValue: 0,
        activeRoleTab: 0
    };

    componentWillMount() {
        this.props.fetchAllRoles();
    }

    handleChangeInput = (e) => {
        this.setState({
            role_name: e.target.value
        })
    };
    handleAddNewRole = () => {
        this.setState({
            role_name: ''
        });
        let userId = JSON.parse(localStorage.getItem('user')).userId;
        if (this.state.role_name) {
            this.props.saveOrEditRole({name: this.state.role_name, user_id: userId});
        }
    };

    handleMakeActiveTab = (id) => {
        this.setState({
            activeRole: id,
        })
    };

    handleDialogRole = () => {
        this.setState({
            deleteRoleDialog: !this.state.deleteRoleDialog
        })
    };

    handleDeleteRole = () => {
        let userId = JSON.parse(localStorage.getItem('user')).userId;
        if (this.state.activeRole) {
            this.props.removeRole({id: this.state.activeRole, user_id: userId})
        }
        this.setState({
            deleteRoleDialog: false
        })
    };

    handleEditRole = (id, role_name) => {
        this.setState({
            editRoleId: id,
            editRoleName: role_name
        })
    };
    handleCloseEditRole = () => {
        this.setState({
            editRoleId: 0,
            editRoleName: ''
        })
    };

    handleTabChange = (event, value) => {
        this.setState({tabValue: value});
    };

    handleTabChangeIndex = index => {
        this.setState({tabValue: index});
    };

    handleRoleTabChange = (event, value) => {
        this.setState({activeRoleTab: value});
    };

    handleRoleChangeIndex = index => {
        this.setState({activeRoleTab: index});
    };

    componentWillReceiveProps(nextProps, nextContext) {
        if (Object.keys(nextProps.roles.total_data).length && Object.keys(nextProps.roles.total_data).length > 1) {
            this.setState({
                activeRole: nextProps.roles.total_data.data[0].id
            })
        }
    }

    render() {
        const {classes, theme} = this.props;
        return (
            <Fragment>
                {
                    2 !== 2 ?
                        <CircularProgress className={classes.progress} size={70}/> :
                        <Fragment>
                            <Grid container spacing={32} className={classes.roleContainerGrid} style={{ margin : 0,height : '100%'}}>
                                <Grid item md={3} className={classes.roleGridFirstItem}>
                                    <div className={classes.roleGridFirstItemDiv}>
                                        <h3 className={classes.roleGridFirstItemDivText}>Rollar</h3></div>
                                    <div className={classes.colDivider}/>
                                    <div className={classes.roleGridFirstItemInputDiv}>

                                        <Grid container style={{backgroundColor: '#e9e9e9'}}>
                                            <Grid item md={10}>
                                                <ProPassInput
                                                    className={"addRoleStyleInput"}
                                                    placeHolderText={"İstifadəçi qrupu əlavə edin"}
                                                    style={{height: 40}}
                                                    inputValue={this.state.role_name}
                                                    onChange={this.handleChangeInput}
                                                />
                                            </Grid>
                                            <Grid item md={2}>
                                                <img src={AddBtnImg} alt="add btn"
                                                     className={classes.roleAddBtnStyle}
                                                     onClick={this.handleAddNewRole}/>
                                            </Grid>
                                        </Grid>

                                    </div>
                                    <div className={classes.colDivider}/>
                                    {
                                        this.props.roles.fetching
                                            ? <CircularProgress size={35} className={classes.roleSidebarLoaderStyle}/>
                                            : this.props.roles.total_data.data.map((role, index) => (
                                                <Fragment key={index}>
                                                    <div className={
                                                        classNames(classes.roleListerStyle,
                                                            this.state.activeRole === role.id
                                                                ? classes.activeRoleTabStyle
                                                                : '')}
                                                         onClick={() => this.handleMakeActiveTab(role.id)}>
                                                        <span className={classNames(classes.roleListItemNumberStyle,
                                                            this.state.activeRole === role.id
                                                                ? classes.activeRoleTabTextStyle
                                                                : '')}>
                                                            {index < 9 ? '0' + (index + 1) : index + 1}
                                                        </span>
                                                        <span className={classNames(classes.roleListItemTextStyle,
                                                            this.state.activeRole === role.id
                                                                ? classes.activeRoleTabTextStyle
                                                                : '')}>{role.name}</span>
                                                        {
                                                            this.state.activeRole === role.id ?
                                                                <span style={{
                                                                    float: 'right',
                                                                    marginRight: 24
                                                                }}>
                                                                    <img src={EditBtnImg} alt="add btn" style={{
                                                                        width: 15,
                                                                        cursor: 'pointer',
                                                                        marginRight: '15px'
                                                                    }}
                                                                         onClick={() => this.handleEditRole(role.id, role.name)}/>
                                                                    <FontAwesomeIcon icon={"trash"}
                                                                                     style={{
                                                                                         color: 'white',
                                                                                         cursor: 'pointer',
                                                                                         paddingTop: 2
                                                                                     }}
                                                                                     onClick={this.handleDialogRole}/>
                                                                </span>
                                                                : ''

                                                        }
                                                    </div>
                                                    {
                                                        this.state.editRoleId === role.id ?
                                                            <Grid container style={{
                                                                backgroundColor: '#e9e9e9',
                                                                paddingTop: 14,
                                                                paddingLeft: 8,
                                                                width: 315
                                                            }}>
                                                                <Grid item md={9}>
                                                                    <ProPassInput
                                                                        className={classes.editRoleStyleInput}
                                                                        style={{height: 40}}
                                                                        inputValue={this.state.editRoleName}
                                                                        onChange={this.handleChangeInput}
                                                                    />
                                                                </Grid>
                                                                <Grid item md={3}>
                                                                    <img src={CheckBtnImg} alt="check btn"
                                                                         className={classes.roleEditBtnStyle}
                                                                         onClick={this.handleAddNewRole}/>
                                                                    <img src={CloseBtnImg} alt="close btn"
                                                                         className={classes.roleEditBtnStyle}
                                                                         onClick={this.handleCloseEditRole}
                                                                         style={{
                                                                             marginLeft: 2,
                                                                             backgroundColor: '#4aa981',
                                                                             padding: '10px',
                                                                             borderRadius: '12%',
                                                                         }}/>

                                                                </Grid>
                                                            </Grid>
                                                            : ''
                                                    }
                                                    <div className={classes.colDivider}/>
                                                </Fragment>
                                            ))
                                    }
                                </Grid>
                                <Grid item md={9}>
                                    <Tabs
                                        className={"right_side_div"}
                                        value={this.state.tabValue}
                                        onChange={this.handleTabChange}
                                        indicatorColor="none"
                                        // textColor="primary"
                                        variant="fullWidth"
                                    >
                                        <Tab label="İstifadəçi qrupun adı"
                                             className={this.state.tabValue === 0 ? classes.activeTabStyle : classes.otherTabStyle}
                                             style={{
                                                 height: 60
                                             }}/>
                                        <Tab label="İstifadəçilər"
                                             className={this.state.tabValue === 1 ? classes.activeTabStyle : classes.otherTabStyle}/>
                                    </Tabs>
                                    <Grid item xl={12} md={12} xs={12} sm={12}>
                                        <SwipeableViews
                                            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                                            index={this.state.tabValue}
                                            onChangeIndex={this.handleTabChangeIndex}
                                        >
                                            <TabContainer dir={theme.direction}>
                                                <Grid container spacing={24}>
                                                    <Grid item md={12}>
                                                        <Tabs
                                                            className={"right_side_div"}
                                                            value={this.state.activeRoleTab}
                                                            onChange={this.handleRoleTabChange}
                                                            indicatorColor="none"
                                                            variant="fullWidth"
                                                            style={{
                                                                float: 'left',
                                                                width: 'calc(100% + 45px)',
                                                                marginLeft: '-22px',
                                                                paddingTop: '5px'
                                                            }}
                                                        >
                                                            <Tab label="Menyular"
                                                                 className={
                                                                     this.state.activeRoleTab === 0
                                                                         ? classes.activeRoleMenuTabStyle
                                                                         : classes.otherRoleMenuTabStyle}
                                                                 style={{ marginLeft : 10 }}
                                                            />
                                                            <Tab label="Xüsusi kökləmələr 1"
                                                                 className={
                                                                     this.state.activeRoleTab === 1
                                                                         ? classes.activeRoleMenuTabStyle
                                                                         : classes.otherRoleMenuTabStyle}
                                                                 style={{ marginLeft : 10 }}/>
                                                            <Tab label="Xüsusi kökləmələr 2"
                                                                 className={this.state.activeRoleTab === 2
                                                                     ? classes.activeRoleMenuTabStyle
                                                                     : classes.otherRoleMenuTabStyle}
                                                                 style={{ marginLeft : 10 }}/>
                                                            <Tab label="Xüsusi kökləmələr 3"
                                                                 className={this.state.activeRoleTab === 3
                                                                     ? classes.activeRoleMenuTabStyle
                                                                     : classes.otherRoleMenuTabStyle}
                                                                 style={{ marginLeft : 10 }}/>
                                                        </Tabs>
                                                    </Grid>
                                                    <Grid item md={12} style={{marginLeft: '-45px'}}>
                                                        <SwipeableViews
                                                            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                                                            index={this.state.activeRoleTab}
                                                            onChangeIndex={this.handleRoleChangeIndex}
                                                        >
                                                            <TabContainer dir={theme.direction}>
                                                                <Paper className={classes.root} style={{ width : '75%'}}>
                                                                    <Table className={classes.table}>
                                                                        <TableHead>
                                                                            <TableRow>
                                                                                <TableCell/>
                                                                                <TableCell
                                                                                    align="center">Gizlət</TableCell>
                                                                                <TableCell
                                                                                    align="center">Oxu</TableCell>
                                                                                <TableCell
                                                                                    align="center">Tam</TableCell>
                                                                            </TableRow>
                                                                        </TableHead>
                                                                        <TableBody>
                                                                            {
                                                                                propassRoutes.map((value) => {
                                                                                    if (value.isMenu) {
                                                                                        if (value.hasSubMenu) {
                                                                                            return (
                                                                                                <Fragment>
                                                                                                    <TableRow
                                                                                                        key={value.route_name}>
                                                                                                        <TableCell
                                                                                                            component="th"
                                                                                                            scope="row"
                                                                                                            align={"left"}
                                                                                                            style={
                                                                                                                {"color":"#5a5a5a","fontSize":"15px","fontWeight":"bold","lineHeight":"16px"}
                                                                                                            }
                                                                                                        >
                                                                                                            {value.sidebarName}
                                                                                                        </TableCell>
                                                                                                        <TableCell
                                                                                                            align="center"><ProCheckBox/></TableCell>
                                                                                                        <TableCell
                                                                                                            align="center"><ProCheckBox/></TableCell>
                                                                                                        <TableCell
                                                                                                            align="center"><ProCheckBox/></TableCell>
                                                                                                    </TableRow>
                                                                                                    {
                                                                                                        value.sub_menus.map(sub_prop => {
                                                                                                            const icon = require(`../../assets/images/icons/sidebar/gray_ellipse.svg`);
                                                                                                            return (
                                                                                                                <TableRow
                                                                                                                    key={sub_prop.route_name}>
                                                                                                                    <TableCell
                                                                                                                        component="th"
                                                                                                                        scope="row"
                                                                                                                        align={"left"}
                                                                                                                        style={{ paddingLeft : '45px'}}
                                                                                                                    >
                                                                                                                        <img
                                                                                                                            src={icon}
                                                                                                                            alt="ellipse"/>
                                                                                                                            <span
                                                                                                                            style={
                                                                                                                                {"color":"#5a5a5a","fontSize":"15px","lineHeight":"16px","marginLeft":"10px"}
                                                                                                                            }>{sub_prop.sidebarName}</span>
                                                                                                                    </TableCell>
                                                                                                                    <TableCell
                                                                                                                        align="center"><ProCheckBox/></TableCell>
                                                                                                                    <TableCell
                                                                                                                        align="center"><ProCheckBox/></TableCell>
                                                                                                                    <TableCell
                                                                                                                        align="center"><ProCheckBox/></TableCell>
                                                                                                                </TableRow>
                                                                                                            );
                                                                                                        })
                                                                                                    }
                                                                                                </Fragment>
                                                                                            )
                                                                                        } else {
                                                                                            return (
                                                                                                <TableRow
                                                                                                    key={value.route_name}>
                                                                                                    <TableCell
                                                                                                        component="th"
                                                                                                        scope="row"
                                                                                                        align={"left"}
                                                                                                        style={
                                                                                                            {"color":"#5a5a5a","fontSize":"15px","fontWeight":"bold","lineHeight":"16px"}
                                                                                                        }
                                                                                                    >
                                                                                                        {value.sidebarName}
                                                                                                    </TableCell>
                                                                                                    <TableCell
                                                                                                        align="center"><ProCheckBox/></TableCell>
                                                                                                    <TableCell
                                                                                                        align="center"><ProCheckBox/></TableCell>
                                                                                                    <TableCell
                                                                                                        align="center"><ProCheckBox/></TableCell>
                                                                                                </TableRow>
                                                                                            )
                                                                                        }
                                                                                    }
                                                                                })
                                                                            }
                                                                        </TableBody>
                                                                    </Table>
                                                                </Paper>
                                                            </TabContainer>
                                                            <TabContainer dir={theme.direction}>
                                                                Xüsusi kökləmələr 1
                                                            </TabContainer>
                                                            <TabContainer dir={theme.direction}>
                                                                Xüsusi kökləmələr 2
                                                            </TabContainer>
                                                            <TabContainer dir={theme.direction}>
                                                                Xüsusi kökləmələr 3
                                                            </TabContainer>
                                                        </SwipeableViews>
                                                    </Grid>
                                                </Grid>
                                            </TabContainer>
                                            <TabContainer dir={theme.direction}>
                                                <UserRoleUsersPage/>
                                            </TabContainer>
                                        </SwipeableViews>
                                    </Grid>
                                    <ProPassDialog
                                        is_open={this.state.deleteRoleDialog}
                                        message_title="Xəbərdarlıq"
                                        message_content="Bu rolu silmək istədiyinizə əminsiniz ?"
                                        handleToggleProcess={this.handleDialogRole}
                                        handleDeleteProcess={this.handleDeleteRole}
                                    />
                                </Grid>
                            </Grid>
                        </Fragment>
                }
            </Fragment>
        );
    }
}

UserRolesPage.propTypes = {};

const mapStateToProps = ({roles}) => {
    return {roles}
};

const mapDispatchToProps = {
    fetchAllRoles,
    saveOrEditRole,
    removeRole
};

export default withStyles(rolePageStyle, {withTheme: true})(connect(mapStateToProps, mapDispatchToProps)(UserRolesPage));