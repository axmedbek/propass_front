import React, {Component} from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import brokenLambImg from '../assets/images/broken_lamb2.jpg';
// import PropTypes from 'prop-types';

class PageNotFound extends Component {
    render() {
        return (
            <Grid container spacing={32} style={{
                height: 'calc(100% + 16px)',
                backgroundColor: '#FFFFFF',
                paddingLeft: '25%',
                paddingTop: '15%'
            }}>
                <Grid item md={4}>
                    <img src={brokenLambImg} alt="broken lamb img" style={{
                        width : '400px',
                    }}/>
                </Grid>
                <Grid item md={8}>
                   <h2 style={{
                       marginTop: 115
                   }}> 404 NOT FOUND</h2>
                </Grid>
            </Grid>
        );
    }
}

PageNotFound.propTypes = {};

export default PageNotFound;