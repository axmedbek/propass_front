import React, {Component} from 'react';
import {Redirect} from "react-router-dom";
import Paper from "@material-ui/core/Paper/Paper";
import Typography from "@material-ui/core/Typography/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid/Grid";
import ProSelect from "../../components/ProSelect";
import makeAnimated from "react-select/lib/animated";
import {
    Button,
    ButtonGroup, Col,
    FormGroup,
    Input,
    Label,
    Modal,
    ModalBody,
    ModalFooter,
    ModalHeader,
    Row
} from "reactstrap";

import {reduxForm} from "redux-form";
import {fetchAccessTypes} from '../../actions/msk/device_access_type.action';
import {fetchDeviceManufactures} from '../../actions/msk/device_manufactures.action';
import {fetchDeviceModels} from '../../actions/msk/device_models.action';
import {fetchDeviceDestinations} from '../../actions/msk/device_destination.action';
import {fetchDeviceTypes} from '../../actions/msk/device_type.action';
import {
    fetchDepartmentFilters,
    fetchDepartmentFilterBySelect,
    removeDepartmentFilter,
    getDevicesByDestinationId
} from '../../actions/device/devices.action';
import { addNewTransition } from '../../actions/transition/transition.action';
import '../../assets/css/device_add.css';

import {connect} from "react-redux";
import {USER_ID} from "../../config/env";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Table from "@material-ui/core/Table/Table";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";

import TrashIcon from '../../assets/images/icons/trash.svg';
import {findElementById, makeDataForSelect, makeDevicesForSelect, removeElementById} from "../../helper/standart";

const styles = theme => ({
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        height: '45px'
    },
    cssOutlinedInput: {
        '&$cssFocused $notchedOutline': {
            borderColor: '#63c79d !important',
        }
    },

    cssFocused: {
        color: 'rgba(0, 0, 0, 0.54) !important'
    },

    notchedOutline: {},

    departmentFilterSelectBtn: {
        backgroundColor: '#63c79d',
        borderColor: '#63c79d',
        '&:hover': {
            backgroundColor: '#63c79d',
            borderColor: '#63c79d',
        },
    },
    errorLable: {
        color: 'red'
    },
    errorInput: {
        border: '1px solid red',
        '::-webkit-input-placeholder': {
            color: 'red'
        },
        ':focused': {
            boxShadow: '0 0 red',
        }
    }
});

class NewTransitionPage extends Component {

    state = {
        modalSelectLoader: false,
        deviceModelValue: null,
        deviceSdkTypeName: null,
        deviceFingerTypeName: null,
        deviceModelLoader: false,
        departmentFilterModal: false,
        departmentFilterSelectedId: 0,
        redirect: false,
        //    form data
        transition_name: null,
        device_name_error: false,
        selectedDepartments: [
            "VO/Araz Supermarket"
        ],
        location: null,
        device_google_map: null,
        selected_destination : null,
        selected_device : null,
        selected_devices : []
    };


    componentWillMount() {
        this.props.fetchDeviceDestinations();
        this.props.fetchDeviceManufactures();
        this.props.fetchDeviceTypes();
        this.props.fetchDepartmentFilters();
        this.props.fetchAccessTypes();
        // this.props.getDevicesByDestinationId(3);
        // this.props.fetchDepartmentFilterBySelect(1097,0);
        // console.log(this.props.department_filters.total_data.data);
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.device_models.total_data.data !== undefined) {
            if (nextProps.device_models.total_data.data.length > 0) {
                this.setState({
                    modalSelectLoader: true
                })
            } else {
                this.setState({
                    modalSelectLoader: false
                })
            }
        }

        if (nextProps.department_filters.total_data.length !== this.props.department_filters.total_data.length) {
            if (nextProps.department_filters.total_data.data !== undefined) {
                if (nextProps.department_filters.total_data.data.length > 0) {
                    let fisrtFilterElement = nextProps.department_filters.total_data.data[0];
                    this.props.fetchDepartmentFilterBySelect(fisrtFilterElement.id, 0, fisrtFilterElement.tip.toLowerCase());
                    nextProps.department_filters.total_data.data.map(value => {
                        this.setState({
                            [value.tip.toLowerCase() + "Value"]: null
                        });
                        return null;
                    });
                }
            }
        }
    }


    handleManufactureSelectChange = (e) => {
        this.setState({
            modalSelectLoader: false,
            deviceModelValue: null,
            deviceModelLoader: false
        });
        if (e != null) {
            this.props.fetchDeviceModels(e.value);
        }
    };

    handleModelSelectChange = (value) => {
        this.setState({
            deviceModelValue: value,
            deviceModelLoader: !!value
        });

        if (value) {
            let data = findElementById(this.props.device_models.total_data.data, value.id);
            this.setState({
                deviceSdkTypeName: data.sdk_types.label,
                deviceFingerTypeName: data.finger_codes_types.label
            });
        }
    };

    handleFormSubmit = (e) => {
        e.preventDefault();
        const data = new FormData(this.form);
        this.props.addNewTransition(data);
        this.setState({
            redirect: true
        });
        // // if (this.state.device_name.length < 1) {
        //     this.setState({
        //         device_name_error: true
        //     })
        // } else {
        //     this.props.addNewTransition(data);
        // }
    };

    departmentFilterToggle = () => {
        this.setState({
            departmentFilterModal: !this.state.departmentFilterModal
        });
    };

    handleRedirectDevicePage = () => {
        this.setState({
            redirect: true
        });
    };

    handleFilterFirstSelectChange = (val) => {
        if (val && val.value > 0) {
            this.setState({
                ["parentSelectId" + 0]: val.value,
            });
            this.props.department_filters.total_data.data.map((value, index) => {
                if (index > 0) {
                    this.props.fetchDepartmentFilterBySelect(value.id, val.value, value.tip.toLowerCase())
                }
                return null;
            });
        } else {
            this.setState({
                ["parentSelectId" + 0]: 0
            });
            this.props.department_filters.total_data.data.map((value, index) => {
                if (index > 0) {
                    this.props.removeDepartmentFilter(value.tip.toLowerCase());
                    this.setState({
                        [value.tip.toLowerCase() + "Value"]: null
                    })
                }
                return null;
            });
        }
    };

    handleDestinationSelectChange = (val) => {
        this.setState({
            selected_device : null
        });
        if (val){
            this.setState({
                selected_destination : val
            });
            this.props.getDevicesByDestinationId(val.value);
        }
        else{
            this.setState({
                selected_destination : null
            });
            this.props.getDevicesByDestinationId(0);
        }
    };

    handleDeviceSelectChange = (val) => {
        if (val){
            this.setState({
                selected_device : val
            });
        }
        else{
            this.setState({
                selected_device : null
            });
        }
    };

    handleDeviceAddClick = (e) => {
        e.preventDefault();
        if (this.state.selected_device){
            if (findElementById(this.state.selected_devices,this.state.selected_device.value) === undefined) {
                this.setState({
                    selected_devices : [
                        ...this.state.selected_devices,
                        this.state.selected_device
                    ],
                    selected_device : null,
                    selected_destination : null
                })
            }
        }

        // console.log(findElementById(this.state.selected_devices,this.state.selected_device.value));
    };

    handleFilterSelectChange = (val, selectedIndex, tip) => {
        if (val && val.value > 0) {
            this.setState({
                ["parentSelectId" + selectedIndex]: val.value,
                [tip + "Value"]: val
            });
            this.props.department_filters.total_data.data.map((value, index) => {
                if (index > selectedIndex) {
                    this.props.fetchDepartmentFilterBySelect(value.id, val.value, value.tip.toLowerCase())
                }
                return null;
            });
        } else {
            let parentSelectedIndex = 0;
            for (let i = selectedIndex - 1; selectedIndex - 1 >= 0; i--) {
                if (this.state["parentSelectId" + i] && this.state["parentSelectId" + i] !== 0) {
                    parentSelectedIndex = i;
                    break;
                }
            }

            const selectedIndexCount = this.props.department_filters.total_data.data.length;
            for (let i = selectedIndexCount; i > parentSelectedIndex; i--) {
                this.setState({
                    ["parentSelectId" + i]: 0,
                    [tip + "Value"]: null
                })
            }

        }
        this.props.department_filters.total_data.data.map((value, index) => {
            if (index > selectedIndex) {
                this.props.removeDepartmentFilter(value.tip.toLowerCase());
                this.setState({
                    [value.tip.toLowerCase() + "Value"]: null
                })
            }
            return null;
        });
    };

    handleRemoveSelectedDevice = (e,id) => {
        e.preventDefault();
        this.setState({
            selected_devices : removeElementById(this.state.selected_devices,id)
        })
    };

    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    };

    render() {
        const {classes} = this.props;
        const {redirect} = this.state;
        if (redirect) {
            return <Redirect to='/transition'/>;
        }
        return (
            <Grid container spacing={24} style={{
                padding: '45px'
            }}>
                <Grid item md={12}>
                    <Typography variant="h5" component="h3" style={{fontWeight: 'bold'}}>
                        Yeni keçid yarat
                    </Typography>
                </Grid>
                <Grid item md={12}>
                    <Paper className={classes.root}>
                        <form className={classes.container} noValidate autoComplete="off" ref={el => (this.form = el)}>
                            <input type="hidden" name="id" value="0"/>
                            <input type="hidden" name="user_id" value={USER_ID}/>
                            <Grid container spacing={24} style={{marginTop: '20px'}}>
                                <Grid item md={4}>
                                    <FormGroup>
                                        <Label className={this.state.device_name_error ? classes.errorLable : ''}>Keçidin
                                            adı</Label>
                                        <Input
                                            type="text"
                                            name="name"
                                            placeholder="Keçidin adı"
                                            value={this.state.transition_name}
                                            onChange={this.handleInputChange}
                                            className={this.state.device_name_error ? classes.errorInput : ''}
                                        />

                                    </FormGroup>
                                </Grid>
                                <Grid item md={4}>
                                    <FormGroup>
                                        <Label>Keçid tipi</Label>
                                        <ProSelect
                                            id={"select_components"}
                                            className="menu-outer-top"
                                            classNamePrefix="select"
                                            isDisabled={false}
                                            placeholder={"Keçid tipi"}
                                            isLoading={this.props.access_types.fetching}
                                            isClearable={true}
                                            isSearchable={true}
                                            components={makeAnimated()}
                                            name="access_type"
                                            options={makeDataForSelect(this.props.access_types.total_data.data)}
                                            onChange={this.handleManufactureSelectChange}
                                        />
                                    </FormGroup>
                                </Grid>
                                <Grid item md={4}>
                                    <FormGroup>
                                        <Label>Qurğunun təyinatı</Label>
                                        <ProSelect
                                            id={"select_components"}
                                            className="menu-outer-top"
                                            classNamePrefix="select"
                                            isDisabled={false}
                                            placeholder={"Qurğunun təyinatı"}
                                            isLoading={this.props.device_types.fetching}
                                            isClearable={true}
                                            isSearchable={true}
                                            components={makeAnimated()}
                                            name="device_type"
                                            options={makeDataForSelect(this.props.device_types.total_data.data)}
                                        />
                                    </FormGroup>
                                </Grid>
                            </Grid>
                            <Grid container spacing={24} style={{marginTop: '20px'}}>
                                <Grid item md={4}>
                                    <FormGroup>
                                        <Label>Yerləşdiyi məkan</Label><br/>
                                        <Input
                                            type="text"
                                            name="location"
                                            placeholder="Yerləşdiyi məkan"
                                            value={this.state.location}
                                            onChange={this.handleInputChange}
                                        />
                                    </FormGroup>
                                </Grid>
                                <Grid item md={8}>
                                    <FormGroup>
                                        <Label>Google Map</Label>
                                        <Input type="text"
                                               name="google_map"
                                               placeholder="Google Map"/>
                                    </FormGroup>
                                </Grid>

                                <Grid item md={12}>
                                    <FormGroup>
                                        <Label>Bölmə</Label>
                                        <Button type={"button"} style={{marginLeft: '10px'}}
                                                onClick={this.departmentFilterToggle}>
                                            <FontAwesomeIcon icon={"filter"}/> <span
                                            style={{marginLeft: '8px'}}>Seç</span>
                                        </Button>
                                        <Modal isOpen={this.state.departmentFilterModal}
                                               toggle={this.departmentFilterToggle}
                                               className={"department-filter-modal"}
                                               style={{marginTop: '10%'}} size={"lg"}>
                                            <ModalHeader toggle={this.departmentFilterToggle}>Bölmə filter
                                                et</ModalHeader>
                                            <ModalBody>
                                                <Row>
                                                    {!this.props.department_filters.fetching &&
                                                    this.props.department_filters.total_data.data.map((value, index) => {
                                                        return (
                                                            <Col md={"4"} key={index + 1}>
                                                                <FormGroup>
                                                                    {
                                                                        index === 0 ?
                                                                            <ProSelect
                                                                                id={"select_components"}
                                                                                className="menu-outer-top"
                                                                                classNamePrefix="select"
                                                                                isDisabled={false}
                                                                                placeholder={value.struktur_tipi}
                                                                                isLoading={this.props.filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase() + "Fetching"]}
                                                                                isClearable={true}
                                                                                isSearchable={true}
                                                                                components={makeAnimated()}
                                                                                name={value.tip.toLowerCase()}
                                                                                options={makeDataForSelect(this.props.filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase()].data)}
                                                                                onChange={this.handleFilterFirstSelectChange}
                                                                            /> :
                                                                            <ProSelect
                                                                                id={"select_components"}
                                                                                className="menu-outer-top"
                                                                                classNamePrefix="select"
                                                                                isDisabled={false}
                                                                                placeholder={value.struktur_tipi}
                                                                                isLoading={this.props.filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase() + "Fetching"]}
                                                                                isClearable={true}
                                                                                isSearchable={true}
                                                                                components={makeAnimated()}
                                                                                name={value.tip.toLowerCase()}
                                                                                options={
                                                                                    makeDataForSelect(this.props.filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase()].data)
                                                                                }
                                                                                onChange={(e) => this.handleFilterSelectChange(e, index, value.tip.toLowerCase())}
                                                                                value={this.state[value.tip.toLowerCase() + "Value"]}
                                                                            />
                                                                    }
                                                                </FormGroup>
                                                            </Col>)
                                                    })}
                                                </Row>
                                                <div style={{
                                                    height: 1,
                                                    backgroundColor: '#d0c9c9',
                                                    marginTop: 15
                                                }}/>
                                                <Row style={{
                                                    padding: 14
                                                }}>
                                                    {
                                                        this.state.selectedDepartments.map((value, index) => {
                                                            return (
                                                                <Col md={"12"} key={index}>
                                                                    {
                                                                        (index + 1) + ". " + value
                                                                    }
                                                                </Col>
                                                            )
                                                        })
                                                    }
                                                </Row>
                                            </ModalBody>
                                            <ModalFooter>
                                                <Button className={classes.departmentFilterSelectBtn} color="primary"
                                                        onClick={this.departmentFilterToggle}>Seç</Button>{' '}
                                                <Button color="secondary"
                                                        onClick={this.departmentFilterToggle}>Cancel</Button>
                                            </ModalFooter>
                                        </Modal>
                                    </FormGroup>
                                </Grid>
                            </Grid>
                            <Grid container spacing={24} style={{marginTop: '20px'}}>
                                <Grid item md={4}>
                                    <Label>Əlaqəli qurğular</Label>
                                    <ProSelect
                                        id={"select_components"}
                                        className="menu-outer-top"
                                        classNamePrefix="select"
                                        isDisabled={false}
                                        placeholder={"İstiqamət"}
                                        isLoading={this.props.device_destinations.fetching}
                                        isClearable={true}
                                        isSearchable={true}
                                        components={makeAnimated()}
                                        name="device_destination_name"
                                        value={this.state.selected_destination}
                                        options=
                                            {
                                                this.props.device_destinations.total_data ?
                                                    makeDataForSelect(this.props.device_destinations.total_data.data) :
                                                    []
                                            }
                                        onChange={(e) => this.handleDestinationSelectChange(e)}
                                    />
                                </Grid>
                                <Grid item md={4}>
                                    <Label style={{ marginTop : 17}}/>
                                    <ProSelect
                                        id={"select_components"}
                                        className="menu-outer-top"
                                        classNamePrefix="select"
                                        isDisabled={false}
                                        placeholder={"Qurğular"}
                                        isLoading={this.props.devices_with_destinations.fetching}
                                        isClearable={true}
                                        isSearchable={true}
                                        components={makeAnimated()}
                                        name="devices"
                                        value={this.state.selected_device}
                                        options=
                                            {
                                                this.props.devices_with_destinations.total_data ?
                                                    makeDevicesForSelect(this.props.devices_with_destinations.total_data.data) :
                                                    []
                                            }
                                        onChange={(e) => this.handleDeviceSelectChange(e)}
                                    />
                                </Grid>
                                <Grid item md={4}>
                                    <Label style={{ marginTop : 45}}/>
                                    <Button contained onClick={(e) => this.handleDeviceAddClick(e)}><FontAwesomeIcon icon={"plus"}/></Button>
                                </Grid>
                                <Grid item md={12}>
                                    {
                                        this.state.selected_devices.length > 0 ?

                                                <Table className={classes.table}>
                                                    <TableHead>
                                                        <TableRow>
                                                            <TableCell>#</TableCell>
                                                            <TableCell align="right">İstiqamət</TableCell>
                                                            <TableCell align="right">Qurğu</TableCell>
                                                            <TableCell align="right">Sil</TableCell>
                                                        </TableRow>
                                                    </TableHead>
                                                    <TableBody>
                                                        {this.state.selected_devices.map((row,index) => (
                                                            <TableRow key={index}>
                                                                <TableCell component="th" scope="row">
                                                                    { index + 1}
                                                                </TableCell>
                                                                <TableCell align="right">{row.destination}</TableCell>
                                                                <TableCell align="right">{row.label}</TableCell>
                                                                <TableCell align="right">
                                                                    <img src={TrashIcon} alt="delete icon" style={{ cursor : 'pointer'}}
                                                                    onClick={(e) => this.handleRemoveSelectedDevice(e,row.value)}/>
                                                                </TableCell>
                                                            </TableRow>
                                                        ))}
                                                    </TableBody>
                                                </Table> : ''

                                    }
                                </Grid>
                            </Grid>
                            <Grid container spacing={24} style={{marginTop: '20px'}}>
                                <Grid item md={12}>
                                    <FormGroup>
                                        <Label for="device_note_id">Qeyd</Label>
                                        <Input type="textarea" name="device_note" id="device_note_id"
                                               style={{marginTop: '0px', marginBottom: '0px', height: '97px'}}/>
                                    </FormGroup>
                                </Grid>
                            </Grid>
                        </form>
                    </Paper>
                    <Grid container spacing={24}>
                        <Grid item md={12}>
                            <ButtonGroup style={{marginTop: '20px'}}>
                                <Button onClick={this.handleFormSubmit} style={{
                                    borderColor: '#63c79d',
                                    backgroundColor: '#63c79d',
                                    marginRight: '4px',
                                    borderRadius: '0',
                                }}>Əlavə et</Button>
                                <Button onClick={this.handleRedirectDevicePage} style={{
                                    borderColor: '#a5a4aa',
                                    backgroundColor: 'white',
                                    color: '#75747d',
                                    borderRadius: '0',
                                }}>İmtina et</Button>
                            </ButtonGroup>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>
        );
    }
}

NewTransitionPage.propTypes = {};

const mapStateToProps =
    ({
         device_manufactures,
         device_models,
         device_types,
         device_destinations,
         department_filters,
         filter_portfels,

         access_types,
         devices_with_destinations,
         add_transition

     }) => {
        return {
            device_manufactures,
            device_models,
            device_destinations,
            device_types,
            department_filters,
            filter_portfels,

            access_types,
            devices_with_destinations,
            add_transition
        }
    };

const mapDispatchToProps = {
    fetchDeviceManufactures,
    fetchDeviceModels,
    fetchDeviceDestinations,
    fetchDeviceTypes,
    fetchDepartmentFilters,
    fetchDepartmentFilterBySelect,
    removeDepartmentFilter,

    fetchAccessTypes,
    getDevicesByDestinationId,
    addNewTransition
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(reduxForm({
    form: 'device_add'
})(NewTransitionPage)));