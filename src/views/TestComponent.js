import React, {Component} from 'react';
import PropTypes from 'prop-types';
import PageCreatorHoc from "../components/PageCreatorHOC";
import { fetchDevices } from "../actions/device/devices.action";
import { connect } from "react-redux";

class TestComponent extends Component {

    componentWillMount() {
        this.props.fetchDevices();
    }

    render() {
        return (
            <div>
                Hello
            </div>
        );
    }
}

TestComponent.propTypes = {};


const mapStateToProps = ({ devices }) => {
    return {
        devices
    }
};

const mapDispatchtoProps = {
    fetchDevices
};

export default connect(mapStateToProps,mapDispatchtoProps)(PageCreatorHoc(TestComponent,"Test et","devices"));