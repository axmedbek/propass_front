import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {Tabs, Typography, Tab, Button} from "@material-ui/core";
import SwipeableViews from 'react-swipeable-views';
import {withStyles} from '@material-ui/core/styles';
import {NavLink} from "react-router-dom";
import Table from "@material-ui/core/Table/Table";
import TableBody from "@material-ui/core/TableBody/TableBody";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import Grid from "@material-ui/core/Grid/Grid";

function TabContainer({children, dir}) {
    return (
        <Typography component="div" dir={dir} style={{padding: 8 * 3}}>
            {children}
        </Typography>
    );
}

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired,
};


const styles = theme => ({
    root: {
        backgroundColor: theme.palette.background.paper,
        width: 500,
    },
    activeDeviceTabStyle: {
        backgroundColor: '#66be98',
        color: 'white !important',
        borderRadius: '30px'
    },
    otherDeviceTabStyle: {
        backgroundColor: '#d9eee5',
        color: '#3b7c60',
        borderRadius: '30px',
        border: '1px solid #66be98',
        "fontFamily":"Fira Sans",
        "lineHeight":"16px",
        "fontSize":"14px",
        // "color":"#FFFFFF"
    },
    margin: {
        margin: theme.spacing.unit,
        float: 'right',
        color: '#65be97',
        fontWeight: 800,
        textDecoration: 'underline'
    },
    table: {
        // minWidth: 700,
    },
});

class DeviceLogInfoPage extends Component {

    state = {
        value: 0,
    };

    handleChange = (event, value) => {
        this.setState({value});
    };

    handleChangeIndex = index => {
        this.setState({value: index});
    };

    render() {
        const {classes, theme, device} = this.props;
        return (
            <Grid container spacing={32}>
                <Grid item md={2} style={{
                    "marginTop":"12px",
                    "marginLeft":"25px",
                    "marginRight":"40px",
                    "fontFamily":"'Fira Sans'",
                    "lineHeight":"24px",
                    "fontSize":"18px",
                    "color":"#505050",
                    "fontWeight":"900",
                    "textAlign" : 'center'}}>
                    {device.device_name}
                </Grid>
                <Grid item md={8}>
                    <Tabs
                        value={this.state.value}
                        onChange={this.handleChange}
                        indicatorColor="none"
                        // textColor="primary"
                        variant="fullWidth"
                    >
                        <Tab label="Əməkdaşlar"
                             className={this.state.value === 0 ? classes.activeDeviceTabStyle : classes.otherDeviceTabStyle}/>
                        <Tab label="Loglar"
                             className={this.state.value === 1 ? classes.activeDeviceTabStyle : classes.otherDeviceTabStyle}/>
                        <Tab label="Keçidlər"
                             className={this.state.value === 2 ? classes.activeDeviceTabStyle : classes.otherDeviceTabStyle}/>
                    </Tabs>
                </Grid>
                <Grid item md={1}>
                    <NavLink to={"/devices"} style={{textDecoration: 'none'}}>
                        <Button size="small" className={classes.margin}
                                style={{textTransform: 'capitalize', fontSize: '15px'}}>
                            Qurğular
                        </Button>
                    </NavLink>
                </Grid>
                <Grid item md={12}>
                            <SwipeableViews
                               axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                               index={this.state.value}
                               onChangeIndex={this.handleChangeIndex}
                           >
                               <TabContainer dir={theme.direction}>
                                   <Table className={classes.table}>
                                       <TableBody>
                                           <TableRow>
                                               <TableCell colSpan={5} style={{textAlign: 'center'}}>Hazırda
                                                   göstəriləcək qurğu yoxdur</TableCell>
                                           </TableRow>
                                       </TableBody>
                                   </Table>
                               </TabContainer>
                               <TabContainer dir={theme.direction}>
                                   <Table className={classes.table}>
                                       <TableBody>
                                           <TableRow>
                                               <TableCell colSpan={5} style={{textAlign: 'center'}}>Hazırda
                                                   göstəriləcək məlumat yoxdur</TableCell>
                                           </TableRow>
                                       </TableBody>
                                   </Table>
                               </TabContainer>
                               <TabContainer dir={theme.direction}>
                                   <Table className={classes.table}>
                                       <TableBody>
                                           <TableRow>
                                               <TableCell colSpan={5} style={{textAlign: 'center'}}>Hazırda
                                                   göstəriləcək qurğu yoxdur</TableCell>
                                           </TableRow>
                                       </TableBody>
                                   </Table>
                               </TabContainer>
                           </SwipeableViews>
                </Grid>
            </Grid>
        );
    }
}

DeviceLogInfoPage.propTypes = {};

export default withStyles(styles, {withTheme: true})(DeviceLogInfoPage);