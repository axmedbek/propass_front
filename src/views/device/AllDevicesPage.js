import React, {Component, Fragment} from 'react';
import {withStyles} from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import {fetchDevices, fetchDeviceById} from '../../actions/device/devices.action';
import connect from "react-redux/es/connect/connect";
import {CircularProgress} from "@material-ui/core";
import DeviceLogInfoPage from "./DeviceLogInfoPage";
import Grid from "@material-ui/core/Grid/Grid";

const styles = theme => ({
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: 'white',
        '&:hover': {
            backgroundColor: 'white',
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {},
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        right: '-10px',
        cursor: 'pointer'
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
        height: 45
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit + 47, // 45
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10 - 65, // 15
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    componentParts: {
        padding: 40
    },
    colDivider: {
        "backgroundColor": 'white',
        "height": "1px",
        "marginLeft": "-5px",
        "marginRight": "-15px"
    },
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '50%',
        marginTop: '25%',
    },
    activeDeviceClass: {
        backgroundColor: '#66be99',
        color: 'white !important',
        lineHeight: '24px',
        fontSize: '16px',
        fontFamily: 'Fira Sans',
        paddingTop: 10,
        paddingBottom: 10,
        height: 80,
        marginLeft: '-5px',
        marginRight: '-15px'
    },
    deviceListElement: {
        padding: '10px 0px 10px 22px',
        cursor: 'pointer'
    },
    deviceNameStyle: {
        fontWeight: 'bold',
        fontSize: '18px'
    },
    otherDevicesClass: {
        color: '#4a4956',
        lineHeight: '24px',
        fontSize: '16px',
        fontFamily: 'Fira Sans',
        height: 80,
        marginLeft: '-5px',
        marginRight: '-15px'
    }
});

class AllDevicesPage extends Component {

    state = {
        activeDeviceId: 1
    };

    componentWillMount() {
        this.props.fetchDevices();
        this.props.fetchDeviceById(this.state.activeDeviceId);
    }

    handleActiveDeviceInfo = (id) => {
        this.setState({
            activeDeviceId: id
        });
        this.props.fetchDeviceById(id);
    };

    render() {
        const {classes, devices} = this.props;
        return (
            <Fragment>
                {
                    devices.fetching ?
                        <CircularProgress className={classes.progress} size={70}/> :
                        <Fragment>
                            <Grid container spacing={32} style={{height: '100%'}}>
                                <Grid item md={2} style={{
                                    flexBasis: '21%',
                                    maxWidth: '21%',
                                    height: '100%',
                                    backgroundColor: '#E9E9E9',
                                }}>
                                    <h4 style={{
                                        fontFamily: "'Fira Sans',sans-serif",
                                        lineHeight: '30px',
                                        fontSize: '22px',
                                        color: '#383D3E',
                                        fontWeight: 'bold',
                                        padding: '40px 141px 21px 32px',
                                    }}>Qurğular</h4>
                                    <div className={classes.colDivider}/>
                                    <div style={{
                                        paddingLeft: '22px',
                                        paddingTop: '28px',
                                        paddingBottom: '28px'
                                    }}>
                                        <div className={classes.search}>
                                            <InputBase
                                                placeholder="Axtar…"
                                                classes={{
                                                    root: classes.inputRoot,
                                                    input: classes.inputInput,
                                                }}
                                            />
                                            <div className={classes.searchIcon}>
                                                <SearchIcon/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={classes.colDivider}/>
                                    <div style={{
                                        padding: 22,
                                        color: '#7d7777'
                                    }}>Cəmi qurğu sayı {devices.total_data.data.length}</div>

                                    {
                                        this.props.devices.total_data.data.map((value, index) => (
                                            <Fragment>
                                                <div
                                                    onClick={() => this.handleActiveDeviceInfo(value.id)}
                                                    key={index} data-device-id={value.id}
                                                    className={this.state.activeDeviceId === value.id ? classes.activeDeviceClass : classes.otherDevicesClass}>
                                                    <div className={classes.deviceListElement}>
                                                        <div
                                                            className={classes.deviceNameStyle}>{value.device_name}</div>
                                                        <div>{`ID-${value.id}`}</div>
                                                    </div>
                                                </div>
                                                <div className={classes.colDivider}/>
                                            </Fragment>
                                        ))
                                    }
                                </Grid>
                                <Grid item md={9} style={{
                                    padding: 50,
                                    paddingLeft : 0,
                                    paddingRight : 0
                                }}>
                                    {
                                        this.props.device.fetching ?
                                            <CircularProgress className={classes.progress} size={70}/> :
                                            <DeviceLogInfoPage device={this.props.device.total_data.data}/>
                                    }
                                </Grid>
                            </Grid>
                        </Fragment>
                }
            </Fragment>
        );
    }
}

AllDevicesPage.propTypes = {};

const mapStateToProps = ({devices, device}) => {
    return {
        devices,
        device
    }
};

const mapDispatchToProps = {
    fetchDevices,
    fetchDeviceById
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(AllDevicesPage));