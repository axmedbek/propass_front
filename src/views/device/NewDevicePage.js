// react
import React, {Component} from 'react';
// react router dom
import { Redirect } from "react-router-dom";
//redux
import {reduxForm} from "redux-form";
import {connect} from "react-redux";
//material ui , bootstrap and vs
import Paper from "@material-ui/core/Paper/Paper";
import Typography from "@material-ui/core/Typography/Typography";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid/Grid";
import { Button, ButtonGroup,FormGroup,Input,Label } from "reactstrap";
import InputMask from 'react-input-mask';
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import makeAnimated from "react-select/lib/animated";
// components
import ProSelect from "../../components/ProSelect";
import ProPassModal from "../../components/ProPassModal";
import FilterModal from "../modals/FilterModal";
// actions
import {fetchDeviceManufactures} from '../../actions/msk/device_manufactures.action';
import {fetchDeviceModels} from '../../actions/msk/device_models.action';
import {fetchDeviceDestinations} from '../../actions/msk/device_destination.action';
import {fetchDeviceTypes} from '../../actions/msk/device_type.action';
import {removeSelectedDepartments} from '../../actions/device/devices.action';
// imports
import '../../assets/css/device_add.css';
import {API_BASE, USER_ID} from "../../config/env";
import {findElementById, makeDataForSelect} from "../../helper/standart";
import deviceAddStyle from '../../assets/jss/deviceAddStyle';
import {Table, TableBody, TableCell, TableHead, TableRow} from "@material-ui/core";
import TrashIcon from "../../assets/images/icons/trash.svg";
import ProPassLoading from "../../components/ProPassLoading";
import axios from "axios";


const TOKEN = JSON.parse(localStorage.getItem("user"))
    ? JSON.parse(localStorage.getItem("user")).auth_token
    : null;

const getConfig = () => {
    return {
        headers: {
            'Accept': 'application/json',
            'Authorization': 'Bearer '+TOKEN
        }
    };
};

class NewDevicePage extends Component {

    state = {
        modalSelectLoader: false,
        deviceModelValue: null,
        deviceSdkTypeName: null,
        deviceFingerTypeName: null,
        deviceModelLoader: false,
        departmentFilterModal: false,
        redirect : false,
        redirectTime : 0,
        device_name : '',
        device_name_error : false,
        pageLoading : false
    };

    componentWillMount() {
        this.props.fetchDeviceDestinations();
        this.props.fetchDeviceManufactures();
        this.props.fetchDeviceTypes();
    }

    componentWillReceiveProps(nextProps,nextContext) {
        if (nextProps.device_models.total_data.data !== undefined) {
            if (nextProps.device_models.total_data.data.length > 0) {
                this.setState({
                    modalSelectLoader: true
                })
            } else {
                this.setState({
                    modalSelectLoader: false
                })
            }
        }
    }


    handleManufactureSelectChange = (e) => {
        this.setState({
            modalSelectLoader: false,
            deviceModelValue: null,
            deviceModelLoader: false
        });
        if (e != null) {
            this.props.fetchDeviceModels(e.value);
        }
    };

    handleRemoveSelectedFilter = (e, id) => {
        e.preventDefault();
        this.props.removeSelectedDepartments(id);
    };

    handleModelSelectChange = (value) => {
        this.setState({
            deviceModelValue: value,
            deviceModelLoader: !!value
        });

        if (value) {
            let data = findElementById(this.props.device_models.total_data.data, value.id);
            this.setState({
                deviceSdkTypeName: data.sdk_types.label,
                deviceFingerTypeName: data.finger_codes_types.label
            });
        }
    };

    handleFormSubmit = (e) => {
        e.preventDefault();

        const data = new FormData(this.form);
        data.append("departments",JSON.stringify(this.props.department_filters.selected_departments));
        if (this.state.device_name.length < 1) {
            this.setState({
                device_name_error : true
            })
        }
        else{
           try {
               this.setState({
                   pageLoading : true
               });
               axios.post(`${API_BASE}/device/save`, data, getConfig())
                   .then(result => {
                       if (result.data.status){
                           this.setState({
                               redirect : true
                           });
                       }
                       else{
                           alert("error bas verdi");
                           this.setState({
                               pageLoading : false
                           });
                       }
                   })
                   .catch(error => console.log(error))
           }
           catch (e) {
               console.log(e)
           }
        }
    };

    departmentFilterToggle = () => {
        this.setState({
            departmentFilterModal: !this.state.departmentFilterModal
        });
    };

    handleRedirectDevicePage = () => {
       this.setState({
           redirect: true
       });
    };

    handleInputChange = (e) => {
        this.setState({
            [e.target.name] : e.target.value
        })
    };

    render() {
        const {classes} = this.props;
        const { redirect } = this.state;
        if (redirect) {
            return <Redirect to='/devices'/>;
        }
        return (
            <Grid container spacing={24} style={{
                padding: '45px'
            }}>
                <ProPassLoading fetching={this.state.pageLoading}/>
                {/*<ProPassLoading classes={classes} fetching={this.props.save_or_edit_device.fetching}/>*/}

                <Grid item md={12}>
                    <Typography variant="h5" component="h3" style={{fontWeight: 'bold'}}>
                        Yeni qurğu yarat
                    </Typography>
                </Grid>
                <Grid item md={12}>
                    <form className={classes.container} noValidate autoComplete="off" ref={el => (this.form = el)}>
                        <input type="hidden" name="id" value="0"/>
                        <input type="hidden" name="user_id" value={USER_ID}/>
                        <Paper className={classes.root} elevation={1}>
                            <Grid container spacing={24} style={{
                                marginTop: '20px',
                            }}>
                                <Grid item md={4}>
                                    <FormGroup>
                                        <Label className={this.state.device_name_error ? classes.errorLable : ''}>Qurğunun adı</Label>
                                        <Input
                                            type="text"
                                            name="device_name"
                                            placeholder="Qurğunun adı"
                                            value={this.state.device_name}
                                            onChange={this.handleInputChange}
                                            className={this.state.device_name_error ? classes.errorInput : ''}
                                            />

                                    </FormGroup>
                                </Grid>
                                <Grid item md={4}>
                                    <FormGroup>
                                        <Label>İstehsalçı</Label>
                                        <ProSelect
                                            id={"select_components"}
                                            className="menu-outer-top"
                                            classNamePrefix="select"
                                            isDisabled={false}
                                            placeholder={"İstehsalçı"}
                                            isLoading={this.props.device_manufactures.fetching}
                                            isClearable={true}
                                            isSearchable={true}
                                            components={makeAnimated()}
                                            name="device_manufacture_name"
                                            options={makeDataForSelect(this.props.device_manufactures.total_data.data)}
                                            onChange={this.handleManufactureSelectChange}
                                        />
                                    </FormGroup>
                                </Grid>
                                <Grid item md={4}>
                                    <FormGroup>
                                        <Label>Model</Label>
                                        <ProSelect
                                            id={"select_components"}
                                            className="menu-outer-top"
                                            classNamePrefix="select"
                                            isDisabled={false}
                                            placeholder={"Model"}
                                            isLoading={this.state.modalSelectLoader ? this.props.device_models.fetching : this.state.modalSelectLoader}
                                            isClearable={true}
                                            isSearchable={true}
                                            components={makeAnimated()}
                                            value={this.state.deviceModelValue}
                                            name="device_model_name"
                                            options=
                                                {
                                                    this.state.modalSelectLoader
                                                        ? this.props.device_models.fetching
                                                        ? []
                                                        : makeDataForSelect(this.props.device_models.total_data.data)
                                                        : []
                                                }
                                            onChange={this.handleModelSelectChange}
                                        />
                                    </FormGroup>
                                </Grid>
                            </Grid>
                            <Grid container spacing={24} style={{
                                marginTop: '20px',
                            }}>
                                <Grid item md={4}
                                      style={this.state.deviceModelLoader ? {display: "block"} : {display: "none"}}>
                                    <FormGroup>
                                        <Label>SDK Növü</Label><br/>
                                        <Label
                                            className={"device-sdk-and-finger-type"}>{this.state.deviceSdkTypeName}</Label>
                                    </FormGroup>
                                </Grid>
                                <Grid item md={4}
                                      style={this.state.deviceModelLoader ? {display: "block"} : {display: "none"}}>
                                    <FormGroup>
                                        <Label>Barmaq izi kod tipi</Label><br/>
                                        <Label
                                            className={"device-sdk-and-finger-type"}>{this.state.deviceFingerTypeName}</Label>
                                    </FormGroup>
                                </Grid>
                                <Grid item md={4}>
                                    <FormGroup>
                                        <Label>Qurğunun təyinatı</Label>
                                        <ProSelect
                                            id={"select_components"}
                                            className="menu-outer-top"
                                            classNamePrefix="select"
                                            isDisabled={false}
                                            placeholder={"Qurğunun təyinatı"}
                                            isLoading={this.props.device_types.fetching}
                                            isClearable={true}
                                            isSearchable={true}
                                            components={makeAnimated()}
                                            name="device_type_name"
                                            options={makeDataForSelect(this.props.device_types.total_data.data)}
                                        />
                                    </FormGroup>
                                </Grid>
                                <Grid item md={4}>
                                    <FormGroup>
                                        <Label>İstiqamət</Label>
                                        <ProSelect
                                            id={"select_components"}
                                            className="menu-outer-top"
                                            classNamePrefix="select"
                                            isDisabled={false}
                                            placeholder={"İstiqamət"}
                                            isLoading={this.props.device_destinations.fetching}
                                            isClearable={true}
                                            isSearchable={true}
                                            components={makeAnimated()}
                                            name="device_destination_name"
                                            options={this.props.device_destinations.total_data ? makeDataForSelect(this.props.device_destinations.total_data.data) : []}
                                        />
                                    </FormGroup>
                                </Grid>
                                <Grid item md={4}>
                                    <FormGroup>
                                        <Label>İP ünvanı</Label>
                                        <InputMask mask="999.999.999.999" maskChar="0">
                                            {(inputProps) => <Input {...inputProps} type="text"
                                                                    name="device_ip_address"/>}
                                        </InputMask>
                                    </FormGroup>
                                </Grid>
                                <Grid item md={4}>
                                    <FormGroup>
                                        <Label>Seriya nömrəsi</Label>
                                        <Input type="text" name="device_seria_number"
                                               placeholder="Seriya nömrəsi"/>
                                    </FormGroup>
                                </Grid>
                                <Grid item md={this.state.deviceModelLoader ? 12 : 8}>
                                    <FormGroup>
                                        <Label>Google Map</Label>
                                        <Input type="text" name="device_google_map"
                                               placeholder="Google Map"/>
                                    </FormGroup>
                                </Grid>
                                <Grid item md={12}>
                                    <FormGroup>
                                        <Label>Bölmə</Label>
                                        <Button type={"button"} style={{marginLeft: '10px'}}
                                                onClick={this.departmentFilterToggle}>
                                            <FontAwesomeIcon icon={"filter"}/> <span
                                            style={{marginLeft: '8px'}}>Seç</span>
                                        </Button>
                                        <ProPassModal
                                            classes={classes}
                                            modalSize={"lg"}
                                            modalHeaderText={"Bölmə filter et"}
                                            isOpen={this.state.departmentFilterModal}
                                            handleModalToggle={this.departmentFilterToggle}
                                            modalClassName={"department-filter-modal"}
                                            modalBtnCloseBtnText={"Bağla"}
                                            modalBtnHandleText={"Seç"}
                                            component={<FilterModal
                                                modalBtnCloseBtnText={"Bağla"}
                                                modalBtnHandleText={"Seç"}
                                                handleModalToggle={this.departmentFilterToggle}
                                            />}
                                        />
                                    </FormGroup>
                                </Grid>
                                <Grid item md={12}>
                                    {
                                        this.props.department_filters.selected_departments.length > 0 ?

                                            <Table className={classes.table}>
                                                <TableHead>
                                                    <TableRow>
                                                        <TableCell>#</TableCell>
                                                        <TableCell align="left">Struktur ardıcıllığı</TableCell>
                                                        <TableCell align="right">Sil</TableCell>
                                                    </TableRow>
                                                </TableHead>
                                                <TableBody>
                                                    {this.props.department_filters.selected_departments.map((row, index) => (
                                                        <TableRow key={index}>
                                                            <TableCell component="th" scope="row">
                                                                {index + 1}
                                                            </TableCell>
                                                            <TableCell align="left">{row.label}</TableCell>
                                                            <TableCell align="right">
                                                                <img src={TrashIcon} alt="delete icon"
                                                                     style={{cursor: 'pointer'}}
                                                                     onClick={(e) => this.handleRemoveSelectedFilter(e, row.value)}/>
                                                            </TableCell>
                                                        </TableRow>
                                                    ))}
                                                </TableBody>
                                            </Table> : ''
                                    }
                                </Grid>
                            </Grid>
                            <Grid container spacing={24} style={{
                                marginTop: '20px',
                            }}>
                                <Grid item md={12}>
                                    <FormGroup>
                                        <Label for="device_note_id">Qeyd</Label>
                                        <Input type="textarea" name="device_note" id="device_note_id" placeholder={"Qeyd"}
                                               style={{marginTop: '0px', marginBottom: '0px', height: '97px'}}/>
                                    </FormGroup>
                                </Grid>
                            </Grid>
                        </Paper>
                        <Grid container spacing={24}>
                            <Grid item md={12}>
                                <ButtonGroup style={{marginTop: '20px'}}>
                                    <Button onClick={this.handleFormSubmit} style={{
                                        borderColor: '#63c79d',
                                        backgroundColor: '#63c79d',
                                        marginRight: '4px',
                                        borderRadius: '0',
                                    }}>Əlavə et</Button>
                                    <Button onClick={this.handleRedirectDevicePage} style={{
                                        borderColor: '#a5a4aa',
                                        backgroundColor: 'white',
                                        color: '#75747d',
                                        borderRadius: '0',
                                    }}>İmtina et</Button>
                                </ButtonGroup>
                            </Grid>
                        </Grid>
                    </form>
                </Grid>
            </Grid>
        );
    }
}

NewDevicePage.propTypes = {};

const mapStateToProps =
    ({
         device_manufactures,
         device_models,
         device_types,
         device_destinations,
         department_filters,
         save_or_edit_device
    }) => {
    return {
        device_manufactures,
        device_models,
        device_destinations,
        device_types,
        department_filters,
        save_or_edit_device
    }
};

const mapDispatchToProps = {
    fetchDeviceManufactures,
    fetchDeviceModels,
    fetchDeviceDestinations,
    fetchDeviceTypes,
    removeSelectedDepartments
};

export default withStyles(deviceAddStyle)(connect(mapStateToProps, mapDispatchToProps)(reduxForm({
    form: 'device_add'
})(NewDevicePage)));