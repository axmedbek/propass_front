import React, {Component, Fragment} from 'react';
import {withStyles, Grid, CircularProgress} from "@material-ui/core";
import Button from "@material-ui/core/Button/Button";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import classNames from 'classnames';
import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table/Table";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableBody from "@material-ui/core/TableBody/TableBody";
import * as PropTypes from "prop-types";
import Tooltip from "@material-ui/core/Tooltip/Tooltip";
import TableSortLabel from "@material-ui/core/TableSortLabel/TableSortLabel";
import {lighten} from "@material-ui/core/styles/colorManipulator";
import Toolbar from "@material-ui/core/Toolbar/Toolbar";
import IconButton from "@material-ui/core/IconButton/IconButton";
import FilterListIcon from '@material-ui/icons/FilterList';
import TablePagination from "@material-ui/core/TablePagination/TablePagination";
import {NavLink} from "react-router-dom";
import {fetchDevices, fetchDeviceById} from '../../actions/device/devices.action';
import connect from "react-redux/es/connect/connect";
import {Col, Label, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import infoIcon from '../../assets/images/icons/info.svg';
import customAddIcon from '../../assets/images/icons/add.svg';
import '../../assets/css/device.css';
const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
    button: {
        margin: theme.spacing.unit,
        backgroundColor: '#69CB9D',
        fontSize: '12px',
        fontWeight: 'bold',
        '&:hover': {
            backgroundColor: '#69CB9D'
        },
        textTransform: 'capitalize'
    },
    input: {
        display: 'none',
    },
    processButtonStyle: {
        width: '70%',
        textTransform: 'capitalize',
        fontFamily: 'Fira Sans',
        fontStyle: 'normal',
        fontWeight: 600,
        lineHeight: '24px',
        fontSize: '14px',
        color: '#FFFFFF'
    },
    elementPaddingZeroStyle: {
        paddingBottom: '0px !important'
    },
    approveButtonSytle: {
        backgroundColor: 'white',
        color: '#69CB9D',
        border: '1px solid #63c79d',
        '&:hover': {
            backgroundColor: '#69CB9D',
            color: 'white'
        }
    },
    filterButtonStyle: {
        marginLeft: '60%',
        backgroundColor: 'white',
        color: '#040905',
        '&:hover': {
            backgroundColor: 'white',
            boxShadow: '0px 3px 4px 4px #efe9e9'
        }
    },
    createNewGridStyle: {
        textAlign: 'right',
    },
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#69CB9D',
        marginLeft: '50%',
        marginTop: '25%',
    },
    deviceInfoValue: {
        color: '#676664'
    },
    pageTitle: {
        fontFamily: 'Fira Sans',
        fontStyle: 'normal',
        fontWeight: '800',
        lineHeight: '48px',
        fontSize: '32px',
        letterSpacing: '0.03em',
        color: '#000000'
    },
    pageTitleIdText: {
        paddingLeft: '40px',
        paddingRight: '5px',
        fontFamily: 'Fira Sans',
        fontStyle: 'normal',
        fontWeight: 'normal',
        lineHeight: '24px',
        fontSize: '18px',
        letterSpacing: '0.03em',
        color: '#464646'
    }
});

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

const rows = [
    {id: 'devices', numeric: false, disablePadding: true, label: 'Qurğular'},
    {id: 'destinations', numeric: true, disablePadding: false, label: 'Təyinat'},
    {id: 'location', numeric: true, disablePadding: false, label: 'Yerləşdiyi yer'},
    {id: 'identities', numeric: true, disablePadding: false, label: 'İDlər'}
];

class EnhancedTableHead extends React.Component {
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const {order, orderBy} = this.props;

        return (
            <TableHead>
                <TableRow>
                    <TableCell padding="checkbox" style={{
                        fontFamily: 'Fira Sans',
                        fontStyle: 'normal',
                        fontWeight: 'bold',
                        lineHeight: '16px',
                        fontSize: '14px',
                        color: '#BDBDCC'
                    }}>
                        #
                    </TableCell>
                    {rows.map(row => {
                        return (
                            <TableCell
                                key={row.id}
                                align={row.numeric ? 'right' : 'left'}
                                padding={row.disablePadding ? 'none' : 'default'}
                                sortDirection={orderBy === row.id ? order : false}
                                style={{
                                    fontFamily: 'Fira Sans',
                                    fontStyle: 'normal',
                                    fontWeight: 'bold',
                                    lineHeight: '16px',
                                    fontSize: '14px',
                                    color: '#BDBDCC'
                                }}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={orderBy === row.id}
                                        direction={order}
                                        onClick={this.createSortHandler(row.id)}
                                    >
                                        {row.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}

EnhancedTableHead.propTypes = {
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
};

const toolbarStyles = theme => ({
    root: {
        paddingRight: theme.spacing.unit,
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    spacer: {
        flex: '1 1 100%',
    },
    actions: {
        color: theme.palette.text.secondary,
    },
    title: {
        flex: '0 0 auto',
    },
});

let EnhancedTableToolbar = props => {
    const {classes} = props;

    return (
        <Toolbar
            className={classes.root}
        >
            <div className={classes.title}>

            </div>
            <div className={classes.spacer}/>
            <div className={classes.actions}>
                <Tooltip title="Filter list">
                    <IconButton aria-label="Filter list">
                        <FilterListIcon/>
                    </IconButton>
                </Tooltip>
            </div>
        </Toolbar>
    );
};

EnhancedTableToolbar.propTypes = {
    classes: PropTypes.object.isRequired,
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);


class DevicePage extends Component {
    state = {
        order: 'asc',
        orderBy: 'calories',
        data: [],
        page: 0,
        rowsPerPage: 5,
        device_info_modal: false
    };

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({order, orderBy});
    };


    handleChangePage = (event, page) => {
        this.setState({page});
    };

    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };

    deviceInfoModalToggle = () => {
        this.setState({
            device_info_modal: !this.state.device_info_modal
        })
    };

    handleInfoDevice = id => {
        if (id > 0) {
            this.props.fetchDeviceById(id);
            this.setState({
                device_info_modal: true
            })
        }
    };

    componentWillMount() {
        this.props.fetchDevices();
    }

    render() {
        const {classes, devices} = this.props;
        const {order, orderBy, rowsPerPage, page} = this.state;
        // const emptyRows = rowsPerPage - Math.min(rowsPerPage, devices.total_data.data.length - page * rowsPerPage);
        return (
            <Grid container spacing={32} style={{
                padding: '40px 40px 0px 40px'
            }}>
                <Grid item md={12} style={{paddingBottom: '0px'}}>
                    <Grid container spacing={32}>
                        <Grid item md={6}>
                            <Grid container spacing={32}>
                                <Grid item md={12}>
                                    <span className={classes.pageTitle}>Qurğular</span>
                                    <span className={classes.pageTitleIdText}>ID1589</span>
                                    <span><img style={{paddingBottom: '4px'}} src={infoIcon} alt="info icon"/></span>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item md={6} style={{textAlign: 'right'}}>
                            <NavLink to={"/devices/all"} style={{textDecoration: 'none'}}>
                                <Button variant="contained" color="primary" className={classes.button}>
                                    <FontAwesomeIcon icon={"reply-all"}/> <span
                                    style={{marginLeft: '8px'}}>Bütün qurğular</span>
                                </Button>
                            </NavLink>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item md={12} className={classes.elementPaddingZeroStyle}>
                    <Grid container spacing={32}>
                        <Grid item md={6}>
                            <Grid container spacing={32}>
                                <Grid item md={6}>
                                    <Button variant="contained" color="primary"
                                            className={classNames(classes.button, classes.processButtonStyle)}>
                                        <span style={{marginLeft: '8px'}}>Yeni</span>
                                    </Button>
                                </Grid>
                                <Grid item md={6} style={{marginLeft: '-15%'}}>
                                    <Button variant="contained" color="primary"
                                            className={classNames(classes.button, classes.processButtonStyle, classes.approveButtonSytle)}>
                                        <span style={{marginLeft: '8px'}}>Təsdiqlənib</span>
                                    </Button>
                                </Grid>
                            </Grid>
                        </Grid>
                        <Grid item md={6}>
                            <Grid container spacing={32}>
                                <Grid item md={6}>
                                    <Button variant="contained" color="primary"
                                            className={classNames(classes.button, classes.processButtonStyle, classes.filterButtonStyle)}>
                                        <FontAwesomeIcon icon={"filter"}/> <span
                                        style={{marginLeft: '8px'}}>Filter</span>
                                    </Button>
                                </Grid>
                                <Grid item md={6} className={classes.createNewGridStyle}>
                                    <NavLink to={"/device/new"} style={{textDecoration: 'none'}}>
                                        <Button variant="contained" color="primary"
                                                className={classNames(classes.button, classes.processButtonStyle)}>
                                            <img src={customAddIcon} alt="add device icon"/> <span
                                            style={{marginLeft: '10px'}}>Yeni qurğu əlavə et</span>
                                            {/*<FontAwesomeIcon icon={"plus"}/> <span style={{marginLeft: '8px'}}>Yeni qurğu əlavə et</span>*/}
                                        </Button>
                                    </NavLink>
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item md={12}>
                    {
                        devices.fetching ?
                            <CircularProgress className={classes.progress} size={70}/> :
                            <Paper className={classes.root}>
                                <EnhancedTableToolbar/>
                                <div className={classes.tableWrapper}>
                                    <Table className={classes.table} aria-labelledby="tableTitle">
                                        <EnhancedTableHead
                                            order={order}
                                            orderBy={orderBy}
                                            onRequestSort={this.handleRequestSort}
                                        />
                                        <TableBody style={{cursor: 'pointer'}}>
                                            {stableSort(devices.total_data.data, getSorting(order, orderBy))
                                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                                .map((n, index) => {
                                                    return (
                                                        <TableRow
                                                            hover
                                                            role="checkbox"
                                                            tabIndex={-1}
                                                            key={n.id}
                                                            onClick={() => this.handleInfoDevice(n.id)}
                                                        >
                                                            <TableCell padding="checkbox">
                                                                {index + 1}
                                                            </TableCell>
                                                            <TableCell component="th" scope="row" padding="none">
                                                                {n.device_name}
                                                            </TableCell>
                                                            <TableCell align="right">{n.device_type_name}</TableCell>
                                                            <TableCell align="right">Araz 1</TableCell>
                                                            <TableCell align="right">0</TableCell>
                                                        </TableRow>
                                                    );
                                                })}
                                            {devices.total_data.data.length < 1 && (
                                                <TableRow>
                                                    <TableCell colSpan={5} style={{textAlign: 'center'}}>Hazırda
                                                        göstəriləcək qurğu yoxdur</TableCell>
                                                </TableRow>
                                            )}
                                        </TableBody>
                                    </Table>
                                </div>
                                <TablePagination
                                    rowsPerPageOptions={[5, 10, 25]}
                                    component="div"
                                    count={devices.total_data.data.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    backIconButtonProps={{
                                        'aria-label': 'Previous Page',
                                    }}
                                    nextIconButtonProps={{
                                        'aria-label': 'Next Page',
                                    }}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                />
                                <Modal isOpen={this.state.device_info_modal}
                                       toggle={this.deviceInfoModalToggle}
                                       className={"device-info-modal"}
                                       style={{marginTop: '10%', borderRadius: '0px'}} size={"md"}>
                                    <ModalHeader toggle={this.deviceInfoModalToggle} classes={"device-detailed-title"}>Ətraflı baxış</ModalHeader>
                                    <ModalBody style={{marginLeft: '10px'}}>
                                        {this.props.device.fetching ? 'loading' :
                                            <Fragment>
                                                <Row>
                                                    <Col md={"4"}>
                                                        <Label>Adı</Label>
                                                    </Col>
                                                    <Col md={"8"}>
                                                        <Label
                                                            className={classes.deviceInfoValue}>{this.props.device.total_data.data.device_name}</Label>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col md={"4"}>
                                                        <Label>İstehsalçı</Label>
                                                    </Col>
                                                    <Col md={"8"}>
                                                        <Label
                                                            className={classes.deviceInfoValue}>{this.props.device.total_data.data.manufacture}</Label>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col md={"4"}>
                                                        <Label>Model</Label>
                                                    </Col>
                                                    <Col md={"8"}>
                                                        <Label
                                                            className={classes.deviceInfoValue}>{this.props.device.total_data.data.model}</Label>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col md={"4"}>
                                                        <Label>Barmaq izi kod tipi</Label>
                                                    </Col>
                                                    <Col md={"8"}>
                                                        <Label
                                                            className={classes.deviceInfoValue}>{this.props.device.total_data.data.finger}</Label>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col md={"4"}>
                                                        <Label>SDK növü</Label>
                                                    </Col>
                                                    <Col md={"8"}>
                                                        <Label
                                                            className={classes.deviceInfoValue}>{this.props.device.total_data.data.sdk_type}</Label>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col md={"4"}>
                                                        <Label>Təyinatı</Label>
                                                    </Col>
                                                    <Col md={"8"}>
                                                        <Label
                                                            className={classes.deviceInfoValue}>{this.props.device.total_data.data.type}</Label>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col md={"4"}>
                                                        <Label>İstiqamət</Label>
                                                    </Col>
                                                    <Col md={"8"}>
                                                        <Label
                                                            className={classes.deviceInfoValue}>{this.props.device.total_data.data.destination}</Label>
                                                    </Col>
                                                </Row>
                                                <Row>
                                                    <Col md={"4"}>
                                                        <Label>İP ünvanı</Label>
                                                    </Col>
                                                    <Col md={"8"}>
                                                        <Label
                                                            className={classes.deviceInfoValue}>{this.props.device.total_data.data.ip_address}</Label>
                                                    </Col>
                                                </Row>
                                            </Fragment>
                                        }
                                    </ModalBody>
                                    <ModalFooter>
                                        <Button style={{
                                            border: '1px solid #a5a4aa',
                                            backgroundColor: 'white',
                                            color: '#75747d',
                                            borderRadius: '0',
                                        }}
                                                onClick={this.deviceInfoModalToggle}>Bağla</Button>
                                    </ModalFooter>
                                </Modal>
                            </Paper>
                    }
                </Grid>
            </Grid>
        );
    }
}

DevicePage.propTypes = {};

const mapStateToProps = ({devices, device}) => {
    return {
        devices,
        device
    }
};

const mapDispatchToProps = {
    fetchDevices,
    fetchDeviceById
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(DevicePage));