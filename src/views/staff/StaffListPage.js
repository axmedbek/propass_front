import React, {Component, Fragment} from 'react';
import {Label} from "reactstrap";
import classNames from 'classnames';
import {withStyles} from '@material-ui/core/styles';
import Button from "@material-ui/core/Button/Button";
import staffStyles from '../../styles/staffStyles';
import Grid from "@material-ui/core/Grid/Grid";
import EyeIcon from '../../assets/images/icons/eye.svg';
import {connect} from "react-redux";
import TableCreatorComponent from "../../components/TableCreatorComponent";
import UnRegisteredUsers from "../tbodies/staffs/unRegisteredUsers";
import RegisteredUsers from "../tbodies/staffs/registeredUsers";
import { fetchStaff } from '../../actions/staff/staff.action';
import UserDetailed from "./userDetailed";

const notregisteredHeaders = [
    { id : 0 , name : '№'},
    { id : 1 , name : 'Ad Soyad'},
    { id : 2 , name : 'Şirkət'},
    { id : 3 , name : 'Dep'},
    { id : 4 , name : 'Şöbə'},
    { id : 5 , name : 'Bölmə'},
    { id : 6 , name : 'Vəzifə'},
    { id : 7 , name : 'Əməliyyat'},
];

const registeredHeaders = [
    { id : 0 , name : '№'},
    { id : 1 , name : 'Ad Soyad'},
    { id : 2 , name : 'Status'},
    { id : 3 , name : 'Barmaq izi'},
    { id : 4 , name : 'Keçidlər'},
    { id : 5 , name : 'Qurğular'},
];

class StaffListPage extends Component {

    state = {
        isActiveuserDetailed : false,
        activeuserDetailed : 0
    };

    // handleClickRegisterUserFinger = (e,id) => {
    //
    // };

    handleRegisteredUserDetailed = (e,id) => {
        if (this.state.activeuserDetailed !== id) {
            this.setState({
                isActiveuserDetailed : true,
                activeuserDetailed : id
            });
        }
        else {
            this.setState({
                isActiveuserDetailed : false,
                activeuserDetailed : 0
            });
        }
        if (id > 0){
            this.props.fetchStaff(id);
        }
    };
    render() {
        const {classes} = this.props;
        return (
            <Fragment>
                <Grid container spacing={32} style={{
                    marginTop: '17px',
                    marginLeft: '31px'
                }}>
                    <Grid item md={2}>
                        <Label className={classes.titleCountText}>0</Label><br/>
                        <Label className={classes.titleCountSubText}>Ümumi say</Label>
                    </Grid>
                    <Grid item md={2}>
                        <Label className={classes.titleCountText}>{
                            !this.props.staffs.fetching ?
                                !this.props.contentLoadStatus ? 0 :
                                    this.props.staffs.total_data.data.length : 0 }</Label><br/>
                        <Label className={classes.titleCountSubText}>Seçilmiş</Label>
                    </Grid>
                </Grid>
                <Grid container spacing={32}>
                    <Grid item md={2}>
                        <Button variant="contained"
                                className={classNames(classes.button,classes.registeredUserBtnStyle, this.props.activeButtonTab === 0 ? classes.activeTabButtonStyle : '')}
                                onClick={(e) => this.props.handleActiveTabClick(e, 0)}>
                            Qeydiyyatlı ({
                            !this.props.staffs.fetching ?
                                !this.props.contentLoadStatus ? 0 :
                                    this.props.staffs.total_data.data.length : 0
                        })
                        </Button>
                    </Grid>
                    <Grid item md={2}>
                        <Button variant="contained"
                                className={classNames(classes.button,classes.unRegisteredUserBtnStyle,this.props.activeButtonTab === 1 ? classes.activeTabButtonStyle : '')}
                                onClick={(e) => this.props.handleActiveTabClick(e, 1)}>
                            Qeydiyyatsız ({
                            !this.props.staffs.fetching ?
                                !this.props.contentLoadStatus ? 0 :
                                    this.props.staffs.total_data.data.length : 0
                        })
                        </Button>
                    </Grid>
                    <Grid item md={8} style={this.props.activeButtonTab === 1 ? { display : "none" } : { display : 'block'}}>
                        <Button variant="contained"
                                className={classNames(classes.button,classes.detailedBtnStyle, classes.buttonRegistrationStyle, classes.tableBodyRowStyle)}>
                            <img src={EyeIcon} alt="detailed look icon" className={classes.detailedBtnImgStyle}/>
                            <span className={classes.detailedBtnTextStyle}>Ətraflı baxış</span>
                        </Button>
                    </Grid>
                </Grid>
                <Grid container spacing={32} style={{
                    marginTop: -40
                }}>

                        {
                            this.props.activeButtonTab === 0
                                ? <TableCreatorComponent
                                    key={1}
                                    classes={classes}
                                    headers={registeredHeaders}
                                    data={this.props.staffs}
                                    loadStatus={this.props.contentLoadStatus}
                                    usersComponent={<RegisteredUsers
                                        classes={classes}
                                        data={this.props.staffs}
                                        handleRegisteredUserDetailed={this.handleRegisteredUserDetailed}
                                        activeuserDetailed={this.state.activeuserDetailed}/>}
                                    isActiveuserDetailed={this.state.isActiveuserDetailed}
                                    userDetailedComponent={<UserDetailed classes={classes} detailedData={this.props.staff}/>}
                                    />

                                        : this.props.activeButtonTab === 1
                                        ? <TableCreatorComponent
                                            key={2}
                                            classes={classes}
                                            headers={notregisteredHeaders}
                                            data={this.props.staffs}
                                            loadStatus={this.props.contentLoadStatus}
                                            usersComponent={<UnRegisteredUsers classes={classes} data={this.props.staffs}/>}
                                            isActiveuserDetailed={false}
                                            userDetailedComponent={<UserDetailed classes={classes} detailedData={this.props.staff}/>}
                                />
                                : ''
                        }

                </Grid>
            </Fragment>
        );
    }
}

StaffListPage.propTypes = {};

const mapStateToProps = ({staffs,staff}) => {
    return {
        staffs,
        staff
    }
};

const mapDispatchToProps = {
    fetchStaff
};

export default withStyles(staffStyles)(connect(mapStateToProps, mapDispatchToProps)(StaffListPage));