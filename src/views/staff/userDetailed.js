import React, {Fragment} from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import {CircularProgress} from "@material-ui/core";
import randomColor from "randomcolor";
import Paper from "@material-ui/core/Paper/Paper";
import classNames from 'classnames';

const UserDetailed = ({ classes , detailedData }) => {
    return (
        <Paper className={classNames(classes.root)} style={{
            marginLeft: '10px',
            height: '470px !important',
            overflowX: 'hidden',
        }}>
            <Grid container spacing={32}>
                {
                    detailedData.fetching
                        ? <CircularProgress className={classes.progress} size={40}/>
                        : <Fragment>
                            <Grid item md={12}>
                                <h3 style={
                                    {
                                        "fontFamily":"Fira Sans",
                                        "lineHeight":"28px",
                                        "fontSize":"20px",
                                        "color":"rgb(56, 61, 62)",
                                        "marginLeft":"24px",
                                        "marginTop":"24px",
                                        "fontWeight":"bold",
                                        "letterSpacing":"0.4px"
                                    }
                                }>Ətraflı</h3>
                            </Grid>
                            <Grid item md={12} style={{"marginLeft":"16px","paddingTop":"0px"}}>
                                <Grid container spacing={32}>
                                    <Grid item md={3}>
                                        {
                                            detailedData.total_data.data["profile_photo"]
                                                ? <img src={"http://"+detailedData.total_data.data["profile_photo"]} alt="avatar icon" style={{ borderRadius: 30,"width":"64px","height":"64px"}}/>
                                                :
                                                <div className={"empty_avatar"} style={{
                                                    backgroundColor : randomColor()
                                                }}>
                                                    {
                                                        detailedData.total_data.data["user_name"].split(" ")[0][0].toUpperCase() +
                                                        detailedData.total_data.data["user_name"].split(" ")[1][0].toUpperCase()
                                                    }
                                                </div>
                                        }
                                    </Grid>
                                    <Grid item md={9} style={{
                                        marginTop: 15
                                    }}>{detailedData.total_data.data["user_name"]}</Grid>
                                </Grid>
                            </Grid>
                            <Grid item md={12} style={{"marginLeft":"23px","paddingTop":"8px","paddingBottom":"0px"}}>
                                <Grid container spacing={32}>
                                    <Grid item md={12}>
                                        <h6 style={
                                            {"fontFamily":"Fira Sans","lineHeight":"20px","fontSize":"12px","color":"#A3A3A3"}
                                        }>Vəzifə</h6>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item md={12} style={{"marginLeft":"23px","paddingTop":"0px","marginTop":"-6px","paddingBottom":"4px"}}>
                                <Grid container spacing={32}>
                                    <Grid item md={12}>
                                        <h6>{detailedData.total_data.data["position_name"]}</h6>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item md={12} style={{"marginLeft":"23px","paddingTop":"0px","paddingBottom":"0px"}}>
                                <Grid container spacing={32}>
                                    <Grid item md={12}>
                                        <h6 style={
                                            {"fontFamily":"Fira Sans","lineHeight":"20px","fontSize":"12px","color":"#A3A3A3"}
                                        }>Status</h6>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item md={12} style={{"marginLeft":"23px","paddingTop":"0px","marginTop":"-6px"}}>
                                <Grid container spacing={32}>
                                    <Grid item md={12}>
                                        <h6>İş tapşırığı</h6>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item md={12} style={{"marginLeft":"23px","paddingTop":"8px","paddingBottom":"0px"}}>
                                <Grid container spacing={32}>
                                    <Grid item md={12}>
                                        <h6 style={
                                            {"fontFamily":"Fira Sans","lineHeight":"28px","fontSize":"16px","color":"#55AB80","fontWeight" : "bold"}
                                        }>Son Əməliyyat</h6>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item md={12} style={{"marginLeft":"23px","paddingTop":"8px","paddingBottom":"0px"}}>
                                <Grid container spacing={32}>
                                    <Grid item md={12}>
                                        <h6 style={
                                            {"fontFamily":"Fira Sans","lineHeight":"20px","fontSize":"12px","color":"#A3A3A3"}
                                        }>İşə gəlib</h6>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item md={12} style={{"marginLeft":"23px","paddingTop":"0px","marginTop":"-6px","paddingBottom":"0px"}}>
                                <Grid container spacing={32}>
                                    <Grid item md={4}>
                                        <h6 style={{lineHeight: '22px',fontSize: '14px'}}>7:50</h6>
                                    </Grid>
                                    <Grid item md={4}>
                                        <h6 style={{lineHeight: '22px',fontSize: '14px'}}>Qurğu1</h6>
                                    </Grid>
                                    <Grid item md={4}>
                                        <h6 style={{lineHeight: '22px',fontSize: '14px'}}>Keçid1</h6>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item md={12} style={{"marginLeft":"23px","paddingTop":"0px","paddingBottom":"0px"}}>
                                <Grid container spacing={32}>
                                    <Grid item md={12}>
                                        <h6 style={
                                            {"fontFamily":"Fira Sans","lineHeight":"20px","fontSize":"12px","color":"#A3A3A3"}
                                        }>İşə gəlib</h6>
                                    </Grid>
                                </Grid>
                            </Grid>
                            <Grid item md={12} style={{"marginLeft":"23px","paddingTop":"0px","marginTop":"-6px"}}>
                                <Grid container spacing={32}>
                                    <Grid item md={4}>
                                        <h6 style={{lineHeight: '22px',fontSize: '14px'}}>8:50</h6>
                                    </Grid>
                                    <Grid item md={4}>
                                        <h6 style={{lineHeight: '22px',fontSize: '14px'}}>Qurğu2</h6>
                                    </Grid>
                                    <Grid item md={4}>
                                        <h6 style={{lineHeight: '22px',fontSize: '14px'}}>Keçid2</h6>
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Fragment>
                }
            </Grid>
        </Paper>
    );
};

export default UserDetailed;