import React, {Fragment} from 'react';
import randomColor from "randomcolor";
import Grid from "@material-ui/core/Grid/Grid";
import {CircularProgress} from "@material-ui/core";
import posed from 'react-pose';


const AvatarBox = posed.div({
    hoverable: true,
    init: {
        scale: 1,
        boxShadow: '0px 0px 0px rgba(0,0,0,0)',
        cursor : 'pointer'
    },
    hover: {
        scale: 1.2,
        boxShadow: '0px 5px 10px rgba(0,0,0,0.2)',
        cursor : 'pointer'
    }
});

const UserDetailedFingerRegister = ({classes, detailedData}) => {
    return (
        <Fragment>
            {
                detailedData.fetching
                    ? <CircularProgress className={classes.progress} size={40}/>
                    :
                    <Fragment>
                        {

                            detailedData.total_data.data["profile_photo"]
                                ?
                                <AvatarBox>
                                    <img src={"http://" + detailedData.total_data.data["profile_photo"]}
                                         alt="avatar icon"
                                         style={{borderRadius: 30, "width": "80px", "height": "80px"}}/>
                                </AvatarBox>
                                :
                                <div style={{
                                    paddingTop: 35
                                }}>
                                    {/*<div className={"empty_avatar_detail"} style={{*/}
                                        {/*backgroundColor: randomColor()*/}
                                    {/*}}>*/}
                                        <AvatarBox className={"empty_avatar_detail"}>
                                            {
                                                detailedData.total_data.data["user_name"].split(" ")[0][0].toUpperCase() +
                                                detailedData.total_data.data["user_name"].split(" ")[1][0].toUpperCase()
                                            }
                                        </AvatarBox>
                                    {/*</div>*/}
                                </div>

                        }
                        <Grid container spacing={24} style={{
                            marginTop: 30,
                            marginLeft: 30
                        }}>
                            <Grid item md={12} className={"detail_label"}>
                                Ad
                            </Grid>
                            <Grid item md={12} className={"detail_text"}>
                                {detailedData.total_data.data["user_name"].split(" ")[0]}
                            </Grid>
                            <Grid item md={12} className={"detail_label"}>
                                Soyad
                            </Grid>
                            <Grid item md={12} className={"detail_text"}>
                                {detailedData.total_data.data["user_name"].split(" ")[1]}
                            </Grid>
                            <Grid item md={12} className={"detail_label"}>
                                Şirkət
                            </Grid>
                            <Grid item md={12} className={"detail_text"}>
                                {detailedData.total_data.data["company_name"]}
                            </Grid>
                            <Grid item md={12} className={"detail_label"}>
                                Bölmə
                            </Grid>
                            <Grid item md={12} className={"detail_text"}>
                                {detailedData.total_data.data["structure_name"]}
                            </Grid>
                            <Grid item md={12} className={"detail_label"}>
                                Vəzifə
                            </Grid>
                            <Grid item md={12} className={"detail_text"}>
                                {detailedData.total_data.data["position_name"]}
                            </Grid>
                        </Grid>
                    </Fragment>
            }
        </Fragment>
    );
};

export default UserDetailedFingerRegister;