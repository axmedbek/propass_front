import React, {Component, Fragment} from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import '../../assets/css/staff_register_finger.css';
import UserDetailedFingerRegister from "./UserDetailedFingerRegister";
import PageNotFound from "../PageNotFound";
import {fetchStaff,fetchRegIdWithUserId,fetchSelectedFingers,fetchFingerTemplatesWithFingerIndex} from '../../actions/staff/staff.action';
import {fetchDevices} from '../../actions/device/devices.action';
import connect from "react-redux/es/connect/connect";
import {CircularProgress, Tab, Tabs, Typography, withStyles} from "@material-ui/core";
import SwipeableViews from "react-swipeable-views";
import Table from "@material-ui/core/Table/Table";
import TableBody from "@material-ui/core/TableBody/TableBody";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import PropTypes from "prop-types";
import Radio from "@material-ui/core/Radio/Radio";
import classNames from 'classnames';
import CheckedIcon from '../../assets/images/icons/checked.svg';
import TrashIcon from '../../assets/images/icons/trash.svg';
import LeftHandImg from '../../assets/images/icons/left_hand.svg';
import RightHandImg from '../../assets/images/icons/right_hand.svg';
import SelectectableFingerImg from '../../assets/images/biometric_icons/selectable_finger.svg';
import SelectedFingerImg from '../../assets/images/biometric_icons/selected.svg';
import SelectedFingerIcon from '../../assets/images/biometric_icons/selected_fp_icon.svg';
import WarningFingerImg from '../../assets/images/icons/selected.png';
import DeviceCloseIcon from '../../assets/images/icons/close_icon.svg';
import ListItem from "@material-ui/core/ListItem/ListItem";
import List from "@material-ui/core/List/List";
import Collapse from "@material-ui/core/Collapse/Collapse";
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import ProSelect from "../../components/ProSelect";
import ProPassDialog from "../../components/ProPassDialog";
import axios from "axios";
import ProPassLoading from "../../components/ProPassLoading";
import {
    checkFingerIsSelected,
    findFingerNameWithIndex,
    makeDataForFingerSelectDevice,
    removeElementById
} from "../../helper/standart";
import registerStaffFingerStyle from "../../assets/jss/registerStaffFingerStyle";
import Paper from "@material-ui/core/Paper/Paper";
import ProStepper from "../../components/ProStepper";
import Button from "@material-ui/core/Button/Button";


function TabContainer({children, dir}) {
    return (
        <Typography component="div" dir={dir} style={{padding: 8 * 3}}>
            {children}
        </Typography>
    );
}

function FingerSelectorComponent({isSelected, imgStyle, isOpenFingerSelector, handleSelectableFingerClick,handleRemoveFingerClick,isAlreadySelected}) {
    return (
        <Fragment>
            {
                isAlreadySelected ?
                    <div className={"finger"}>
                        {/*<img src={WarningFingerImg} alt="warning finger" style={imgStyle}/>*/}
                        <img className={"selected_finger_img"} src={SelectedFingerImg} alt="already selected finger"
                             style={imgStyle} onClick={(e) => handleRemoveFingerClick(e)}/>
                        <img className={"finger_icon"} src={SelectedFingerIcon} alt="selected finger icon"
                             style={imgStyle} onClick={(e) => handleRemoveFingerClick(e)}/>
                    </div>
                    :
                    isSelected ?
                        <div className={"finger"}>
                            {/*<img src={WarningFingerImg} alt="warning finger" style={imgStyle}/>*/}
                            <img className={"selected_finger_img"} src={WarningFingerImg} alt="selected finger"
                                 style={imgStyle} onClick={(e) => handleSelectableFingerClick(e)}/>
                            <img className={"finger_icon"} src={SelectedFingerIcon} alt="selected finger icon"
                                 style={imgStyle} onClick={(e) => handleSelectableFingerClick(e)}/>
                        </div>
                        : <div className={"finger"}>
                            <img src={SelectectableFingerImg} alt="selectable finger img" style={imgStyle}
                                 onClick={(e) => handleSelectableFingerClick(e)}/>
                        </div>
            }
        </Fragment>
    );
}

const FingerRadioComponent = ({classes, fingerIndex, handleRadioChange, currentIndex, fingerName}) => {
    return (
        <div
            className={classNames("finger_select_radio_content", fingerIndex === currentIndex ? classes.isActiveFinger : '')}>
            <span>{fingerName}</span>
            <Radio
                checked={fingerIndex === currentIndex}
                onChange={(e) => handleRadioChange(e, currentIndex)}
                classes={{
                    root: classes.radioButtonRoot,
                    checked: classes.radioButtonChecked,
                }}
                className={"finger_select_radio_btn"}
            />
        </div>
    );
};

TabContainer.propTypes = {
    children: PropTypes.node.isRequired,
    dir: PropTypes.string.isRequired,
};

class RegisterStaffPage extends Component {

    state = {
        isNotNumeric: false,
        userId : null,
        tabValue: 0,
        fingerIndex: 1,
        fingerTabValue: 0,
        activeFingerDevice: 1,
        isDeviceSelect: true,
        selectedDevices: [],
        isOpenFingerCode: true,
        isOpenFingerSelector: true,
        selectedDeviceId: 0,
        selectedDeviceIpAddress : 0,
        deviceIsSelected: false,
        fingerFetching: false,
        loadStep : 1,
        errorStep : 0
    };

    handleTabChange = (event, value) => {
        this.setState({tabValue: value});
    };

    handleTabChangeIndex = index => {
        this.setState({tabValue: index});
    };

    handleFingerTabChange = (event, value) => {
        this.setState({fingerTabValue: value});
    };

    handleFingerTabChangeIndex = index => {
        this.setState({fingerTabValue: index});
    };

    handleRadioChange = (event, index) => {
        this.setState({
            fingerIndex: index
        });

        if (this.state.selectedDeviceId > 0 && this.state.userId > 0) {
            this.props.fetchFingerTemplatesWithFingerIndex(this.state.userId,this.state.selectedDeviceId,index);
        }
    };

    handleSelectFingerDevice = (e) => {
        this.setState({
            selectedDevices: e
        });
    };

    handleRemoveSelectedDevice = (e, id) => {
        // console.log(this.state.selectedDevices);
        this.setState({
            selectedDevices: removeElementById(this.state.selectedDevices, id)
        })
    };

    handleFingerCodeClick = () => {
        this.setState({
            isOpenFingerCode: !this.state.isOpenFingerCode
        })
    };

    handelDeviceSelectedDialogToggle = () => {
        this.setState({
            deviceIsSelected: !this.state.deviceIsSelected
        })
    };

    handleRemoveFingerClick = (e) => {
        alert('Barmagi silmek isteyirsiniz')
    };


    handleSelectableFingerClick = (e, fingerIndex) => {
        if (fingerIndex === this.state.fingerIndex) {
            if (this.state.selectedDeviceId === 0) {
                this.setState({
                    deviceIsSelected: true
                })
            } else {
                this.setState({
                    fingerFetching: true
                });

                axios.get('http://172.16.0.132:8081/iclock/pull_sdk?prospectsbs:connect:'+this.state.selectedDeviceIpAddress+':'+this.state.selectedDeviceId+'')
                    .then(result => {
                        if (result.data.status === "success") {
                            // alert("connected");
                            this.setState({
                                loadStep : 2
                            });
                            axios.get('http://172.16.0.132:8081/iclock/pull_sdk?prospectsbs:enroll:'+this.props.staff.regId.data.reg_id+':'+this.state.fingerIndex+':1:'+this.state.selectedDeviceIpAddress+':'+this.state.selectedDeviceId+'')
                                .then(result => {
                                    if (result.data.status === "success"){
                                        // alert("enrolled");
                                        this.setState({
                                            loadStep : 3
                                        });
                                        if (this.state.selectedDeviceId > 0 && this.state.userId > 0) {
                                            this.props.fetchFingerTemplatesWithFingerIndex(this.state.userId,this.state.selectedDeviceId,this.state.fingerIndex);
                                            setTimeout({
                                                loadStep : 0
                                            },2000);
                                        }
                                    }
                                    else if(result.data.status === "error"){
                                        // alert("error bas verdi");
                                        this.setState({
                                            errorStep: 2
                                        })
                                    }
                                    // this.setState({
                                    //     fingerFetching: false
                                    // })
                                })
                                .catch(error => console.log(error));
                        }
                        else{
                            // alert("qurguya qosula bilmedi");
                            this.setState({
                                errorStep: 1
                            })
                        }
                    })
                    .catch(error => console.log(error));
            }
        } else {
            this.setState({
                fingerIndex: fingerIndex
            });
            if (this.state.selectedDeviceId > 0 && this.state.userId > 0) {
                this.props.fetchFingerTemplatesWithFingerIndex(this.state.userId,this.state.selectedDeviceId,fingerIndex);
            }
        }
    };
    handleSelectFingerWriteDevice = (e, deviceId , deviceIpAddress ) => {
        this.setState({
            selectedDeviceId: deviceId,
            selectedDeviceIpAddress : deviceIpAddress
        });
        this.props.fetchSelectedFingers(this.state.userId,deviceId);
        if (deviceId > 0 && this.state.userId > 0) {
            this.props.fetchFingerTemplatesWithFingerIndex(this.state.userId,deviceId,this.state.fingerIndex);
        }
    };

    componentWillMount() {
        let user_id = this.props.match.params.user_id;
        if (isNaN(user_id)) {
            this.setState({
                isNotNumeric: true
            })
        }

        this.setState({
            userId : user_id
        });
        // console.log(user_id);
        this.props.fetchStaff(user_id);
        this.props.fetchRegIdWithUserId(user_id);
        this.props.fetchDevices();
    }

    render() {
        const {classes, staff, theme} = this.props;
        if (this.state.isNotNumeric) {
            return <PageNotFound/>
        }
        return (
            <Grid container spacing={32} style={{height: 'calc(100% + 16px)'}}>
                        <Fragment>
                            <Grid item xl={3} md={3} xs={3} sm={12}>
                                <Grid item md={12} className={"finger_register_page_title"}>
                                    Qeydiyyat
                                </Grid>
                                <Grid item md={12} style={{height: 'calc(100% - 83px)'}}>
                                    <div className={"left_side_div"}>
                                        <UserDetailedFingerRegister classes={classes} detailedData={staff}/>
                                    </div>
                                </Grid>
                            </Grid>
                            <Grid item xl={9} md={9} xs={9} sm={12}>
                                <Tabs
                                    className={"right_side_div"}
                                    value={this.state.tabValue}
                                    onChange={this.handleTabChange}
                                    indicatorColor="none"
                                    // textColor="primary"
                                    variant="fullWidth"
                                >
                                    <Tab label="Yeni Bİ"
                                         className={this.state.tabValue === 0 ? classes.activeDeviceTabStyle : classes.otherDeviceTabStyle}
                                         style={{
                                             height: 60
                                         }}/>
                                    <Tab label="Mövcud Bİ"
                                         className={this.state.tabValue === 1 ? classes.activeDeviceTabStyle : classes.otherDeviceTabStyle}/>
                                    <Tab label="Kart"
                                         className={this.state.tabValue === 2 ? classes.activeDeviceTabStyle : classes.otherDeviceTabStyle}/>
                                    <Tab label="Loglar"
                                         className={this.state.tabValue === 3 ? classes.activeDeviceTabStyle : classes.otherDeviceTabStyle}/>
                                </Tabs>
                                <Grid item xl={12} md={12} xs={12} sm={12}>
                                    <SwipeableViews
                                        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                                        index={this.state.tabValue}
                                        onChangeIndex={this.handleTabChangeIndex}
                                    >
                                        <TabContainer dir={theme.direction}>
                                            <Grid container spacing={24}>
                                                <Grid item xl={5} md={5} xs={5} sm={12}>
                                                    <div className={"finger_select_div"}>
                                                        <div className={"finger_select_title"}
                                                             style={{marginBottom: 8}}>
                                                            Qurğu elave et
                                                        </div>
                                                        <ProSelect
                                                            isMulti
                                                            name="colors"
                                                            isLoading={this.props.devices.fetching}
                                                            options={makeDataForFingerSelectDevice(this.props.devices.total_data.data)}
                                                            className="basic-multi-select"
                                                            classNamePrefix="select"
                                                            value={this.state.selectedDevices}
                                                            onChange={this.handleSelectFingerDevice}
                                                            styles={{
                                                                ...registerStaffFingerStyle,
                                                                control: (base, state) => ({
                                                                    ...base,
                                                                    '&:hover': {borderColor: '#55AB80'}, // border style on hover
                                                                    border: '1px solid #55AB80', // default border color
                                                                    boxShadow: 'none', // no box-shadow
                                                                }),
                                                            }}
                                                        />
                                                    </div>
                                                    <div className={"finger_select_div"}>
                                                        <div className={"finger_select_title"}>
                                                            Barmaq
                                                        </div>
                                                        <FingerRadioComponent fingerName={"Baş barmaq"}
                                                                              classes={classes}
                                                                              fingerIndex={this.state.fingerIndex}
                                                                              handleRadioChange={this.handleRadioChange}
                                                                              currentIndex={1}/>
                                                        <FingerRadioComponent fingerName={"Şəhadət barmağı"}
                                                                              classes={classes}
                                                                              fingerIndex={this.state.fingerIndex}
                                                                              handleRadioChange={this.handleRadioChange}
                                                                              currentIndex={2}/>
                                                        <FingerRadioComponent fingerName={"Orta barmaq"}
                                                                              classes={classes}
                                                                              fingerIndex={this.state.fingerIndex}
                                                                              handleRadioChange={this.handleRadioChange}
                                                                              currentIndex={3}/>
                                                        <FingerRadioComponent fingerName={"Adsız barmaq"}
                                                                              classes={classes}
                                                                              fingerIndex={this.state.fingerIndex}
                                                                              handleRadioChange={this.handleRadioChange}
                                                                              currentIndex={4}/>
                                                        <FingerRadioComponent fingerName={"Çeçele barmaq"}
                                                                              classes={classes}
                                                                              fingerIndex={this.state.fingerIndex}
                                                                              handleRadioChange={this.handleRadioChange}
                                                                              currentIndex={5}/>
                                                    </div>
                                                    <div className={"finger_select_div"}>
                                                        <ListItem button onClick={this.handleFingerCodeClick} style={{
                                                            backgroundColor: '#55ab7f',
                                                            height: 40
                                                        }}>
                                                            <span className={"register_finger_code_title"}>Kodu</span>
                                                            {this.state.isOpenFingerCode
                                                                ? <ExpandLess
                                                                    className={"register_finger_code_list_icon"}/>
                                                                : <ExpandMore
                                                                    className={"register_finger_code_list_icon"}/>}
                                                        </ListItem>
                                                        <Collapse in={this.state.isOpenFingerCode} timeout="auto"
                                                                  unmountOnExit>
                                                            <List component="div" disablePadding
                                                                  className={"register_finger_code_list"}>
                                                                {
                                                                    this.props.staff.finger_templates !== undefined
                                                                        ? this.props.staff.finger_templates.data !== undefined ?
                                                                            this.props.staff.finger_templates.data.data.map(value => {
                                                                       return(
                                                                           <ListItem button className={classes.nested}>
                                                                               {/*<div className={"finger_code"}>*/}
                                                                               <img src={CheckedIcon} alt="checked icon" style={{
                                                                                   position: 'absolute',
                                                                                   left: 12
                                                                               }}/>
                                                                               <span>{ findFingerNameWithIndex(value.finger_index)} - {
                                                                                   value.fing_template.substring(0,10)
                                                                               }</span>
                                                                               <img src={TrashIcon} alt="trash icon"
                                                                                    className={"finger_trash_icon"}/>
                                                                               {/*</div>*/}
                                                                           </ListItem>
                                                                       );
                                                                    }) : '' : ''
                                                                }
                                                            </List>
                                                        </Collapse>
                                                    </div>
                                                </Grid>
                                                <Grid item xl={7} md={7} xs={7} sm={12} style={{
                                                    paddingLeft: 0
                                                }}>
                                                    <Tabs
                                                        className={"finger_tabs"}
                                                        value={this.state.fingerTabValue}
                                                        onChange={this.handleFingerTabChange}
                                                        indicatorColor="none"
                                                        // textColor="primary"
                                                        variant="fullWidth"
                                                    >
                                                        <Tab label="Sol əl"
                                                             className={this.state.fingerTabValue === 0 ? classes.activeFingerTabStyle : classes.otherFingerTabStyle}
                                                        />
                                                        <Tab label="Sağ əl"
                                                             className={this.state.fingerTabValue === 1 ? classes.activeFingerTabStyle : classes.otherFingerTabStyle}/>
                                                    </Tabs>
                                                    <Grid item xl={12} md={12} xs={12} sm={12}>
                                                        <SwipeableViews
                                                            axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                                                            index={this.state.fingerTabValue}
                                                            onChangeIndex={this.handleFingerTabChangeIndex}
                                                        >
                                                            <TabContainer dir={theme.direction}>
                                                                <div className={"finger_hand_tabs"}>
                                                                    <img src={LeftHandImg} alt="left hand img" style={{
                                                                        height: '90%',
                                                                        marginTop: '30px'
                                                                    }}/>
                                                                    <FingerSelectorComponent
                                                                        isSelected={this.state.fingerIndex === 1}
                                                                        isAlreadySelected = {checkFingerIsSelected(this.props.staff.selected_fingers,1)}
                                                                        isOpenFingerSelector={this.state.isOpenFingerSelector}
                                                                        imgStyle={{
                                                                            position: 'absolute',
                                                                            top: '43%',
                                                                            marginLeft: '-185px',
                                                                            cursor: 'pointer'
                                                                        }}
                                                                        handleSelectableFingerClick={(e) => this.handleSelectableFingerClick(e, 1)}
                                                                        handleRemoveFingerClick={(e) => this.handleRemoveFingerClick(e)}
                                                                    />
                                                                    <FingerSelectorComponent
                                                                        isSelected={this.state.fingerIndex === 2}
                                                                        isOpenFingerSelector={this.state.isOpenFingerSelector}
                                                                        imgStyle={{
                                                                            position: 'absolute',
                                                                            top: '10%',
                                                                            marginLeft: '-111px',
                                                                            cursor: 'pointer'
                                                                        }}
                                                                        handleSelectableFingerClick={(e) => this.handleSelectableFingerClick(e, 2)}
                                                                    />
                                                                    <FingerSelectorComponent
                                                                        isSelected={this.state.fingerIndex === 3}
                                                                        isOpenFingerSelector={this.state.isOpenFingerSelector}
                                                                        imgStyle={{
                                                                            position: 'absolute',
                                                                            top: '5%',
                                                                            marginLeft: '-21px',
                                                                            cursor: 'pointer'
                                                                        }}
                                                                        handleSelectableFingerClick={(e) => this.handleSelectableFingerClick(e, 3)}
                                                                    />
                                                                    <FingerSelectorComponent
                                                                        isSelected={this.state.fingerIndex === 4}
                                                                        isOpenFingerSelector={this.state.isOpenFingerSelector}
                                                                        imgStyle={{
                                                                            position: 'absolute',
                                                                            top: '10%',
                                                                            marginLeft: '77px',
                                                                            cursor: 'pointer'
                                                                        }}
                                                                        handleSelectableFingerClick={(e) => this.handleSelectableFingerClick(e, 4)}
                                                                    />
                                                                    <FingerSelectorComponent
                                                                        isSelected={this.state.fingerIndex === 5}
                                                                        isOpenFingerSelector={this.state.isOpenFingerSelector}
                                                                        imgStyle={{
                                                                            position: 'absolute',
                                                                            top: '24%',
                                                                            marginLeft: '127px',
                                                                            cursor: 'pointer'
                                                                        }}
                                                                        handleSelectableFingerClick={(e) => this.handleSelectableFingerClick(e, 5)}
                                                                    />
                                                                </div>
                                                            </TabContainer>
                                                            <TabContainer dir={theme.direction}>
                                                                <div className={"finger_hand_tabs"}>
                                                                    <img src={RightHandImg} alt="left hand img" style={{
                                                                        height: '90%',
                                                                        marginTop: '30px',
                                                                        cursor: 'pointer'
                                                                    }}/>
                                                                    <FingerSelectorComponent
                                                                        isSelected={this.state.fingerIndex === 1}
                                                                        imgStyle={{
                                                                            position: 'absolute',
                                                                            top: '43%',
                                                                            marginLeft: '118px',
                                                                            cursor: 'pointer'
                                                                        }}
                                                                        handleSelectableFingerClick={(e) => this.handleSelectableFingerClick(e, 1)}
                                                                    />
                                                                    <FingerSelectorComponent
                                                                        isSelected={this.state.fingerIndex === 2}
                                                                        imgStyle={{
                                                                            position: 'absolute',
                                                                            top: '10%',
                                                                            marginLeft: '48px',
                                                                            cursor: 'pointer'
                                                                        }}
                                                                        handleSelectableFingerClick={(e) => this.handleSelectableFingerClick(e, 2)}
                                                                    />
                                                                    <FingerSelectorComponent
                                                                        isSelected={this.state.fingerIndex === 3}
                                                                        imgStyle={{
                                                                            position: 'absolute',
                                                                            top: '5%',
                                                                            marginLeft: '-41px',
                                                                            cursor: 'pointer'
                                                                        }}
                                                                        handleSelectableFingerClick={(e) => this.handleSelectableFingerClick(e, 3)}
                                                                    />
                                                                    <FingerSelectorComponent
                                                                        isSelected={this.state.fingerIndex === 4}
                                                                        imgStyle={{
                                                                            position: 'absolute',
                                                                            top: '10%',
                                                                            marginLeft: '-137px',
                                                                            cursor: 'pointer'
                                                                        }}
                                                                        handleSelectableFingerClick={(e) => this.handleSelectableFingerClick(e, 4)}
                                                                    />
                                                                    <FingerSelectorComponent
                                                                        isSelected={this.state.fingerIndex === 5}
                                                                        imgStyle={{
                                                                            position: 'absolute',
                                                                            top: '24%',
                                                                            marginLeft: '-189px',
                                                                            cursor: 'pointer'
                                                                        }}
                                                                        handleSelectableFingerClick={(e) => this.handleSelectableFingerClick(e, 5)}
                                                                    />
                                                                </div>
                                                            </TabContainer>
                                                        </SwipeableViews>
                                                        <Grid container spacing={24}>
                                                            {
                                                                this.state.selectedDevices.map((value, index) => (
                                                                    <Grid item md={2} style={{
                                                                        paddingTop: 0,
                                                                        paddingRight: 0,
                                                                        minWidth: 115
                                                                    }}>
                                                                        <div key={index}
                                                                             className={classNames("finger_device_btn", this.state.selectedDeviceId === value.value ? "active_finger_device_btn" : "")}
                                                                             onClick={(e) => this.handleSelectFingerWriteDevice(e, value.value,value.ip_address)}>
                                                                            <span>{value.label}</span>
                                                                            <img src={DeviceCloseIcon}
                                                                                 alt="device close img"
                                                                                 className={"finger_device_close_icon"}
                                                                                 onClick={(e) => this.handleRemoveSelectedDevice(e, value.value)}/>
                                                                        </div>
                                                                    </Grid>
                                                                ))
                                                            }
                                                        </Grid>
                                                    </Grid>
                                                </Grid>
                                            </Grid>
                                        </TabContainer>
                                        <TabContainer dir={theme.direction}>
                                            <Table className={classes.table}>
                                                <TableBody>
                                                    <TableRow>
                                                        <TableCell colSpan={5} style={{textAlign: 'center'}}>Hazırda
                                                            göstəriləcək məlumat yoxdur</TableCell>
                                                    </TableRow>
                                                </TableBody>
                                            </Table>
                                        </TabContainer>
                                        <TabContainer dir={theme.direction}>
                                            <Table className={classes.table}>
                                                <TableBody>
                                                    <TableRow>
                                                        <TableCell colSpan={5} style={{textAlign: 'center'}}>Hazırda
                                                            göstəriləcək qurğu yoxdur</TableCell>
                                                    </TableRow>
                                                </TableBody>
                                            </Table>
                                        </TabContainer>
                                        <TabContainer dir={theme.direction}>
                                            <Table className={classes.table}>
                                                <TableBody>
                                                    <TableRow>
                                                        <TableCell colSpan={5} style={{textAlign: 'center'}}>Hazırda
                                                            göstəriləcək qurğu yoxdur</TableCell>
                                                    </TableRow>
                                                </TableBody>
                                            </Table>
                                        </TabContainer>
                                    </SwipeableViews>
                                </Grid>
                            </Grid>
                            <ProPassDialog
                                is_open={this.state.deviceIsSelected}
                                message_title="Xəbərdarlıq"
                                message_content="Davam etmək üçün qurğu seçməyiniz tələb olunur"
                                handleToggleProcess={this.handelDeviceSelectedDialogToggle}
                                isInfo={true}
                            />
                        </Fragment>
                        <div className={classes.stepperStyle} style={this.state.fingerFetching ? { display : 'block' } : { display : 'none'}}>
                            <Paper className={classes.stepperDivStyle}>
                                <Grid container spacing={24}>
                                    <Grid item md={4}>
                                        <ProStepper classes={classes} currentStep={1} loadStep={this.state.loadStep} stepLoaderStyle={"stepLoaderOneStyle"} stepperText={"Connection"} errorStep={this.state.errorStep}/>
                                    </Grid>
                                    <Grid item md={4}>
                                        <ProStepper classes={classes} currentStep={2} loadStep={this.state.loadStep} stepLoaderStyle={"stepLoaderTwoStyle"} stepperText={"Finger Enroll"} errorStep={this.state.errorStep}/>
                                    </Grid>
                                    <Grid item md={4}>
                                        <ProStepper classes={classes} currentStep={3} loadStep={this.state.loadStep} stepLoaderStyle={"stepLoaderThreeStyle"} stepperText={"Finish"} errorStep={this.state.errorStep}/>
                                    </Grid>
                                    <Grid item md={12} style={this.state.errorStep > 0 ? { display : 'block',marginLeft : '40%' } : { display : 'none' }}>
                                        <Button contained style={{
                                            backgroundColor: '#99a49f',
                                            color: 'white',
                                            textTransform: 'inherit',
                                            marginTop: '20px'}} onClick={() => this.setState({ fingerFetching : false , errorStep : 0 , loadStep : 1 })}>Əməliyyatı sonlandır</Button>
                                    </Grid>
                                </Grid>
                            </Paper>
                        </div>
            </Grid>
        );
    }
}

RegisterStaffPage.propTypes = {};

const mapStateToProps = ({staff, devices}) => {
    return {
        staff,
        devices
    }
};

const mapDispatchToProps = {
    fetchStaff,
    fetchDevices,
    fetchRegIdWithUserId,
    fetchSelectedFingers,
    fetchFingerTemplatesWithFingerIndex
};

export default withStyles(registerStaffFingerStyle, {withTheme: true})(connect(mapStateToProps, mapDispatchToProps)(RegisterStaffPage));