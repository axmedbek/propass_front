import React, {Component, Fragment} from 'react';
import {withStyles} from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';
import connect from "react-redux/es/connect/connect";
import {CircularProgress} from "@material-ui/core";
import '../../assets/css/staff.css';
import StaffListPage from "./StaffListPage";
import Grid from "@material-ui/core/Grid/Grid";
import { fetchDepartmentFilters , fetchDepartmentFilterBySelect, removeDepartmentFilter } from '../../actions/device/devices.action';
import { fetchStaffs } from '../../actions/staff/staff.action';
import ProSelect from "../../components/ProSelect";
import makeAnimated from "react-select/lib/animated";


const styles = theme => ({
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: 'white',
        '&:hover': {
            backgroundColor: 'white',
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {},
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        right: '-10px',
        cursor: 'pointer'
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
        height: 45
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit + 47, // 45
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10 - 65, // 15
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    componentParts: {
        paddingLeft: '22px',
        paddingTop: '28px'
    },
    colDivider: {
        "backgroundColor": 'white',
        "height": "1px",
        "marginLeft": "-5px",
        "marginRight": "-15px"
    },
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '50%',
        marginTop: '25%',
    },
    button: {
        margin: theme.spacing.unit,
    },
    sidebarDateButtons: {
        width: '100%',
        height: '33px',
        backgroundColor: '#BBBBBB',
        marginLeft: 0,
        borderRadius: '4px',
        top: '-10px',
        color: 'white',
        '&:hover': {
            backgroundColor: '#959191'
        },
        fontFamily: 'Fira Sans',
        lineHeight: '12px',
        fontSize: '11px',
    },
    datepickerInputs: {
        backgroundColor: 'white !important',
        padding: '4px 0px 4px 0px !important'
    },
    datepickerInputsDiv: {
        paddingRight: '12px !important'
    },
    registeredStaffActiveRow : {
        "background":"#F6F6F6",
        "boxShadow":"0px 1px 4px rgba(0, 0, 0, 0.25)"
    }
});

function makeDataForSelect(array) {
    const newArray = [];
    if (array !== undefined) {
        array.map(arr => newArray.push({value: arr.id, label: arr.name}))
    }
    return newArray
}

class StaffPage extends Component {

    state = {
        activeButtonTab : 1,
        contentLoadStatus : false,
    };

    handleActiveTabClick = (e,type) => {
        this.setState({
            activeButtonTab : type
        });

        const selectedIndex = this.props.department_filters.total_data.data.length;
        for (let i = selectedIndex ; i > 0 ; i-- ) {
            if (this.state["parentSelectId"+i] && this.state["parentSelectId"+i] !== 0) {
                this.props.fetchStaffs(this.state.activeButtonTab,this.state["parentSelectId"+i]);
                break;
            }
        }
    };

    handleSidebarDateChange = (date, type) => {
        this.setState({
            [`selectedSideBarDate${type}`]: date
        });
    };

    componentWillMount() {
        this.props.fetchDepartmentFilters();
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.department_filters.total_data.length !== this.props.department_filters.total_data.length) {
            if (nextProps.department_filters.total_data.data !== undefined) {
                if (nextProps.department_filters.total_data.data.length > 0) {
                    let fisrtFilterElement = nextProps.department_filters.total_data.data[0];
                    this.props.fetchDepartmentFilterBySelect(fisrtFilterElement.id,0,fisrtFilterElement.tip.toLowerCase());
                    nextProps.department_filters.total_data.data.map(value =>{
                        this.setState({
                           [value.tip.toLowerCase()+"Value"] :  null
                        });
                        return null;
                    });
                }
            }
        }
    }

    handleFilterFirstSelectChange = (val) => {
        if (val && val.value > 0){
            this.setState({
                ["parentSelectId"+0] : val.value,
                contentLoadStatus : true
            });
            this.props.department_filters.total_data.data.map((value,index) => {
                if (index > 0){
                    this.props.fetchDepartmentFilterBySelect(value.id,val.value,value.tip.toLowerCase())
                }
                return null;
            });

            this.props.fetchStaffs(this.state.activeButtonTab,val.value);
        }
        else{
            this.setState({
                ["parentSelectId"+0] : 0,
                contentLoadStatus : false
            });
            this.props.department_filters.total_data.data.map((value,index) => {
                if (index > 0){
                    this.props.removeDepartmentFilter(value.tip.toLowerCase());
                    this.setState({
                        [value.tip.toLowerCase()+"Value"] : null
                    })
                }
                return null;
            });
        }
    };

    handleFilterSelectChange = (val,selectedIndex,tip) => {
        // console.log(tip+"Value");
        if (val && val.value > 0){
                this.props.fetchStaffs(this.state.activeButtonTab,val.value);
                this.setState({
                    ["parentSelectId"+selectedIndex] : val.value,
                    [tip+"Value"] : val
                });
                this.props.department_filters.total_data.data.map((value,index) => {
                if (index > selectedIndex) {
                    this.props.fetchDepartmentFilterBySelect(value.id,val.value,value.tip.toLowerCase())
                }
                return null;
            });
        }
        else{
            let parentSelectedIndex = 0;
            for (let i = selectedIndex - 1 ; selectedIndex - 1 >= 0 ; i-- ) {
                if (this.state["parentSelectId"+i] && this.state["parentSelectId"+i] !== 0) {
                    parentSelectedIndex = i;
                    this.props.fetchStaffs(this.state.activeButtonTab,this.state["parentSelectId"+i]);
                    break;
                }
            }

            const selectedIndexCount = this.props.department_filters.total_data.data.length;
            for (let i = selectedIndexCount ; i > parentSelectedIndex ; i-- ) {
                this.setState({
                    ["parentSelectId"+i] : 0,
                    [tip+"Value"] : null
                })
            }

        }
        this.props.department_filters.total_data.data.map((value,index) => {
            if (index > selectedIndex){
                this.props.removeDepartmentFilter(value.tip.toLowerCase());
                this.setState({
                    [value.tip.toLowerCase()+"Value"] : null
                })
            }
            return null;
        });
    };


    render() {
        const {classes} = this.props;
        return (
            <Fragment>
                {
                    this.props.department_filters.fetching ?
                        <CircularProgress className={classes.progress} size={70}/> :
                        <Fragment>
                            <Grid container spacing={32} style={{ height: '100%',margin : 0 }}>
                                <Grid item md={2} style={{
                                    flexBasis: '21%',
                                    maxWidth: '21%',
                                    height: '100%',
                                    backgroundColor: '#E9E9E9',
                                }}>
                                    <h4 className={"staff-sidebar-title"}>Əməkdaşlar</h4>
                                    <div className={classes.colDivider}/>
                                    <div style={{
                                        paddingLeft: '22px',
                                        paddingTop: '28px',
                                        paddingBottom: '28px'
                                    }}>
                                        <div className={classes.search}>
                                            <InputBase
                                                placeholder="Axtar…"
                                                classes={{
                                                    root: classes.inputRoot,
                                                    input: classes.inputInput,
                                                }}
                                            />
                                            <div className={classes.searchIcon}>
                                                <SearchIcon/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className={classes.colDivider}/>
                                    {/*<div style={{*/}
                                        {/*paddingLeft: '24px',*/}
                                        {/*paddingTop: '20px'*/}
                                    {/*}}>*/}
                                        {/*<h3 style={{*/}
                                            {/*"fontFamily": "Fira Sans",*/}
                                            {/*"lineHeight": "22px",*/}
                                            {/*"fontSize": "14px",*/}
                                            {/*"color": "#505050"*/}
                                        {/*}}>Tarix üzrə axtarış</h3>*/}
                                        {/*<MuiPickersUtilsProvider utils={DateFnsUtils}>*/}
                                            {/*<Grid container spacing={24} style={{*/}
                                                {/*marginTop: '-24px'*/}
                                            {/*}}>*/}
                                                {/*<Grid item md={6} className={classes.datepickerInputsDiv}>*/}
                                                    {/*<InlineDatePicker*/}
                                                        {/*name={"selectedSideBarDate1"}*/}
                                                        {/*margin="normal"*/}
                                                        {/*value={selectedSideBarDate1}*/}
                                                        {/*onChange={(e) => this.handleSidebarDateChange(e, '1')}*/}
                                                        {/*format={"dd-MM-yyyy"}*/}
                                                        {/*className={"custom-datepicker-input"}*/}
                                                        {/*InputProps={{*/}
                                                            {/*disableUnderline: true,*/}
                                                        {/*}}*/}

                                                    {/*/>*/}
                                                    {/*<Button variant="contained"*/}
                                                            {/*className={classNames(classes.button, classes.sidebarDateButtons)}>*/}
                                                        {/*30 gündən az*/}
                                                    {/*</Button>*/}
                                                {/*</Grid>*/}
                                                {/*<Grid item md={6} className={classes.datepickerInputsDiv} style={{*/}
                                                    {/*marginLeft: '-10px'*/}
                                                {/*}}>*/}
                                                    {/*<InlineDatePicker*/}
                                                        {/*name={"selectedSideBarDate2"}*/}
                                                        {/*margin="normal"*/}
                                                        {/*value={selectedSideBarDate2}*/}
                                                        {/*onChange={(e) => this.handleSidebarDateChange(e, '2')}*/}
                                                        {/*format={"dd-MM-yyyy"}*/}
                                                        {/*className={"custom-datepicker-input"}*/}
                                                        {/*InputProps={{*/}
                                                            {/*disableUnderline: true,*/}
                                                        {/*}}*/}
                                                    {/*/>*/}
                                                    {/*<Button variant="contained"*/}
                                                            {/*className={classNames(classes.button, classes.sidebarDateButtons)}>*/}
                                                        {/*30 gündən çox*/}
                                                    {/*</Button>*/}
                                                {/*</Grid>*/}
                                            {/*</Grid>*/}
                                        {/*</MuiPickersUtilsProvider>*/}
                                    {/*</div>*/}
                                    <div className={classes.colDivider}/>
                                    <div style={{
                                        padding: 20,
                                        paddingRight: 10
                                    }}>
                                        {
                                            this.props.department_filters.total_data.data.map((value,index) => {
                                                // if (index > 5) return null;
                                                return (
                                                    <div key={value.id}
                                                         style={{
                                                             marginBottom: 10
                                                         }}>
                                                        {
                                                            index === 0 ?
                                                                <ProSelect
                                                                    id={"select_components"}
                                                                    className="menu-outer-top"
                                                                    classNamePrefix="select"
                                                                    isDisabled={false}
                                                                    placeholder={value.struktur_tipi}
                                                                    isLoading={this.props.filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase()+"Fetching"]}
                                                                    isClearable={true}
                                                                    isSearchable={true}
                                                                    components={makeAnimated()}
                                                                    name={value.tip.toLowerCase()}
                                                                    options={makeDataForSelect(this.props.filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase()].data)}
                                                                    onChange={this.handleFilterFirstSelectChange}
                                                                /> :
                                                                <ProSelect
                                                                    id={"select_components"}
                                                                    className="menu-outer-top"
                                                                    classNamePrefix="select"
                                                                    isDisabled={false}
                                                                    placeholder={value.struktur_tipi}
                                                                    isLoading={this.props.filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase()+"Fetching"]}
                                                                    isClearable={true}
                                                                    isSearchable={true}
                                                                    components={makeAnimated()}
                                                                    name={value.tip.toLowerCase()}
                                                                    options={
                                                                        makeDataForSelect(this.props.filter_portfels[value.tip.toLowerCase()][value.tip.toLowerCase()].data)
                                                                    }
                                                                    onChange={(e) => this.handleFilterSelectChange(e,index,value.tip.toLowerCase())}
                                                                    value={this.state[value.tip.toLowerCase()+"Value"]}
                                                                />
                                                        }
                                                    </div>
                                                )
                                            })
                                        }
                                    </div>
                                </Grid>
                                <Grid item md={9}>
                                    <StaffListPage
                                        activeButtonTab={this.state.activeButtonTab}
                                        handleActiveTabClick={this.handleActiveTabClick}
                                        contentLoadStatus = {this.state.contentLoadStatus}
                                        // totalStaffRegisteredCount = {this.state.totalStaffRegisteredCount}
                                        // totalStaffCount = {this.state.totalStaffCount}
                                    />
                                </Grid>
                            </Grid>
                        </Fragment>
                }
            </Fragment>
        );
    }
}

StaffPage.propTypes = {};

const mapStateToProps = ({department_filters , filter_portfels , staffs }) => {
    return {
        department_filters,
        filter_portfels,
        staffs
    }
};

const mapDispatchToProps = {
    fetchDepartmentFilters,
    fetchDepartmentFilterBySelect,
    fetchStaffs,
    removeDepartmentFilter
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(StaffPage));