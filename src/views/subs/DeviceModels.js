import React, {Fragment} from 'react';
import classNames from 'classnames';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {
    Table, TableBody, TableCell, TableHead, TablePagination, TableRow, TableSortLabel,
    Toolbar, Typography, Paper, IconButton, Tooltip, TextField, CircularProgress,
} from '@material-ui/core';
import {
    Add as AddIcon,
    Delete as DeleteIcon,
    Edit as EditIcon,
    Save as SaveIcon,
    Cancel as CancelIcon
} from '@material-ui/icons';
import {lighten} from '@material-ui/core/styles/colorManipulator';

import {connect} from 'react-redux';
import {
    addNewDeviceModel,cancelDeviceModel,deleteDeviceModel,saveOrUpdateDeviceModel
} from '../../actions/msk/device_models.action';
import {fetchDeviceSdkTypes} from '../../actions/msk/device_sdk_types.action';
import {fetchDeviceFingerCodeTypes} from '../../actions/msk/device_finger_code_types.action';

import ProPassDialog from "../../components/ProPassDialog";
import ProSelect from "../../components/ProSelect";
import makeAnimated from 'react-select/lib/animated';

function desc(a, b, orderBy) {
    if (b[orderBy] < a[orderBy]) {
        return -1;
    }
    if (b[orderBy] > a[orderBy]) {
        return 1;
    }
    return 0;
}

function stableSort(array, cmp) {
    const stabilizedThis = array.map((el, index) => [el, index]);
    stabilizedThis.sort((a, b) => {
        const order = cmp(a[0], b[0]);
        if (order !== 0) return order;
        return a[1] - b[1];
    });
    return stabilizedThis.map(el => el[0]);
}

function getSorting(order, orderBy) {
    return order === 'desc' ? (a, b) => desc(a, b, orderBy) : (a, b) => -desc(a, b, orderBy);
}

function makeDataForSelect(array) {
    const newArray = [];
    if (array !== undefined) {
        array.map(arr => newArray.push({value: arr.id, label: arr.name}))
    }
    return newArray
}

const rows = [
    {id: 'name', numeric: false, disablePadding: true, sortDirection: true, label: 'Qurğu modeli'},
    {id: 'sdk_name', numeric: false, disablePadding: true, sortDirection: true, label: 'SDK növü'},
    {id: 'finger_code_name', numeric: false, disablePadding: true, sortDirection: true, label: 'Barmaq izi kod tipi'},
    {id: 'operation', numeric: false, disablePadding: true, sortDirection: false, label: 'Əməliyyat'},
];

class EnhancedTableHead extends React.Component {
    createSortHandler = property => event => {
        this.props.onRequestSort(event, property);
    };

    render() {
        const {order, orderBy} = this.props;

        return (
            <TableHead>
                <TableRow>
                    <TableCell padding="checkbox">
                        <span>#</span>
                    </TableCell>
                    {rows.map((row, index) => {
                        return (
                            <TableCell
                                key={row.id}
                                style={rows.length === index + 1 ? {
                                    textAlign: 'end',
                                    marginRight: '24px !important',
                                    width: '150px',
                                } : index === 2 ? {width: '190px'} : {}}
                                align={row.numeric ? 'right' : 'left'}
                                padding={row.disablePadding ? 'none' : 'default'}
                                sortDirection={orderBy === row.id && row.sortDirection ? order : false}
                            >
                                <Tooltip
                                    title="Sort"
                                    placement={row.numeric ? 'bottom-end' : 'bottom-start'}
                                    enterDelay={300}
                                >
                                    <TableSortLabel
                                        active={false}
                                        direction={order}
                                        onClick={this.createSortHandler(row.id)}
                                    >
                                        {row.label}
                                    </TableSortLabel>
                                </Tooltip>
                            </TableCell>
                        );
                    }, this)}
                </TableRow>
            </TableHead>
        );
    }
}

EnhancedTableHead.propTypes = {
    numSelected: PropTypes.number.isRequired,
    onRequestSort: PropTypes.func.isRequired,
    order: PropTypes.string.isRequired,
    orderBy: PropTypes.string.isRequired,
    rowCount: PropTypes.number.isRequired,
};

const toolbarStyles = theme => ({
    root: {
        paddingRight: theme.spacing.unit,
    },
    highlight:
        theme.palette.type === 'light'
            ? {
                color: theme.palette.secondary.main,
                backgroundColor: lighten(theme.palette.secondary.light, 0.85),
            }
            : {
                color: theme.palette.text.primary,
                backgroundColor: theme.palette.secondary.dark,
            },
    spacer: {
        flex: '1 1 100%',
    },
    actions: {
        color: theme.palette.text.secondary,
    },
    title: {
        flex: '0 0 auto',
    }
});

let EnhancedTableToolbar = props => {
    const {numSelected, classes , handleAddDeviceModel } = props;

    return (
        <Toolbar
            className={classNames(classes.root, {
                [classes.highlight]: numSelected > 0,
            })}
        >
            <div className={classes.title}>
                {numSelected > 0 ? (
                    <Typography color="inherit" variant="subtitle1">
                        {numSelected} selected
                    </Typography>
                ) : (
                    <Typography variant="h6" id="tableTitle">
                        Qurğu modelləri
                    </Typography>
                )}
            </div>
            <div className={classes.spacer}/>
            <div className={classes.actions}>
                <Tooltip title="Yeni qurğu modeli">
                    <IconButton aria-label="Add" onClick={handleAddDeviceModel}>
                        <AddIcon className={classes.customIconSizes}/>
                    </IconButton>
                </Tooltip>
            </div>
        </Toolbar>
    );
};

EnhancedTableToolbar.propTypes = {
    classes: PropTypes.object.isRequired,
    numSelected: PropTypes.number.isRequired,
};

EnhancedTableToolbar = withStyles(toolbarStyles)(EnhancedTableToolbar);

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        height : '100%'
    },
    table: {
        minWidth: '100%',
        marginBottom : '208px'
    },
    tableWrapper: {
        overflowX: 'auto',
        padding: '15px'
    },
    fab: {
        position: 'absolute',
        bottom: '44px',
        right: '4%',
        color: 'white',
        backgroundColor: '63c79d'
    },
    addBtnCss: {
        color: 'rgb(230, 237, 234)',
        backgroundColor: 'rgb(99, 199, 157)',
        '&:hover': {
            backgroundColor: 'rgb(88, 183, 142)',
            color: 'rgb(230, 237, 234)'
        },
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        height: '38px',
        bottom: '3px'
    },
    // cssLabel: {
    //     color : 'green'
    // },

    cssOutlinedInput: {
        '&$cssFocused $notchedOutline': {
            borderColor: '#63c79d !important',
        }
    },

    cssFocused: {},

    notchedOutline: {
        // borderWidth: '1px',
        // borderColor: 'green !important'
    },
    dense: {
        marginTop: 16,
    },
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '50%',
        marginTop: '25%',
    },
    customIconSizes : {
        fontSize : '21px'
    },
    resize:{
        fontSize:13
    },
});

class DeviceModels extends React.Component {
    state = {
        order: 'asc',
        orderBy: 'device_models',
        selected: [],
        page: 0,
        rowsPerPage: 4,
        lastRowId: 0,
        editRowId: 0,
        device_name: '',
        hasAddRow: false,
        deleteDialogIsOpen: false,
        deletedRowId: 0,
        device_finger_code : 0,
        device_sdk_type : 0
    };

    handleRequestSort = (event, property) => {
        const orderBy = property;
        let order = 'desc';

        if (this.state.orderBy === property && this.state.order === 'desc') {
            order = 'asc';
        }

        this.setState({order, orderBy});
    };


    handleChangePage = (event, page) => {
        this.setState({page});
    };

    handleChangeRowsPerPage = event => {
        this.setState({rowsPerPage: event.target.value});
    };


    // start my functions
    handleEditDeviceModel = (e, id) => {
        e.preventDefault();
        if (!this.state.hasAddRow) {
            this.setState({
                editRowId: id,
            });
        }
    };

    handleDeleteDeviceModel = (e, id) => {
        e.preventDefault();
        this.props.deleteDeviceModel(id);
        this.setState({
            deleteDialogIsOpen: !this.state.deleteDialogIsOpen
        });
    };

    handleSaveDeviceModel = (e, id, isAdd) => {
           const data_id = isAdd ? 0 : id;
           const data =  {
               id : data_id ,
               name : this.state.device_name ,
               device_sdk_type_id : this.state.device_sdk_type ,
               device_finger_code_id : this.state.device_finger_code,
               device_manufacture_id : this.props.device_manufacture_id
            };

        if (isAdd) {
            this.props.saveOrUpdateDeviceModel(data);
            this.props.cancelDeviceModel(data_id);
            this.setState({
                hasAddRow: false
            })
        } else {
            this.props.saveOrUpdateDeviceModel(data);
            this.setState({
                editRowId: 0
            });
        }
    };

    handleCancelDeviceModel = (e, id, isAdd) => {
        const data_id = isAdd ? 0 : id;
        e.preventDefault();
        e.stopPropagation();
        if (isAdd) {
            this.props.cancelDeviceModel(data_id);
            this.setState({
                hasAddRow: false
            })
        } else {
            this.setState({
                editRowId: 0
            });
        }
    };

    handleAddDeviceModel = (e) => {
        e.preventDefault();
        e.stopPropagation();
        if (!this.state.hasAddRow && this.state.editRowId === 0) {
            this.props.addNewDeviceModel(this.props.device_models.total_data.data.length + 1);
            this.setState({
                hasAddRow: true
            });
        }
    };

    componentWillMount() {
        this.props.fetchDeviceSdkTypes();
        this.props.fetchDeviceFingerCodeTypes();
    };


    handleInputChange = (e) => {
        e.preventDefault();
        e.stopPropagation();
        this.setState({
            device_name: e.target.value
        })
    };


    handleSelectChange = (name,e) => {
        if (name === "device_sdk_type"){
            this.setState({
                device_sdk_type:e !== null ? e.value : 0
            });
        }
        else if (name === "device_finger_code") {
            this.setState({
                device_finger_code: e !== null ? e.value : 0
            });
        }
    };

    handelDeleteDialogToggle = (e, id) => {
        e.preventDefault();
        this.setState({
            deleteDialogIsOpen: !this.state.deleteDialogIsOpen,
            deletedRowId: !this.state.deleteDialogIsOpen ? id : 0
        });
    };


    // end my functions

    render() {

        const {classes} = this.props;
        const {order, orderBy, selected, rowsPerPage, page} = this.state;

        return (
            <Fragment>
                {
                    this.props.device_models.fetching ?
                        <CircularProgress className={classes.progress} size={70}/> :
                        <Fragment>
                            <Paper className={classes.root}>
                                <EnhancedTableToolbar numSelected={selected.length} handleAddDeviceModel={(e) => this.handleAddDeviceModel(e)}/>
                                <div className={classes.tableWrapper}>
                                    <Table className={classes.table} aria-labelledby="tableTitle">
                                        <EnhancedTableHead
                                            numSelected={selected.length}
                                            order={order}
                                            orderBy={orderBy}
                                            onRequestSort={this.handleRequestSort}
                                            rowCount={this.props.device_models.total_data.data.length}
                                        />
                                        <TableBody>
                                            {stableSort(this.props.device_models.total_data.data, getSorting(order, orderBy))
                                                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                                                .map((n, index) => {
                                                    const {editRowId} = this.state;
                                                    return (
                                                        <TableRow
                                                            hover
                                                            tabIndex={-1}
                                                            key={n.id}
                                                        >
                                                            <TableCell component="th" scope="row" padding="none">
                                                                <span style={{marginLeft: '10px'}}>{index + 1}</span>
                                                            </TableCell>
                                                            <TableCell component="th" scope="row" padding="none" style={{ width : '230px'}}>
                                                                {
                                                                    editRowId === n.id || n.isAdd ?
                                                                        <TextField
                                                                            id="outlined-bare"
                                                                            className={classes.textField}
                                                                            defaultValue={n.name}
                                                                            margin="normal"
                                                                            placeholder={"Qurğu modeli"}
                                                                            variant="outlined"
                                                                            name={"device_name"}
                                                                            onChange={e => this.handleInputChange(e, n.isAdd)}
                                                                            InputLabelProps={{
                                                                                classes: {
                                                                                    // root: classes.cssLabel,
                                                                                    focused: classes.cssFocused,
                                                                                },
                                                                            }}
                                                                            InputProps={{
                                                                                classes: {
                                                                                    root: classes.cssOutlinedInput,
                                                                                    focused: classes.cssFocused,
                                                                                    notchedOutline: classes.notchedOutline,
                                                                                    input: classes.resize,
                                                                                },
                                                                            }}
                                                                        /> :
                                                                        <span
                                                                            style={{paddingLeft: '10px'}}>{n.name}</span>
                                                                }
                                                            </TableCell>
                                                            <TableCell component="th" scope="row" padding="none"
                                                                       style={{paddingLeft: '5px', zIndex: '1'}}>
                                                                {
                                                                    editRowId === n.id || n.isAdd ?
                                                                        <ProSelect
                                                                            id={"select_components"}
                                                                            className="menu-outer-top"
                                                                            classNamePrefix="select"
                                                                            isDisabled={false}
                                                                            placeholder={"SDK növü"}
                                                                            isLoading={this.props.device_sdk_types.fetching}
                                                                            isClearable={true}
                                                                            isSearchable={true}
                                                                            components={makeAnimated()}
                                                                            name="device_sdk_type"
                                                                            // value={makeDataForSelect(this.props.device_sdk_types.total_data.data).filter(option => option.value === n.sdk_types.value )}
                                                                            options={makeDataForSelect(this.props.device_sdk_types.total_data.data)}
                                                                            onChange={(e) => this.handleSelectChange("device_sdk_type",e)}
                                                                        /> : <span> { n.sdk_types.label } </span>
                                                                }
                                                            </TableCell>
                                                            <TableCell component="th" scope="row" padding="none"
                                                                       style={{paddingLeft: '5px', zIndex: '1'}}>
                                                                {
                                                                    editRowId === n.id || n.isAdd ?
                                                                        <ProSelect
                                                                            id={"select_components"}
                                                                            className="menu-outer-top"
                                                                            classNamePrefix="select"
                                                                            isDisabled={false}
                                                                            isLoading={this.props.device_finger_code_types.fetching}
                                                                            isClearable={true}
                                                                            placeholder={"Barmaq kodu tipi"}
                                                                            isSearchable={true}
                                                                            components={makeAnimated()}
                                                                            name="device_finger_code"
                                                                            // value={makeDataForSelect(this.props.device_finger_code_types.total_data.data).filter(option => option.value === n.finger_codes_types.value )}
                                                                            options={makeDataForSelect(this.props.device_finger_code_types.total_data.data)}
                                                                            onChange={(e) => this.handleSelectChange("device_finger_code",e)}
                                                                        />
                                                                        : <span>{ n.finger_codes_types.label }</span>
                                                                }
                                                            </TableCell>
                                                            <TableCell component="th" scope="row" padding={"dense"}
                                                                       style={{textAlign: 'end' , width: '95px' }}>
                                                                {
                                                                    editRowId === n.id || n.isAdd ?
                                                                        <Fragment>
                                                                            <Tooltip title="Save">
                                                                                <IconButton aria-label="Save"
                                                                                            onClick={(e) => this.handleSaveDeviceModel(e, n.id, n.isAdd)}>
                                                                                    <SaveIcon className={classes.customIconSizes}/>
                                                                                </IconButton>
                                                                            </Tooltip>
                                                                            <Tooltip title="Cancel">
                                                                                <IconButton aria-label="Cancel"
                                                                                            onClick={(e) => this.handleCancelDeviceModel(e, n.id, n.isAdd)}>
                                                                                    <CancelIcon className={classes.customIconSizes}/>
                                                                                </IconButton>
                                                                            </Tooltip>
                                                                        </Fragment>
                                                                        :
                                                                        <Fragment>
                                                                            <Tooltip title="Edit">
                                                                                <IconButton aria-label="Edit"
                                                                                            onClick={(e) => this.handleEditDeviceModel(e, n.id)}>
                                                                                    <EditIcon className={classes.customIconSizes}/>
                                                                                </IconButton>
                                                                            </Tooltip>
                                                                            <Tooltip title="Delete">
                                                                                <IconButton aria-label="Delete"
                                                                                            onClick={(e) => this.handelDeleteDialogToggle(e, n.id)}>
                                                                                    <DeleteIcon className={classes.customIconSizes}/>
                                                                                </IconButton>
                                                                            </Tooltip>
                                                                        </Fragment>
                                                                }
                                                            </TableCell>
                                                        </TableRow>
                                                    );
                                                })}
                                        </TableBody>
                                        <ProPassDialog
                                            is_open={this.state.deleteDialogIsOpen}
                                            message_title="Xəbərdarlıq"
                                            message_content="Bu qurğu modelini silmək istədiyinizə əminsiniz ?"
                                            handleToggleProcess={this.handelDeleteDialogToggle}
                                            handleDeleteProcess={e => this.handleDeleteDeviceModel(e, this.state.deletedRowId)}
                                        />
                                    </Table>
                                </div>
                                <TablePagination
                                    rowsPerPageOptions={[]}
                                    component="div"
                                    count={this.props.device_models.total_data.data.length}
                                    rowsPerPage={rowsPerPage}
                                    page={page}
                                    backIconButtonProps={{
                                        'aria-label': 'Previous Page',
                                    }}
                                    nextIconButtonProps={{
                                        'aria-label': 'Next Page',
                                    }}
                                    onChangePage={this.handleChangePage}
                                    onChangeRowsPerPage={this.handleChangeRowsPerPage}
                                />
                            </Paper>
                        </Fragment>

                }
            </Fragment>
        );
    }
}

DeviceModels.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ device_models , device_sdk_types, device_finger_code_types }) => {
    return {
        device_models,
        device_sdk_types,
        device_finger_code_types
    }
};

const mapDispatchToProps = {
    fetchDeviceSdkTypes,
    fetchDeviceFingerCodeTypes,
    addNewDeviceModel,
    cancelDeviceModel,
    deleteDeviceModel,
    saveOrUpdateDeviceModel
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(DeviceModels));