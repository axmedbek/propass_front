import React, {Component, Fragment} from 'react';
import PropTypes from 'prop-types';
import Grid from "@material-ui/core/Grid";
import {CircularProgress, withStyles} from "@material-ui/core";
import bgLoginImg from '../../../assets/images/login_bg_screen.jpg';
import prospectLogo from '../../../assets/images/logo_prospect_black.png';
import TextField from "@material-ui/core/TextField";
import classNames from 'classnames';
import Button from "@material-ui/core/Button";
import FormHelperText from "@material-ui/core/FormHelperText";
import {login} from "../../../actions/auth/login.action";
import {connect} from "react-redux";
import {Redirect} from "react-router-dom";

const styles = theme => ({
    bgStyle: {
        backgroundImage: `url(${bgLoginImg})`,
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
        height: '100%'
    },
    bgTitleStyle: {
        "color": "white",
        "fontFamily": "\"Fira Sans\"",
        "textAlign": "center",
        "marginTop": "50px",
        "fontWeight": "bold",
        "fontSize": "40px",
        "paddingTop": "80px"
    },
    titleDivStyle: {"backgroundColor": "#180c5b", "textAlign": "center", "marginTop": "-50px", "paddingBottom": "1px"},
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
    },
    dense: {
        marginTop: 16,
    },
    button: {
        backgroundColor: '#3b8cb8',
        '&:hover': {
            backgroundColor: '#146b9a',
        },
    },
    signInBtnStyle: {
        "width": "85%",
        "marginLeft": "8px",
        "marginTop": "10px",
        "height": "45px"
    },
    footerStyle: {
        position: 'absolute',
        bottom: '2px',
        paddingLeft: '20px',
        color: '#484848'
    },
    progress: {
        position: 'absolute',
        top: '45%',
        marginLeft: '8%'
    }
});

class LoginPage extends Component {
    state = {
        username: null,
        password: null,
        usernameError: false,
        usernameErrorMessage: null,
        passwordError: false,
        passwordErrorMessage: null
    };

    handleInputChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    };

    handleLoginProcess = (e) => {
        e.preventDefault();
        if (this.state.username === null || this.state.username.length < 1) {
            this.setState({
                usernameError: true,
                usernameErrorMessage: 'Username can not be blank ! '
            })
        } else if (this.state.username.length > 30) {
            this.setState({
                usernameError: true,
                usernameErrorMessage: 'Username can be max 30 character'
            })
        } else {
            this.setState({
                usernameError: false,
                usernameErrorMessage: null
            })
        }
        if (this.state.password === null || this.state.password.length < 1) {
            this.setState({
                passwordError: true,
                passwordErrorMessage: 'Password can not be blank ! '
            });
        } else if (this.state.password.length > 60) {
            this.setState({
                passwordError: true,
                passwordErrorMessage: 'Password can be max 60 character'
            })
        } else {
            this.setState({
                passwordError: false,
                passwordErrorMessage: null
            });
        }

        if (
            this.state.password &&
            this.state.username &&
            this.state.password.length < 60 &&
            this.state.username.length < 30
        ) {
            this.props.login({login: this.state.username, pass: this.state.password});
            this.setState({
                username: null,
                password: null
            })
        }
    };

    componentWillReceiveProps(nextProps, nextContext) {
        if (Object.keys(nextProps.auth_user.errors).length) {
            this.setState({
                usernameError: true,
                passwordError : true,
                passwordErrorMessage: nextProps.auth_user.errors.errorMsg
            })
        }
    }


    render() {
        const {classes} = this.props;
        if (this.props.auth_user.isLogin) {
            return <Redirect to={"/"}/>
        }
        return (
            <Grid container style={{
                height: '100%',
                position: 'absolute',
                left: 0,
                overflow: 'hidden',
            }}>
                <Grid item md={10} sm={10} xs={10} style={{
                    flexGrow: 0,
                    maxWidth: '80%',
                    flexBasis: '80%'

                }}>
                    <Grid item md={12} className={classes.titleDivStyle}>
                        <h3 className={classes.bgTitleStyle}>ProPass - Control System of Biometric Devices</h3>
                    </Grid>
                    <Grid className={classes.bgStyle} item md={12}>

                    </Grid>
                </Grid>
                <Grid item md={2} sm={2} xs={2} style={{
                    backgroundColor: 'white',
                    flexGrow: 0,
                    maxWidth: '20%',
                    flexBasis: '20%'
                }}>
                    {
                        this.props.auth_user.fetching ?
                            <CircularProgress className={classes.progress} size={70}/> :
                            <Fragment>
                                <Grid item md={12} style={{
                                    marginLeft: 30,
                                    marginTop: '80%'
                                }}>
                                    <Grid item md={12}>
                                        <img src={prospectLogo} alt="prospect logo" style={{
                                            marginLeft: 8
                                        }}/>
                                    </Grid>
                                    <Grid item md={12}>
                                        <TextField
                                            type="text"
                                            label="Username"
                                            className={classNames(classes.textField, classes.dense)}
                                            margin="dense"
                                            name={"username"}
                                            value={this.state.username}
                                            onChange={this.handleInputChange}
                                            error={this.state.usernameError}
                                            variant="outlined"
                                            autoComplete="off"
                                            style={{
                                                width: '85%'
                                            }}
                                        />
                                        <FormHelperText style={{marginLeft: 8, color: 'red'}}>
                                            {this.state.usernameErrorMessage}
                                        </FormHelperText>
                                    </Grid>
                                    <Grid item md={12}>
                                        <TextField
                                            type="password"
                                            label="Password"
                                            className={classNames(classes.textField, classes.dense)}
                                            margin="dense"
                                            variant="outlined"
                                            name={"password"}
                                            value={this.state.password}
                                            onChange={this.handleInputChange}
                                            error={this.state.passwordError}
                                            autoComplete="off"
                                            style={{
                                                width: '85%'
                                            }}
                                        />
                                        <FormHelperText style={{marginLeft: 8, color: 'red'}}>
                                            {this.state.passwordErrorMessage}
                                        </FormHelperText>
                                    </Grid>
                                    <Grid item md={12}>
                                        <Button variant="contained" color="secondary"
                                                className={classNames(classes.button, classes.signInBtnStyle)}
                                                onClick={this.handleLoginProcess}>
                                            Sign In
                                        </Button>
                                    </Grid>
                                </Grid>
                                <Grid item md={12} className={classes.footerStyle}>
                                    © PRONET MMC 2003 - 2019 <br/>
                                    <a href="https://prospect.az" target="_blank">www.prospect.az</a>
                                </Grid>
                            </Fragment>
                    }
                </Grid>
            </Grid>
        );
    }
}

LoginPage.propTypes = {};

const mapStateToProps = ({auth_user}) => {
    return {
        auth_user
    }
};

const mapDispatchToProps = {
    login
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(LoginPage));