import React, {Component} from 'react';
import PropTypes from 'prop-types';
import TableCreatorComponent from "../../components/TableCreatorComponent";
import { fetchSysUsers } from '../../actions/auth/login.action';
import {connect} from "react-redux";
import SysUsersTbody from "../tbodies/sys_users/SysUsersTbody";
import {withStyles} from "@material-ui/core";

const userHeaders = [
    { id : 0 , name : '№'},
    { id : 1 , name : 'Ad Soyad'},
    { id : 2 , name : 'Vəzifə'},
    { id : 3 , name : 'Rol'},
    { id : 4 , name : 'Əməliyyat'},
];

const styles = theme => ({
    tableBodyRowStyle: {
        padding: '4px 20px 4px 24px',
        textAlign: 'center'
    },
});

class SoftUsersPage extends Component {

    componentWillMount() {
        this.props.fetchSysUsers();
    }

    render() {
        const { classes } = this.props;
        return (
           <TableCreatorComponent
               headers={userHeaders}
               isHasToolBar={true}
               toolBarTitle={"İstifadəçilər"}
               data={this.props.sys_users}
               loadStatus={true}
               usersComponent={
                    <SysUsersTbody data={this.props.sys_users} classes={classes}/>
               }
           />
        );
    }
}

SoftUsersPage.propTypes = {};

const mapStateToProps = ({ sys_users }) => {
    return{
        sys_users
    }
};

const mapDispatchToProps = {
    fetchSysUsers
};

export default withStyles(styles)(connect(mapStateToProps,mapDispatchToProps)(SoftUsersPage));