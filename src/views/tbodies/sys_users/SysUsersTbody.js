import React, {Fragment} from 'react';
import TableRow from "../staffs/registeredUsers";
import TableCell from "@material-ui/core/TableCell";

const SysUsersTbody = ({ data , classes }) => {
    return (
        <Fragment>
            {
                data.total_data.length ? data.total_data.data.map((row, index) => {
                    return (
                        <TableRow key={index + 1} style={{
                            cursor : 'pointer'
                        }} onClick={() => alert()}>
                            <TableCell component="th" scope="row"
                                       className={classes.tableBodyRowStyle}>
                                {index + 1}
                            </TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>{row.created_at}</TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>-</TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>-</TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>-</TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>-</TableCell>

                        </TableRow>
                    );
                }) : ''
            }
        </Fragment>
    );
};

export default SysUsersTbody;