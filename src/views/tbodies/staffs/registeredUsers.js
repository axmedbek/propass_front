import React,{ Fragment } from 'react';
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";

const RegisteredUsers = ({ classes , data , handleRegisteredUserDetailed , activeuserDetailed }) => {
    return (
        // registeredStaffActiveRow
        <Fragment>
            {
                data.total_data.data.map((row, index) => {
                    return (
                        <TableRow key={index + 1} style={{
                            cursor : 'pointer'
                        }} onClick={(e) => handleRegisteredUserDetailed(e,row.userId)} className={activeuserDetailed === row.userId ? "unregistered_staff_active_row" : ""}>
                            <TableCell component="th" scope="row"
                                       className={classes.tableBodyRowStyle}>
                                {index + 1}
                            </TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>{row.userName}</TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>-</TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>-</TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>-</TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>-</TableCell>

                        </TableRow>
                    );
                })
            }
        </Fragment>
    );
};

export default RegisteredUsers;