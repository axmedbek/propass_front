import React,{ Fragment } from 'react';
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import Button from "@material-ui/core/Button/Button";
import AddUserIcon from "../../../assets/images/icons/add_user.svg";
import classNames from 'classnames';
import { NavLink } from 'react-router-dom';

const UnRegisteredUsers = ({ classes , data , handleClickRegisterUserFinger }) => {
    return (

        <Fragment>
            {
                data.total_data.data.map((row, index) => {
                    return (
                        <TableRow key={index + 1}>
                            <TableCell component="th" scope="row"
                                       className={classes.tableBodyRowStyle}>
                                {index + 1}
                            </TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>{row.userName}</TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>{row.ShirketName}</TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>{row.DepartamentName}</TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>{row.ShobeName}</TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>{row.BolmeName}</TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>{row.vezifeName}</TableCell>
                            <TableCell align="right"
                                       className={classes.tableBodyRowStyle}>
                                <NavLink exact to={"/staff/"+row.userId} style={{ textDecoration : 'none' , color : 'inherit' }}>
                                    <Button variant="contained"
                                            className={classNames(classes.button, classes.buttonRegistrationStyle, classes.tableBodyRowStyle)}>
                                        <img src={AddUserIcon} alt="add user icon"
                                             style={{
                                                 width: '20px'
                                             }}/>
                                    </Button>
                                </NavLink>
                            </TableCell>

                        </TableRow>
                    );
                })
            }
        </Fragment>
    );
};

export default UnRegisteredUsers;