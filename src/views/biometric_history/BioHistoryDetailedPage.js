import React, {Component, Fragment} from 'react';
import {withStyles} from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid/Grid";
import SearchIcon from '@material-ui/icons/Search';
import {CircularProgress, Tab, Tabs} from "@material-ui/core";
import classNames from 'classnames';
import Button from "@material-ui/core/Button";
import InputBase from "@material-ui/core/InputBase";
import {
    fetchAllRegisteredUsersWithFingers,
    getDevicesAndTransitionsWithFingerCode,
    getFingersWithUserId
} from "../../actions/fp/fb_bio_history.action";
import { connect } from "react-redux";
import {getFingerAndHandWithIndex} from "../../helper/standart";
import {NavLink} from "react-router-dom";
import BioHistoryDevicePage from "./content_pages/detailed_pages/BioHistoryDevicePage";
import BioHistoryLogPage from "./content_pages/detailed_pages/BioHistoryLogPage";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Badge from "@material-ui/core/Badge";
import bioHistoryDetailedPageStyle from "../../assets/jss/bioHistoryDetailedPageStyle";

class BioHistoryPage extends Component {

    state = {
        activeTab: 0,
        activeStaffId : 0,
        activeStaffFingerId : 0,
        activeFingerTemplate : null
    };

    handleChange = (event, value) => {
        this.setState({activeTab: value});
    };

    componentWillMount() {
        this.props.fetchAllRegisteredUsersWithFingers();
    }

    shouldComponentUpdate(nextProps, nextState, nextContext) {
        if (this.state.activeStaffId !== nextState.activeStaffId) {
            this.props.getFingersWithUserId(nextState.activeStaffId);
        }
        if (this.state.activeFingerTemplate !== nextState.activeFingerTemplate) {
            // if (nextProps.fingers_with_user_id.total_data.data !== undefined){
            //     this.setState({
            //         activeStaffFingerId : nextProps.fingers_with_user_id.total_data.data[0].id
            //     });
            // }
            this.props.getDevicesAndTransitionsWithFingerCode(nextState.activeFingerTemplate);
        }
        return true;
    }

    componentWillReceiveProps(nextProps, nextContext) {
        if (nextProps.registered_users_with_fingers.total_data.data !== undefined) {
            if (nextProps.registered_users_with_fingers.total_data.data.length > 0 && this.state.activeStaffId === 0) {
                this.setState({
                    activeStaffId : nextProps.registered_users_with_fingers.total_data.data[0].id
                });
            }
        }

        if (nextProps.fingers_with_user_id.total_data.data !== undefined){
            if (nextProps.fingers_with_user_id.total_data.data.length > 0 && this.state.activeStaffFingerId === 0){
                this.setState({
                    activeStaffFingerId : nextProps.fingers_with_user_id.total_data.data[0].id,
                    activeFingerTemplate : nextProps.fingers_with_user_id.total_data.data[0].fing_template,
                })
            }
        }
    }

    handleUserClickForFinger = (id) => {
        if (id !== this.state.activeStaffId){
            this.setState({
                activeStaffId : id,
                // activeStaffFingerId : 0
                // activeFingerTemplate : ''
            });
            // console.log(id);
            this.props.getFingersWithUserId(id);
        }
    };

    handleStaffFingerClick = (id,fing_template) => {
        if (id !== this.state.activeStaffFingerId) {
            this.setState({
                activeStaffFingerId : id,
                activeFingerTemplate : fing_template
            });
            this.props.getDevicesAndTransitionsWithFingerCode(fing_template);
        }
    };

    render() {
        const {classes} = this.props;
        const { data } = this.props.registered_users_with_fingers.total_data;
        return (
            <Fragment>
                {
                    this.props.registered_users_with_fingers.fetching ?
                        <CircularProgress className={classes.progress} size={70}/> :
                        <Grid container spacing={32} style={{height: '100%',margin : 0  }}>
                            <Grid item md={2} className={classes.bioHistoryDivStyle}>
                                <h4 className={classes.bioHistoryTitleStyle}>Biometrik Arxiv Ətraflı</h4>
                                <div className={classes.colDivider}/>
                                <div className={classes.bioHistorySideBarStyle}>
                                    <div className={classes.search}>
                                        <InputBase
                                            placeholder="Axtar…"
                                            classes={{
                                                root: classes.inputRoot,
                                                input: classes.inputInput,
                                            }}
                                        />
                                        <div className={classes.searchIcon}>
                                            <SearchIcon/>
                                        </div>
                                    </div>
                                </div>
                                <div className={classes.colDivider}/>
                                <div style={{
                                    padding: 22,
                                    color: '#7d7777'
                                }}>Cəmi əməkdaş sayı {data.length}</div>

                                {
                                    data.map((value, index) => (
                                        <Fragment>
                                            <div
                                                onClick={() => this.handleUserClickForFinger(value.id)}
                                                key={index} data-staff-id={value.id}
                                                className={this.state.activeStaffId === value.id ? classes.activeDeviceClass : classes.otherDevicesClass}>
                                                <div className={classes.deviceListElement}>
                                                    <div
                                                        className={classes.deviceNameStyle}>{value.username}</div>
                                                    <div>{`${value.position}`}</div>
                                                </div>
                                            </div>
                                            <div className={classes.colDivider}/>
                                        </Fragment>
                                    ))
                                }
                            </Grid>
                            <Grid item md={2} style={{
                                padding: '50px 0px',
                                backgroundColor: 'rgb(233, 233, 233)',
                            }}>
                                {
                                    this.props.fingers_with_user_id.fetching ?
                                        <CircularProgress className={classes.progressFingerSide} size={70}/> :
                                        <Fragment>
                                            <h4 className={classes.bioHistoryFingerTitleStyle}>Barmaqlar</h4>
                                            <div className={classes.colDivider}/>
                                            <div style={{
                                                padding: 22,
                                                color: '#7d7777'
                                            }}>Cəmi barmaq sayı {this.props.fingers_with_user_id.total_data.data.length}</div>

                                            {
                                                this.props.fingers_with_user_id.total_data.data.map((value, index) => (
                                                    <Fragment>
                                                        <div
                                                            onClick={() => this.handleStaffFingerClick(value.id,value.fing_template)}
                                                            key={index} data-device-id={value.id}
                                                            className={this.state.activeStaffFingerId === value.id ? classes.activeDeviceClass : classes.otherDevicesClass}
                                                            style={{ marginLeft : 0 , marginRight : 0 , height : 50 , paddingTop : 5}}
                                                        >
                                                            <div className={classes.deviceListElement}>
                                                                <div className={classes.deviceNameStyle} style={{ fontSize : '15px' }}>{`${getFingerAndHandWithIndex(value.finger_index).handName} - ${getFingerAndHandWithIndex(value.finger_index).fingerName}`}</div>
                                                            </div>
                                                        </div>
                                                        <div className={classes.colDivider}/>
                                                    </Fragment>
                                                ))
                                            }
                                        </Fragment>
                                }
                            </Grid>
                            <Grid item md={6} style={{
                                padding: 50,
                                paddingLeft: 0,
                                paddingRight: 0
                            }}>
                                <Grid container spacing={24}>
                                    <Grid item md={12} style={{ marginLeft: 100 }}>
                                        <Grid item md={4} className={classes.detailedDivStyle}>
                                            <NavLink to={"/biometric-history-and-events"} style={{textDecoration: 'none'}}>
                                                <Button variant="contained"
                                                        className={classNames(classes.button,classes.detailedBtnStyle, classes.buttonRegistrationStyle, classes.tableBodyRowStyle)}>
                                                    {/*<img src={EyeIcon} alt="detailed look icon" className={classes.detailedBtnImgStyle}/>*/}
                                                    <FontAwesomeIcon icon={"backspace"}/>
                                                    <span className={classes.detailedBtnTextStyle}>Geri qayıt</span>
                                                </Button>
                                            </NavLink>
                                        </Grid>
                                        <Grid item md={8} style={{
                                            position: 'absolute',
                                            marginLeft: '35px'
                                        }}>
                                            <Tabs
                                                value={this.state.activeTab}
                                                onChange={this.handleChange}
                                                indicatorColor="none"
                                                variant="fullWidth"
                                                style={{ float: 'right'}}
                                            >
                                                <Tab label={
                                                 <Fragment>
                                                     <Badge className={classes.padding} color="secondary" badgeContent={this.props.devices_and_transitions_with_finger_code.fetching ? '-' : this.props.devices_and_transitions_with_finger_code.total_data.data.data.length} classes={{ badge : classes.badge}}>
                                                         Qurğular
                                                     </Badge>
                                                     <span style={{ marginLeft: 15}}>-</span>
                                                     <Badge className={classes.padding} color="secondary" badgeContent={this.props.devices_and_transitions_with_finger_code.fetching ? '-' : this.props.devices_and_transitions_with_finger_code.total_data.data.transitionCount} classes={{ badge : classes.badge}}>
                                                         Keçidlər
                                                     </Badge>
                                                 </Fragment>
                                                }
                                                     className={this.state.activeTab === 0 ? classes.activeDeviceTabStyle : classes.otherDeviceTabStyle}/>
                                                <Tab label={
                                                    <Badge className={classes.padding} color="secondary" badgeContent={0} classes={{ badge : classes.badge}}>
                                                        Log (Tarixçə)
                                                    </Badge>
                                                }
                                                     className={this.state.activeTab === 1 ? classes.activeDeviceTabStyle : classes.otherDeviceTabStyle}/>
                                            </Tabs>
                                        </Grid>
                                    </Grid>
                                    <Grid item md={12} style={{ marginLeft: 40,marginTop: '10%',minWidth: '110%' }}>
                                        {
                                            this.state.activeTab === 0
                                                ? <BioHistoryDevicePage classes={classes} devices_and_transitions_with_finger_code={this.props.devices_and_transitions_with_finger_code}/>
                                                : <BioHistoryLogPage classes={classes}/>
                                        }
                                    </Grid>
                                </Grid>
                            </Grid>
                        </Grid>
                }
            </Fragment>
        );
    }
}

BioHistoryPage.propTypes = {};

const mapStateToProps =
    ({
         registered_users_with_fingers,
         fingers_with_user_id,
         devices_and_transitions_with_finger_code
    }) => {
    return {
        registered_users_with_fingers,
        fingers_with_user_id,
        devices_and_transitions_with_finger_code
    }
};

const mapDispatchToProps = {
    fetchAllRegisteredUsersWithFingers,
    getFingersWithUserId,
    getDevicesAndTransitionsWithFingerCode
};

export default withStyles(bioHistoryDetailedPageStyle)(connect(mapStateToProps,mapDispatchToProps)(BioHistoryPage));