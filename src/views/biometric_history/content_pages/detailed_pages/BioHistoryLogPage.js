import React from 'react';
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableRow from "@material-ui/core/TableRow";
import TableHead from "@material-ui/core/TableHead";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";

const BioHistoryLogPage = ({ classes }) => {
    return (
        <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell className={classes.headTitle}>#</TableCell>
                        <TableCell align="center" className={classes.headTitle}>Tarix</TableCell>
                        <TableCell align="center"
                                   className={classes.headTitle}>Əməkdaş</TableCell>
                        <TableCell align="center"
                                   className={classes.headTitle}>Qurğu</TableCell>
                        <TableCell align="center"
                                   className={classes.headTitle}>Keçid</TableCell>
                        <TableCell align="center"
                                   className={classes.headTitle}>İstiqamət</TableCell>
                        <TableCell align="center"
                                   className={classes.headTitle}>Səbəb</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    <TableRow>
                        <TableCell colSpan={7} align="center">
                            Hazırda göstəriləcək məlumat yoxdur
                        </TableCell>
                    </TableRow>
                </TableBody>
            </Table>
        </Paper>
    );
};

export default BioHistoryLogPage;