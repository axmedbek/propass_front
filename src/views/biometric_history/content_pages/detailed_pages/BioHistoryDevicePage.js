import React from 'react';
import {CircularProgress} from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import Table from "@material-ui/core/Table";

const BioHistoryDevicePage = ({ classes, devices_and_transitions_with_finger_code }) => {
    return (
        <Paper className={classes.root}>
            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        <TableCell className={classes.headTitle}>#</TableCell>
                        <TableCell align="center" className={classes.headTitle}>Qurğu</TableCell>
                        <TableCell align="center"
                                   className={classes.headTitle}>Keçid</TableCell>
                    </TableRow>
                </TableHead>
                {
                        devices_and_transitions_with_finger_code.fetching ?
                            <TableBody>
                                <TableRow>
                                    <TableCell colSpan={6}>
                                        <CircularProgress
                                            className={classes.progressTable}
                                            size={70}/>
                                    </TableCell>
                                </TableRow>
                            </TableBody> :
                            <TableBody>
                                {
                                    devices_and_transitions_with_finger_code.total_data.data.data.map((row, index) => (
                                        <TableRow
                                        key={row.id}
                                        onClick={() => alert()}
                                    >
                                        <TableCell component="th" scope="row">
                                            {index + 1}
                                        </TableCell>
                                        <TableCell
                                            align="center">{ row.device_name }</TableCell>
                                        <TableCell
                                            align="center">{ row.transition_name ? row.transition_name : 'Yoxdur' }</TableCell>
                                    </TableRow>
                                    ))
                                }
                            </TableBody>
                }
                <TableBody>

                </TableBody>
            </Table>
        </Paper>
    );
};

export default BioHistoryDevicePage;