import React, {Component} from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table/Table";
import {withStyles} from '@material-ui/core/styles';
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/es/TableBody/TableBody";


const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
        marginRight: 60
    },
    table: {
        minWidth: 700
    },
    headTitle: {
        top: "0",
        color: "white",
        position: "sticky",
        backgroundColor: "#55AB80",
        padding: "0px 20px 0px 23px"
    }
});

class BiometricEventLogPage extends Component {

    render() {
        const {classes} = this.props;

        return (
            <Grid item md={12}>
                <Paper className={classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell className={classes.headTitle}>#</TableCell>
                                <TableCell align="center" className={classes.headTitle}>Tarix</TableCell>
                                <TableCell align="center"
                                           className={classes.headTitle}>Əməkdaş</TableCell>
                                <TableCell align="center"
                                           className={classes.headTitle}>Əl</TableCell>
                                <TableCell align="center"
                                           className={classes.headTitle}>Barmağın adı</TableCell>
                                <TableCell align="center"
                                           className={classes.headTitle}>Kecid</TableCell>
                                <TableCell align="center"
                                           className={classes.headTitle}>Qurğu</TableCell>
                                <TableCell align="center"
                                           className={classes.headTitle}>İstiqamət</TableCell>
                                <TableCell align="center"
                                           className={classes.headTitle}>Səbəb</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow>
                                <TableCell colSpan={9} style={{textAlign: 'center'}}>
                                    Hazırda göstəriləcək məlumat yoxdur</TableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </Paper>
            </Grid>
        );
    }
}

BiometricEventLogPage.propTypes = {};


export default withStyles(styles)(BiometricEventLogPage);