import React, {Component, Fragment} from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table/Table";
import {withStyles} from '@material-ui/core/styles';
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import {CircularProgress} from "@material-ui/core";
import moment from "moment";
import connect from "react-redux/es/connect/connect";
import {fetchAllBioHistory, fetchBioHistoryById} from '../../../actions/fp/fb_bio_history.action';
import {Col, Label, Modal, ModalBody, ModalFooter, ModalHeader, Row} from "reactstrap";
import Button from "@material-ui/core/Button/Button";
import {getFingerAndHandWithIndex} from "../../../helper/standart";

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
        marginRight: 60
    },
    table: {
        minWidth: 500
    },
    progressTable: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '30%',
        marginTop: '5%',
        position: "absolute"
    },
    headTitle: {
        top: "0",
        color: "white",
        position: "sticky",
        backgroundColor: "#55AB80",
        padding: "0px 20px 0px 23px"
    },
    deviceInfoValue: {
        color: '#676664'
    },
});

class BiometricHistoryContentPage extends Component {

    state = {
        history_info_modal: false
    };

    historyInfoModalToggle = () => {
        this.setState({
            history_info_modal: !this.state.history_info_modal
        })
    };

    handleInfoBioHistory = id => {
        if (id > 0) {
            this.props.fetchBioHistoryById(id);
            this.setState({
                history_info_modal: true
            })
        }
    };

    componentWillMount() {
        this.props.fetchAllBioHistory();
    }

    render() {
        const {classes} = this.props;

        return (
            <Grid item md={12}>
                <Paper className={classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell className={classes.headTitle}>#</TableCell>
                                <TableCell align="center" className={classes.headTitle}>Qeydiyyat tarixi</TableCell>
                                <TableCell align="center"
                                           className={classes.headTitle}>Əməkdaş</TableCell>
                                <TableCell align="center"
                                           className={classes.headTitle}>Əl</TableCell>
                                <TableCell align="center"
                                           className={classes.headTitle}>Barmaq</TableCell>
                                <TableCell align="center"
                                           className={classes.headTitle}>Bİ kod tipi</TableCell>
                                <TableCell align="center"
                                           className={classes.headTitle}>Status</TableCell>
                            </TableRow>
                        </TableHead>
                        {
                            <TableBody>
                                {
                                    this.props.fp_bio_history_logs.fetching ?
                                        <TableBody>
                                            <TableRow>
                                                <TableCell colSpan={6}>
                                                    <CircularProgress
                                                        className={classes.progressTable}
                                                        size={70}/>
                                                </TableCell>
                                            </TableRow>
                                        </TableBody> :
                                        this.props.fp_bio_history_logs.total_data.data.data.map((row, index) => (
                                            <TableRow
                                                key={row.id}
                                                onClick={() => this.handleInfoBioHistory(row.id)}
                                            >
                                                <TableCell component="th" scope="row">
                                                    {index + 1}
                                                </TableCell>
                                                <TableCell
                                                    align="right">{moment(row.created_at).format('DD.MM.YYYY')}</TableCell>
                                                <TableCell
                                                    align="right">{row.username ? row.username : '-'}</TableCell>
                                                <TableCell
                                                    align="right">{getFingerAndHandWithIndex(row.finger_index).handName}</TableCell>
                                                <TableCell
                                                    align="right">{getFingerAndHandWithIndex(row.finger_index).fingerName}</TableCell>
                                                <TableCell
                                                    align="right">{row.sdk_type}</TableCell>
                                                <TableCell
                                                    align="right">Aktiv</TableCell>
                                            </TableRow>
                                        ))}
                            </TableBody>
                        }
                    </Table>
                    <Modal isOpen={this.state.history_info_modal}
                           toggle={this.historyInfoModalToggle}
                           className={"device-info-modal"}
                           style={{marginTop: '10%', borderRadius: '0px'}} size={"md"}>
                        <ModalHeader toggle={this.historyInfoModalToggle} classes={"device-detailed-title"}>Ətraflı baxış</ModalHeader>
                        <ModalBody style={{marginLeft: '10px'}}>
                            {this.props.fp_get_bio_history.fetching ? 'loading' :
                                <Fragment>
                                    <Row>
                                        <Col md={"4"}>
                                            <Label>Əməkdaş</Label>
                                        </Col>
                                        <Col md={"8"}>
                                            <Label
                                                className={classes.deviceInfoValue}>{this.props.fp_get_bio_history.total_data.data.username}</Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={"4"}>
                                            <Label>Tip</Label>
                                        </Col>
                                        <Col md={"8"}>
                                            <Label
                                                className={classes.deviceInfoValue}>{"Barmaq izi"}</Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={"4"}>
                                            <Label>Əl</Label>
                                        </Col>
                                        <Col md={"8"}>
                                            <Label
                                                className={classes.deviceInfoValue}>{getFingerAndHandWithIndex(this.props.fp_get_bio_history.total_data.data.finger_index).handName}</Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={"4"}>
                                            <Label>Barmağ adı</Label>
                                        </Col>
                                        <Col md={"8"}>
                                            <Label
                                                className={classes.deviceInfoValue}>{getFingerAndHandWithIndex(this.props.fp_get_bio_history.total_data.data.finger_index).fingerName}</Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={"4"}>
                                            <Label>Bİ kod növü</Label>
                                        </Col>
                                        <Col md={"8"}>
                                            <Label
                                                className={classes.deviceInfoValue}>{this.props.fp_get_bio_history.total_data.data.sdk_type}</Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={"4"}>
                                            <Label>Bİ kodu</Label>
                                        </Col>
                                        <Col md={"8"}>
                                            <Label
                                                className={classes.deviceInfoValue} style={{ wordBreak : 'break-all' }}>{this.props.fp_get_bio_history.total_data.data.fing_template}</Label>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col md={"4"}>
                                            <Label>Status</Label>
                                        </Col>
                                        <Col md={"8"}>
                                            <Label
                                                className={classes.deviceInfoValue}>{"Aktiv"}</Label>
                                        </Col>
                                    </Row>
                                </Fragment>
                            }
                        </ModalBody>
                        <ModalFooter>
                            <Button style={{
                                border: '1px solid #a5a4aa',
                                backgroundColor: 'white',
                                color: '#75747d',
                                borderRadius: '0',
                            }}
                                    onClick={this.historyInfoModalToggle}>Bağla</Button>
                        </ModalFooter>
                    </Modal>
                </Paper>
            </Grid>
        );
    }
}

BiometricHistoryContentPage.propTypes = {};

const mapStateToProps = ({fp_bio_history_logs, fp_get_bio_history}) => {
    return {
        fp_bio_history_logs,
        fp_get_bio_history
    }
};

const mapDispatchToProps = {
    fetchAllBioHistory,
    fetchBioHistoryById
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(BiometricHistoryContentPage));