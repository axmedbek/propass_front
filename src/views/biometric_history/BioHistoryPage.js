import React, {Component, Fragment} from 'react';
import {withStyles} from '@material-ui/core/styles';
import Grid from "@material-ui/core/Grid/Grid";
import BioHistoryFilterPage from "./filter_pages/BioHistoryFilterPage";
import BioEventsFilterPage from "./filter_pages/BioEventsFilterPage";
import BiometricHistoryContentPage from "./content_pages/BiometricHistoryContentPage";
import BiometricEventLogPage from "./content_pages/BiometricEventLogPage";
import {Tab, Tabs} from "@material-ui/core";
import EyeIcon from "../../assets/images/icons/eye.svg";
import classNames from 'classnames';
import Button from "@material-ui/core/Button";
import {NavLink} from "react-router-dom";
import SwipeableViews from "react-swipeable-views";
import TabContainer from "../../components/TabContainer";

const styles = theme => ({
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: 'white',
        '&:hover': {
            backgroundColor: 'white',
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {},
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        right: '-10px',
        cursor: 'pointer'
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
        height: 45
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit + 47, // 45
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10 - 65, // 15
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    componentParts: {
        padding: 40
    },
    colDivider: {
        "backgroundColor": 'white',
        "height": "1px",
        "marginLeft": "-5px",
        "marginRight": "-15px"
    },
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '50%',
        marginTop: '25%',
    },
    activeDeviceClass: {
        backgroundColor: '#66be99',
        color: 'white !important',
        lineHeight: '24px',
        fontSize: '16px',
        fontFamily: 'Fira Sans',
        paddingTop: 10,
        paddingBottom: 10,
        height: 80,
        marginLeft: '-5px',
        marginRight: '-15px'
    },
    deviceListElement: {
        padding: '10px 0px 10px 22px',
        cursor: 'pointer'
    },
    deviceNameStyle: {
        fontWeight: 'bold',
        fontSize: '18px'
    },
    otherDevicesClass: {
        color: '#4a4956',
        lineHeight: '24px',
        fontSize: '16px',
        fontFamily: 'Fira Sans',
        height: 80,
        marginLeft: '-5px',
        marginRight: '-15px'
    },
    activeDeviceTabStyle: {
        backgroundColor: '#66be98',
        color: 'white !important',
        borderRadius: '30px'
    },
    otherDeviceTabStyle: {
        backgroundColor: '#d9eee5',
        color: '#3b7c60',
        borderRadius: '30px',
        border: '1px solid #66be98',
        "fontFamily": "Fira Sans",
        "lineHeight": "16px",
        "fontSize": "14px",
    },
    tableBodyRowStyle: {
        padding: '4px 20px 4px 24px',
        textAlign: 'center'
    },
    buttonRegistrationStyle: {
        backgroundColor: '#FDE081',
        borderRadius: '4px',
        textTransform: 'capitalize',
        fontFamily: 'Fira Sans',
        lineHeight: '24px',
        fontSize: '14px',
        color: '#383D3E',
        '&:hover': {
            backgroundColor: '#fdd03e',
        }
    },
    detailedBtnStyle : {
        float: 'right',
        marginRight: -10,
        fontSize: 14,
        letterSpacing: '0.03em',
        color: '#383D3E',
        fontWeight : 'bold'
    },
    detailedBtnImgStyle : {
        width: '20px',
        marginLeft: '-12px',
    },
    detailedDivStyle : {
        float: 'right',
        marginRight: '12px',
        marginTop: '8px',
    },
    detailedBtnTextStyle : {
        marginLeft : 10
    }
});

class BioHistoryPage extends Component {

    state = {
        activeTab: 0
    };

    handleTabChange = (event, value) => {
        this.setState({activeTab: value});
    };

    handleTabChangeIndex = index => {
        this.setState({activeTab: index});
    };

    render() {
        const {classes,theme} = this.props;
        return (
            <Fragment>
                <Grid container spacing={32} style={{height: '100%',margin : 0}}>
                    <Grid item md={2} style={{
                        flexBasis: '21%',
                        maxWidth: '21%',
                        height: '100%',
                        backgroundColor: '#E9E9E9',
                    }}>
                        {
                            this.state.activeTab === 1 ?
                                <BioEventsFilterPage classes={classes} selected_date={null}/> :
                                <BioHistoryFilterPage classes={classes}/>
                        }
                    </Grid>
                    <Grid item md={9} style={{
                        padding: 50,
                        paddingLeft: 0,
                        paddingRight: 0,
                        marginLeft: 14
                    }}>
                        <Grid container spacing={24}>
                            <Grid item md={12}>
                                <Grid item md={4} className={classes.detailedDivStyle}>
                                    <NavLink to={"/biometric-history/detailed"} style={{textDecoration: 'none'}}>
                                    <Button variant="contained"
                                            className={classNames(classes.button,classes.detailedBtnStyle, classes.buttonRegistrationStyle, classes.tableBodyRowStyle)}>
                                        <img src={EyeIcon} alt="detailed look icon" className={classes.detailedBtnImgStyle}/>
                                        <span className={classes.detailedBtnTextStyle}>Ətraflı baxış</span>
                                    </Button>
                                    </NavLink>
                                </Grid>
                                <Grid item md={8}>
                                    <Tabs
                                        value={this.state.activeTab}
                                        onChange={this.handleTabChange}
                                        indicatorColor="none"
                                        variant="fullWidth"
                                        style={{ float: 'right'}}
                                    >
                                        <Tab label="Biometrik Arxiv"
                                             className={this.state.activeTab === 0 ? classes.activeDeviceTabStyle : classes.otherDeviceTabStyle}/>
                                        <Tab label="Hərəkət logları"
                                             className={this.state.activeTab === 1 ? classes.activeDeviceTabStyle : classes.otherDeviceTabStyle}/>
                                    </Tabs>
                                </Grid>
                            </Grid>
                            <Grid item md={12}>

                                    <SwipeableViews
                                        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
                                        index={this.state.activeTab}
                                        onChangeIndex={this.handleTabChangeIndex}

                                    >
                                        <TabContainer dir={theme.direction}>
                                            <BiometricHistoryContentPage/>
                                        </TabContainer>
                                        <TabContainer dir={theme.direction}>
                                            <BiometricEventLogPage/>
                                        </TabContainer>
                                    </SwipeableViews>
                            </Grid>
                        </Grid>
                    </Grid>
                </Grid>
            </Fragment>
        );
    }
}

BioHistoryPage.propTypes = {};

export default withStyles(styles,{ withTheme : true })(BioHistoryPage);