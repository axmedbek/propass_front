import React from 'react';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from "@material-ui/core/InputBase";
import ProSelect from "../../../components/ProSelect";
import makeAnimated from "react-select/lib/animated";

const BioHistoryFilterPage = ({classes}) => {
    return (
        <React.Fragment>
            <h4 style={{
                fontFamily: "'Fira Sans',sans-serif",
                lineHeight: '30px',
                fontSize: '22px',
                color: '#383D3E',
                fontWeight: 'bold',
                padding: '40px 141px 21px 32px',
            }}>Filter</h4>
            <div className={classes.colDivider}/>
            <div style={{
                paddingLeft: '22px',
                paddingTop: '28px',
                paddingBottom: '28px'
            }}>
                <div className={classes.search}>
                    <InputBase
                        placeholder="Axtar…"
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                    />
                    <div className={classes.searchIcon}>
                        <SearchIcon/>
                    </div>
                </div>
            </div>
            <div className={classes.colDivider}/>
            <div style={{
                padding: '15px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Əməkdaş"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="history_name"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Tenant"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="history_tenant"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Departament"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="history_department"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Söbə"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="history_department"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Bölmə"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="history_sectiont"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Vəzifə"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="history_position"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Bİ kodun tipi"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="history_code_type"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Qurğu"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="history_device"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Keçid"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="history_transition"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Status"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="history_status"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
        </React.Fragment>
    );
};

export default BioHistoryFilterPage;