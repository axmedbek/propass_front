import React from 'react';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from "@material-ui/core/InputBase";
import ProSelect from "../../../components/ProSelect";
import makeAnimated from "react-select/lib/animated";
import ProDatePicker from "../../../components/ProDatePicker";

const BioEventsFilterPage = ({ classes,selected_date }) => {
    return (
        <React.Fragment>
            <h4 style={{
                fontFamily: "'Fira Sans',sans-serif",
                lineHeight: '30px',
                fontSize: '22px',
                color: '#383D3E',
                fontWeight: 'bold',
                padding: '40px 141px 21px 32px',
            }}>Filter</h4>
            <div className={classes.colDivider}/>
            <div style={{
                paddingLeft: '22px',
                paddingTop: '28px',
                paddingBottom: '28px'
            }}>
                <div className={classes.search}>
                    <InputBase
                        placeholder="Axtar…"
                        classes={{
                            root: classes.inputRoot,
                            input: classes.inputInput,
                        }}
                    />
                    <div className={classes.searchIcon}>
                        <SearchIcon/>
                    </div>
                </div>
            </div>
            <div className={classes.colDivider}/>
            <div style={{
                padding: '0px 0px 3px 20px'
            }}>
                <ProDatePicker
                    // datepickerName={"selected_date"}
                    formatDate={"dd-MM-yyyy"}
                    selected_date={selected_date}
                    // handleChangeDate={(e) => this.handleChangeDate(e)}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Əməkdaş"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="event_name"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Tenant"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="event_tenant"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Departament"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="event_department"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Söbə"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="event_department"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Bölmə"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="event_sectiont"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Vəzifə"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="event_position"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"İtentifikasiya növü"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="event_auth_type"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Barmağın adı"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="event_finger_name"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Qurğu"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="event_device"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Keçid"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="event_transition"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"İstiqamət"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="history_direction"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
            <div style={{
                padding: '0px 0px 10px 20px'
            }}>
                <ProSelect
                    id={"select_components"}
                    className="menu-outer-top"
                    classNamePrefix="select"
                    isDisabled={false}
                    placeholder={"Səbəb"}
                    // isLoading={this.props.device_finger_operations.fetching}
                    isClearable={true}
                    isSearchable={true}
                    components={makeAnimated()}
                    name="history_reason"
                    // value={this.state.selected_operation}
                    // options={makeDataForFPOperationSelect(this.props.device_finger_operations.total_data.data)}
                    // onChange={this.handleChangeOperation}
                />
            </div>
        </React.Fragment>
    );
};

export default BioEventsFilterPage;