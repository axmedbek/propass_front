import {
    DELETE_ROLE_FULFILLED,
    DELETE_ROLE_PENDING, DELETE_ROLE_REJECTED,
    FETCH_ROLES_FULFILLED,
    FETCH_ROLES_PENDING,
    FETCH_ROLES_REJECTED, SAVE_OR_EDIT_ROLE_FULFILLED, SAVE_OR_EDIT_ROLE_PENDING, SAVE_OR_EDIT_ROLE_REJECTED
} from "../../actions/role/role.action_types";
import {removeData} from "../../helper/standart";

const initialState = {
    fetching: true,
    total_data: [],
    errors: {},
};

const total_data_reducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_ROLES_FULFILLED :
            return {
                ...state,
                total_data: action.payload,
                fetching: false
            };
        case FETCH_ROLES_REJECTED :
            return {
                ...state,
                errors: action.payload,
                fetching: false
            };
        case FETCH_ROLES_PENDING :
            return {
                ...state,
                fetching: true
            };
        case SAVE_OR_EDIT_ROLE_PENDING :
            return {
                ...state,
                fetching: true
            };
        case SAVE_OR_EDIT_ROLE_FULFILLED :
            state.total_data.data.push(action.payload);
            return {
                ...state,
                fetching: false
            };
        case SAVE_OR_EDIT_ROLE_REJECTED :
            return {
                ...state,
                errors: action.payload,
                fetching: false
            };
        case DELETE_ROLE_PENDING :
            return {
                ...state,
                fetching: true
            };
        case DELETE_ROLE_FULFILLED :
            return {
                total_data : {
                    ...state.total_data,
                    data : removeData(action.payload,state.total_data.data)
                },
            };
        case DELETE_ROLE_REJECTED :
            return {
                ...state,
                errors: action.payload,
                fetching: false
            };
        default :
            return state;
    }
};

export default total_data_reducer;