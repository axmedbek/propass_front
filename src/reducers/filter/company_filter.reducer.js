import {
    FETCH_DEPARTMENT_COMPANY_BY_SELECT_FULFILLED,
    FETCH_DEPARTMENT_COMPANY_BY_SELECT_PENDING,
    FETCH_DEPARTMENT_COMPANY_BY_SELECT_REJECTED,
} from "../../actions/device/device_action_types";

const initailState = {
    fetching : true,
    total_data : [],
    errors : {}
};


const total_data_reducer = (state = initailState , action ) => {
    switch (action.type)
    {
        case FETCH_DEPARTMENT_COMPANY_BY_SELECT_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false
            };
        case FETCH_DEPARTMENT_COMPANY_BY_SELECT_PENDING :
            return {
                ...state,
                errors : action.payload,
                fetching : false
            };
        case FETCH_DEPARTMENT_COMPANY_BY_SELECT_REJECTED :
            return {
                ...state,
                fetching : true
            };
        default :
            return state;
    }
};

export default total_data_reducer;