import {
    FETCH_DEPARTMENT_FILTER_BY_SELECT_FULFILLED,
    FETCH_DEPARTMENT_FILTER_BY_SELECT_REJECTED,
    FETCH_DEPARTMENT_FILTER_BY_SELECT_PENDING,
    REMOVE_DEPARTMENT_FILTER,
} from "../../actions/device/device_action_types";
import axios from "axios";

const initailState = {
    bashdirektorluq: {
        bashdirektorluq: [],
        bashdirektorluqFetching: false,
        bashdirektorluqErrors: {}
    },
    bolge: {
        bolge: [],
        bolgeFetching: false,
        bolgeErrors: {}
    },
    bolme: {
        bolme: [],
        bolmeFetching: false,
        bolmeErrors: {}
    },
    departament: {
        departament: [],
        departamentFetching: false,
        departamentErrors: {}
    },
    direktorluq: {
        direktorluq: [],
        direktorluqFetching: false,
        direktorluqErrors: {}
    },
    ishyeri: {
        ishyeri: [],
        ishyeriFetching: false,
        ishyeriErrors: {}
    },
    portfel: {
        portfel: [],
        portfelFetching: false,
        portfelErrors: {}
    },
    shirket: {
        shirket: [],
        shirketFetching: false,
        shirketErrors: {}
    },
    shobe: {
        shobe: [],
        shobeFetching: false,
        shobeErrors: {}
    }
};

// axios.get('http://172.16.123.4/VeyselOqlu/dev/api/general/structureTypes.php')
//     .then(result => {
//         result.data.data.map((value) => {
//             let stateData = {
//                 [value.tip.toLowerCase()]: [],
//                 [value.tip.toLowerCase() + "Fetching"]: false,
//                 [value.tip.toLowerCase() + "Errors"]: {},
//             };
//             initailState[value.tip.toLowerCase()] = stateData;
//             return null;
//         })
//     });


const total_data_reducer = (state = initailState, action) => {
    const typeData = action.type.split('|'),
        typePart1 = typeData[0],
        typePart2 = typeData[2] == null ? '' : typeData[2],
        type = typePart1 + typePart2;
    // console.log(typePart1+typePart2);
    switch (type) {
        case FETCH_DEPARTMENT_FILTER_BY_SELECT_FULFILLED :
            // console.log(state);
            const updatedFiltersForFullFilled = {
                ...state[typeData[1]],
                [typeData[1]]: action.payload,
                [typeData[1] + "Fetching"]: false
            };
            return {
                ...state,
                [typeData[1]]: updatedFiltersForFullFilled
            };
        case FETCH_DEPARTMENT_FILTER_BY_SELECT_REJECTED :
            // console.log(state);
            const updatedFiltersForRejected = {
                ...state[typeData[1]],
                [typeData[1] + "Errors"]: action.payload,
                [typeData[1] + "Fetching"]: false
            };
            return {
                ...state,
                [typeData[1]]: updatedFiltersForRejected
            };
        case FETCH_DEPARTMENT_FILTER_BY_SELECT_PENDING :
            // console.log(state);
            const updatedFiltersForPending = {...state[typeData[1]], [typeData[1] + "Fetching"]: true};
            return {
                ...state,
                [typeData[1]]: updatedFiltersForPending
            };
        case REMOVE_DEPARTMENT_FILTER :
            return{
                ...state,
                [action.payload.tip] : {
                    [action.payload.tip]: [],
                    [action.payload.tip+"Fetching"]: false,
                    [action.payload.tip+"Errors"]: {}
                }
            };
        default :
            return state;
    }
};

export default total_data_reducer;