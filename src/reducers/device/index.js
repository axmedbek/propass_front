export { default as devices_reducer  } from './devices.reducer';
export { default as department_filter_reducer  } from './department_filter.reducer';
export { default as get_device_reducer  } from './get_device.reducer';
export { default as get_device_with_destination_reducer  } from './device_with_destination.reducer';
