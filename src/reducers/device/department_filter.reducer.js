import {
    FETCH_DEPARTMENT_FILTER_FULFILLED, FETCH_DEPARTMENT_FILTER_PENDING,
    FETCH_DEPARTMENT_FILTER_REJECTED, REMOVE_SELECTED_DEPARTMENTS_FILTER, SELECTED_DEPARTMENTS_FILTER
} from "../../actions/device/device_action_types";
import {removeElementById} from "../../helper/standart";

const initailState = {
    fetching : true,
    total_data : [],
    selected_departments : [],
    errors : {}
};


const total_data_reducer = (state = initailState , action ) => {
    switch (action.type)
    {
        case FETCH_DEPARTMENT_FILTER_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false
            };
        case FETCH_DEPARTMENT_FILTER_REJECTED :
            return {
                ...state,
                errors : action.payload,
                fetching : false
            };
        case FETCH_DEPARTMENT_FILTER_PENDING :
            return {
                ...state,
                fetching : true
            };
        case SELECTED_DEPARTMENTS_FILTER : {
            return {
                ...state,
                selected_departments : [...state.selected_departments,action.payload]
            };
        }
        case REMOVE_SELECTED_DEPARTMENTS_FILTER : {
            return {
                ...state,
                selected_departments : removeElementById(state.selected_departments,action.payload)
            };
        }
        default :
            return state;
    }
};

export default total_data_reducer;