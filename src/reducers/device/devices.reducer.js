import {
    FETCH_DEVICE_FULFILLED,
    FETCH_DEVICE_PENDING,
    FETCH_DEVICE_REJECTED
} from "../../actions/device/device_action_types";

const initialState = {
    fetching : true,
    total_data : [],
    errors : {}
};

const total_data_reducer = (state = initialState , action ) => {
    switch (action.type) {
        case FETCH_DEVICE_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false
            };
        case FETCH_DEVICE_REJECTED :
            return {
                ...state,
                errors : action.payload,
                fetching : false
            };
        case FETCH_DEVICE_PENDING :
            return {
                ...state,
                fetching : true
            };
        default :
            return state;
    }
};

export default total_data_reducer;