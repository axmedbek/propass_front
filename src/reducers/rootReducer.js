import { combineReducers } from 'redux';
import { device_destinations_reducer,device_manufactures_reducer,device_sdk_types_reducer,device_types_reducer,
         device_finger_code_types_reducer,device_models_reducer,access_types_reducer, device_finger_operation } from './msk';
import { devices_reducer,department_filter_reducer,get_device_reducer,get_device_with_destination_reducer } from './device';
import { portfel_filter_reducer } from './filter';
import { fetch_staff_reducer,fetch_staff_info_reducer } from './staff';
import {fetch_transition_info_reducer, fetch_transition_reducer,add_transition_reducer} from "./transition";
import {fetch_fp_logs_reducer, fetch_bio_history_logs_reducer, get_bio_history_reducer,fetch_registered_users_history_reducer,
    fetch_fingers_with_user_id_reducer,fetch_devices_and_transitions_with_finger_code_reducer } from "./fp";
import {auth_user_reducer, sys_users_reducer} from "./auth";
import {fetch_all_roles_reducer} from "./role";

export default combineReducers({
    // msk
    device_types : device_types_reducer,
    device_finger_operations: device_finger_operation,
    device_destinations: device_destinations_reducer,
    device_manufactures : device_manufactures_reducer,
    device_sdk_types : device_sdk_types_reducer,
    device_finger_code_types : device_finger_code_types_reducer,
    device_models : device_models_reducer,
    //    access_types
    access_types : access_types_reducer,

//    devices
    devices : devices_reducer,
    department_filters : department_filter_reducer,
    devices_with_destinations : get_device_with_destination_reducer,


//    filters
    filter_portfels : portfel_filter_reducer,
    device : get_device_reducer,
    staffs : fetch_staff_reducer,
    staff :  fetch_staff_info_reducer,

// transitions
    transitions : fetch_transition_reducer,
    transition : fetch_transition_info_reducer,
    add_transition : add_transition_reducer,

    // fp
    fp_logs : fetch_fp_logs_reducer,
    fp_bio_history_logs: fetch_bio_history_logs_reducer,
    fp_get_bio_history: get_bio_history_reducer,
    registered_users_with_fingers : fetch_registered_users_history_reducer,
    fingers_with_user_id : fetch_fingers_with_user_id_reducer,
    devices_and_transitions_with_finger_code : fetch_devices_and_transitions_with_finger_code_reducer,

    // auth
    auth_user : auth_user_reducer,
    sys_users : sys_users_reducer,

    // roles
    roles : fetch_all_roles_reducer,
})