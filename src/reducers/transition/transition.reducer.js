import {
    FETCH_TRANSITIONS_FULFILLED,
    FETCH_TRANSITIONS_PENDING,
    FETCH_TRANSITIONS_REJECTED
} from "../../actions/transition/transition_action_types";

const initialState = {
    fetching : true,
    total_data : [],
    errors : {}
};

const total_data_reducer = (state = initialState , action ) => {
    switch (action.type) {
        case FETCH_TRANSITIONS_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false
            };
        case FETCH_TRANSITIONS_REJECTED :
            return {
                ...state,
                errors : action.payload,
                fetching : false
            };
        case FETCH_TRANSITIONS_PENDING :
            return {
                ...state,
                fetching : true
            };
        default :
            return state;
    }
};

export default total_data_reducer;