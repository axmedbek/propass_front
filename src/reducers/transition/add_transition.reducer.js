import {
    SAVE_OR_EDIT_TRANSITION_FULFILLED,
    SAVE_OR_EDIT_TRANSITION_PENDING,
    SAVE_OR_EDIT_TRANSITION_REJECTED
} from "../../actions/transition/transition_action_types";

const initialState = {
    fetching : true,
    total_data : [],
    errors : {}
};

const total_data_reducer = (state = initialState , action ) => {
    switch (action.type) {
        case SAVE_OR_EDIT_TRANSITION_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false
            };
        case SAVE_OR_EDIT_TRANSITION_REJECTED :
            return {
                ...state,
                errors : action.payload,
                fetching : false
            };
        case SAVE_OR_EDIT_TRANSITION_PENDING :
            return {
                ...state,
                fetching : true
            };
        default :
            return state;
    }
};

export default total_data_reducer;