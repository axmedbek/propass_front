import {
    FETCH_TRANSITION_BY_ID_FULFILLED,
    FETCH_TRANSITION_BY_ID_PENDING,
    FETCH_TRANSITION_BY_ID_REJECTED
} from "../../actions/transition/transition_action_types";


const initialState = {
    fetching : true,
    total_data : [],
    errors : {}
};

const total_data_reducer = (state = initialState , action ) => {
    switch (action.type) {
        case FETCH_TRANSITION_BY_ID_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false
            };
        case FETCH_TRANSITION_BY_ID_REJECTED :
            return {
                ...state,
                errors : action.payload,
                fetching : false
            };
        case FETCH_TRANSITION_BY_ID_PENDING :
            return {
                ...state,
                fetching : true
            };
        default :
            return state;
    }
};

export default total_data_reducer;