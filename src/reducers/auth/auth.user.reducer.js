import {
    CHECK_LOGIN_FULFILLED,
    CHECK_LOGIN_PENDING,
    CHECK_LOGIN_REJECTED,
    LOGOUT_PROCESS_FULFILLED, LOGOUT_PROCESS_PENDING, LOGOUT_PROCESS_REJECTED
} from "../../actions/auth/auth.action_types";

const initialState = {
    total_data : [],
    errors : {},
    fetching : false,
    isLogin : true
};

const total_data_reducer = (state = initialState , action) => {
    switch (action.type) {
        case CHECK_LOGIN_FULFILLED :
            localStorage.setItem('user',JSON.stringify(action.payload.data.user));
            return {
                ...state,
                total_data : action.payload,
                fetching : false,
                isLogin : action.payload.data.user !== undefined
            };
        case CHECK_LOGIN_REJECTED :
            return {
                ...state,
                errors : action.payload,
                fetching : false,
            };
        case CHECK_LOGIN_PENDING :
            return {
                ...state,
                fetching : true
            };
        case LOGOUT_PROCESS_FULFILLED :
            localStorage.removeItem('user');
            return {
                ...state,
                fetching : false,
                total_data : [],
                isLogin : false
            };
        case LOGOUT_PROCESS_REJECTED :
            return {
                ...state,
                errors : action.payload,
                fetching : false,
            };
        case LOGOUT_PROCESS_PENDING :
            return {
                ...state,
                fetching : true
            };
        default :
            return state
    }
};

export default total_data_reducer;