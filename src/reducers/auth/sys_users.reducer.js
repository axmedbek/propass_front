import {
    FETCH_SYS_USERS_FULFILLED,
    FETCH_SYS_USERS_PENDING,
    FETCH_SYS_USERS_REJECTED
} from "../../actions/auth/auth.action_types";

const initialState = {
    total_data : [],
    errors : {},
    fetching : false,
};

const total_data_reducer = (state = initialState , action) => {
    switch (action.type) {
        case FETCH_SYS_USERS_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false,
            };
        case FETCH_SYS_USERS_REJECTED :
            return {
                ...state,
                errors : action.payload,
                fetching : false,
            };
        case FETCH_SYS_USERS_PENDING :
            return {
                ...state,
                fetching : true
            };
        default :
            return state
    }
};

export default total_data_reducer;