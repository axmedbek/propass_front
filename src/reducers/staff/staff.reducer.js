import {FETCH_STAFFS_FULFILLED} from "../../actions/staff/staff_action_types";
import {FETCH_STAFFS_REJECTED} from "../../actions/staff/staff_action_types";
import {FETCH_STAFFS_PENDING} from "../../actions/staff/staff_action_types";

const initialState = {
    fetching : true,
    total_data : [],
    errors : {}
};

const total_data_reducer = (state = initialState , action ) => {
    switch (action.type) {
        case FETCH_STAFFS_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false
            };
        case FETCH_STAFFS_REJECTED :
            return {
                ...state,
                errors : action.payload,
                fetching : false
            };
        case FETCH_STAFFS_PENDING :
            return {
                ...state,
                fetching : true
            };
        default :
            return state;
    }
};

export default total_data_reducer;