import {
    FETCH_STAFF_FULFILLED,
    FETCH_STAFF_REJECTED,
    FETCH_STAFF_PENDING,
    FETCH_STAFF_REG_ID,
    FETCH_STAFF_REG_ID_FULFILLED,
    FETCH_STAFF_SELECTED_FINGERS_FULFILLED, FETCH_STAFF_FINGER_TEMPLATES_FULFILLED
} from "../../actions/staff/staff_action_types";


const initialState = {
    fetching : true,
    total_data : [],
    errors : {},
    regId : [],
    selected_fingers: [],
    finger_templates : []
};

const total_data_reducer = (state = initialState , action ) => {
    switch (action.type) {
        case FETCH_STAFF_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false
            };
        case FETCH_STAFF_REJECTED :
            return {
                ...state,
                errors : action.payload,
                fetching : false
            };
        case FETCH_STAFF_PENDING :
            return {
                ...state,
                fetching : true
            };
        case FETCH_STAFF_REG_ID_FULFILLED :
            return {
                ...state,
                regId : action.payload
            };
        case FETCH_STAFF_SELECTED_FINGERS_FULFILLED :
            return {
                ...state,
                selected_fingers : action.payload
            };
        case FETCH_STAFF_FINGER_TEMPLATES_FULFILLED :
            return {
                ...state,
                finger_templates : action.payload
            };
        default :
            return state;
    }
};

export default total_data_reducer;