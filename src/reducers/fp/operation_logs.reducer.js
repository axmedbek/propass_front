import {
    FETCH_OPERATION_LOGS_FULFILLED,
    FETCH_OPERATION_LOGS_PENDING,
    FETCH_OPERATION_LOGS_REJECTED
} from "../../actions/fp/fp_action_types";

const initialState = {
    fetching : true,
    total_data : [],
    errors : {}
};

const total_data_reducer = (state = initialState , action ) => {
    switch (action.type) {
        case FETCH_OPERATION_LOGS_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false
            };
        case FETCH_OPERATION_LOGS_REJECTED :
            return {
                ...state,
                errors : action.payload,
                fetching : false
            };
        case FETCH_OPERATION_LOGS_PENDING :
            return {
                ...state,
                fetching : true
            };
        default :
            return state;
    }
};

export default total_data_reducer;