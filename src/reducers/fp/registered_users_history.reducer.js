import {
    FETCH_REGISTERED_USERS_HISTORY_FULFILLED, FETCH_REGISTERED_USERS_HISTORY_PENDING,
    FETCH_REGISTERED_USERS_HISTORY_REJECTED
} from "../../actions/fp/fp_action_types";

const initialState = {
    fetching : true,
    total_data : [],
    errors : {}
};

const total_data_reducer = (state = initialState , action ) => {
    switch (action.type) {
        case FETCH_REGISTERED_USERS_HISTORY_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false
            };
        case FETCH_REGISTERED_USERS_HISTORY_REJECTED :
            return {
                ...state,
                errors : action.payload,
                fetching : false
            };
        case FETCH_REGISTERED_USERS_HISTORY_PENDING :
            return {
                ...state,
                fetching : true
            };
        default :
            return state;
    }
};

export default total_data_reducer;