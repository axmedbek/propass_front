import {
    FETCH_BIO_HISTORY_LOGS_FULFILLED,
    FETCH_BIO_HISTORY_LOGS_PENDING,
    FETCH_BIO_HISTORY_LOGS_REJECTED
} from "../../actions/fp/fp_action_types";

const initialState = {
    fetching : true,
    total_data : [],
    errors : {}
};

const total_data_bio_history = (state = initialState , action ) => {
    switch (action.type) {
        case FETCH_BIO_HISTORY_LOGS_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false
            };
        case FETCH_BIO_HISTORY_LOGS_REJECTED :
            return {
                ...state,
                errors : action.payload,
                fetching : false
            };
        case FETCH_BIO_HISTORY_LOGS_PENDING :
            return {
                ...state,
                fetching : true
            };
        default :
            return state;
    }
};

export default total_data_bio_history;