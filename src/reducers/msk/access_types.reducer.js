import {
    ADD_ACCESS_TYPE,
    CANCEL_ACCESS_TYPE,
    DELETE_ACCESS_TYPE_FULFILLED,
    DELETE_ACCESS_TYPE_PENDING,
    DELETE_ACCESS_TYPE_REJECTED,
    FETCH_ACCESS_TYPES_FULFILLED,
    FETCH_ACCESS_TYPES_PENDING,
    FETCH_ACCESS_TYPES_REJECTED,
    SAVE_OR_EDIT_ACCESS_TYPE_FULFILLED,
    SAVE_OR_EDIT_ACCESS_TYPE_PENDING,
    SAVE_OR_EDIT_ACCESS_TYPE_REJECTED
} from "../../actions/msk/msk_action_types";


const initialState = {
    fetching : true,
    total_data : [],
    errors : {}
};

const total_data_reducer = (state = initialState , action) => {
    switch (action.type) {
        // fetch all device types
        case FETCH_ACCESS_TYPES_FULFILLED :
            return {
                ...state,
                total_data: action.payload,
                fetching: false
            };
        case FETCH_ACCESS_TYPES_REJECTED :
            return {
                ...state,
                errors: action.payload,
                fetching : false
            };
        case FETCH_ACCESS_TYPES_PENDING :
            return {
                ...state,
                fetching : true
            };
        //    add device type
        case ADD_ACCESS_TYPE :
            state.total_data.data.push(action.payload);
            return {
                ...state,
                total_data : {
                    ...state.total_data,
                    data : state.total_data.data
                }
            };
        //     cancel device type
        case CANCEL_ACCESS_TYPE :
            return {
                ...state,
                total_data : {
                    ...state.total_data,
                    data : removeData(action.payload,state.total_data.data)
                }
            };
        //    save or edit device type
        case SAVE_OR_EDIT_ACCESS_TYPE_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false
            };
        case SAVE_OR_EDIT_ACCESS_TYPE_PENDING :
            return {
                ...state,
                fetching : true
            };
        case SAVE_OR_EDIT_ACCESS_TYPE_REJECTED :
            return {
                ...state,
                errors: action.payload,
                fetching : false
            };
        //    delete device type
        case DELETE_ACCESS_TYPE_FULFILLED :
            return {
                ...state,
                total_data : {
                    ...state.total_data,
                    data : removeData(action.payload.data.id,state.total_data.data)
                },
                fetching : false
            };
        case DELETE_ACCESS_TYPE_PENDING :
            return {
                ...state,
                fetching : true
            };
        case DELETE_ACCESS_TYPE_REJECTED :
            return {
                ...state,
                errors: action.payload,
                fetching : false
            };
        default :
            return state;
    }
};

function removeData(id,data){
    for(let i = 0; i < data.length; i++) {
        if(data[i].id === id) {
            data.splice(i, 1);
            return data;
        }
    }
}


export default total_data_reducer;
