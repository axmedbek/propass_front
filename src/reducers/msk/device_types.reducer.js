import {
    ADD_DEVICE_TYPE,
    CANCEL_DEVICE_TYPE, DELETE_DEVICE_TYPE_FULFILLED, DELETE_DEVICE_TYPE_PENDING, DELETE_DEVICE_TYPE_REJECTED,
    FETCH_DEVICE_TYPES_FULFILLED,
    FETCH_DEVICE_TYPES_PENDING,
    FETCH_DEVICE_TYPES_REJECTED,
    SAVE_OR_EDIT_DEVICE_TYPE_FULFILLED, SAVE_OR_EDIT_DEVICE_TYPE_PENDING, SAVE_OR_EDIT_DEVICE_TYPE_REJECTED
} from "../../actions/msk/msk_action_types";


const initialState = {
    fetching : true,
    total_data : [],
    errors : {}
};

const total_data_reducer = (state = initialState , action) => {
    switch (action.type) {
        // fetch all device types
        case FETCH_DEVICE_TYPES_FULFILLED :
            return {
                ...state,
                total_data: action.payload,
                fetching: false
            };
        case FETCH_DEVICE_TYPES_REJECTED :
            return {
                ...state,
                errors: action.payload,
                fetching : false
            };
        case FETCH_DEVICE_TYPES_PENDING :
            return {
                ...state,
                fetching : true
            };
        //    add device type
        case ADD_DEVICE_TYPE :
            state.total_data.data.push(action.payload);
             return {
                 ...state,
                 total_data : {
                     ...state.total_data,
                     data : state.total_data.data
                 }
             };
        //     cancel device type
        case CANCEL_DEVICE_TYPE :
            return {
            ...state,
            total_data : {
                ...state.total_data,
                data : removeData(action.payload,state.total_data.data)
            }
        };
        //    save or edit device type
        case SAVE_OR_EDIT_DEVICE_TYPE_FULFILLED :
            return {
                ...state,
                total_data : action.payload,
                fetching : false
            };
        case SAVE_OR_EDIT_DEVICE_TYPE_PENDING :
            return {
                ...state,
                fetching : true
            };
        case SAVE_OR_EDIT_DEVICE_TYPE_REJECTED :
            return {
                ...state,
                errors: action.payload,
                fetching : false
            };
        //    delete device type
        case DELETE_DEVICE_TYPE_FULFILLED :
            console.log(action.payload);
            return {
                ...state,
                total_data : {
                    ...state.total_data,
                    data : removeData(action.payload.id,state.total_data.data)
                },
                fetching : false
            };
        case DELETE_DEVICE_TYPE_PENDING :
            return {
                ...state,
                fetching : true
            };
        case DELETE_DEVICE_TYPE_REJECTED :
            return {
                ...state,
                errors: action.payload,
                fetching : false
            };
        default :
            return state;
    }
};

function removeData(id,data){
    for(let i = 0; i < data.length; i++) {
        if(data[i].id === id) {
            data.splice(i, 1);
            return data;
        }
    }
}


export default total_data_reducer;
