import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import Typography from '@material-ui/core/Typography';
import Badge from '@material-ui/core/Badge';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import {fade} from '@material-ui/core/styles/colorManipulator';
import {withStyles} from '@material-ui/core/styles';
import MailIcon from '@material-ui/icons/Mail';
import NotificationsIcon from '@material-ui/icons/Notifications';
import MoreIcon from '@material-ui/icons/MoreVert';
import logo from '../assets/images/full_logo.png';
import Avatar from "@material-ui/core/Avatar/Avatar";
import avatarUrl from '../assets/images/avatar_default.svg';
import companyUrl from '../icon-enterprise.png';
import notificationIcon from '../assets/images/icons/notification.svg';
import letterIcon from '../assets/images/icons/letter.svg';
import {connect} from "react-redux";
import {logoutProcess} from "../actions/auth/login.action";
import {Redirect} from "react-router-dom";
import {CircularProgress} from "@material-ui/core";
import moment from 'moment';
import 'moment/locale/az';

const styles = theme => ({
    root: {
        width: '100%',
    },
    grow: {
        flexGrow: 1,
    },
    menuButton: {
        marginLeft: -12,
        marginRight: 20,
    },
    title: {
        display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: fade(theme.palette.common.white, 0.15),
        '&:hover': {
            backgroundColor: fade(theme.palette.common.white, 0.25),
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing.unit * 3,
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit,
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10,
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    sectionDesktop: {
        display: 'none',
        [theme.breakpoints.up('md')]: {
            display: 'flex',
        },
    },
    sectionMobile: {
        display: 'flex',
        [theme.breakpoints.up('md')]: {
            display: 'none',
        },
    },
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '50%',
        marginTop: '25%',
    },
});

class Header extends React.Component {
    state = {
        anchorEl: null,
        mobileMoreAnchorEl: null,
    };

    handleProfileMenuOpen = event => {
        this.setState({anchorEl: event.currentTarget});
    };

    handleMenuClose = () => {
        this.setState({anchorEl: null});
        this.handleMobileMenuClose();
    };

    handleLogoutProfile = () => {
        this.props.logoutProcess();
        this.handleMenuClose();
    };

    handleMobileMenuOpen = event => {
        this.setState({mobileMoreAnchorEl: event.currentTarget});
    };

    handleMobileMenuClose = () => {
        this.setState({mobileMoreAnchorEl: null});
    };

    render() {
        if (!this.props.auth_user.isLogin) {
            return <Redirect to={"/login"}/>
        }
        const {anchorEl, mobileMoreAnchorEl} = this.state;
        const {classes} = this.props;
        const isMenuOpen = Boolean(anchorEl);
        const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

        const renderMenu = (
            <Menu
                anchorEl={anchorEl}
                anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                transformOrigin={{vertical: 'top', horizontal: 'right'}}
                open={isMenuOpen}
                onClose={this.handleMenuClose}
            >
                <MenuItem onClick={this.handleMenuClose}>Profile</MenuItem>
                <MenuItem onClick={this.handleLogoutProfile}>Logout</MenuItem>
            </Menu>
        );

        const renderMobileMenu = (
            <Menu
                anchorEl={mobileMoreAnchorEl}
                anchorOrigin={{vertical: 'top', horizontal: 'right'}}
                transformOrigin={{vertical: 'top', horizontal: 'right'}}
                open={isMobileMenuOpen}
                onClose={this.handleMobileMenuClose}
            >
                <MenuItem>
                    <IconButton color="inherit">
                        <Badge badgeContent={4} color="secondary">
                            <MailIcon/>
                        </Badge>
                    </IconButton>
                    <p>Messages</p>
                </MenuItem>
                <MenuItem>
                    <IconButton color="inherit">
                        <Badge badgeContent={11} color="secondary">
                            <NotificationsIcon/>
                        </Badge>
                    </IconButton>
                    <p>Notifications</p>
                </MenuItem>
                <MenuItem onClick={this.handleProfileMenuOpen}>
                    <IconButton color="inherit">
                        <Avatar alt="Profile Picture" src={avatarUrl}/>
                    </IconButton>
                    <p>Profile</p>
                </MenuItem>
            </Menu>
        );

        return (
            <Fragment>
                {this.props.auth_user.fetching
                    ? <div style={{
                        position:'absolute',
                        backgroundColor: '#423c3c59',
                        width: '100%',
                        height: '100%',
                        zIndex: '9999',
                    }}>
                        <CircularProgress className={classes.progress} size={70}/>
                    </div>
                    : ''
                }
                <AppBar position="fixed" style={{backgroundColor: '#6FC99C'}}>
                    <Toolbar>
                       <div style={{
                           position: 'absolute',
                           width: 96,
                           height: 64,
                           left: 0,
                           top: 0,
                           background: '#55AB80'
                       }}>
                           <Typography className={classes.title} variant="h6" color="inherit" noWrap>
                               <img src={logo} alt="" style={{
                                   position: 'absolute',
                                   width: 32,
                                   height: 36,
                                   left: 28,
                                   top: 14
                               }}/>
                           </Typography>
                       </div>
                        <div style={{
                            position: 'absolute',
                            left: 128
                        }}>
                            <div style={{
                                "fontFamily":"Fira Sans",
                                "lineHeight":"24px",
                                "fontSize":"16px",
                                "color":"#FFFFFF"
                            }}>Bugün, {moment().locale('az').format('LL')}
                            </div>
                            <div style={{
                                "fontFamily":"Fira Sans",
                                "lineHeight":"24px",
                                "fontSize":"14px",
                                "color":"#FFFFFF",
                                "mixBlendMode":"normal",
                                "opacity":"0.5"
                            }}>{ moment().locale('az').format('dddd') }</div>
                        </div>
                        <div className={classes.grow}/>
                        <div className={classes.sectionDesktop}>
                            <IconButton color="inherit">
                                <img src={notificationIcon} alt="notification icon"/>
                            </IconButton>
                            <IconButton color="inherit">
                                <img src={letterIcon} alt="letter icon"/>
                            </IconButton>
                            <IconButton
                                aria-owns={isMenuOpen ? 'material-appbar' : undefined}
                                aria-haspopup="true"
                                onClick={this.handleProfileMenuOpen}
                                color="inherit"
                            >
                                <Avatar alt="Profile Picture" src={avatarUrl}/>
                            </IconButton>
                            <IconButton
                                aria-owns={isMenuOpen ? 'material-appbar' : undefined}
                                aria-haspopup="true"
                                onClick={this.handleProfileMenuOpen}
                                color="inherit"
                            >
                                <Avatar alt="Profile Picture" src={companyUrl} style={{
                                    width: '32px',
                                    height: '32px'
                                }}/>
                            </IconButton>
                        </div>
                        <div className={classes.sectionMobile}>
                            <IconButton aria-haspopup="true" onClick={this.handleMobileMenuOpen}
                                        color="inherit">
                                <MoreIcon/>
                            </IconButton>
                        </div>
                    </Toolbar>
                </AppBar>
                {renderMenu}
                {renderMobileMenu}
            </Fragment>
        );
    }
}

Header.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({auth_user}) => {
    return {
        auth_user
    }
};

const mapDispatchToProps = {
    logoutProcess,
};

export default withStyles(styles)(connect(mapStateToProps, mapDispatchToProps)(Header));