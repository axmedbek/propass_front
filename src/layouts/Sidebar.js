import React, {Fragment} from 'react';
import PropTypes from 'prop-types';
import {withStyles} from '@material-ui/core/styles';
import {List,Collapse} from '@material-ui/core';
import '../assets/css/sidebar.css';
import NavLink from "react-router-dom/es/NavLink";
import propassRoutes from "../routes/propass";
import Grid from "@material-ui/core/Grid";

const styles = theme => ({
    root: {
        width: '100%',
        maxWidth: 360,
        height: '100%',
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    },
    active: {
        backgroundColor: '#17191a',
        width: 96,
        marginLeft: '-16px',
        marginTop: '-8px',
        borderLeft: '4px solid #55ab80',
    },
    menuItemStyle: {
        "fontFamily":"Fira Sans",
        "lineHeight":"14px",
        "fontSize":"13px",
        "textAlign":"center",
        "color":"#FFFFFF",
        "mixBlendMode":"normal",
        "opacity":"0.5",
        "marginTop": 4,
        "marginBottom": 8,
    },
    parentMenuStyle : {
        "backgroundColor":"#17191a !important",
        "width":"96px",
        // "marginLeft":"-16px",
        "marginTop":"-4px",
        "border":"2px solid #55ab80",
        "paddingTop":"4px",
        "marginBottom":"4px"
    }
});

class Sidebar extends React.Component {
    state = {
        open: false,
    };

    handleClick = (prop) => {
        this.setState(state => ({[prop]: state ? !state[prop] : true}));
    };

    render() {
        const {classes} = this.props;

        return (
            <List
                component="nav"
                className={classes.root}
            >
                {propassRoutes.map((prop, key) => {
                    if (prop.isMenu) {
                        if (prop.hasSubMenu) {
                            const icon = require(`../assets/images/icons/sidebar/${prop.icon}.svg`);
                            return (
                                <Fragment key={key}>

                                    <Grid container onClick={() => this.handleClick(prop.sidebarName.toLowerCase())}
                                          style={{ cursor : 'pointer'}}
                                          className={this.state[prop.sidebarName.toLowerCase()] ? classes.parentMenuStyle : ''}>
                                        <Grid item md={12} style={{ "textAlign" : "center"}}>
                                            <img src={icon} alt={`${prop.sidebarName.toLowerCase()} icon`} style={{ width : 32 }}/>
                                        </Grid>
                                        <Grid item md={12} className={classes.menuItemStyle}>
                                            {prop.sidebarName}
                                        </Grid>
                                    </Grid>

                                    <Collapse in={this.state[prop.sidebarName.toLowerCase()]} timeout="auto" unmountOnExit>
                                        <List component="div" disablePadding>
                                            {
                                                prop.sub_menus.map((sub_prop, sub_key) => {
                                                    const icon = require(`../assets/images/icons/sidebar/${sub_prop.icon}.svg`);
                                                    return (
                                                        <NavLink exact to={sub_prop.path}
                                                                 style={{textDecoration: 'none', color: 'inherit'}}
                                                                 key={key + sub_key}>
                                                            <Grid container>
                                                                <Grid item md={12} style={{ "textAlign" : "center"}}>
                                                                    <img src={icon} alt={`${sub_prop.sidebarName.toLowerCase()} icon`} style={{ width : 20 }}/>
                                                                </Grid>
                                                                <Grid item md={12} className={classes.menuItemStyle} style={{ fontSize : 10}}>
                                                                    {sub_prop.sidebarName}
                                                                </Grid>
                                                            </Grid>
                                                        </NavLink>
                                                    );
                                                })
                                            }
                                            <div style={{marginTop: 20}}/>
                                        </List>
                                    </Collapse>
                                </Fragment>
                            );
                        } else {
                            const icon = require(`../assets/images/icons/sidebar/${prop.icon}.svg`);
                            return (
                                <NavLink exact to={prop.path} style={{textDecoration: 'none', color: 'inherit'}}
                                         key={key}>
                                    <Grid container>
                                        <Grid item md={12} style={{ "textAlign" : "center"}}>
                                            <img src={icon} alt={`${prop.sidebarName.toLowerCase()} icon`} style={{ width : 32 }}/>
                                        </Grid>
                                        <Grid item md={12} className={classes.menuItemStyle}>
                                            {prop.sidebarName}
                                        </Grid>
                                    </Grid>
                                </NavLink>
                            );
                        }
                    }
                    return null;
                })}
            </List>
        );
    }
}

Sidebar.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Sidebar);