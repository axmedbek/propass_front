// views
import {
    AllDevicesPage,
    DashboardPage,
    DeviceDestination,
    DeviceFingerCodeTypes,
    DeviceManufactures,
    DevicePage,
    DeviceSdkTypes,
    DeviceTypes,
    NewDevicePage,
    registerStaffPage,
    StaffPage,
    SurveillancePage,
    PageNotFound,
    AccessType,
    AccessPage,
    NewTransitionPage,
    FpProcesses,
    DevMonitorinq,
    DeviceFingerOperation,
    BioHistoryPage,
    BioHistoryDetailedPage,
    SoftUsersPage,
    UserRolesPage
} from "../views";

const propassRoutes = [
    {
        path: "/",
        sidebarName: "Dashboard",
        icon: 'dashboard',
        component: DashboardPage,
        hasSubMenu : false,
        isMenu : true,
        route_name : 'dashboard'
    },
    {
        path: "/devices",
        sidebarName: "Qurğular",
        icon: 'finger',
        component: DevicePage,
        hasSubMenu : false,
        isMenu : true,
        route_name : 'devices'
    },
    {
        path: "/surveillance",
        sidebarName: "Nəzarət",
        icon: 'check',
        component: SurveillancePage,
        hasSubMenu : false,
        isMenu : true,
        route_name : 'surveillance'
    },
    {
        path: "/staffs",
        sidebarName: "Əməkdaşlar",
        icon: 'staff',
        component: StaffPage,
        hasSubMenu : false,
        isMenu : true,
        route_name : 'staffs'
    },
    {
        path: "/canteen",
        sidebarName: "Yeməkxana",
        icon: 'canteen',
        component: SurveillancePage,
        hasSubMenu : false,
        isMenu : true,
        route_name : 'canteen'
    },
    {
        path: "/permission-issuance",
        sidebarName: "Buraxılış",
        icon: 'access',
        component: SurveillancePage,
        hasSubMenu : false,
        isMenu : true,
        route_name : 'permission.issuance'
    },
    {
        path: "/transition",
        sidebarName: "Keçidlər",
        icon: 'exchange',
        component: AccessPage,
        hasSubMenu : false,
        isMenu : true,
        route_name : 'transition'
    },
    {
        path: "/device-trouble",
        sidebarName: "Device Trouble",
        icon: 'device_trouble',
        component: SurveillancePage,
        hasSubMenu : false,
        isMenu : true,
        route_name : 'device.trouble'
    },
    {
        path: "/crossing-right",
        sidebarName: "Hüquqlar",
        icon: 'privilegia',
        component: SurveillancePage,
        hasSubMenu : false,
        isMenu : true,
        route_name : 'crossing.right'
    },
    {
        path: "/biometric-history-and-events",
        sidebarName: "Biometrik arxiv/Hərəkət logları",
        icon: 'finger',
        component: BioHistoryPage,
        hasSubMenu : false,
        isMenu : true,
        route_name : 'biometric.history.and.events'
    },
    {
        path: "/soft-users",
        sidebarName: "İstifadəçilər",
        icon: 'staff',
        component: SoftUsersPage,
        hasSubMenu : false,
        isMenu : true,
        route_name : 'soft.users'
    },
    {
        path: "/user-roles",
        sidebarName: "Rollar",
        icon: 'privilegia',
        component: UserRolesPage,
        hasSubMenu : false,
        isMenu : true,
        route_name : 'user.roles'
    },
    {
        path: "/reports",
        sidebarName: "Hesabatlar",
        icon: 'report',
        component: SurveillancePage,
        hasSubMenu : false,
        isMenu : true,
        route_name : 'reports'
    },
    {
        path: "/device/new",
        component: NewDevicePage,
        hasSubMenu : false,
        isMenu : false,
        route_name : 'device.new'
    },
    {
        path: "/transition/new",
        component: NewTransitionPage,
        hasSubMenu : false,
        isMenu : false,
        route_name : 'transition.new'
    },
    {
        path: "/devices/all",
        component: AllDevicesPage,
        hasSubMenu : false,
        isMenu : false,
        route_name : 'devices.all'
    },
    {
        path: "/biometric-history/detailed",
        component: BioHistoryDetailedPage,
        hasSubMenu : false,
        isMenu : false,
        route_name : 'biometric.history.detailed'
    },
    {
        path: "/staff/:user_id",
        component: registerStaffPage,
        hasSubMenu : false,
        isMenu : false,
        route_name : 'staff.user'
    },
    {
        path: "/404",
        component: PageNotFound,
        hasSubMenu : false,
        isMenu : false,
        route_name : '404'
    },
    {
        path: null,
        sidebarName: "Development Menu",
        icon: 'device_trouble',
        component: null,
        hasSubMenu : true,
        isMenu : true,
        route_name : 'dev.menu',
        sub_menus : [
                {
                    path: "/dev/devices/monitorinq",
                    sidebarName: "Qurğu logları",
                    icon: 'addition',
                    component: DevMonitorinq,
                    hasSubMenu : false,
                    isMenu : true,
                    route_name : 'dev.devices.monitorinq'
                },
                {
                    path: "/dev/fp/process",
                    sidebarName: "fp prosesləri",
                    icon: 'addition',
                    component: FpProcesses,
                    hasSubMenu : false,
                    isMenu : true,
                    route_name : 'dev.fp.process'
                }
            ]
    },
    {
        path: null,
        sidebarName: "Ayarlar",
        icon: 'setting',
        component: null,
        hasSubMenu : true,
        isMenu : true,
        route_name : 'msk',
        sub_menus : [
            {
                path: "/msk/device/manufactures",
                sidebarName: "Qurğu istehsalçıları",
                icon: 'addition',
                component: DeviceManufactures,
                hasSubMenu : false,
                isMenu : true,
                route_name : 'msk.device.manufactures'
            },
            {
                path: "/msk/device/types",
                sidebarName: "Qurğunun təyinatı",
                icon: 'search',
                component: DeviceTypes,
                hasSubMenu : false,
                isMenu : true,
                route_name : 'msk.device.types'
            },
            {
                path: "/msk/device/destination",
                sidebarName: "İstiqamətlər",
                icon: 'exchange',
                component: DeviceDestination,
                hasSubMenu : false,
                isMenu : true,
                route_name : 'msk.device.destination'
            },
            {
                path: "/msk/device/sdk-types",
                sidebarName: "SDK növləri",
                icon: 'integration',
                component: DeviceSdkTypes,
                hasSubMenu : false,
                isMenu : true,
                route_name : 'msk.device.sdk.types'
            },
            {
                path: "/msk/device/finger-code-types",
                sidebarName: "Barmaq izi kod tipləri",
                icon: 'finger',
                component: DeviceFingerCodeTypes,
                hasSubMenu : false,
                isMenu : true,
                route_name : 'msk.device.finger.code.types'
            },
            {
                path: "/msk/access/access-types",
                sidebarName: "Keçid tipləri",
                icon: 'exchange',
                component: AccessType,
                hasSubMenu : false,
                isMenu : true,
                route_name : 'msk.device.access.types'
            },
            {
                path: "/msk/device/fp-operation",
                sidebarName: "Fp Əməliyyatlar",
                icon: 'addition',
                component: DeviceFingerOperation,
                hasSubMenu : false,
                isMenu : true,
                route_name : 'msk.device.fp.operation'
            },
        ]
    },
];

export default propassRoutes;