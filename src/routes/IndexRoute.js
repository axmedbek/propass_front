import {DashboardPage} from "../views";

const indexRoute = [{ path: "/", component: DashboardPage }];

export default indexRoute;