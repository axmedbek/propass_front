import React from 'react';
import { Modal , ModalBody , ModalHeader } from "reactstrap";

const ProPassModal = ({ isOpen,handleModalToggle,modalSize,modalHeaderText,modalClassName,component }) => {
    return (
        <Modal isOpen={isOpen}
               toggle={handleModalToggle}
               size={modalSize}
               className={modalClassName}
               style={{ marginTop: '10%' }}>
            <ModalHeader toggle={handleModalToggle}>{modalHeaderText}</ModalHeader>
            <ModalBody>
                {
                    component
                }
            </ModalBody>
        </Modal>
    );
};

export default ProPassModal;