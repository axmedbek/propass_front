import React, {Fragment} from 'react';
import {Helmet} from "react-helmet";
import {Grid} from "@material-ui/core";
import ProPassLoading from "./ProPassLoading";
import CircularProgress from "@material-ui/core/es/CircularProgress/CircularProgress";

const PageCreatorHoc = (WrappedComponent,title,dataName) => {
    class PageCreatorHOC extends React.Component {
        render() {
            console.log(this.props);
            return (
                <Fragment>
                    <Helmet>
                        <title>{title}</title>
                    </Helmet>
                    {
                        this.props[dataName].fetching ?
                            <Grid container spacing={32} style={{ padding : 45 }}>
                                <CircularProgress style={{
                                    color: '#63c79d',
                                    marginLeft: '50%',
                                    marginTop: '25%',
                                }} size={70} />
                            </Grid> : ''
                    }
                    <Grid container spacing={32} style={
                        this.props[dataName].fetching ?
                            { display : 'none'} :
                            { display : 'block', padding : 45}
                    }>
                        <WrappedComponent {...this.props}/>
                    </Grid>
                </Fragment>
            )
        }
    }

    return PageCreatorHOC;
};

export default PageCreatorHoc;
