import React from 'react';
import MskTableCreatorComponent from "../MskTableCreatorComponent";

const MskTableLoaderHoc = (WrappedComponent) => {
    return class MskTableLoaderHOC extends React.Component{
        render(){
            return(
              <MskTableCreatorComponent/>
            );
        }
    }
};

export default MskTableLoaderHoc;