import React from 'react';

const tableCreatorComponent = (WrappedComponent) => {
    class HOC extends React.Component{
        render(){
            return <WrappedComponent {...this.props} />
        }
    }
    return HOC;
};