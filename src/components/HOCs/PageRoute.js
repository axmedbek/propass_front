import {Col, Row} from "reactstrap";
import {Header, Sidebar} from "../../layouts";
import React, {Fragment} from "react";
import Grid from "@material-ui/core/Grid";

const PageRoute = (WrappedComponent) => {
    class PageRoute extends React.Component {
        render() {
            return (
                <Grid container style={{marginLeft: '-15px',height : '100%'}}>
                    <Grid item md={12} style={{ position : 'absolute'}}>
                        <Header/>
                    </Grid>
                   <Grid container>
                       <Grid item md={1} style={{backgroundColor: '#373737', marginTop: 64,maxWidth  : 96 }}>
                           <Sidebar/>
                       </Grid>
                       <Grid item md={11} style={{marginTop: 64,maxWidth  : 'calc(100% - 96px)'}}>
                           <WrappedComponent {...this.props} />
                       </Grid>
                   </Grid>
                </Grid>
            );
        }
    }

    return PageRoute;
};

export default PageRoute;