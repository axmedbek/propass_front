import React, { Fragment } from 'react';
import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table/Table";
import classNames from 'classnames';
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import {CircularProgress, withStyles} from "@material-ui/core";
import Grid from "@material-ui/core/Grid/Grid";
import MskToolBarComponent from "./MskToolBarComponent";

const tableCreatorStyle = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
        overflowY : 'auto',
        height : 470
    },
    table: {
        minWidth: 700,
    },
    tableRowStyle: {
        backgroundColor: '#55AB80',
        // borderRadius: '4px',
        color: 'white',
        position: "sticky",
        top: 0,
        // zIndex : 9999
    },
    tableBodyRowStyle: {
        padding: '4px 20px 4px 24px',
        textAlign: 'center'
    },
    hasTablePage : {
        padding: '40px',
        marginLeft: '-15px'
    }
});


const TableCreatorComponent = ({
                                   classes,
                                   headers,
                                   data,
                                   loadStatus,
                                   usersComponent ,
                                   isActiveuserDetailed ,
                                   detailedData ,
                                   userDetailedComponent,
                                   handleAddNewRow ,
                                   toolBarTitle,
                                   isHasToolBar
}) => {
    return (
        <Fragment>
            <Grid item md={isActiveuserDetailed ? 8 : 12} className={isHasToolBar ? classes.hasTablePage : ''}>
                <Paper className={classNames(classes.root, "registeredTable")} style={{
                    marginLeft: '10px',
                    height: '470px !important',
                    overflow: 'auto !important',
                }}>
                    {
                        isHasToolBar
                            ? <MskToolBarComponent classes={classes} toolBarTitle={toolBarTitle} handleAddNewRow={handleAddNewRow}/>
                            : ''
                    }

                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                {
                                    headers.map((value, key) => (
                                        key === 0
                                            ? <TableCell key={key} className={classes.tableRowStyle}>№</TableCell>
                                            :
                                            <TableCell key={key} align="right"
                                                       className={classes.tableRowStyle}>{value.name}</TableCell>
                                    ))
                                }
                            </TableRow>
                        </TableHead>
                        {
                            loadStatus
                                ? data.fetching
                                ? <TableBody>
                                    <TableRow>
                                        <TableCell colSpan={headers.length} style={{textAlign: 'center'}}>
                                            <CircularProgress className={classes.tableProgress} size={40}/>
                                        </TableCell>
                                    </TableRow>
                                </TableBody>
                                : <TableBody>
                                    {
                                        usersComponent
                                    }
                                    {
                                        data.total_data.data < 1 &&

                                        <TableRow>
                                            <TableCell colSpan={headers.length} style={{textAlign: 'center'}}>
                                                Hazırda göstəriləcək məlumat yoxdur</TableCell>
                                        </TableRow>

                                    }
                                </TableBody>
                                :
                                <TableBody>
                                    <TableRow>
                                        <TableCell colSpan={headers.length} style={{textAlign: 'center'}}>
                                            Hazırda göstəriləcək məlumat yoxdur</TableCell>
                                    </TableRow>
                                </TableBody>
                        }
                    </Table>
                </Paper>
            </Grid>
            <Grid item md={4} style={{
                display : isActiveuserDetailed ? "block" : "none"
            }}>
                {
                    userDetailedComponent
                }

            </Grid>
        </Fragment>
    );
};

export default withStyles(tableCreatorStyle)(TableCreatorComponent);