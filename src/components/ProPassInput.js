import React from 'react';
import {FormGroup, Input, Label} from "reactstrap";

const ProPassInput = ({ hasLabel , labelText , inputType , inputName , inputId , placeHolderText,inputValue , ...rest }) => {
    return (
        <FormGroup>
            {
                labelText ?  <Label for={inputId} >{labelText}</Label> : ''
            }
            <Input
                {...rest}
                type={inputType ? inputType : "text"}
                name={inputName}
                id={inputId}
                placeholder={placeHolderText ? placeHolderText : labelText}
                value={inputValue}
            />
        </FormGroup>
    );
};

export default ProPassInput;