import React, {Fragment} from 'react';
import {Button, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle} from "@material-ui/core";

const ProPassDialog = ({ fullScreen , is_open , message_title , message_content , handleDeleteProcess , handleToggleProcess , isInfo }) => {

    return (
        <Dialog
            fullScreen={fullScreen}
            open={is_open}
            onClose={handleToggleProcess}
            aria-labelledby="responsive-dialog-title"
        >
            <DialogTitle
                id="responsive-dialog-title">{message_title}</DialogTitle>
            <DialogContent>
                <DialogContentText>
                    {message_content}
                </DialogContentText>
            </DialogContent>
            <DialogActions>
                {
                    isInfo ?
                        <Fragment>
                            <Button onClick={handleToggleProcess} color="primary">
                                Bağla
                            </Button>
                        </Fragment>
                        :   <Fragment>
                            <Button
                                onClick={handleDeleteProcess}
                                color="primary" autoFocus>
                                Bəlİ
                            </Button>
                            <Button onClick={handleToggleProcess} color="primary">
                                Xeyİr
                            </Button>
                        </Fragment>
                }

            </DialogActions>
        </Dialog>
    );
};

export default ProPassDialog;