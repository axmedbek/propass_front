import React from 'react';
import Radio from "@material-ui/core/Radio";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import {withStyles} from "@material-ui/core";

const useStyles = theme => ({
    root: {
        color: '#55AB80',
        '&$checked': {
            color: '#55AB80',
        },
    },
    checked: {},
});

const RadioPass = ({ classes,radioState,handleRadioChange,radioValue,radioName,radioLabel }) => {
    return (
        <FormControlLabel
            value={radioValue}
            control={
                <Radio
                    checked={radioState}
                    onChange={handleRadioChange}
                    value={radioValue}
                    name={radioName}
                    classes={{
                        root: classes.root,
                        checked: classes.checked,
                    }}
                />
            }
            label={radioLabel}
            labelPlacement="start"
        />
    );
};

export default withStyles(useStyles)(RadioPass);