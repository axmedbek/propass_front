import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import React from "react";
import {withStyles} from "@material-ui/core";

const useStyles = theme => ({
    root: {
        color: '#C9C9C9',
        '&$checked': {
            color: '#55AB80',
        },
    },
    checked: {},
});

const ProCheckBox = ({labelText, checkValue, handleChange, classes}) => {
    if (labelText) {
        return (
            <FormControlLabel
                control={
                    <Checkbox
                        checked={checkValue}
                        onChange={handleChange}
                        classes={{
                            root: classes.root,
                            checked: classes.checked,
                        }}
                    />
                }
                label="Custom color"
            />
        )
    } else {
        return (
            <Checkbox
                checked={checkValue}
                onChange={handleChange}
                classes={{
                    root: classes.root,
                    checked: classes.checked,
                }}
            />
        )
    }
};

export default withStyles(useStyles)(ProCheckBox);