import React from 'react';
import Paper from "@material-ui/core/Paper/Paper";
import Table from "@material-ui/core/Table/Table";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import {CircularProgress, withStyles} from "@material-ui/core";
import MskToolBarComponent from "./MskToolBarComponent";

const styles = theme => ({
    tableRoot: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    table: {
        minWidth: 700,
    },
});

const MskTableCreatorComponent = ({ classes,headers,data,tableBodyComponent,toolBarTitle,handleAddNewRow }) => {
    return (
        <Paper className={classes.tableRoot} style={{
            marginLeft: '10px',
            height: '470px !important',
            overflow: 'auto !important',
        }}>
            <MskToolBarComponent classes={classes} toolBarTitle={toolBarTitle} handleAddNewRow={handleAddNewRow}/>

            <Table className={classes.table}>
                <TableHead>
                    <TableRow>
                        {
                            headers.map((value, key) => (
                                key === 0
                                    ? <TableCell key={key} className={classes.tableRowStyle}>№</TableCell>
                                    :
                                    <TableCell key={key} align="right"
                                               className={classes.tableRowStyle}>{value.name}</TableCell>
                            ))
                        }
                    </TableRow>
                </TableHead>
                {
                    data.fetching ?
                        <TableBody>
                            <TableRow>
                                <TableCell colSpan={headers.length} style={{textAlign: 'center'}}>
                                    <CircularProgress className={classes.tableProgress} size={40}/>
                                </TableCell>
                            </TableRow>
                        </TableBody>
                        : <TableBody>
                            {
                                'hello'
                            }
                            {
                                data.total_data.data.length < 1 &&

                                <TableRow>
                                    <TableCell colSpan={headers.length} style={{textAlign: 'center'}}>
                                        Hazırda göstəriləcək məlumat yoxdur</TableCell>
                                </TableRow>

                            }
                        </TableBody>
                }
            </Table>
        </Paper>
    );
};

export default withStyles(styles)(MskTableCreatorComponent);