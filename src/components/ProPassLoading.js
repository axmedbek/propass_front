import React from 'react';
import {CircularProgress} from "@material-ui/core";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = theme => ({
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#69CB9D',
        marginTop: '20%',
        marginLeft: 'calc(40% - -20px)'
    },
});

const ProPassLoading = ({ fetching,classes }) => {
    return (
        fetching ?
            <div style={{
                zIndex : 99,
                position: 'fixed',
                width: '100%',
                height: '100%',
                backgroundColor: '#00000026',
                margin : '-40px'
            }}>
                <CircularProgress className={classes.progress}/>
            </div> : ''
    );
};

export default withStyles(styles)(ProPassLoading);