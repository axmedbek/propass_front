import React from 'react';
import Grid from "@material-ui/core/Grid/Grid";
import {CircularProgress} from "@material-ui/core";
import classNames from "classnames";

const ProStepper = ({ loadStep , currentStep ,classes , stepperText , stepLoaderStyle , errorStep }) => {
    return (
        <React.Fragment>
            <div style={{ height: 30 }}/>
            <Grid container spacing={24}>
                <Grid item md={2} style={{
                    marginLeft: 30,
                    marginTop: 30
                }}>{currentStep}. </Grid>
                <Grid item md={10} style={{
                    marginLeft: -65
                }}>
                    <div className={classNames(classes.stepStyle,loadStep === currentStep ? classes.activeStep : loadStep > currentStep ? classes.finishedStep : '',errorStep === currentStep ? classes.errorStepStyle : '')}>
                        <div className={classes.stepTextStyle}>{stepperText}</div>
                    </div>
                    {
                        loadStep === currentStep  && errorStep !== currentStep ?
                            <CircularProgress className={classNames(classes.progress,classes.stepLoaderStyle,loadStep === currentStep ? classes[stepLoaderStyle] : '')} size={70}/>
                            : ''
                    }
                </Grid>
            </Grid>
        </React.Fragment>
    );
};

export default ProStepper;