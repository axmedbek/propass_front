import {IconButton, Toolbar, Tooltip, Typography, withStyles} from "@material-ui/core";
import {Add as AddIcon} from "@material-ui/icons";
import PropTypes from "prop-types";
import React from "react";

const toolbarStyles = theme => ({
    toolBarRoot: {
        paddingRight: theme.spacing.unit,
    },
    toolBarSpacer: {
        flex: '1 1 100%',
    },
    toolBarActions: {
        color: theme.palette.text.secondary,
    },
    toolBarTitle: {
        flex: '0 0 auto',
    },
});


const MskToolBarComponent = ({ classes,toolBarTitle,handleAddNewRow }) => {

    return (
        <Toolbar
            className={classes.toolBarRoot}
        >
            <div className={classes.toolBarTitle}>
                <Typography variant="h6" id="tableTitle" style={{ fontWeight : 'bold', color : '#696969'}}>
                    { toolBarTitle }
                </Typography>
            </div>
            <div className={classes.toolBarSpacer}/>
            <div className={classes.toolBarActions}>
                <Tooltip placement={"top"} title={`Yeni ${toolBarTitle.toLowerCase().substring(0,toolBarTitle.length - 2)} əlavə et`} aria-label="Add">
                    <IconButton aria-label="Add" onClick={handleAddNewRow} >
                        <AddIcon/>
                    </IconButton>
                </Tooltip>
            </div>
        </Toolbar>
    );
};

MskToolBarComponent.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(toolbarStyles)(MskToolBarComponent);