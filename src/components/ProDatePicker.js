import React from 'react';
import {MuiPickersUtilsProvider, InlineDatePicker} from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';
import '../assets/css/prodatepicker_filter.css';
import { MuiThemeProvider,createMuiTheme } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";

const materialTheme = createMuiTheme({
    overrides: {
        MuiPickersToolbar: {
            toolbar: {
                backgroundColor: '#55ab7f',
            },
        },
        MuiPickersCalendarHeader: {
            switchHeader: {
                // backgroundColor: 'red',
                // color: 'white',
            },
        },
        MuiPickersDay: {
            "&day": {
                color: '#7b7878',
            },
            "&$selected": {
                backgroundColor: '#55ab7f',
            },
            "&$current": {
                color: 'white',
                backgroundColor: '#55ab7f',
            },
        },
        MuiPickersModal: {
            dialogAction: {
                color: 'red',
            },
        },
    },
});

const ProDatePicker = ({ datepickerName , selected_date , formatDate , handleChangeDate,handleClearDate }) => {
    return (
        <MuiThemeProvider theme={materialTheme}>
            <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Grid container spacing={0}>
                    <Grid item md={10} style={{
                        flexGrow: 0,
                        maxWidth: '81.333333%',
                        flexBasis: '81.333333%'
                    }}>
                        <InlineDatePicker
                            name={datepickerName}
                            margin="normal"
                            value={selected_date}
                            onChange={handleChangeDate}
                            format={formatDate}
                            className={"custom-datepicker-input"}
                            placeholder={"Tarix"}
                            InputProps={{
                                disableUnderline: true,
                            }}
                            style={{ backgroundColor : 'red'}}
                        />
                    </Grid>
                    <Grid item md={2} className={"custom-datepicker-close-icon"} onClick={handleClearDate}>
                        <FontAwesomeIcon icon={"times"}/>
                    </Grid>
                </Grid>
            </MuiPickersUtilsProvider>
        </MuiThemeProvider>
    );
};

export default ProDatePicker;