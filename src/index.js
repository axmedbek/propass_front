import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import rootReducer from './reducers/rootReducer';
import {Provider} from 'react-redux';
import {HashRouter} from "react-router-dom";
import reduxPromise from 'redux-promise-middleware';
import logger from 'redux-logger';
import {composeWithDevTools} from 'redux-devtools-extension';
import 'bootstrap/dist/css/bootstrap.min.css';
import {MuiThemeProvider, createMuiTheme} from "@material-ui/core/styles";
const store = createStore(
    rootReducer,
    composeWithDevTools(applyMiddleware(reduxPromise(), thunk, logger)),
);


const theme = createMuiTheme({
    typography: {
        // Use the system font.
        useNextVariants: true,
        fontFamily:
            "Fira Sans,"+
            '-apple-system,system-ui,BlinkMacSystemFont,' +
            '"Fira Sans",Roboto,"Quicksand",Arial,sans-serif',
    },
});

ReactDOM.render(
    <MuiThemeProvider theme={theme}>
        <HashRouter>
            <Provider store={store}>
                <App/>
            </Provider>
        </HashRouter>
    </MuiThemeProvider>,
    document.getElementById('root')
);

serviceWorker.unregister();
