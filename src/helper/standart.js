export function makeDataForSelect(array) {
    const newArray = [];
    if (array !== undefined) {
        array.map(arr => newArray.push({value: arr.id, label: arr.name}))
    }
    return newArray
}

export function makeDataForUserSelect(array){
    const newArray = [];
    if (array !== undefined) {
        array.map(arr => newArray.push({value: arr.userId, label: arr.userName}))
    }
    return newArray
}

export function makeDataForFPOperationSelect(array) {
    const newArray = [];
    if (array !== undefined) {
        array.map(arr => newArray.push({value: arr.slug, label: arr.name}))
    }
    return newArray
}

export function findElementById(array, id) {
    return array.find(arr => arr.value === id);
}

export function makeDevicesForSelect(array) {
    const newArray = [];
    if (array !== undefined) {
        array.map(arr => newArray.push({value: arr.id, label: arr.name , destination : arr.destination_name}))
    }
    return newArray
}

export const removeElementById = (array, id) => {
    for(let i = 0; i < array.length; i++) {
        if(array[i].value === id) {
            array.splice(i, 1);
            return array;
        }
    }
};
export function checkFingerIsSelected(array,fingerIndex){
    if (array !== undefined ) {
        if (array.data !== undefined ) {
            if (array.data.data !== undefined) {
                return !!array.data.data.find(value => value.finger_index === fingerIndex);
            }
            return false;
        }
        return false;
    }
    return false
}

export function findFingerNameWithIndex(index){
    switch (index) {
        case "1" :
            return "Baş barmaq";
        case "2" :
            return "Şəhadət barmağı";
        case "3" :
            return "Orta barmaq";
        case "4" :
            return "Adsız barmaq";
        case "5" :
            return "Çeçele barmaq";
        default:
            return ''
    }
}

export function makeDataForFingerSelectDevice(array) {
    const newArray = [];
    if (array !== undefined) {
        array.map(arr => newArray.push({value: arr.id, label: arr.device_name,ip_address : arr.ip_address}))
    }
    return newArray
}

export function getFingerAndHandWithIndex(index){
    let handName = "",fingerName = "";
    if (index > 4){
        handName = "Sağ";
        switch (parseInt(index)) {
            case 5 :
                fingerName =  "Baş barmaq";
                break;
            case 6 :
                fingerName =  "Şəhadət barmağı";
                break;
            case 7 :
                fingerName =  "Orta barmaq";
                break;
            case 8 :
                fingerName = "Adsız barmaq";
                break;
            case 9 :
                fingerName =  "Çeçele barmaq";
                break;
            default:
                fingerName = "";
        }
    }
    else{
        handName = "Sol";
        switch (parseInt(index)) {
            case 0 :
                fingerName =  "Baş barmaq";
                break;
            case 1 :
                fingerName =  "Şəhadət barmağı";
                break;
            case 2 :
                fingerName =  "Orta barmaq";
                break;
            case 3 :
                fingerName = "Adsız barmaq";
                break;
            case 4 :
                fingerName =  "Çeçele barmaq";
                break;
            default:
                fingerName = "";
        }
    }
    return {handName : handName , fingerName : fingerName}
}

export function removeData(id,data){
    for(let i = 0; i < data.length; i++) {
        if(data[i].id === id) {
            data.splice(i, 1);
            return data;
        }
    }
}