const bioHistoryDetailedPageStyle = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
        marginRight: 60
    },
    table: {
        minWidth: 500
    },
    progressTable: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '30%',
        marginTop: '5%',
        position: "absolute"
    },
    headTitle: {
        top: "0",
        color: "white",
        position: "sticky",
        backgroundColor: "#55AB80",
        padding: "0px 20px 0px 23px"
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: 'white',
        '&:hover': {
            backgroundColor: 'white',
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {},
    },
    padding: {
        padding: `0 10px`,
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        right: '-10px',
        cursor: 'pointer'
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
        height: 45
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit + 47, // 45
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10 - 65, // 15
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    componentParts: {
        padding: 40
    },
    colDivider: {
        "backgroundColor": 'white',
        "height": "1px",
        "marginLeft": "-5px",
        "marginRight": "-15px"
    },
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '50%',
        marginTop: '25%',
    },
    progressFingerSide : {
        color: '#63c79d',
        margin: '16px',
        marginTop: '140%',
        marginLeft: '32%',
    },
    activeDeviceClass: {
        backgroundColor: '#66be99',
        color: 'white !important',
        lineHeight: '24px',
        fontSize: '16px',
        fontFamily: 'Fira Sans',
        paddingTop: 10,
        paddingBottom: 10,
        height: 80,
        marginLeft: '-5px',
        marginRight: '-15px'
    },
    deviceListElement: {
        padding: '10px 0px 10px 22px',
        cursor: 'pointer'
    },
    deviceNameStyle: {
        fontWeight: 'bold',
        fontSize: '18px'
    },
    otherDevicesClass: {
        color: '#4a4956',
        lineHeight: '24px',
        fontSize: '16px',
        fontFamily: 'Fira Sans',
        height: 80,
        marginLeft: '-5px',
        marginRight: '-15px'
    },
    activeDeviceTabStyle: {
        backgroundColor: '#66be98',
        color: 'white !important',
        borderRadius: '30px'
    },
    otherDeviceTabStyle: {
        backgroundColor: '#d9eee5',
        color: '#3b7c60',
        borderRadius: '30px',
        border: '1px solid #66be98',
        "fontFamily": "Fira Sans",
        "lineHeight": "16px",
        "fontSize": "14px",
    },
    tableBodyRowStyle: {
        padding: '4px 20px 4px 24px',
        textAlign: 'center'
    },
    buttonRegistrationStyle: {
        backgroundColor: '#FDE081',
        borderRadius: '4px',
        textTransform: 'capitalize',
        fontFamily: 'Fira Sans',
        lineHeight: '24px',
        fontSize: '14px',
        color: '#383D3E',
        '&:hover': {
            backgroundColor: '#fdd03e',
        }
    },
    detailedBtnStyle : {
        float: 'right',
        marginRight: -10,
        fontSize: 14,
        letterSpacing: '0.03em',
        color: '#383D3E',
        fontWeight : 'bold'
    },
    detailedBtnImgStyle : {
        width: '20px',
        marginLeft: '-12px',
    },
    detailedDivStyle : {
        float: 'right',
        marginRight: '12px',
        marginTop: '8px',
        position: 'absolute',
        right: 10
    },
    detailedBtnTextStyle : {
        marginLeft : 10
    },
    badge : {
        color: '#6c6c6c',
        backgroundColor: '#fae081',
        fontWeight : 'bold'
    },
    bioHistoryTitleStyle : {
        fontFamily: "'Fira Sans',sans-serif",
        lineHeight: '30px',
        fontSize: '22px',
        color: '#383D3E',
        fontWeight: 'bold',
        padding: '35px 15px 82px 32px',
        height: 83
    },
    bioHistoryDivStyle : {
        flexBasis: '21%',
        maxWidth: '21%',
        height: '100%',
        backgroundColor: '#d8d8d8',
    },
    bioHistorySideBarStyle : {
        paddingLeft: '22px',
        paddingTop: '28px',
        paddingBottom: '28px'
    },
    bioHistoryFingerTitleStyle : {
        fontFamily: "'Fira Sans',sans-serif",
        lineHeight: '30px',
        fontSize: '22px',
        color: '#383D3E',
        fontWeight: 'bold',
        padding: '1px 13px 20px 32px',
        height: 83
    }
});

export default bioHistoryDetailedPageStyle;
