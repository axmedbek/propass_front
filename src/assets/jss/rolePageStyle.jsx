const rolePageStyle  = theme => ({
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: 'white',
        '&:hover': {
            backgroundColor: 'white',
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {},
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        right: '-10px',
        cursor: 'pointer'
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
        height: 45
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit + 47, // 45
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10 - 65, // 15
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    componentParts: {
        paddingLeft: '22px',
        paddingTop: '28px'
    },
    colDivider: {
        "backgroundColor": 'white',
        "height": "1px",
        "marginLeft": "-5px",
        "marginRight": "-15px"
    },
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '50%',
        marginTop: '25%',
    },
    button: {
        margin: theme.spacing.unit,
    },
    sidebarDateButtons: {
        width: '100%',
        height: '33px',
        backgroundColor: '#BBBBBB',
        marginLeft: 0,
        borderRadius: '4px',
        top: '-10px',
        color: 'white',
        '&:hover': {
            backgroundColor: '#959191'
        },
        fontFamily: 'Fira Sans',
        lineHeight: '12px',
        fontSize: '11px',
    },
    datepickerInputs: {
        backgroundColor: 'white !important',
        padding: '4px 0px 4px 0px !important'
    },
    datepickerInputsDiv: {
        paddingRight: '12px !important'
    },
    registeredStaffActiveRow: {
        "background": "#F6F6F6",
        "boxShadow": "0px 1px 4px rgba(0, 0, 0, 0.25)"
    },
    activeRoleTabStyle: {
        backgroundColor: '#55AB80'
    },
    activeRoleTabTextStyle : {
        color : '#ffffff !important'
    },
    roleContainerGrid : {
        height: 'calc(100% + 16px)'
    },
    roleGridFirstItem : {
        // flexBasis: '21%',
        // maxWidth: '332px',
        // height: '100%',
        backgroundColor: '#E9E9E9',
    },
    roleGridFirstItemDiv : {
        width: 296,
        height: 72,
        textAlign: 'center'
    },
    roleGridFirstItemDivText : {
        "position": "absolute",
        marginLeft: 32,
        marginTop: 15,
        "fontFamily": "Fira Sans",
        "lineHeight": "24px",
        "fontSize": "22px",
        "color": "#383D3E",
        "fontWeight": "bold"
    },
    roleGridFirstItemInputDiv : {
        paddingLeft: '22px',
        paddingTop: '28px',
        paddingBottom: '28px'
    },
    roleAddBtnStyle : {
        width: 38,
        marginLeft: 2,
        height: 40,
        cursor: 'pointer'
    },
    roleSidebarLoaderStyle : {
        color: '#6fc99c',
        position: 'absolute',
        left: 135,
        marginTop: '10%',
    },
    roleListerStyle : {
        width: 'calc(100% + 31px)',
        height: 59,
        position: 'relative',
        paddingTop: 16,
        paddingLeft: 24,
        cursor:'pointer',
        marginLeft : '-16px'
    },
    roleListItemNumberStyle : {
        "fontFamily": "Fira Sans",
        "lineHeight": "16px",
        "fontSize": "14px",
        "color": "#777777"
    },
    roleListItemTextStyle : {
        "marginLeft": "24px",
        "fontFamily": "Fira Sans",
        "lineHeight": "22px",
        "fontSize": "14px",
        "color": "#505050"
    },
    editRoleStyleInput : {
        width : 228,
        height : '32px !important'
    },
    roleEditBtnStyle : {
        width : 32,
        cursor: 'pointer'
    },
    activeTabStyle : {
        borderBottom: '2px solid #55AB80',
        minWidth: '100px !important',
        fontSize: 16,
        color: '#55AB80',
        textTransform: 'inherit',
        fontWeight: 'bold',
    },
    otherTabStyle : {
        fontFamily: "Fira Sans",
        fontSize: 16,
        color: '#505050',
        minWidth: '100px !important',
        textTransform: 'capitalize',
        fontWeight: 'bold'
    },
    activeRoleMenuTabStyle : {
        color: '#55AB80 !important',
        borderRadius: '30px',
        fontSize: '14px',
        backgroundColor: '#E2F0E9',
        fontWeight: 'bold',
        border: '2px solid #D5EDE1',
        textTransform : 'inherit'
    },
    otherRoleMenuTabStyle : {
        color: '#B4B4B4 !important',
        borderRadius: '30px',
        fontSize: '14px',
        backgroundColor: '#F2F2F2',
        fontWeight: 'bold',
        border: '2px solid #E6E6E6',
        textTransform : 'inherit'
    }
});

export default rolePageStyle;