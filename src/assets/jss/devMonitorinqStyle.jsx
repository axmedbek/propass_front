const devMonitorinqStyles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
        marginRight: 60
    },
    table: {
        minWidth: 700,
    },
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '50%',
        marginTop: '25%',
    },
    messageBtnStyle: {
        padding: 5,
        borderRadius: 4
    },
    stepperStyle: {
        zIndex: 99,
        position: 'fixed',
        width: '100%',
        height: '100%',
        backgroundColor: '#00000070',
        margin: '5px'
    },
    stepperDivStyle: {
        width: '40%',
        height: '230px',
        marginLeft: '14%',
        marginTop: '20%',
    },
    stepStyle: {
        width: '90px',
        height: '90px',
        borderRadius: '45px',
        backgroundColor: '#99a59f',
        marginLeft: '45px',
        paddingTop: '24px',
        textAlign: 'center',
    },
    stepTextStyle: {
        "fontSize": "14px",
        "color": "white",
        "marginTop": "8px",
        "fontFamily": "\"Fira Sans\",sans-serif",
        "fontWeight": "bold"
    },
    stepLoaderStyle: {
        "width": "100px !important",
        "height": "100px !important",
        "position": "absolute",
        "left": "0",
        "top": "0",
        "marginTop": "calc(22% - 7px)"
    },
    stepLoaderOneStyle: {
        marginLeft: 'calc(17% - 7px)'
    },
    stepLoaderTwoStyle: {
        marginLeft: 'calc(34% - 10px)'
    },
    stepLoaderThreeStyle: {
        marginLeft: 'calc(48% - 10px)'
    },
    activeStep: {
        boxShadow: '0px 0px 6px 8px #e6dfdf'
    },
    finishedStep: {
        backgroundColor: '#6fc99c',
    },
    errorStepStyle: {
        backgroundColor: '#fb5252 !important'
    },
    deviceControlModalLine: {
        width: '100%',
        height: '1px',
        marginTop: '12px',
    },
    deviceControlModalIpAddress: {
        "backgroundColor": "#8c8787",
        "padding": "6px",
        "color": "white",
        "borderRadius": "4px",
        minWidth: 115
    },
    deviceControlModalIndex: {
        marginRight: '-25px',
        marginTop: '7px',
    },
    colDivider: {
        "backgroundColor": 'white',
        "height": "1px",
        "marginLeft": "-5px",
        "marginRight": "-15px"
    },
    inputRoot: {
        color: 'inherit',
        width: '100%',
        height: 45
    },
    inputInput: {
        paddingTop: theme.spacing.unit,
        paddingRight: theme.spacing.unit + 47, // 45
        paddingBottom: theme.spacing.unit,
        paddingLeft: theme.spacing.unit * 10 - 65, // 15
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    searchIcon: {
        width: theme.spacing.unit * 9,
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        top: 0,
        right: '-10px',
        cursor: 'pointer'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: 'white',
        '&:hover': {
            backgroundColor: 'white',
        },
        marginRight: theme.spacing.unit * 2,
        marginLeft: 0,
        width: '100%',
        [theme.breakpoints.up('sm')]: {},
    },
    progressTable: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '30%',
        marginTop: '5%',
        position: "absolute"
    },
    headTitle: {
        top: "0",
        color: "white",
        position: "sticky",
        backgroundColor: "#55AB80"
    },
    writeBtnStyle: {
        width: 82,
        color: 'white',
        backgroundColor: '#55AB80',
        borderColor: '#55AB80',
        '&:hover': {
            backgroundColor: '#55AB80',
            borderColor: '#55AB80',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#55AB80',
            borderColor: '#55AB80',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem #55AB80',
        },
    },
    deleteBtnStyle: {
        width: 82,
        color: 'white',
        backgroundColor: '#d32f2f',
        borderColor: '#d32f2f',
        '&:hover': {
            backgroundColor: '#d32f2f',
            borderColor: '#d32f2f',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#d32f2f',
            borderColor: '#d32f2f',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem #d32f2f',
        },
    },
    activeBtnStyle: {
        width: 90,
        color: 'white',
        backgroundColor: '#007ACD',
        borderColor: '#007ACD',
        '&:hover': {
            backgroundColor: '#007ACD',
            borderColor: '#007ACD',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#007ACD',
            borderColor: '#007ACD',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem #007ACD',
        },
    },
    deActiveBtnStyle: {
        color: 'white',
        backgroundColor: '#8855a2',
        borderColor: '#8855a2',
        '&:hover': {
            backgroundColor: '#8855a2',
            borderColor: '#8855a2',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#8855a2',
            borderColor: '#8855a2',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem #8855a2',
        },
    },
    checkFPWithDBBtnStyle: {
        color: 'white',
        backgroundColor: '#deb624',
        borderColor: '#deb624',
        '&:hover': {
            backgroundColor: '#deb624',
            borderColor: '#deb624',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#deb624',
            borderColor: '#deb624',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem #deb624',
        },
    },
    refreshBtnStyle: {
        color: 'white',
        backgroundColor: '#68c0f2',
        borderColor: '#68c0f2',
        '&:hover': {
            backgroundColor: '#68c0f2',
            borderColor: '#68c0f2',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#68c0f2',
            borderColor: '#68c0f2',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem #68c0f2',
        },
    },
    deviceControlBtnStyle: {
        color: 'white',
        backgroundColor: '#041dcd',
        borderColor: '#041dcd',
        '&:hover': {
            backgroundColor: '#041dcd',
            borderColor: '#041dcd',
        },
        '&:active': {
            boxShadow: 'none',
            backgroundColor: '#041dcd',
            borderColor: '#041dcd',
        },
        '&:focus': {
            boxShadow: '0 0 0 0.2rem #041dcd',
        },
    },
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
});

export default devMonitorinqStyles;