const filterModalStyle = ({
    table: {
        minWidth: 700,
    },
    departmentFilterSelectBtn : {
        backgroundColor: '#63c79d',
        borderColor: '#63c79d',
        '&:hover':{
            backgroundColor: '#63c79d',
            borderColor: '#63c79d',
        },
        '&:focused' :{
            backgroundColor: '#63c79d',
            borderColor: '#63c79d',
        }
    },
});

export default filterModalStyle;