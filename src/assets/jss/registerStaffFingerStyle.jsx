const registerStaffFingerStyle = theme => ({
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '50%',
        marginTop: '25%',
    },
    activeDeviceTabStyle: {
        borderBottom: '2px solid #55AB80',
        minWidth: '100px !important',
        fontSize: 16,
        color: '#55AB80',
        textTransform: 'capitalize',
        fontWeight: 'bold'
    },
    otherDeviceTabStyle: {
        fontFamily: "Fira Sans",
        fontSize: 16,
        color: '#505050',
        minWidth: '100px !important',
        textTransform: 'capitalize',
        fontWeight: 'bold'
    },
    otherFingerTabStyle: {
        backgroundColor: '#FFFFFF',
        border: '1px solid #DEDEDE',
        boxSizing: 'border-box',
        minWidth: '50%',
        "fontFamily": "Fira Sans",
        "lineHeight": "16px",
        "fontSize": "14px",
        "color": "#B4B4B4",
        "textTransform": "inherit",
        "minHeight": "40px",
        "marginLeft": "5px"
    },
    activeFingerTabStyle: {
        backgroundColor: '#55AB80',
        mixBlendMode: 'normal',
        minWidth: '50%',
        "fontFamily": "Fira Sans",
        "lineHeight": "24px",
        "fontSize": "14px",
        "textAlign": "center",
        "color": "#FFFFFF",
        "fontWeight": "bold",
        "textTransform": "inherit",
        "minHeight": "40px",
        "marginLeft": "5px"
    },
    radioButtonRoot: {
        color: "#55AB80 !important",
        '&$checked': {
            color: "#318A5E !important",
        },
    },
    radioButtonChecked: {},
    isActiveFinger: {
        backgroundColor: '#E2E2E2',
        mixBlendMode: 'normal'
    },
    nested: {
        paddingLeft: theme.spacing.unit * 4,
    },
    stepperStyle : {
        zIndex : 99,
        position: 'fixed',
        width: '100%',
        height: '100%',
        backgroundColor: '#00000070',
        margin : '10px'
    },
    stepperDivStyle : {
        width: '40%',
        height: '230px',
        marginLeft: '17%',
        marginTop: '17%',
    },
    stepStyle : {
        width: '90px',
        height: '90px',
        borderRadius: '45px',
        backgroundColor: '#99a59f',
        marginLeft: '45px',
        paddingTop: '24px',
        textAlign: 'center',
    },
    stepTextStyle : {
        "fontSize":"14px",
        "color":"white",
        "marginTop":"8px",
        "fontFamily":"\"Fira Sans\",sans-serif",
        "fontWeight":"bold"
    },
    stepLoaderStyle : {
        "width":"100px !important",
        "height":"100px !important",
        "position":"absolute",
        "left":"0",
        "top":"0",
        "marginTop":"calc(19% - 7px)"
    },
    stepLoaderOneStyle : {
        marginLeft: 'calc(20% - 10px)'
    },
    stepLoaderTwoStyle : {
        marginLeft: 'calc(34% - 10px)'
    },
    stepLoaderThreeStyle : {
        marginLeft: 'calc(48% - 10px)'
    },
    activeStep : {
        boxShadow: '0px 0px 6px 8px #e6dfdf'
    },
    finishedStep : {
        backgroundColor: '#6fc99c',
    },
    errorStepStyle : {
        backgroundColor : '#fb5252 !important'
    }
});

export default registerStaffFingerStyle;