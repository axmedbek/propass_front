const deviceAddStyle = theme => ({
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing.unit,
        marginRight: theme.spacing.unit,
        height: '45px'
    },
    cssOutlinedInput: {
        '&$cssFocused $notchedOutline': {
            borderColor: '#63c79d !important',
        }
    },

    cssFocused: {
        color: 'rgba(0, 0, 0, 0.54) !important'
    },

    notchedOutline: {},

    departmentFilterSelectBtn : {
        backgroundColor: '#63c79d',
        borderColor: '#63c79d',
        '&:hover':{
            backgroundColor: '#63c79d',
            borderColor: '#63c79d',
        },
    },
    errorLable : {
        color : 'red'
    },
    errorInput : {
        border : '1px solid red',
        '::-webkit-input-placeholder' : {
            color : 'red'
        },
        ':focused':{
            boxShadow : '0 0 red',
        }
    }
});

export default deviceAddStyle;