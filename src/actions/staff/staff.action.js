import axios from 'axios';
import {
    FETCH_STAFF,
    FETCH_STAFFS,
    FETCH_STAFF_REG_ID,
    FETCH_STAFF_SELECTED_FINGERS,
    FETCH_STAFF_FINGER_TEMPLATES
} from "./staff_action_types";
import {API_BASE} from "../../config/env";


export function fetchStaffs( userType, structureId ) {
    const config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    };
    const formData = new FormData();
    formData.append('userType',userType);
    formData.append('structureId',structureId);
    return dispatch => {
        dispatch({
            type: FETCH_STAFFS,
            payload:
                axios.post('http://172.16.123.4/VeyselOqlu/dev/api/general/employeeList.php',formData,config)
                    .then(result => {
                        return result.data;
                    })
                    .catch(function (error) {
                        console.error(error)
                    })
        })
    }
}

export function fetchStaff(userId){
    const config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    };
    const formData = new FormData();
    formData.append('userId',userId);
    return dispatch => {
        dispatch({
            type: FETCH_STAFF,
            payload:
                axios.post('http://172.16.123.4/VeyselOqlu/dev/api/general/employeeInfo.php',formData,config)
                    .then(result => {
                        return result.data;
                    })
                    .catch(function (error) {
                        console.error(error)
                    })
        })
    }
}

export function fetchRegIdWithUserId(userId){
    return dispatch => {
        dispatch({
            type : FETCH_STAFF_REG_ID,
            payload : axios.post(`${API_BASE}/fp/register-user`,{ uid : userId} , {
                headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                .then(result => result.data)
                .catch(error => console.log(error))
        })
    }
}


export function fetchSelectedFingers(user_id,device_id){
    return dispatch => {
        dispatch({
            type : FETCH_STAFF_SELECTED_FINGERS,
            payload : axios.post(`${API_BASE}/fp/selected-fingers`,{ device_id : device_id , user_id : user_id } , { headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' +  JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                .then(result => result.data)
                .catch(error => console.log(error))
        })
    }
}

export function fetchFingerTemplatesWithFingerIndex(user_id,device_id,fingerIndex){
    return dispatch => {
        dispatch({
            type : FETCH_STAFF_FINGER_TEMPLATES,
            payload : axios.post(`${API_BASE}/fp/finger-templates`,{ device_id : device_id , user_id : user_id,fingerIndex : fingerIndex } , { headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                .then(result => result.data)
                .catch(error => console.log(error))
        })
    }
}
