// all transitions
export const FETCH_TRANSITIONS = "FETCH_TRANSITIONS";
export const FETCH_TRANSITIONS_FULFILLED = "FETCH_TRANSITIONS_FULFILLED";
export const FETCH_TRANSITIONS_REJECTED = "FETCH_TRANSITIONS_REJECTED";
export const FETCH_TRANSITIONS_PENDING = "FETCH_TRANSITIONS_PENDING";

// get transition
export const FETCH_TRANSITION_BY_ID = "FETCH_TRANSITION_BY_ID";
export const FETCH_TRANSITION_BY_ID_FULFILLED = "FETCH_TRANSITION_BY_ID_FULFILLED";
export const FETCH_TRANSITION_BY_ID_REJECTED = "FETCH_TRANSITION_BY_ID_REJECTED";
export const FETCH_TRANSITION_BY_ID_PENDING = "FETCH_TRANSITION_BY_ID_PENDING";

// save or edit transition
export const SAVE_OR_EDIT_TRANSITION = "SAVE_OR_EDIT_TRANSITION";
export const SAVE_OR_EDIT_TRANSITION_FULFILLED = "SAVE_OR_EDIT_TRANSITION_FULFILLED";
export const SAVE_OR_EDIT_TRANSITION_REJECTED = "SAVE_OR_EDIT_TRANSITION_REJECTED";
export const SAVE_OR_EDIT_TRANSITION_PENDING = "SAVE_OR_EDIT_TRANSITION_PENDING";