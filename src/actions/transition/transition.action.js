import axios from 'axios';
import {API_BASE} from "../../config/env";
import {FETCH_TRANSITION_BY_ID, FETCH_TRANSITIONS, SAVE_OR_EDIT_TRANSITION} from "./transition_action_types";


export function fetchTransitions(){
    return dispatch => {
        dispatch({
            type : FETCH_TRANSITIONS,
            payload : axios.get(`${API_BASE}/transition/all`,{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                .then(result => result.data)
        })
    }
}

export function fetchTransitionById(id){
    return dispatch => {
        dispatch({
            type : FETCH_TRANSITION_BY_ID,
            payload : axios.get(`${API_BASE}/transition/get/${id}`,{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                .then(result => result.data)
        })
    }
}


export function addNewTransition(data) {
    return dispatch => {
        dispatch({
            type: SAVE_OR_EDIT_TRANSITION,
            payload: axios.post(`${API_BASE}/transition/save`, data, { headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                .then(result => result.data)
                .catch(function (error) {
                    console.log(error)
                })
        })
    }
}