import axios from 'axios';
import {API_BASE, USER_ID} from '../../config/env';
import {
    FETCH_FINGER_OPERATION,
    ADD_FINGER_OPERATION,
    CANCEL_FINGER_OPERATION,
    SAVE_OR_EDIT_FINGER_OPERATION,
    DELETE_FINGER_OPERATION
} from "./msk_action_types";


export function fetchDeviceFingerOperations() {
    return dispatch => {
        dispatch({
            type: FETCH_FINGER_OPERATION,
            payload: axios.get(`${API_BASE}/msk/fp-operation/all`, { headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                .then(result => result.data)
        })
    }
}

export function addNewDeviceFingerOperation(lastId) {
    return dispatch => {
        dispatch({
            type: ADD_FINGER_OPERATION,
            payload: {
                id: lastId, name: '', isAdd: true
            }
        })
    }
}

export function cancelDeviceFingerOperation(id) {
    return dispatch => {
        dispatch({
            type: CANCEL_FINGER_OPERATION,
            payload: id
        })
    }
}

export function saveOrUpdateDeviceFingerOperation(data) {
    if (Object.keys(data).length === 3) {
        return dispatch => {
            dispatch({
                type: SAVE_OR_EDIT_FINGER_OPERATION,
                payload: axios.post(`${API_BASE}/msk/fp-operation/save`, {...data, ...{user_id: USER_ID}}, { headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            });
        }
    }
}


export function deleteDeviceFingerOperation(id) {
    if (id > 0) {
        return dispatch => {
            dispatch({
                type: DELETE_FINGER_OPERATION,
                payload: axios.post(`${API_BASE}/msk/fp-operation/delete`, {id: id, user_id: USER_ID}, { headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            })
        }
    }
}