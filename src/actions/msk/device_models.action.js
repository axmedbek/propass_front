import axios from 'axios';
import {API_BASE, USER_ID} from '../../config/env';
import {
    ADD_DEVICE_MODEL, CANCEL_DEVICE_MODEL, DELETE_DEVICE_MODEL,
    FETCH_DEVICE_MODELS, SAVE_OR_EDIT_DEVICE_MODEL,
} from "./msk_action_types";


export function fetchDeviceModels(id){
    return dispatch => {
        dispatch({
            type : FETCH_DEVICE_MODELS,
            payload : axios.post(`${API_BASE}/msk/device-models/all`,{ device_manufacture_id : id , user_id : USER_ID },{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                .then(result => result.data)
                .catch(function (error) {
                    console.log(error)
                })
        })
    }
}

export function addNewDeviceModel(){
    return dispatch => {
        dispatch({
            type : ADD_DEVICE_MODEL,
            payload : {
                id : 0 , name : '', isAdd : true
            }
        })
    }
}

export function cancelDeviceModel(id){
    return dispatch => {
        dispatch({
            type : CANCEL_DEVICE_MODEL,
            payload : id
        })
    }
}

export function saveOrUpdateDeviceModel(data){
    console.log(data);
    if(Object.keys(data).length === 5){
        return dispatch => {
            dispatch({
                type : SAVE_OR_EDIT_DEVICE_MODEL ,
                payload : axios.post(`${API_BASE}/msk/device-models/save`,{...data,...{user_id : USER_ID}},{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            });
        }
    }
}


export function deleteDeviceModel(id){
    if (id > 0){
        return dispatch => {
            dispatch({
                type : DELETE_DEVICE_MODEL,
                payload : axios.post(`${API_BASE}/msk/device-models/delete`,{id: id , user_id : USER_ID},{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            })
        }
    }
}