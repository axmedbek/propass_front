import axios from 'axios';
import {API_BASE, USER_ID} from '../../config/env';
import {
    FETCH_DEVICE_DESTINATIONS,
    ADD_DEVICE_DESTINATIONS,
    CANCEL_DEVICE_DESTINATIONS,
    SAVE_OR_EDIT_DEVICE_DESTINATIONS,
    DELETE_DEVICE_DESTINATIONS
} from "./msk_action_types";


export function fetchDeviceDestinations(){
    return dispatch => {
        dispatch({
            type : FETCH_DEVICE_DESTINATIONS,
            payload : axios.get(`${API_BASE}/msk/device-destination/all`,{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                .then(result => result.data)
        })
    }
}

export function addNewDeviceDestination(lastId){
    return dispatch => {
        dispatch({
            type : ADD_DEVICE_DESTINATIONS,
            payload : {
                id : lastId , name : '', isAdd : true
            }
        })
    }
}

export function cancelDeviceDestination(id){
    return dispatch => {
        dispatch({
            type : CANCEL_DEVICE_DESTINATIONS,
            payload : id
        })
    }
}

export function saveOrUpdateDeviceDestination(data){
    if(Object.keys(data).length === 2){
        return dispatch => {
            dispatch({
                type : SAVE_OR_EDIT_DEVICE_DESTINATIONS ,
                payload : axios.post(`${API_BASE}/msk/device-destination/save`,{...data,...{user_id : USER_ID}},{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            });
        }
    }
}


export function deleteDeviceDestination(id){
    if (id > 0){
        return dispatch => {
            dispatch({
                type : DELETE_DEVICE_DESTINATIONS,
                payload : axios.post(`${API_BASE}/msk/device-destination/delete`,{id: id , user_id : USER_ID},{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            })
        }
    }
}