import axios from 'axios';
import {API_BASE, USER_ID} from '../../config/env';
import {
    ADD_DEVICE_SDK_TYPES,
    CANCEL_DEVICE_SDK_TYPES, DELETE_DEVICE_SDK_TYPES,
    FETCH_DEVICE_SDK_TYPES,
    SAVE_OR_EDIT_DEVICE_SDK_TYPES
} from "./msk_action_types";


export function fetchDeviceSdkTypes(){
    return dispatch => {
        dispatch({
            type : FETCH_DEVICE_SDK_TYPES,
            payload : axios.get(`${API_BASE}/msk/device-sdk-types/all`,{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                .then(result => result.data)
        })
    }
}

export function addNewDeviceSdkTypes(lastId){
    return dispatch => {
        dispatch({
            type : ADD_DEVICE_SDK_TYPES,
            payload : {
                id : lastId , name : '', isAdd : true
            }
        })
    }
}

export function cancelDeviceSdkTypes(id){
    return dispatch => {
        dispatch({
            type : CANCEL_DEVICE_SDK_TYPES,
            payload : id
        })
    }
}

export function saveOrUpdateDeviceSdkTypes(data){
    if(Object.keys(data).length === 2){
        return dispatch => {
            dispatch({
                type : SAVE_OR_EDIT_DEVICE_SDK_TYPES,
                payload : axios.post(`${API_BASE}/msk/device-sdk-types/save`,{...data,...{user_id : USER_ID}},{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            });
        }
    }
}


export function deleteDeviceSdkTypes(id){
    if (id > 0){
        return dispatch => {
            dispatch({
                type : DELETE_DEVICE_SDK_TYPES,
                payload : axios.post(`${API_BASE}/msk/device-sdk-types/delete`,{id: id , user_id : USER_ID},{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            })
        }
    }
}