import axios from 'axios';
import {API_BASE, USER_ID} from '../../config/env';
import {
    ADD_DEVICE_FINGER_CODE_TYPES,
    CANCEL_DEVICE_FINGER_CODE_TYPES, DELETE_DEVICE_FINGER_CODE_TYPES,
    FETCH_DEVICE_FINGER_CODE_TYPES, SAVE_OR_EDIT_DEVICE_FINGER_CODE_TYPES
} from "./msk_action_types";


export function fetchDeviceFingerCodeTypes(){
    return dispatch => {
        dispatch({
            type : FETCH_DEVICE_FINGER_CODE_TYPES,
            payload : axios.get(`${API_BASE}/msk/device-finger-code-types/all`,{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                .then(result => result.data)
        })
    }
}

export function addNewDeviceFingerCodeTypes(lastId){
    return dispatch => {
        dispatch({
            type : ADD_DEVICE_FINGER_CODE_TYPES,
            payload : {
                id : lastId , name : '', isAdd : true
            }
        })
    }
}

export function cancelDeviceFingerCodeTypes(id){
    return dispatch => {
        dispatch({
            type : CANCEL_DEVICE_FINGER_CODE_TYPES,
            payload : id
        })
    }
}

export function saveOrUpdateDeviceFingerCodeTypes(data){
    if(Object.keys(data).length === 2){
        return dispatch => {
            dispatch({
                type : SAVE_OR_EDIT_DEVICE_FINGER_CODE_TYPES,
                payload : axios.post(`${API_BASE}/msk/device-finger-code-types/save`,{...data,...{user_id : USER_ID}},{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            });
        }
    }
}


export function deleteDeviceFingerCodeTypes(id){
    if (id > 0){
        return dispatch => {
            dispatch({
                type : DELETE_DEVICE_FINGER_CODE_TYPES,
                payload : axios.post(`${API_BASE}/msk/device-finger-code-types/delete`,{id: id , user_id : USER_ID},{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            })
        }
    }
}