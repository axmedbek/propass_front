import axios from 'axios';
import {API_BASE, USER_ID} from '../../config/env';
import {
    FETCH_DEVICE_TYPES,
    ADD_DEVICE_TYPE,
    CANCEL_DEVICE_TYPE,
    SAVE_OR_EDIT_DEVICE_TYPE,
    DELETE_DEVICE_TYPE
} from "./msk_action_types";


export function fetchDeviceTypes(){
    return dispatch => {
        dispatch({
            type : FETCH_DEVICE_TYPES,
            payload : axios.get(`${API_BASE}/msk/device-types/all`,{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' +  JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                .then(result => result.data)
        })
    }
}

export function addNewDeviceType(lastId){
    return dispatch => {
        dispatch({
            type : ADD_DEVICE_TYPE,
            payload : {
                id : lastId , name : '', isAdd : true
            }
        })
    }
}

export function cancelDeviceType(id){
    return dispatch => {
        dispatch({
            type : CANCEL_DEVICE_TYPE,
            payload : id
        })
    }
}

export function saveOrUpdateDeviceType(data){
    if(Object.keys(data).length === 2){
        return dispatch => {
            dispatch({
               type : SAVE_OR_EDIT_DEVICE_TYPE ,
                payload : axios.post(`${API_BASE}/msk/device-types/save`,{...data,...{user_id : USER_ID}},{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            });
        }
    }
}


export function deleteDeviceType(id){
    if (id > 0){
        return dispatch => {
            dispatch({
                type : DELETE_DEVICE_TYPE,
                payload : axios.post(`${API_BASE}/msk/device-types/delete`,{id: id , user_id : USER_ID},{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            })
        }
    }
}