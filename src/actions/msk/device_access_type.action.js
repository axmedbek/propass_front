import axios from 'axios';
import {API_BASE, USER_ID} from '../../config/env';
import {
    FETCH_ACCESS_TYPES,
    ADD_ACCESS_TYPE,
    CANCEL_ACCESS_TYPE,
    SAVE_OR_EDIT_ACCESS_TYPE, DELETE_ACCESS_TYPE
} from "./msk_action_types";


export function fetchAccessTypes() {
    return dispatch => {
        dispatch({
            type: FETCH_ACCESS_TYPES,
            payload: axios.get(`${API_BASE}/msk/access/access-types/all`, {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': JSON.parse(localStorage.getItem("user"))
                        ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                        : null
                }
            })
                .then(result => result.data)
        })
    }
}

export function addNewAccessType(lastId) {
    return dispatch => {
        dispatch({
            type: ADD_ACCESS_TYPE,
            payload: {
                id: lastId, name: '', isAdd: true
            }
        })
    }
}

export function cancelAccessType(id) {
    return dispatch => {
        dispatch({
            type: CANCEL_ACCESS_TYPE,
            payload: id
        })
    }
}

export function saveOrUpdateAccessType(data) {
    if (Object.keys(data).length === 2) {
        return dispatch => {
            dispatch({
                type: SAVE_OR_EDIT_ACCESS_TYPE,
                payload: axios.post(`${API_BASE}/msk/access/access-types/save`, {...data, ...{user_id: USER_ID}}, {
                    headers: {
                        'Accept': 'application/json',
                        'Authorization': JSON.parse(localStorage.getItem("user"))
                            ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                            : null
                    }
                })
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            });
        }
    }
}


export function deleteAccessType(id) {
    if (id > 0) {
        return dispatch => {
            dispatch({
                type: DELETE_ACCESS_TYPE,
                payload: axios.post(`${API_BASE}/msk/access/access-types/delete`, {id: id, user_id: USER_ID}, {
                    headers: {
                        'Accept': 'application/json',
                        'Authorization': JSON.parse(localStorage.getItem("user"))
                            ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                            : null
                    }
                })
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            })
        }
    }
}