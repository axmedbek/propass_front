import axios from 'axios';
import { API_BASE , USER_ID } from "../../config/env";
import {
    ADD_DEVICE_MANUFACTURE, CANCEL_DEVICE_MANUFACTURE, DELETE_DEVICE_MANUFACTURE,
    FETCH_DEVICE_MANUFACTURES, SAVE_OR_EDIT_DEVICE_MANUFACTURE,
} from "./msk_action_types";


export function fetchDeviceManufactures(){
    return dispatch => {
        dispatch({
            type : FETCH_DEVICE_MANUFACTURES,
            payload : axios.get(`${API_BASE}/msk/device-manufacture/all`,{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                .then(result => result.data)
                .catch(result => console.log(result))
        })
    }
}

export function addNewDeviceManufacture(lastId){
    return dispatch => {
        dispatch({
            type : ADD_DEVICE_MANUFACTURE,
            payload : {
                id : 0 , name : '', isAdd : true
            }
        })
    }
}

export function cancelDeviceManufacture(id){
    return dispatch => {
        dispatch({
            type : CANCEL_DEVICE_MANUFACTURE,
            payload : id
        })
    }
}

export function saveOrUpdateDeviceManufacture(data){
    if(Object.keys(data).length === 2){
        return dispatch => {
            dispatch({
                type : SAVE_OR_EDIT_DEVICE_MANUFACTURE ,
                payload : axios.post(`${API_BASE}/msk/device-manufacture/save`,{...data,...{user_id : USER_ID}},{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            });
        }
    }
}


export function deleteDeviceManufacture(id){
    if (id > 0){
        return dispatch => {
            dispatch({
                type : DELETE_DEVICE_MANUFACTURE,
                payload : axios.post(`${API_BASE}/msk/device-manufacture/delete`,{id: id , user_id : USER_ID},{ headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                    .then(result => result.data)
                    .catch(function (error) {
                        console.log(error)
                    })
            })
        }
    }
}