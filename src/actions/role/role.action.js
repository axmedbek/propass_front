import axios from 'axios';
import {API_BASE} from "../../config/env";
import {
    DELETE_ROLE_FULFILLED,
    DELETE_ROLE_PENDING, DELETE_ROLE_REJECTED,
    FETCH_ROLES,
    SAVE_OR_EDIT_ROLE, SAVE_OR_EDIT_ROLE_FULFILLED,
    SAVE_OR_EDIT_ROLE_PENDING,
    SAVE_OR_EDIT_ROLE_REJECTED
} from "./role.action_types";


export function fetchAllRoles(){
    return dispatch => {
        dispatch({
            type : FETCH_ROLES,
            payload : axios.get(`${API_BASE}/roles/all`,{
                headers: {
                    'Accept': 'application/json',
                    'Authorization': JSON.parse(localStorage.getItem("user"))
                        ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                        : null
                }
            })
                .then(result => result.data)
                .catch(error => console.log(error))
        })
    }
}

export function saveOrEditRole(data) {
    return dispatch => {
        dispatch({
            type: SAVE_OR_EDIT_ROLE_PENDING
        });
        axios.post(`${API_BASE}/roles/save`,data,{
              headers: {
                  'Accept': 'application/json',
                  'Authorization': JSON.parse(localStorage.getItem("user"))
                      ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                      : null
              }
          }).then(result => {
              if (result.data.data.status) {
                  dispatch({
                      type: SAVE_OR_EDIT_ROLE_FULFILLED,
                      payload : result.data.data.data
                  });
              }
              else{
                  dispatch({
                      type: SAVE_OR_EDIT_ROLE_REJECTED,
                      payload : result.data.data.errors
                  });
              }
        }).catch(error => console.log(error))
    }
}

export function removeRole(data){
    return dispatch => {
        dispatch({
            type: DELETE_ROLE_PENDING
        });
        axios.post(`${API_BASE}/roles/delete`,data,{
            headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }
        }).then(result => {
            if (result.data.data.status) {
                dispatch({
                    type: DELETE_ROLE_FULFILLED,
                    payload : result.data.data.data.id
                });
            }
            else{
                dispatch({
                    type: DELETE_ROLE_REJECTED,
                    payload : result.data.data.errors
                });
            }
        }).catch(error => console.log(error))
    }
}