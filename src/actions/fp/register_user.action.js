import axios from "axios";
import {API_BASE} from "../../config/env";
import {ADD_USER_FINGER} from "./fp_action_types";

export function addUserFinger(uid,operation_name,device_id) {
    return dispatch => {
        dispatch({
            type: ADD_USER_FINGER,
            payload : axios.post(`${API_BASE}/fp/register-user`,{ uid : uid,operation_name : operation_name, device_id : device_id} , {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': JSON.parse(localStorage.getItem("user"))
                        ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                        : null
                }
            })
                .then(result => result.data)
                .catch(error => console.log(error))
        });
    }
}