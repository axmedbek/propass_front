import axios from "axios";
import {API_BASE} from "../../config/env";
import {
    FETCH_BIO_HISTORY_LOGS,
    FETCH_BIO_HISTORY_BY_ID, FETCH_REGISTERED_USERS_HISTORY, FETCH_FINGERS_WITH_USER_ID, FETCH_DEVICES_WITH_FINGER_CODE
} from "./fp_action_types";


export function fetchAllBioHistory(data) {
    return dispatch => {
        dispatch({
            type: FETCH_BIO_HISTORY_LOGS,
            payload : axios.post(`${API_BASE}/fp/register-finger-logs`,data, {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': JSON.parse(localStorage.getItem("user"))
                        ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                        : null
                }
            })
                .then(result => result.data)
                .catch(error => console.log(error))
        });
    }
}

export function fetchAllRegisteredUsersWithFingers() {
    return dispatch => {
        dispatch({
            type: FETCH_REGISTERED_USERS_HISTORY,
            payload : axios.get(`${API_BASE}/fp/get/register-users`, {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': JSON.parse(localStorage.getItem("user"))
                        ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                        : null
                }
            })
                .then(result => result.data)
                .catch(error => console.log(error))
        });
    }
}

export function fetchBioHistoryById(id){
    return dispatch => {
        dispatch({
            type : FETCH_BIO_HISTORY_BY_ID,
            payload : axios.post(`${API_BASE}/fp/get/register-finger-info`,{ id : id} ,{
                headers: {
                    'Accept': 'application/json',
                    'Authorization': JSON.parse(localStorage.getItem("user"))
                        ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                        : null
                }
            })
                .then(result => result.data)
                .catch(error => console.log(error))
        })
    }
}

export function getFingersWithUserId(id){
    return dispatch => {
        dispatch({
            type: FETCH_FINGERS_WITH_USER_ID,
            payload : axios.get(`${API_BASE}/fp/get/user-fingers/${id}`, {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': JSON.parse(localStorage.getItem("user"))
                        ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                        : null
                }
            })
                .then(result => result.data)
                .catch(error => console.log(error))
        });
    }
}

export function getDevicesAndTransitionsWithFingerCode(fingerCode){
    return dispatch => {
        dispatch({
            type: FETCH_DEVICES_WITH_FINGER_CODE,
            payload : axios.post(`${API_BASE}/fp/get/devices-and-transitions-with-finger-code`,{ finger_template : fingerCode }, {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': JSON.parse(localStorage.getItem("user"))
                        ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                        : null
                }
            })
                .then(result => result.data)
                .catch(error => console.log(error))
        });
    }
}