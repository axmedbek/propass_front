import axios from "axios";
import {API_BASE} from "../../config/env";
import {FETCH_OPERATION_LOGS} from "./fp_action_types";

export function fetchAllOperations(data) {
    return dispatch => {
        dispatch({
            type: FETCH_OPERATION_LOGS,
            payload : axios.post(`${API_BASE}/fp/operation-logs`,data, { headers: {
                'Accept': 'application/json',
                'Authorization': JSON.parse(localStorage.getItem("user"))
                    ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                    : null
            }})
                .then(result => result.data)
                .catch(error => console.log(error))
        });
    }
}