import axios from 'axios';
import {API_BASE} from '../../config/env';
import {
    FETCH_DEVICE,
    FETCH_DEPARTMENT_FILTER,
    FETCH_DEPARTMENT_FILTER_BY_SELECT,
    FETCH_DEVICE_BY_ID,
    REMOVE_DEPARTMENT_FILTER,
    FETCH_DEVICE_BY_DESTINATION,
    SELECTED_DEPARTMENTS_FILTER,
    REMOVE_SELECTED_DEPARTMENTS_FILTER
} from "./device_action_types";


export function fetchDevices() {
    return dispatch => {
        dispatch({
            type: FETCH_DEVICE,
            payload: axios.get(`${API_BASE}/device/all`, {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': JSON.parse(localStorage.getItem("user"))
                        ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                        : null
                }
            })
                .then(result => result.data)
                .catch(function (error) {
                    console.log(error)
                })
        });
    }
}

export function fetchDepartmentFilters() {
    return dispatch => {
        dispatch({
            type: FETCH_DEPARTMENT_FILTER,
            payload: axios.get('http://172.16.123.4/VeyselOqlu/dev/api/general/structureTypes.php')
                .then(result => result.data)
                .catch(function (error) {
                    console.error(error)
                })
        })
    }
}

export function fetchDepartmentFilterBySelect(id, parent_id , tip) {
    const config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    };
    const formData = new FormData();
    formData.append('typeId',id);
    formData.append('parentId',parent_id);
    return dispatch => {
        dispatch({
            type: FETCH_DEPARTMENT_FILTER_BY_SELECT+'|'+tip+'|',
            payload:
                axios.post('http://172.16.123.4/VeyselOqlu/dev/api/general/structureList.php',formData,config)
                    .then(result => {
                        return result.data;
                    })
                    .catch(function (error) {
                        console.error(error)
                    })
        })
    }
}

export function fetchDeviceById(id){
    return dispatch => {
        dispatch({
            type : FETCH_DEVICE_BY_ID,
            payload : axios.post(`${API_BASE}/device/get`,{ id : id} , {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': JSON.parse(localStorage.getItem("user"))
                        ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                        : null
                }
            })
                .then(result => result.data)
                .catch(error => console.log(error))
        })
    }
}

export function removeDepartmentFilter(tip){
    return dispatch => {
        dispatch({
            type : REMOVE_DEPARTMENT_FILTER,
            payload : { tip : tip }
        })
    }
}

export function getDevicesByDestinationId(id){
    return dispatch => {
        dispatch({
            type : FETCH_DEVICE_BY_DESTINATION,
            payload : axios.get(`${API_BASE}/device/get/devices_with_destination/${id}`,{
                headers: {
                    'Accept': 'application/json',
                    'Authorization': JSON.parse(localStorage.getItem("user"))
                        ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                        : null
                }
            })
                .then(result => result.data)
                .catch(error => console.log(error))
        })
    }
}

export function selectedDepartments(data){
    return dispatch => {
        dispatch({
            type : SELECTED_DEPARTMENTS_FILTER,
            payload : data
        })
    }
}

export function removeSelectedDepartments(id){
    return dispatch => {
        dispatch({
            type : REMOVE_SELECTED_DEPARTMENTS_FILTER,
            payload : id
        })
    }
}