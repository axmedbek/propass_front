import axios from 'axios';
import {
    CHECK_LOGIN_FULFILLED,
    CHECK_LOGIN_PENDING,
    CHECK_LOGIN_REJECTED,
    FETCH_SYS_USERS,
    LOGOUT_PROCESS
} from "./auth.action_types";
import {API_BASE, MAIN_TOKEN} from "../../config/env";

export function login(data) {
    const config = {
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
        }
    };
    const formData = new FormData();
    formData.append('pass',data.pass);
    formData.append('login',data.login);

    return dispatch => {
        dispatch({
           type : CHECK_LOGIN_PENDING
        });
        axios.post(`http://172.16.123.4/veyselogluqeyriresmi/proid/api/general/login.php`, formData,config)
            .then(result => {
                if (result.data.status) {
                    axios.post(`${API_BASE}/register`,result.data.data,{
                        headers : {
                            'Accept': 'application/json',
                            'Authorization': MAIN_TOKEN
                        }
                    })
                        .then(response => {
                            if (response.data.data.status){
                                dispatch({
                                    type : CHECK_LOGIN_FULFILLED,
                                    payload : response.data
                                })
                            }
                            else{
                                dispatch({
                                    type : CHECK_LOGIN_REJECTED,
                                    payload : response.data.data
                                });
                            }
                        })
                        .catch(error => console.log(error));
                } else {
                    dispatch({
                        type : CHECK_LOGIN_REJECTED,
                        payload : result.data
                    });
                }
            })
            .catch(error => console.log(error))

    }
}


export function fetchSysUsers() {
    return dispatch => {
        dispatch({
            type: FETCH_SYS_USERS,
            payload: axios.get(`${API_BASE}/sys-users/all`, {
                headers: {
                    'Accept': 'application/json',
                    'Authorization': JSON.parse(localStorage.getItem("user"))
                        ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                        : null
                }
            })
                .then(result => result.data)
                .catch(error => console.log(error))
        });
    }
}

export function logoutProcess() {
    return dispath => {
        dispath({
            type: LOGOUT_PROCESS,
            payload: axios.get(`${API_BASE}/logout`,{
                headers: {
                    'Accept': 'application/json',
                    'Authorization': JSON.parse(localStorage.getItem("user"))
                        ? 'Bearer ' + JSON.parse(localStorage.getItem("user")).auth_token
                        : null
                }
            })
                .then(result => result.data)
                .catch(function (error) {
                    console.log(error)
                })
        })
    }
}