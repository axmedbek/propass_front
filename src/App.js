import React, {Component, Fragment} from 'react';
import {Container} from 'reactstrap';
import {Route} from 'react-router-dom';
import {library} from '@fortawesome/fontawesome-svg-core'
import {
    faFingerprint, faIndustry, faExchangeAlt, faBusinessTime,
    faQrcode, faBarcode, faHome, faCogs, faArrowsAltH, faBan, faCheckSquare, faExclamationCircle, faEye,
    faTruckMoving, faIdCardAlt, faReplyAll, faPlus, faClipboardCheck, faFilter , faUsers , faUtensils,
    faMobileAlt,faAddressCard,faIdCard,faChartBar,faTimes,faBackspace,faTrash
} from '@fortawesome/free-solid-svg-icons'
import propassRoutes from './routes/propass';
import {LoginPage} from "./views";
import PageRoute from "./components/HOCs/PageRoute";
import {PrivateRoute} from "./routes/PrivateRoute";

library.add(
    faFingerprint, faIndustry, faExchangeAlt, faBusinessTime, faQrcode, faBarcode, faHome, faCogs, faArrowsAltH, faBan,
    faCheckSquare, faExclamationCircle, faEye, faTruckMoving, faIdCardAlt, faReplyAll, faPlus, faClipboardCheck, faFilter,
    faUsers,faUtensils,faMobileAlt,faAddressCard,faIdCard,faChartBar,faTimes,faBackspace,faTrash);

class App extends Component {
    render() {
        return (
            <Container style={{ minWidth : 'calc(100% + 30px)' , height : '100%'}}>
                <Route exact={true} strict path={'/login'} component={LoginPage}
                           key={'login_key'}/>
                        {propassRoutes.map((prop, key) => {
                            if (prop.hasSubMenu) {
                                return (
                                    <Fragment key={key}>
                                        {
                                            prop.sub_menus.map((sub_prop, sub_key) => {
                                                return <PrivateRoute exact={true} strict path={sub_prop.path} component={PageRoute(sub_prop.component)}
                                                              key={key + sub_key}/>;
                                            })
                                        }
                                    </Fragment>
                                );
                            }
                            else {
                                return <PrivateRoute exact={true} strict path={prop.path} component={PageRoute(prop.component)} key={key}/>;
                            }
                        })}
            </Container>
        );
    }
}

export default App;
