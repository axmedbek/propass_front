const staffStyles = theme => ({
    titleCountText: {
        fontFamily: 'Fira Sans',
        lineHeight: '36px',
        fontSize: '28px',
        color: '#6FC99C',
        fontWeight: 'bold'
    },
    titleCountSubText: {
        "fontFamily": "Fira Sans",
        "lineHeight": "20px",
        "fontSize": "14px",
        "color": "#B4B4B4"
    },
    button: {
        margin: theme.spacing.unit,
    },
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
        overflowY : 'auto',
        height : 470
    },
    table: {
        minWidth: 700,
    },
    tableRowStyle: {
        backgroundColor: '#55AB80',
        // borderRadius: '4px',
        color: 'white',
        position: "sticky",
        top: 0,
        zIndex : 9999
    },
    tableBodyRowStyle: {
        padding: '4px 20px 4px 24px',
        textAlign: 'center'
    },
    buttonRegistrationStyle: {
        backgroundColor: '#FDE081',
        borderRadius: '4px',
        textTransform: 'capitalize',
        fontFamily: 'Fira Sans',
        lineHeight: '24px',
        fontSize: '14px',
        color: '#383D3E',
        '&:hover': {
            backgroundColor: '#fdd03e',
        }
    },
    activeTabButtonStyle: {
        boxShadow: '2px 6px 14px 6px #c3c5c3'
    },
    progress: {
        margin: theme.spacing.unit * 2,
        color: '#63c79d',
        marginLeft: '50%',
        marginTop: '25%',
    },
    tableProgress : {
        color: '#63c79d',
        marginTop: '16%',
        marginBottom: 'calc(365px - 16%)'
    },
    registeredUserBtnStyle : {
        "background": "#F2F2F2",
        "border": "2px solid #E6E6E6",
        "boxSizing": "border-box",
        "borderRadius": "20px",
        "fontFamily": "Fira Sans",
        "lineHeight": "22px",
        "fontSize": "14px",
        "color": "#B4B4B4",
        "textTransform": 'capitalize',
        "width": "150px",
        '&:hover': {
            backgroundColor: '#dddddd',
        }
    },
    unRegisteredUserBtnStyle : {
        "background": "#E2F0E9",
        "border": "2px solid #D5EDE1",
        "boxSizing": "border-box",
        "borderRadius": "20px",
        "fontFamily": "Fira Sans",
        "lineHeight": "22px",
        "fontSize": "14px",
        "color": "#55AB80",
        "textTransform": 'capitalize',
        "width": "170px",
        '&:hover': {
            backgroundColor: '#E2F0E9',
        }
    },
    detailedBtnStyle : {
        float: 'right',
        marginRight: -10,
        fontSize: 14,
        letterSpacing: '0.03em',
        color: '#383D3E',
        fontWeight : 'bold'
    },
    detailedBtnImgStyle : {
        width: '20px',
        marginLeft: '-12px',
    },
    detailedBtnTextStyle : {
        marginLeft : 10
    }
});

export default staffStyles;